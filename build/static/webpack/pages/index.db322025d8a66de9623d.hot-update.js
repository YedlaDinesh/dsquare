self["webpackHotUpdate_N_E"]("pages/index",{

/***/ "./pages/index.jsx":
/*!*************************!*\
  !*** ./pages/index.jsx ***!
  \*************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/lib/Helmet.js");
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @apollo/react-hooks */ "./node_modules/@apollo/react-hooks/lib/react-hooks.esm.js");
/* harmony import */ var _server_apollo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../server/apollo */ "./server/apollo.js");
/* harmony import */ var _server_queries__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../server/queries */ "./server/queries.js");
/* harmony import */ var _components_features_modals_newsletter_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/features/modals/newsletter-modal */ "./components/features/modals/newsletter-modal.jsx");
/* harmony import */ var _components_partials_home_intro_section__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/components/partials/home/intro-section */ "./components/partials/home/intro-section.jsx");
/* harmony import */ var _components_partials_home_service_section__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/components/partials/home/service-section */ "./components/partials/home/service-section.jsx");
/* harmony import */ var _components_partials_home_category_section__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/components/partials/home/category-section */ "./components/partials/home/category-section.jsx");
/* harmony import */ var _components_partials_home_best_collection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ~/components/partials/home/best-collection */ "./components/partials/home/best-collection.jsx");
/* harmony import */ var _components_partials_home_deal_section__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ~/components/partials/home/deal-section */ "./components/partials/home/deal-section.jsx");
/* harmony import */ var _components_partials_home_featured_collection__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ~/components/partials/home/featured-collection */ "./components/partials/home/featured-collection.jsx");
/* harmony import */ var _components_partials_home_cta_section__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ~/components/partials/home/cta-section */ "./components/partials/home/cta-section.jsx");
/* harmony import */ var _components_partials_home_brand_section__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ~/components/partials/home/brand-section */ "./components/partials/home/brand-section.jsx");
/* harmony import */ var _components_partials_home_blog_section__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ~/components/partials/home/blog-section */ "./components/partials/home/blog-section.jsx");
/* harmony import */ var _components_partials_product_small_collection__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ~/components/partials/product/small-collection */ "./components/partials/product/small-collection.jsx");
/* module decorator */ module = __webpack_require__.hmd(module);


var _jsxFileName = "D:\\dsquare\\pages\\index.jsx",
    _s = $RefreshSig$();



 // Import Apollo Server and Query


 // import Home Components













function HomePage() {
  _s();

  var _useQuery = (0,_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_16__.useQuery)(_server_queries__WEBPACK_IMPORTED_MODULE_4__.GET_HOME_DATA, {
    variables: {
      productsCount: 7
    }
  }),
      data = _useQuery.data,
      loading = _useQuery.loading,
      error = _useQuery.error;

  var featured = data && data.specialProducts.featured;
  var bestSelling = data && data.specialProducts.bestSelling;
  var latest = data && data.specialProducts.latest;
  var onSale = data && data.specialProducts.onSale;
  var posts = data && data.posts.data;
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "main home",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_helmet__WEBPACK_IMPORTED_MODULE_2__.Helmet, {
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("title", {
        children: "Dsqaure"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h1", {
      className: "d-none",
      children: "Dsqaure"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "page-content",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "intro-section",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_intro_section__WEBPACK_IMPORTED_MODULE_6__.default, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 45,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_service_section__WEBPACK_IMPORTED_MODULE_7__.default, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 47,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_category_section__WEBPACK_IMPORTED_MODULE_8__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_featured_collection__WEBPACK_IMPORTED_MODULE_11__.default, {
        products: featured,
        loading: loading
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_cta_section__WEBPACK_IMPORTED_MODULE_12__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_blog_section__WEBPACK_IMPORTED_MODULE_14__.default, {
        posts: posts
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_brand_section__WEBPACK_IMPORTED_MODULE_13__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_home_best_collection__WEBPACK_IMPORTED_MODULE_9__.default, {
        products: bestSelling,
        loading: loading
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_modals_newsletter_modal__WEBPACK_IMPORTED_MODULE_5__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 35,
    columnNumber: 9
  }, this);
}

_s(HomePage, "tP+6C5plfRwxqCbBj3cMUcL7Opk=", false, function () {
  return [_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_16__.useQuery];
});

_c = HomePage;
/* harmony default export */ __webpack_exports__["default"] = ((0,_server_apollo__WEBPACK_IMPORTED_MODULE_3__.default)({
  ssr: false
})(HomePage)); //export default HomePage;

var _c;

$RefreshReg$(_c, "HomePage");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanN4Il0sIm5hbWVzIjpbIkhvbWVQYWdlIiwidXNlUXVlcnkiLCJHRVRfSE9NRV9EQVRBIiwidmFyaWFibGVzIiwicHJvZHVjdHNDb3VudCIsImRhdGEiLCJsb2FkaW5nIiwiZXJyb3IiLCJmZWF0dXJlZCIsInNwZWNpYWxQcm9kdWN0cyIsImJlc3RTZWxsaW5nIiwibGF0ZXN0Iiwib25TYWxlIiwicG9zdHMiLCJ3aXRoQXBvbGxvIiwic3NyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7Q0FJQTs7QUFDQTtDQUdBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0EsUUFBVCxHQUFvQjtBQUFBOztBQUFBLGtCQUNpQkMsOERBQVEsQ0FBRUMsMERBQUYsRUFBaUI7QUFBRUMsYUFBUyxFQUFFO0FBQUVDLG1CQUFhLEVBQUU7QUFBakI7QUFBYixHQUFqQixDQUR6QjtBQUFBLE1BQ1JDLElBRFEsYUFDUkEsSUFEUTtBQUFBLE1BQ0ZDLE9BREUsYUFDRkEsT0FERTtBQUFBLE1BQ09DLEtBRFAsYUFDT0EsS0FEUDs7QUFFaEIsTUFBTUMsUUFBUSxHQUFHSCxJQUFJLElBQUlBLElBQUksQ0FBQ0ksZUFBTCxDQUFxQkQsUUFBOUM7QUFDQSxNQUFNRSxXQUFXLEdBQUdMLElBQUksSUFBSUEsSUFBSSxDQUFDSSxlQUFMLENBQXFCQyxXQUFqRDtBQUNBLE1BQU1DLE1BQU0sR0FBR04sSUFBSSxJQUFJQSxJQUFJLENBQUNJLGVBQUwsQ0FBcUJFLE1BQTVDO0FBQ0EsTUFBTUMsTUFBTSxHQUFHUCxJQUFJLElBQUlBLElBQUksQ0FBQ0ksZUFBTCxDQUFxQkcsTUFBNUM7QUFDQSxNQUFNQyxLQUFLLEdBQUdSLElBQUksSUFBSUEsSUFBSSxDQUFDUSxLQUFMLENBQVdSLElBQWpDO0FBS0Esc0JBQ0k7QUFBSyxhQUFTLEVBQUMsV0FBZjtBQUFBLDRCQUNJLDhEQUFDLGdEQUFEO0FBQUEsNkJBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFNSTtBQUFJLGVBQVMsRUFBQyxRQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBTkosZUFRSTtBQUFLLGVBQVMsRUFBQyxjQUFmO0FBQUEsOEJBQ0k7QUFBSyxpQkFBUyxFQUFDLGVBQWY7QUFBQSxnQ0FDSSw4REFBQyw0RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBR0ksOERBQUMsOEVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQU9JLDhEQUFDLCtFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FQSixlQWFJLDhEQUFDLG1GQUFEO0FBQW9CLGdCQUFRLEVBQUdHLFFBQS9CO0FBQTBDLGVBQU8sRUFBR0Y7QUFBcEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWJKLGVBZUksOERBQUMsMkVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWZKLGVBaUJJLDhEQUFDLDRFQUFEO0FBQWEsYUFBSyxFQUFHTztBQUFyQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBakJKLGVBbUJJLDhEQUFDLDZFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FuQkosZUFxQkksOERBQUMsOEVBQUQ7QUFBZ0IsZ0JBQVEsRUFBR0gsV0FBM0I7QUFBeUMsZUFBTyxFQUFHSjtBQUFuRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBckJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQVJKLGVBa0NJLDhEQUFDLGlGQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFsQ0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFzQ0g7O0dBakRRTixRO1VBQzRCQywwRDs7O0tBRDVCRCxRO0FBbURULCtEQUFlYyx1REFBVSxDQUFFO0FBQUVDLEtBQUc7QUFBTCxDQUFGLENBQVYsQ0FBc0RmLFFBQXRELENBQWYsRSxDQUNBIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LmRiMzIyMDI1ZDhhNjZkZTk2MjNkLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBIZWxtZXQgfSBmcm9tICdyZWFjdC1oZWxtZXQnO1xyXG5cclxuaW1wb3J0IHsgdXNlUXVlcnkgfSBmcm9tIFwiQGFwb2xsby9yZWFjdC1ob29rc1wiO1xyXG5cclxuLy8gSW1wb3J0IEFwb2xsbyBTZXJ2ZXIgYW5kIFF1ZXJ5XHJcbmltcG9ydCB3aXRoQXBvbGxvIGZyb20gJy4uL3NlcnZlci9hcG9sbG8nO1xyXG5pbXBvcnQgeyBHRVRfSE9NRV9EQVRBIH0gZnJvbSAnLi4vc2VydmVyL3F1ZXJpZXMnO1xyXG5cclxuLy8gaW1wb3J0IEhvbWUgQ29tcG9uZW50c1xyXG5pbXBvcnQgTmV3c2xldHRlck1vZGFsIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9tb2RhbHMvbmV3c2xldHRlci1tb2RhbCc7XHJcbmltcG9ydCBJbnRyb1NlY3Rpb24gZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL2hvbWUvaW50cm8tc2VjdGlvbic7XHJcbmltcG9ydCBTZXJ2aWNlQm94IGZyb20gJ34vY29tcG9uZW50cy9wYXJ0aWFscy9ob21lL3NlcnZpY2Utc2VjdGlvbic7XHJcbmltcG9ydCBDYXRlZ29yeVNlY3Rpb24gZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL2hvbWUvY2F0ZWdvcnktc2VjdGlvbic7XHJcbmltcG9ydCBCZXN0Q29sbGVjdGlvbiBmcm9tICd+L2NvbXBvbmVudHMvcGFydGlhbHMvaG9tZS9iZXN0LWNvbGxlY3Rpb24nO1xyXG5pbXBvcnQgRGVhbFNlY3Rpb24gZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL2hvbWUvZGVhbC1zZWN0aW9uJztcclxuaW1wb3J0IEZlYXR1cmVkQ29sbGVjdGlvbiBmcm9tICd+L2NvbXBvbmVudHMvcGFydGlhbHMvaG9tZS9mZWF0dXJlZC1jb2xsZWN0aW9uJztcclxuaW1wb3J0IEN0YVNlY3Rpb24gZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL2hvbWUvY3RhLXNlY3Rpb24nO1xyXG5pbXBvcnQgQnJhbmRTZWN0aW9uIGZyb20gJ34vY29tcG9uZW50cy9wYXJ0aWFscy9ob21lL2JyYW5kLXNlY3Rpb24nO1xyXG5pbXBvcnQgQmxvZ1NlY3Rpb24gZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL2hvbWUvYmxvZy1zZWN0aW9uJztcclxuaW1wb3J0IFNtYWxsQ29sbGVjdGlvbiBmcm9tICd+L2NvbXBvbmVudHMvcGFydGlhbHMvcHJvZHVjdC9zbWFsbC1jb2xsZWN0aW9uJztcclxuXHJcbmZ1bmN0aW9uIEhvbWVQYWdlKCkge1xyXG4gICAgY29uc3QgeyBkYXRhLCBsb2FkaW5nLCBlcnJvciB9ID0gdXNlUXVlcnkoIEdFVF9IT01FX0RBVEEsIHsgdmFyaWFibGVzOiB7IHByb2R1Y3RzQ291bnQ6IDcgfSB9ICk7XHJcbiAgICBjb25zdCBmZWF0dXJlZCA9IGRhdGEgJiYgZGF0YS5zcGVjaWFsUHJvZHVjdHMuZmVhdHVyZWQ7XHJcbiAgICBjb25zdCBiZXN0U2VsbGluZyA9IGRhdGEgJiYgZGF0YS5zcGVjaWFsUHJvZHVjdHMuYmVzdFNlbGxpbmc7XHJcbiAgICBjb25zdCBsYXRlc3QgPSBkYXRhICYmIGRhdGEuc3BlY2lhbFByb2R1Y3RzLmxhdGVzdDtcclxuICAgIGNvbnN0IG9uU2FsZSA9IGRhdGEgJiYgZGF0YS5zcGVjaWFsUHJvZHVjdHMub25TYWxlO1xyXG4gICAgY29uc3QgcG9zdHMgPSBkYXRhICYmIGRhdGEucG9zdHMuZGF0YTtcclxuXHJcblxyXG5cclxuICAgIFxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1haW4gaG9tZVwiPlxyXG4gICAgICAgICAgICA8SGVsbWV0PlxyXG4gICAgICAgICAgICAgICAgPHRpdGxlPkRzcWF1cmU8L3RpdGxlPlxyXG4gICAgICAgICAgICA8L0hlbG1ldD5cclxuXHJcblxyXG4gICAgICAgICAgICA8aDEgY2xhc3NOYW1lPVwiZC1ub25lXCI+RHNxYXVyZTwvaDE+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhZ2UtY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbnRyby1zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEludHJvU2VjdGlvbiAvPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8U2VydmljZUJveCAvPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgPENhdGVnb3J5U2VjdGlvbiAvPlxyXG5cclxuICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgey8qIDxEZWFsU2VjdGlvbiAvPiAqL31cclxuXHJcbiAgICAgICAgICAgICAgICA8RmVhdHVyZWRDb2xsZWN0aW9uIHByb2R1Y3RzPXsgZmVhdHVyZWQgfSBsb2FkaW5nPXsgbG9hZGluZyB9IC8+XHJcblxyXG4gICAgICAgICAgICAgICAgPEN0YVNlY3Rpb24gLz5cclxuXHJcbiAgICAgICAgICAgICAgICA8QmxvZ1NlY3Rpb24gcG9zdHM9eyBwb3N0cyB9IC8+IFxyXG5cclxuICAgICAgICAgICAgICAgIDxCcmFuZFNlY3Rpb24gLz5cclxuXHJcbiAgICAgICAgICAgICAgICA8QmVzdENvbGxlY3Rpb24gcHJvZHVjdHM9eyBiZXN0U2VsbGluZyB9IGxvYWRpbmc9eyBsb2FkaW5nIH0gLz5cclxuXHJcbiAgICAgICAgICAgICAgICB7LyogPFNtYWxsQ29sbGVjdGlvbiBmZWF0dXJlZD17IGZlYXR1cmVkIH0gbGF0ZXN0PXsgbGF0ZXN0IH0gYmVzdFNlbGxpbmc9eyBiZXN0U2VsbGluZyB9IG9uU2FsZT17IG9uU2FsZSB9IGxvYWRpbmc9eyBsb2FkaW5nIH0gLz4gKi99XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPE5ld3NsZXR0ZXJNb2RhbCAvPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCB3aXRoQXBvbGxvKCB7IHNzcjogdHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcgfSApKCBIb21lUGFnZSApO1xyXG4vL2V4cG9ydCBkZWZhdWx0IEhvbWVQYWdlOyJdLCJzb3VyY2VSb290IjoiIn0=