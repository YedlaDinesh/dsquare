exports.id = "components_common_Api_js-components_features_owl-carousel_jsx-server_apollo_js-server_queries-daab03";
exports.ids = ["components_common_Api_js-components_features_owl-carousel_jsx-server_apollo_js-server_queries-daab03"];
exports.modules = {

/***/ "./components/common/Api.js":
/*!**********************************!*\
  !*** ./components/common/Api.js ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "API_BASE_URL": function() { return /* binding */ API_BASE_URL; },
/* harmony export */   "getApiUrl": function() { return /* binding */ getApiUrl; },
/* harmony export */   "SIGNUP_API": function() { return /* binding */ SIGNUP_API; },
/* harmony export */   "SENDOTP_API": function() { return /* binding */ SENDOTP_API; },
/* harmony export */   "VERIFY_API": function() { return /* binding */ VERIFY_API; },
/* harmony export */   "ADD_ADDRESS_API": function() { return /* binding */ ADD_ADDRESS_API; },
/* harmony export */   "GET_ADDRESS_API": function() { return /* binding */ GET_ADDRESS_API; },
/* harmony export */   "DELETE_ADDRESS_API": function() { return /* binding */ DELETE_ADDRESS_API; },
/* harmony export */   "PRODUCT_CATEGORY_API": function() { return /* binding */ PRODUCT_CATEGORY_API; }
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const API_BASE_URL = "http://3.16.7.21:3000/dsquare/api/";
const getApiUrl = endpoint => API_BASE_URL + endpoint;
const SIGNUP_API = getApiUrl("create-account");
const SENDOTP_API = getApiUrl("generate-otp");
const VERIFY_API = getApiUrl("verify-otp");
const ADD_ADDRESS_API = getApiUrl("add-address");
const GET_ADDRESS_API = getApiUrl("get-my-adress");
const DELETE_ADDRESS_API = getApiUrl("delete-my-adress");
const PRODUCT_CATEGORY_API = getApiUrl("product-categories");

/***/ }),

/***/ "./components/features/custom-link.jsx":
/*!*********************************************!*\
  !*** ./components/features/custom-link.jsx ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ ALink; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");

var _jsxFileName = "D:\\dsquare\\components\\features\\custom-link.jsx";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function ALink(_ref) {
  let {
    children,
    className,
    content,
    style
  } = _ref,
      props = _objectWithoutProperties(_ref, ["children", "className", "content", "style"]);

  const preventDefault = e => {
    if (props.href === '#') {
      e.preventDefault();
    }

    if (props.onClick) {
      e.preventDefault();
      props.onClick();
    }
  };

  return content ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_1___default()), _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      className: className,
      style: style,
      onClick: preventDefault,
      dangerouslySetInnerHTML: (0,_utils__WEBPACK_IMPORTED_MODULE_2__.parseContent)(content),
      children: children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 17
    }, this)
  }), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 20,
    columnNumber: 13
  }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_1___default()), _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      className: className,
      style: style,
      onClick: preventDefault,
      children: children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 17
    }, this)
  }), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 25,
    columnNumber: 13
  }, this);
}

/***/ }),

/***/ "./components/features/owl-carousel.jsx":
/*!**********************************************!*\
  !*** ./components/features/owl-carousel.jsx ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_owl_carousel2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-owl-carousel2 */ "react-owl-carousel2");
/* harmony import */ var react_owl_carousel2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_owl_carousel2__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "D:\\dsquare\\components\\features\\owl-carousel.jsx";

 // let prevPath;

function OwlCarousel(props) {
  const {
    adClass,
    options
  } = props;
  const carouselRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
  const defaultOptions = {
    items: 1,
    loop: false,
    margin: 0,
    responsiveClass: "true",
    nav: true,
    navText: ['<i class="d-icon-angle-left">', '<i class="d-icon-angle-right">'],
    navElement: "button",
    dots: true,
    smartSpeed: 400,
    autoplay: false,
    autoHeight: false // autoplayTimeout: 5000,

  };
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (props.onChangeRef) {
      props.onChangeRef(carouselRef);
    }
  }, [carouselRef]);
  let events = {
    onTranslated: function (e) {
      if (!e.target) return;

      if (props.onChangeIndex) {
        props.onChangeIndex(e.item.index);
      }
    }
  };
  events = Object.assign({}, events, props.events);
  let settings = Object.assign({}, defaultOptions, options);
  return props.children ? props.children.length > 0 || props.children && props.children.length === undefined ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_owl_carousel2__WEBPACK_IMPORTED_MODULE_2___default()), {
    ref: carouselRef,
    className: `owl-carousel ${adClass}`,
    options: settings,
    events: events,
    children: props.children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 44,
    columnNumber: 17
  }, this) : "" : "";
}

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(OwlCarousel));

/***/ }),

/***/ "./components/features/product/common/cart-popup.jsx":
/*!***********************************************************!*\
  !*** ./components/features/product/common/cart-popup.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ CartPopup; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");

var _jsxFileName = "D:\\dsquare\\components\\features\\product\\common\\cart-popup.jsx";



function CartPopup(props) {
  const {
    product
  } = props;
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "minipopup-area",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "minipopup-box show",
      style: {
        top: "0"
      },
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
        className: "minipopup-title",
        children: "Successfully added."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "product product-purchased  product-cart mb-0",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("figure", {
          className: "product-media pure-media",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
            href: `/product/default/${product.slug}`,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
              src: "http://localhost:4000" + product.pictures[0].url,
              alt: "product",
              width: "90",
              height: "90"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 18,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 17,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "product-detail",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
            href: `/product/default/${product.slug}`,
            className: "product-name",
            children: product.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 27,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "price-box",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "product-quantity",
              children: product.qty
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 29,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "product-price",
              children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_3__.toDecimal)(product.price)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 30,
              columnNumber: 29
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 28,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 26,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "action-group d-flex",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
          href: "/pages/cart",
          className: "btn btn-sm btn-outline btn-primary btn-rounded",
          children: "View Cart"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
          href: "/pages/checkout",
          className: "btn btn-sm btn-primary btn-rounded",
          children: "Check Out"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 13
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./server/apollo.js":
/*!**************************!*\
  !*** ./server/apollo.js ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var next_apollo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next-apollo */ "next-apollo");
/* harmony import */ var next_apollo__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_apollo__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var apollo_boost__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! apollo-boost */ "apollo-boost");
/* harmony import */ var apollo_boost__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(apollo_boost__WEBPACK_IMPORTED_MODULE_1__);


const API_URI = `${"http://localhost:4000"}/graphql`;
const apolloClient = new (apollo_boost__WEBPACK_IMPORTED_MODULE_1___default())({
  uri: API_URI,
  cache: new apollo_boost__WEBPACK_IMPORTED_MODULE_1__.InMemoryCache()
});
/* harmony default export */ __webpack_exports__["default"] = ((0,next_apollo__WEBPACK_IMPORTED_MODULE_0__.withApollo)(apolloClient));

/***/ }),

/***/ "./server/queries.js":
/*!***************************!*\
  !*** ./server/queries.js ***!
  \***************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "currentDemo": function() { return /* binding */ currentDemo; },
/* harmony export */   "GET_PRODUCTS": function() { return /* binding */ GET_PRODUCTS; },
/* harmony export */   "GET_SPECIAL_PRODUCTS": function() { return /* binding */ GET_SPECIAL_PRODUCTS; },
/* harmony export */   "GET_PRODUCT": function() { return /* binding */ GET_PRODUCT; },
/* harmony export */   "GET_VIDEO": function() { return /* binding */ GET_VIDEO; },
/* harmony export */   "GET_SHOP_SIDEBAR_DATA": function() { return /* binding */ GET_SHOP_SIDEBAR_DATA; },
/* harmony export */   "GET_POSTS": function() { return /* binding */ GET_POSTS; },
/* harmony export */   "GET_POST": function() { return /* binding */ GET_POST; },
/* harmony export */   "GET_POST_SIDEBAR_DATA": function() { return /* binding */ GET_POST_SIDEBAR_DATA; },
/* harmony export */   "GET_HOME_DATA": function() { return /* binding */ GET_HOME_DATA; }
/* harmony export */ });
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! graphql-tag */ "graphql-tag");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_0__);

const currentDemo = `"1"`;
const PRODUCT_SIMPLE = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    fragment ProductSimple on Product {
        name
        slug
        price
        ratings
        reviews
        stock
        short_description
        is_featured
        is_new
        until
        discount
        pictures {
            url
            width
            height
        }
        small_pictures {
            url
            width
            height
        }
        categories {
            name
            slug
        }
        variants {
            price
            sale_price
        }
    }
`;
const PRODUCT_SMALL = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    fragment ProductSmall on Product {
        id
        name
        slug
        price
        ratings
        pictures {
            url
            width
            height
        }
        small_pictures {
            url
            width
            height
        }
        variants {
            price
            sale_price
        }
    }
`;
const GET_PRODUCTS = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query products($search: String, $colors: [String], $sizes: [String], $brands: [String], $min_price: Int, $max_price: Int, $category: String, $tag: String, $sortBy: String, $from: Int, $to: Int, $list: Boolean = false) {
        products(demo: ${currentDemo}, search: $search, colors: $colors, sizes: $sizes, brands: $brands, min_price: $min_price, max_price: $max_price, category: $category, tag: $tag, sortBy: $sortBy, from: $from, to: $to ) {
            data {
                short_description @include(if: $list)
                ...ProductSimple

            }
            total
        }
    }
    ${PRODUCT_SIMPLE}
`;
const GET_SPECIAL_PRODUCTS = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query specialProducts($featured: Boolean = false, $bestSelling: Boolean = false, $topRated: Boolean = false, $onSale: Boolean = false, $count: Int) {
        specialProducts(demo: ${currentDemo}, featured: $featured, bestSelling: $bestSelling, topRated: $topRated, onSale: $onSale, count: $count) {
            featured @include(if: $featured) {
                ...ProductSmall
            }
            bestSelling @include(if: $bestSelling) {
                ...ProductSmall
            }
            topRated @include(if: $topRated) {
                ...ProductSmall
            }
            latest @include(if: $latest) {
                ...ProductSmall
            }
        }
    }
    ${PRODUCT_SMALL}
`;
const GET_PRODUCT = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query product($slug: String!, $onlyData: Boolean = false) {
        product(demo: ${currentDemo}, slug: $slug, onlyData: $onlyData) {
            data {
                id
                slug
                name
                price
                discount
                short_description
                sku
                stock
                ratings
                reviews
                sale_count
                is_new
                is_top
                until
                pictures {
                    url
                    width
                    height
                }
                small_pictures {
                    url
                    width
                    height
                }
                large_pictures {
                    url
                    width
                    height
                }
                categories {
                    name
                    slug
                }
                tags {
                    name
                    slug
                }
                brands {
                    name
                    slug
                }
                variants {
                    price
                    sale_price
                    color {
                        name
                        color
                        thumb {
                            url
                            width
                            height
                        }
                    }
                    size {
                        name
                        size
                        thumb {
                            url
                            width
                            height
                        }
                    }
                }
            }
            prev @skip(if: $onlyData) {
                slug
                name
                pictures {
                    url
                    width
                    height
                }
            }
            next @skip(if: $onlyData) {
                slug
                name
                pictures {
                    url
                    width
                    height
                }
            }
            related @skip(if: $onlyData) {
                ...ProductSimple
            }
        }
    }
    ${PRODUCT_SIMPLE}
`;
const GET_VIDEO = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query video($slug: String!) {
        video(demo: ${currentDemo}, slug: $slug) {
            data {
                url
                width
                height
            }
        }
    }
`;
const GET_SHOP_SIDEBAR_DATA = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query shopSidebarData($featured: Boolean = false) {
        shopSidebarData(demo: ${currentDemo}, featured: $featured) {
            categories {
                name
                slug
                children {
                    name
                    slug
                }
            }
            featured @include(if: $featured) {
                slug
                name
                price
                ratings
                reviews
                pictures {
                    url
                    width
                    height
                }
                variants {
                    price
                }
            }
        }
    }
`;
const GET_POSTS = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query posts($category: String, $from: Int, $to: Int, $categories: [String]) {
        posts(demo: ${currentDemo}, category: $category, from: $from, to: $to, categories: $categories ) {
            data {
                title
                slug
                date
                comments
                content
                type
                author
                large_picture {
                    url
                    width
                    height
                }
                picture {
                    url
                    width
                    height
                }
                small_picture {
                    url
                    width
                    height
                }
                video {
                    url
                    width
                    height
                }
            }
            total
        }
    }
`;
const GET_POST = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query post($slug: String!) {
        post(demo: ${currentDemo}, slug: $slug) {
            data {
                title
                slug
                author
                date
                comments
                content
                type
                large_picture {
                    url
                    width
                    height
                }
                picture {
                    url
                    width
                    height
                }
                video {
                    url
                    width
                    height
                }
                categories {
                    name
                    slug
                }
            }
            related {
                title
                slug
                date
                type
                comments
                content
                picture {
                    url
                    width
                    height
                }
                video {
                    url
                    width
                    height
                }
            }
        }
    }
`;
const GET_POST_SIDEBAR_DATA = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query postSidbarData {
        postSidebarData(demo: ${currentDemo}) {
            categories {
                name
                slug
            }
            recent {
                title
                slug
                date
                small_picture {
                    url
                    width
                    height
                }
            }
        }
    }
`;
const GET_HOME_DATA = (graphql_tag__WEBPACK_IMPORTED_MODULE_0___default())`
    query indexData($productsCount: Int, $postsCount: Int) {
        specialProducts(demo: ${currentDemo}, featured: true, bestSelling: true, topRated: true, latest: true, onSale: true, count: $productsCount) {
            featured {
                ...ProductSimple
            }
            bestSelling {
                ...ProductSimple
            }
            topRated {
                ...ProductSimple
            }
            latest {
                ...ProductSimple
            }
            onSale {
                ...ProductSimple
            }
        }
        posts(demo: ${currentDemo}, to: $postsCount) {
            data {
                title
                slug
                date
                type
                comments
                content
                picture {
                    url
                    width
                    height
                }
                video {
                    url
                    width
                    height
                }
                large_picture {
                    url
                    width
                    height
                }
            }
        }
    }
    ${PRODUCT_SIMPLE}
`;

/***/ }),

/***/ "./store/cart.js":
/*!***********************!*\
  !*** ./store/cart.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cartActions": function() { return /* binding */ cartActions; },
/* harmony export */   "cartSaga": function() { return /* binding */ cartSaga; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-persist */ "redux-persist");
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_persist__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-persist/lib/storage */ "redux-persist/lib/storage");
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_features_product_common_cart_popup__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/features/product/common/cart-popup */ "./components/features/product/common/cart-popup.jsx");

var _jsxFileName = "D:\\dsquare\\store\\cart.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const actionTypes = {
  ADD_TO_CART: 'ADD_TO_CART',
  REMOVE_FROM_CART: 'REMOVE_FROM_CART',
  UPDATE_CART: 'UPDATE_CART',
  REFRESH_STORE: 'REFRESH_STORE'
};
const initialState = {
  data: []
};

function cartReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADD_TO_CART:
      let tmpProduct = _objectSpread({}, action.payload.product);

      if (state.data.findIndex(item => item.name === action.payload.product.name) > -1) {
        let tmpData = state.data.reduce((acc, cur) => {
          if (cur.name === tmpProduct.name) {
            acc.push(_objectSpread(_objectSpread({}, cur), {}, {
              qty: parseInt(cur.qty) + parseInt(tmpProduct.qty)
            }));
          } else {
            acc.push(cur);
          }

          return acc;
        }, []);
        return _objectSpread(_objectSpread({}, state), {}, {
          data: tmpData
        });
      } else {
        return _objectSpread(_objectSpread({}, state), {}, {
          data: [...state.data, tmpProduct]
        });
      }

    case actionTypes.REMOVE_FROM_CART:
      let cart = state.data.reduce((cartAcc, product) => {
        if (product.name !== action.payload.product.name) {
          cartAcc.push(product);
        }

        return cartAcc;
      }, []);
      return _objectSpread(_objectSpread({}, state), {}, {
        data: cart
      });

    case actionTypes.UPDATE_CART:
      return _objectSpread(_objectSpread({}, state), {}, {
        data: action.payload.products
      });

    case actionTypes.REFRESH_STORE:
      return initialState;

    default:
      return state;
  }
}

const cartActions = {
  addToCart: product => ({
    type: actionTypes.ADD_TO_CART,
    payload: {
      product
    }
  }),
  removeFromCart: product => ({
    type: actionTypes.REMOVE_FROM_CART,
    payload: {
      product
    }
  }),
  updateCart: products => ({
    type: actionTypes.UPDATE_CART,
    payload: {
      products
    }
  })
};
function* cartSaga() {
  yield (0,redux_saga_effects__WEBPACK_IMPORTED_MODULE_4__.takeEvery)(actionTypes.ADD_TO_CART, function* saga(e) {
    (0,react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast)( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_product_common_cart_popup__WEBPACK_IMPORTED_MODULE_5__.default, {
      product: e.payload.product
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 16
    }, this));
  });
}
const persistConfig = {
  keyPrefix: "riode-",
  key: "cart",
  storage: (redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_2___default())
};
/* harmony default export */ __webpack_exports__["default"] = ((0,redux_persist__WEBPACK_IMPORTED_MODULE_1__.persistReducer)(persistConfig, cartReducer));

/***/ }),

/***/ "./store/modal.js":
/*!************************!*\
  !*** ./store/modal.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "modalActions": function() { return /* binding */ modalActions; }
/* harmony export */ });
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-persist */ "redux-persist");
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_persist__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-persist/lib/storage */ "redux-persist/lib/storage");
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const actionTypes = {
  OPEN_MODAL: 'OPEN_MODAL',
  CLOSE_MODAL: 'CLOSE_MODAL',
  OPEN_QUICKVIEW: 'OPEN_QUICKVIEW',
  CLOSE_QUICKVIEW: 'CLOSE_QUICKVIEW',
  REFRESH_STORE: 'REFRESH_STORE'
};
const initialState = {
  type: 'video',
  openModal: false,
  quickview: false,
  singleSlug: ''
};

function modalReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.OPEN_QUICKVIEW:
      return _objectSpread(_objectSpread({}, state), {}, {
        quickview: true,
        singleSlug: action.payload.slug
      });

    case actionTypes.CLOSE_QUICKVIEW:
      return _objectSpread(_objectSpread({}, state), {}, {
        quickview: false
      });

    case actionTypes.OPEN_MODAL:
      return _objectSpread(_objectSpread({}, state), {}, {
        singleSlug: action.payload.slug,
        openModal: true
      });

    case actionTypes.CLOSE_MODAL:
      return _objectSpread(_objectSpread({}, state), {}, {
        openModal: false
      });

    case actionTypes.REFRESH_STORE:
      return initialState;

    default:
      return state;
  }
}

const modalActions = {
  openModal: slug => ({
    type: actionTypes.OPEN_MODAL,
    payload: {
      slug
    }
  }),
  closeModal: modalType => ({
    type: actionTypes.CLOSE_MODAL,
    payload: {
      modalType
    }
  }),
  openQuickview: slug => ({
    type: actionTypes.OPEN_QUICKVIEW,
    payload: {
      slug
    }
  }),
  closeQuickview: () => ({
    type: actionTypes.CLOSE_QUICKVIEW
  })
};
const persistConfig = {
  keyPrefix: "riode-",
  key: "modal",
  storage: (redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default())
};
/* harmony default export */ __webpack_exports__["default"] = ((0,redux_persist__WEBPACK_IMPORTED_MODULE_0__.persistReducer)(persistConfig, modalReducer));

/***/ }),

/***/ "./store/wishlist.js":
/*!***************************!*\
  !*** ./store/wishlist.js ***!
  \***************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "wishlistActions": function() { return /* binding */ wishlistActions; }
/* harmony export */ });
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-persist */ "redux-persist");
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_persist__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-persist/lib/storage */ "redux-persist/lib/storage");
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const actionTypes = {
  TOGGLE_WISHLIST: 'TOGGLE_WISHLIST',
  REMOVE_FROM_WISHLIST: 'REMOVE_FROM_WISHLIST',
  REFRESH_STORE: 'REFRESH_STORE'
};
const initialState = {
  data: []
};

function wishlistReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.TOGGLE_WISHLIST:
      let index = state.data.findIndex(item => item.name === action.payload.product.name);
      let tmpData = [...state.data];

      if (index === -1) {
        tmpData.push(action.payload.product);
      } else {
        tmpData.splice(index);
      }

      return _objectSpread(_objectSpread({}, state), {}, {
        data: tmpData
      });

    case actionTypes.REMOVE_FROM_WISHLIST:
      let wishlist = state.data.reduce((wishlistAcc, product) => {
        if (product.name !== action.payload.product.name) {
          wishlistAcc.push(product);
        }

        return wishlistAcc;
      }, []);
      return _objectSpread(_objectSpread({}, state), {}, {
        data: wishlist
      });

    case actionTypes.REFRESH_STORE:
      return initialState;

    default:
  }

  return state;
}

const wishlistActions = {
  toggleWishlist: product => ({
    type: actionTypes.TOGGLE_WISHLIST,
    payload: {
      product
    }
  }),
  removeFromWishlist: product => ({
    type: actionTypes.REMOVE_FROM_WISHLIST,
    payload: {
      product
    }
  })
};
const persistConfig = {
  keyPrefix: "riode-",
  key: "wishlist",
  storage: (redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default())
};
/* harmony default export */ __webpack_exports__["default"] = ((0,redux_persist__WEBPACK_IMPORTED_MODULE_0__.persistReducer)(persistConfig, wishlistReducer));

/***/ }),

/***/ "./utils/data/carousel.js":
/*!********************************!*\
  !*** ./utils/data/carousel.js ***!
  \********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "mainSlider1": function() { return /* binding */ mainSlider1; },
/* harmony export */   "mainSlider2": function() { return /* binding */ mainSlider2; },
/* harmony export */   "mainSlider3": function() { return /* binding */ mainSlider3; },
/* harmony export */   "mainSlider4": function() { return /* binding */ mainSlider4; },
/* harmony export */   "mainSlider5": function() { return /* binding */ mainSlider5; },
/* harmony export */   "mainSlider6": function() { return /* binding */ mainSlider6; },
/* harmony export */   "mainSlider7": function() { return /* binding */ mainSlider7; },
/* harmony export */   "mainSlider8": function() { return /* binding */ mainSlider8; },
/* harmony export */   "mainSlider9": function() { return /* binding */ mainSlider9; },
/* harmony export */   "mainSlider10": function() { return /* binding */ mainSlider10; },
/* harmony export */   "mainSlider11": function() { return /* binding */ mainSlider11; },
/* harmony export */   "mainSlider12": function() { return /* binding */ mainSlider12; },
/* harmony export */   "mainSlider13": function() { return /* binding */ mainSlider13; },
/* harmony export */   "mainSlider14": function() { return /* binding */ mainSlider14; },
/* harmony export */   "mainSlider15": function() { return /* binding */ mainSlider15; },
/* harmony export */   "mainSlider16": function() { return /* binding */ mainSlider16; },
/* harmony export */   "mainSlider17": function() { return /* binding */ mainSlider17; },
/* harmony export */   "mainSlider18": function() { return /* binding */ mainSlider18; },
/* harmony export */   "mainSlider19": function() { return /* binding */ mainSlider19; },
/* harmony export */   "mainSlider20": function() { return /* binding */ mainSlider20; },
/* harmony export */   "introSlider": function() { return /* binding */ introSlider; },
/* harmony export */   "serviceSlider": function() { return /* binding */ serviceSlider; },
/* harmony export */   "brandSlider": function() { return /* binding */ brandSlider; },
/* harmony export */   "productSlider": function() { return /* binding */ productSlider; },
/* harmony export */   "productSlider2": function() { return /* binding */ productSlider2; }
/* harmony export */ });
const mainSlider1 = {
  items: 5,
  nav: false,
  loop: false,
  dots: true,
  margin: 2,
  responsive: {
    0: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4
    },
    1200: {
      items: 5,
      dots: false
    }
  }
};
const mainSlider2 = {
  items: 1,
  loop: false,
  nav: true,
  dots: true,
  responsive: {
    0: {
      items: 1
    },
    768: {
      items: 2
    },
    992: {
      items: 1
    }
  }
};
const mainSlider3 = {
  autoHeight: false,
  dots: false,
  nav: true // dotsContainer: ".product-thumbs"

};
const mainSlider4 = {
  nav: false,
  dots: true,
  items: 1,
  margin: 20,
  loop: false,
  autoPlay: true
};
const mainSlider5 = {
  nav: false,
  dots: true,
  items: 4,
  margin: 20,
  loop: false,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4,
      dots: false
    }
  }
};
const mainSlider6 = {
  nav: false,
  dots: true,
  items: 3,
  margin: 20,
  loop: false,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    992: {
      items: 3,
      dots: false
    }
  }
};
const mainSlider7 = {
  items: 1,
  nav: true,
  dots: false,
  autoPlay: true,
  loop: false,
  margin: 20
};
const mainSlider8 = {
  items: 6,
  nav: false,
  dots: false,
  margin: 0,
  autoplay: true,
  responsive: {
    0: {
      items: 2
    },
    576: {
      items: 3
    },
    768: {
      items: 4
    },
    992: {
      items: 5
    },
    1200: {
      items: 6
    }
  }
};
const mainSlider9 = {
  items: 5,
  nav: false,
  dots: true,
  margin: 20,
  autoplay: true,
  responsive: {
    0: {
      items: 2
    },
    576: {
      items: 3
    },
    992: {
      items: 4
    },
    1200: {
      items: 5
    }
  }
};
const mainSlider10 = {
  items: 1,
  margin: 20,
  loop: false,
  nav: false,
  dots: true,
  responsive: {
    576: {
      items: 2
    },
    768: {
      items: 3
    }
  }
};
const mainSlider11 = {
  nav: false,
  dots: true,
  loop: false,
  margin: 20,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    768: {
      items: 3,
      dots: false
    }
  }
};
const mainSlider12 = {
  nav: false,
  dots: true,
  loop: false,
  margin: 20,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4,
      dots: false
    }
  }
};
const mainSlider13 = {
  items: 3,
  margin: 20,
  loop: false,
  nav: false,
  dots: true,
  responsive: {
    0: {
      items: 1
    },
    768: {
      items: 2
    },
    992: {
      items: 3,
      dots: false
    }
  }
};
const mainSlider14 = {
  items: 2,
  nav: false,
  dots: true,
  loop: false,
  margin: 20,
  responsive: {
    0: {
      items: 1
    },
    992: {
      items: 2,
      dots: false
    }
  }
};
const mainSlider15 = {
  items: 4,
  nav: true,
  autoHeight: false
};
const mainSlider16 = {
  items: 7,
  nav: false,
  dots: false,
  margin: 20,
  autoplay: true,
  autoplayTimeout: 3000,
  loop: true,
  responsive: {
    0: {
      items: 2
    },
    576: {
      items: 3
    },
    768: {
      items: 4
    },
    992: {
      items: 5
    },
    1200: {
      items: 6
    }
  }
};
const mainSlider17 = {
  loop: false,
  // dots: true,
  nav: false,
  margin: 20,
  responsive: {
    0: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4,
      dots: false,
      nav: false
    }
  } // ,
  // dotsContainer: ".product-thumbs"

};
const mainSlider18 = {
  items: 3,
  loop: false,
  nav: true,
  dots: false,
  margin: 20,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    768: {
      items: 3
    }
  }
};
const mainSlider19 = {
  nav: false,
  loop: false,
  dots: false,
  autoPlay: false,
  margin: 20,
  responsive: {
    0: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4
    }
  }
};
const mainSlider20 = {
  nav: false,
  dots: true
}; // home page

const introSlider = {
  nav: false,
  dots: true,
  loop: false,
  items: 1,
  autoplay: false
};
const serviceSlider = {
  items: 3,
  nav: false,
  dots: false,
  margin: 0,
  loop: false,
  autoplay: false,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    768: {
      items: 3,
      loop: false
    }
  }
};
const brandSlider = {
  items: 6,
  nav: false,
  dots: false,
  autoplay: true,
  loop: false,
  margin: 20,
  responsive: {
    0: {
      items: 2
    },
    576: {
      items: 3
    },
    768: {
      items: 4
    },
    992: {
      items: 5
    },
    1200: {
      items: 6
    }
  }
};
const productSlider = {
  items: 5,
  nav: false,
  dots: true,
  autoplay: false,
  loop: false,
  margin: 20,
  responsive: {
    0: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4,
      dots: false,
      nav: true
    }
  }
};
const productSlider2 = {
  items: 5,
  nav: false,
  dots: true,
  autoplay: false,
  loop: false,
  margin: 20,
  responsive: {
    0: {
      items: 2
    },
    768: {
      items: 3
    },
    992: {
      items: 4
    },
    1200: {
      items: 5,
      dots: false,
      nav: true
    }
  }
};

/***/ }),

/***/ "./utils/index.js":
/*!************************!*\
  !*** ./utils/index.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "parseOptions": function() { return /* binding */ parseOptions; },
/* harmony export */   "isIEBrowser": function() { return /* binding */ isIEBrowser; },
/* harmony export */   "isSafariBrowser": function() { return /* binding */ isSafariBrowser; },
/* harmony export */   "isEdgeBrowser": function() { return /* binding */ isEdgeBrowser; },
/* harmony export */   "findIndex": function() { return /* binding */ findIndex; },
/* harmony export */   "findArrayIndex": function() { return /* binding */ findArrayIndex; },
/* harmony export */   "parseContent": function() { return /* binding */ parseContent; },
/* harmony export */   "stickyHeaderHandler": function() { return /* binding */ stickyHeaderHandler; },
/* harmony export */   "resizeHandler": function() { return /* binding */ resizeHandler; },
/* harmony export */   "stickyFooterHandler": function() { return /* binding */ stickyFooterHandler; },
/* harmony export */   "parallaxHandler": function() { return /* binding */ parallaxHandler; },
/* harmony export */   "showScrollTopHandler": function() { return /* binding */ showScrollTopHandler; },
/* harmony export */   "scrollTopHandler": function() { return /* binding */ scrollTopHandler; },
/* harmony export */   "videoHandler": function() { return /* binding */ videoHandler; },
/* harmony export */   "getTotalPrice": function() { return /* binding */ getTotalPrice; },
/* harmony export */   "getCartCount": function() { return /* binding */ getCartCount; },
/* harmony export */   "toDecimal": function() { return /* binding */ toDecimal; }
/* harmony export */ });
/**
 * utils to parse options string to object
 * @param {string} options 
 * @return {object}
 */
const parseOptions = function (options) {
  if ("string" === typeof options) {
    return JSON.parse(options.replace(/'/g, '"').replace(';', ''));
  }

  return {};
};
/**
 * utils to dectect IE browser
 * @return {bool}
 */

const isIEBrowser = function () {
  let sUsrAg = navigator.userAgent;

  if (sUsrAg.indexOf("Trident") > -1) {
    return true;
  }

  return false;
};
/**
 * utils to detect safari browser
 * @return {bool}
 */

const isSafariBrowser = function () {
  let sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf('Safari') !== -1 && sUsrAg.indexOf('Chrome') === -1) return true;
  return false;
};
/**
 * utils to detect Edge browser
 * @return {bool}
 */

const isEdgeBrowser = function () {
  let sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf("Edge") > -1) return true;
  return false;
};
/**
 * utils to find index in array
 * @param {array} array
 * @param {callback} cb
 * @returns {number} index
 */

const findIndex = function (array, cb) {
  for (let i = 0; i < array.length; i++) {
    if (cb(array[i])) {
      return i;
    }
  }

  return -1;
};
/**
 * utils to get the position of the first element of search array in array
 * @param {array} array
 * @param {array} searchArray
 * @param {callback} cb
 * @returns {number} index
 */

const findArrayIndex = function (array, searchArray, cb) {
  for (let i = 0; i < searchArray.length; i++) {
    if (cb(searchArray[i])) {
      return i;
    }
  }

  return -1;
};
/**
 * utils to remove all XSS  attacks potential
 * @param {String} html
 * @return {Object}
 */

const parseContent = html => {
  const SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi; //Removing the <script> tags

  while (SCRIPT_REGEX.test(html)) {
    html = html.replace(SCRIPT_REGEX, '');
  } //Removing all events from tags...


  html = html.replace(/ on\w+="[^"]*"/g, '');
  return {
    __html: html
  };
};
/**
 * Apply sticky header
 */

const stickyHeaderHandler = function () {
  let top = document.querySelector('main') ? document.querySelector('main').offsetTop : 300;
  let stickyHeader = document.querySelector('.sticky-header');
  let height = 0;

  if (stickyHeader) {
    height = stickyHeader.offsetHeight;
  }

  if (window.pageYOffset >= top && window.innerWidth >= 992) {
    if (stickyHeader) {
      stickyHeader.classList.add('fixed');

      if (!document.querySelector('.sticky-wrapper')) {
        let newNode = document.createElement("div");
        newNode.className = "sticky-wrapper";
        stickyHeader.parentNode.insertBefore(newNode, stickyHeader);
        document.querySelector('.sticky-wrapper').insertAdjacentElement('beforeend', stickyHeader);
        document.querySelector('.sticky-wrapper').setAttribute("style", "height: " + height + "px");
      }

      if (!document.querySelector('.sticky-wrapper').getAttribute("style")) {
        document.querySelector('.sticky-wrapper').setAttribute("style", "height: " + height + "px");
      }
    }
  } else {
    if (stickyHeader) {
      stickyHeader.classList.remove('fixed');
    }

    if (document.querySelector('.sticky-wrapper')) {
      document.querySelector('.sticky-wrapper').removeAttribute("style");
    }
  }

  if (window.outerWidth >= 992 && document.querySelector('body').classList.contains('right-sidebar-active')) {
    document.querySelector('body').classList.remove('right-sidebar-active');
  }
};
/**
 * Add or remove settings when the window is resized
 */

const resizeHandler = function (width = 992, attri = 'right-sidebar-active') {
  let bodyClasses = document.querySelector("body") && document.querySelector("body").classList;
  bodyClasses = bodyClasses.value.split(' ').filter(item => item !== 'home' && item !== 'loaded');

  for (let i = 0; i < bodyClasses.length; i++) {
    document.querySelector("body") && document.querySelector('body').classList.remove(bodyClasses[i]);
  }
};
/**
 * Apply sticky footer
 */

const stickyFooterHandler = function () {
  let stickyFooter = document.querySelector('.sticky-footer');
  let top = document.querySelector('main') ? document.querySelector('main').offsetTop : 300;
  let height = 0;

  if (stickyFooter) {
    height = stickyFooter.offsetHeight;
  }

  if (window.pageYOffset >= top && window.innerWidth < 768) {
    if (stickyFooter) {
      stickyFooter.classList.add('fixed');

      if (!document.querySelector('.sticky-content-wrapper')) {
        let newNode = document.createElement("div");
        newNode.className = "sticky-content-wrapper";
        stickyFooter.parentNode.insertBefore(newNode, stickyFooter);
        document.querySelector('.sticky-content-wrapper').insertAdjacentElement('beforeend', stickyFooter);
      }

      document.querySelector('.sticky-content-wrapper').setAttribute("style", "height: " + height + "px");
    }
  } else {
    if (stickyFooter) {
      stickyFooter.classList.remove('fixed');
    }

    if (document.querySelector('.sticky-content-wrapper')) {
      document.querySelector('.sticky-content-wrapper').removeAttribute("style");
    }
  }

  if (window.innerWidth > 768 && document.querySelector('.sticky-content-wrapper')) {
    document.querySelector('.sticky-content-wrapper').style.height = 'auto';
  }
};
/**
 * utils to make background parallax
 */

const parallaxHandler = function () {
  let parallaxItems = document.querySelectorAll('.parallax');

  if (parallaxItems) {
    for (let i = 0; i < parallaxItems.length; i++) {
      // calculate background y Position;
      let parallax = parallaxItems[i],
          yPos,
          parallaxSpeed = 1;

      if (parallax.getAttribute('data-option')) {
        parallaxSpeed = parseInt(parseOptions(parallax.getAttribute('data-option')).speed);
      }

      yPos = (parallax.offsetTop - window.pageYOffset) * 50 * parallaxSpeed / parallax.offsetTop + 50;
      parallax.style.backgroundPosition = "50% " + yPos + "%";
    }
  }
};
/**
 * utils to show scrollTop button
 */

const showScrollTopHandler = function () {
  let scrollTop = document.querySelector(".scroll-top");

  if (window.pageYOffset >= 768) {
    scrollTop.classList.add("show");
  } else {
    scrollTop.classList.remove("show");
  }
};
/**
 * utils to scroll to top
 */

function scrollTopHandler(isCustom = true, speed = 15) {
  let offsetTop = 0;

  if (isCustom && !isEdgeBrowser()) {
    if (document.querySelector('.main .container > .row')) {
      offsetTop = document.querySelector('.main .container > .row').getBoundingClientRect().top + window.pageYOffset - document.querySelector('.sticky-header').offsetHeight + 2;
    }
  } else {
    offsetTop = 0;
  }

  if (isSafariBrowser() || isEdgeBrowser()) {
    let pos = window.pageYOffset;
    let timerId = setInterval(() => {
      if (pos <= offsetTop) clearInterval(timerId);
      window.scrollBy(0, -speed);
      pos -= speed;
    }, 1);
  } else {
    window.scrollTo({
      top: offsetTop,
      behavior: 'smooth'
    });
  }
}
/**
 * utils to play and pause video
 */

const videoHandler = e => {
  e.stopPropagation();
  e.preventDefault();

  if (e.currentTarget.closest('.post-video')) {
    let video = e.currentTarget.closest('.post-video');

    if (video.classList.contains('playing')) {
      video.classList.remove('playing');
      video.classList.add('paused');
      video.querySelector('video').pause();
    } else {
      video.classList.add('playing');
      video.querySelector('video').play();
    }

    video.querySelector('video').addEventListener('ended', function () {
      video.classList.remove('playing');
      video.classList.remove('paused');
    });
  }
};
/**
 * utils to get total Price of products in cart.
 */

const getTotalPrice = cartItems => {
  let total = 0;

  if (cartItems) {
    for (let i = 0; i < cartItems.length; i++) {
      total += cartItems[i].price * parseInt(cartItems[i].qty, 10);
    }
  }

  return total;
};
/**
 * utils to get number of products in cart
 */

const getCartCount = cartItems => {
  let total = 0;

  for (let i = 0; i < cartItems.length; i++) {
    total += parseInt(cartItems[i].qty, 10);
  }

  return total;
};
/**
 * utils to show number to n places of decimals
 */

const toDecimal = (price, fixedCount = 2) => {
  return price.toLocaleString(undefined, {
    minimumFractionDigits: fixedCount,
    maximumFractionDigits: fixedCount
  });
};

/***/ }),

/***/ "?ca47":
/*!******************************************!*\
  !*** ./utils/resolve-rewrites (ignored) ***!
  \******************************************/
/***/ (function() {

/* (ignored) */

/***/ })

};
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvY29tbW9uL0FwaS5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvZmVhdHVyZXMvY3VzdG9tLWxpbmsuanN4Iiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vY29tcG9uZW50cy9mZWF0dXJlcy9vd2wtY2Fyb3VzZWwuanN4Iiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vY29tcG9uZW50cy9mZWF0dXJlcy9wcm9kdWN0L2NvbW1vbi9jYXJ0LXBvcHVwLmpzeCIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3NlcnZlci9hcG9sbG8uanMiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9zZXJ2ZXIvcXVlcmllcy5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3N0b3JlL2NhcnQuanMiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9zdG9yZS9tb2RhbC5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3N0b3JlL3dpc2hsaXN0LmpzIiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vdXRpbHMvZGF0YS9jYXJvdXNlbC5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3V0aWxzL2luZGV4LmpzIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2lnbm9yZWR8RDpcXGRzcXVhcmVcXG5vZGVfbW9kdWxlc1xcbmV4dFxcZGlzdFxcbmV4dC1zZXJ2ZXJcXGxpYlxccm91dGVyfC4vdXRpbHMvcmVzb2x2ZS1yZXdyaXRlcyJdLCJuYW1lcyI6WyJBUElfQkFTRV9VUkwiLCJnZXRBcGlVcmwiLCJlbmRwb2ludCIsIlNJR05VUF9BUEkiLCJTRU5ET1RQX0FQSSIsIlZFUklGWV9BUEkiLCJBRERfQUREUkVTU19BUEkiLCJHRVRfQUREUkVTU19BUEkiLCJERUxFVEVfQUREUkVTU19BUEkiLCJQUk9EVUNUX0NBVEVHT1JZX0FQSSIsIkFMaW5rIiwiY2hpbGRyZW4iLCJjbGFzc05hbWUiLCJjb250ZW50Iiwic3R5bGUiLCJwcm9wcyIsInByZXZlbnREZWZhdWx0IiwiZSIsImhyZWYiLCJvbkNsaWNrIiwicGFyc2VDb250ZW50IiwiT3dsQ2Fyb3VzZWwiLCJhZENsYXNzIiwib3B0aW9ucyIsImNhcm91c2VsUmVmIiwidXNlUmVmIiwiZGVmYXVsdE9wdGlvbnMiLCJpdGVtcyIsImxvb3AiLCJtYXJnaW4iLCJyZXNwb25zaXZlQ2xhc3MiLCJuYXYiLCJuYXZUZXh0IiwibmF2RWxlbWVudCIsImRvdHMiLCJzbWFydFNwZWVkIiwiYXV0b3BsYXkiLCJhdXRvSGVpZ2h0IiwidXNlRWZmZWN0Iiwib25DaGFuZ2VSZWYiLCJldmVudHMiLCJvblRyYW5zbGF0ZWQiLCJ0YXJnZXQiLCJvbkNoYW5nZUluZGV4IiwiaXRlbSIsImluZGV4IiwiT2JqZWN0IiwiYXNzaWduIiwic2V0dGluZ3MiLCJsZW5ndGgiLCJ1bmRlZmluZWQiLCJSZWFjdCIsIkNhcnRQb3B1cCIsInByb2R1Y3QiLCJ0b3AiLCJzbHVnIiwicHJvY2VzcyIsInBpY3R1cmVzIiwidXJsIiwibmFtZSIsInF0eSIsInRvRGVjaW1hbCIsInByaWNlIiwiQVBJX1VSSSIsImFwb2xsb0NsaWVudCIsIkFwb2xsb0NsaWVudCIsInVyaSIsImNhY2hlIiwiSW5NZW1vcnlDYWNoZSIsIndpdGhBcG9sbG8iLCJjdXJyZW50RGVtbyIsIlBST0RVQ1RfU0lNUExFIiwiZ3FsIiwiUFJPRFVDVF9TTUFMTCIsIkdFVF9QUk9EVUNUUyIsIkdFVF9TUEVDSUFMX1BST0RVQ1RTIiwiR0VUX1BST0RVQ1QiLCJHRVRfVklERU8iLCJHRVRfU0hPUF9TSURFQkFSX0RBVEEiLCJHRVRfUE9TVFMiLCJHRVRfUE9TVCIsIkdFVF9QT1NUX1NJREVCQVJfREFUQSIsIkdFVF9IT01FX0RBVEEiLCJhY3Rpb25UeXBlcyIsIkFERF9UT19DQVJUIiwiUkVNT1ZFX0ZST01fQ0FSVCIsIlVQREFURV9DQVJUIiwiUkVGUkVTSF9TVE9SRSIsImluaXRpYWxTdGF0ZSIsImRhdGEiLCJjYXJ0UmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsInRtcFByb2R1Y3QiLCJwYXlsb2FkIiwiZmluZEluZGV4IiwidG1wRGF0YSIsInJlZHVjZSIsImFjYyIsImN1ciIsInB1c2giLCJwYXJzZUludCIsImNhcnQiLCJjYXJ0QWNjIiwicHJvZHVjdHMiLCJjYXJ0QWN0aW9ucyIsImFkZFRvQ2FydCIsInJlbW92ZUZyb21DYXJ0IiwidXBkYXRlQ2FydCIsImNhcnRTYWdhIiwidGFrZUV2ZXJ5Iiwic2FnYSIsInRvYXN0IiwicGVyc2lzdENvbmZpZyIsImtleVByZWZpeCIsImtleSIsInN0b3JhZ2UiLCJwZXJzaXN0UmVkdWNlciIsIk9QRU5fTU9EQUwiLCJDTE9TRV9NT0RBTCIsIk9QRU5fUVVJQ0tWSUVXIiwiQ0xPU0VfUVVJQ0tWSUVXIiwib3Blbk1vZGFsIiwicXVpY2t2aWV3Iiwic2luZ2xlU2x1ZyIsIm1vZGFsUmVkdWNlciIsIm1vZGFsQWN0aW9ucyIsImNsb3NlTW9kYWwiLCJtb2RhbFR5cGUiLCJvcGVuUXVpY2t2aWV3IiwiY2xvc2VRdWlja3ZpZXciLCJUT0dHTEVfV0lTSExJU1QiLCJSRU1PVkVfRlJPTV9XSVNITElTVCIsIndpc2hsaXN0UmVkdWNlciIsInNwbGljZSIsIndpc2hsaXN0Iiwid2lzaGxpc3RBY2MiLCJ3aXNobGlzdEFjdGlvbnMiLCJ0b2dnbGVXaXNobGlzdCIsInJlbW92ZUZyb21XaXNobGlzdCIsIm1haW5TbGlkZXIxIiwicmVzcG9uc2l2ZSIsIm1haW5TbGlkZXIyIiwibWFpblNsaWRlcjMiLCJtYWluU2xpZGVyNCIsImF1dG9QbGF5IiwibWFpblNsaWRlcjUiLCJtYWluU2xpZGVyNiIsIm1haW5TbGlkZXI3IiwibWFpblNsaWRlcjgiLCJtYWluU2xpZGVyOSIsIm1haW5TbGlkZXIxMCIsIm1haW5TbGlkZXIxMSIsIm1haW5TbGlkZXIxMiIsIm1haW5TbGlkZXIxMyIsIm1haW5TbGlkZXIxNCIsIm1haW5TbGlkZXIxNSIsIm1haW5TbGlkZXIxNiIsImF1dG9wbGF5VGltZW91dCIsIm1haW5TbGlkZXIxNyIsIm1haW5TbGlkZXIxOCIsIm1haW5TbGlkZXIxOSIsIm1haW5TbGlkZXIyMCIsImludHJvU2xpZGVyIiwic2VydmljZVNsaWRlciIsImJyYW5kU2xpZGVyIiwicHJvZHVjdFNsaWRlciIsInByb2R1Y3RTbGlkZXIyIiwicGFyc2VPcHRpb25zIiwiSlNPTiIsInBhcnNlIiwicmVwbGFjZSIsImlzSUVCcm93c2VyIiwic1VzckFnIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwiaW5kZXhPZiIsImlzU2FmYXJpQnJvd3NlciIsImlzRWRnZUJyb3dzZXIiLCJhcnJheSIsImNiIiwiaSIsImZpbmRBcnJheUluZGV4Iiwic2VhcmNoQXJyYXkiLCJodG1sIiwiU0NSSVBUX1JFR0VYIiwidGVzdCIsIl9faHRtbCIsInN0aWNreUhlYWRlckhhbmRsZXIiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJvZmZzZXRUb3AiLCJzdGlja3lIZWFkZXIiLCJoZWlnaHQiLCJvZmZzZXRIZWlnaHQiLCJ3aW5kb3ciLCJwYWdlWU9mZnNldCIsImlubmVyV2lkdGgiLCJjbGFzc0xpc3QiLCJhZGQiLCJuZXdOb2RlIiwiY3JlYXRlRWxlbWVudCIsInBhcmVudE5vZGUiLCJpbnNlcnRCZWZvcmUiLCJpbnNlcnRBZGphY2VudEVsZW1lbnQiLCJzZXRBdHRyaWJ1dGUiLCJnZXRBdHRyaWJ1dGUiLCJyZW1vdmUiLCJyZW1vdmVBdHRyaWJ1dGUiLCJvdXRlcldpZHRoIiwiY29udGFpbnMiLCJyZXNpemVIYW5kbGVyIiwid2lkdGgiLCJhdHRyaSIsImJvZHlDbGFzc2VzIiwidmFsdWUiLCJzcGxpdCIsImZpbHRlciIsInN0aWNreUZvb3RlckhhbmRsZXIiLCJzdGlja3lGb290ZXIiLCJwYXJhbGxheEhhbmRsZXIiLCJwYXJhbGxheEl0ZW1zIiwicXVlcnlTZWxlY3RvckFsbCIsInBhcmFsbGF4IiwieVBvcyIsInBhcmFsbGF4U3BlZWQiLCJzcGVlZCIsImJhY2tncm91bmRQb3NpdGlvbiIsInNob3dTY3JvbGxUb3BIYW5kbGVyIiwic2Nyb2xsVG9wIiwic2Nyb2xsVG9wSGFuZGxlciIsImlzQ3VzdG9tIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwicG9zIiwidGltZXJJZCIsInNldEludGVydmFsIiwiY2xlYXJJbnRlcnZhbCIsInNjcm9sbEJ5Iiwic2Nyb2xsVG8iLCJiZWhhdmlvciIsInZpZGVvSGFuZGxlciIsInN0b3BQcm9wYWdhdGlvbiIsImN1cnJlbnRUYXJnZXQiLCJjbG9zZXN0IiwidmlkZW8iLCJwYXVzZSIsInBsYXkiLCJhZGRFdmVudExpc3RlbmVyIiwiZ2V0VG90YWxQcmljZSIsImNhcnRJdGVtcyIsInRvdGFsIiwiZ2V0Q2FydENvdW50IiwiZml4ZWRDb3VudCIsInRvTG9jYWxlU3RyaW5nIiwibWluaW11bUZyYWN0aW9uRGlnaXRzIiwibWF4aW11bUZyYWN0aW9uRGlnaXRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFTyxNQUFNQSxZQUFZLEdBQUcsb0NBQXJCO0FBQ0EsTUFBTUMsU0FBUyxHQUFJQyxRQUFELElBQWNGLFlBQVksR0FBR0UsUUFBL0M7QUFDQSxNQUFNQyxVQUFVLEdBQUdGLFNBQVMsQ0FBQyxnQkFBRCxDQUE1QjtBQUNBLE1BQU1HLFdBQVcsR0FBR0gsU0FBUyxDQUFDLGNBQUQsQ0FBN0I7QUFDQSxNQUFNSSxVQUFVLEdBQUdKLFNBQVMsQ0FBQyxZQUFELENBQTVCO0FBQ0EsTUFBTUssZUFBZSxHQUFHTCxTQUFTLENBQUMsYUFBRCxDQUFqQztBQUNBLE1BQU1NLGVBQWUsR0FBR04sU0FBUyxDQUFDLGVBQUQsQ0FBakM7QUFDQSxNQUFNTyxrQkFBa0IsR0FBR1AsU0FBUyxDQUFDLGtCQUFELENBQXBDO0FBQ0EsTUFBTVEsb0JBQW9CLEdBQUdSLFNBQVMsQ0FBQyxvQkFBRCxDQUF0QyxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNWUDtBQUVBO0FBRWUsU0FBU1MsS0FBVCxPQUFxRTtBQUFBLE1BQXBEO0FBQUVDLFlBQUY7QUFBWUMsYUFBWjtBQUF1QkMsV0FBdkI7QUFBZ0NDO0FBQWhDLEdBQW9EO0FBQUEsTUFBVkMsS0FBVTs7QUFFaEYsUUFBTUMsY0FBYyxHQUFLQyxDQUFGLElBQVM7QUFDNUIsUUFBS0YsS0FBSyxDQUFDRyxJQUFOLEtBQWUsR0FBcEIsRUFBMEI7QUFDdEJELE9BQUMsQ0FBQ0QsY0FBRjtBQUNIOztBQUVELFFBQUtELEtBQUssQ0FBQ0ksT0FBWCxFQUFxQjtBQUNqQkYsT0FBQyxDQUFDRCxjQUFGO0FBQ0FELFdBQUssQ0FBQ0ksT0FBTjtBQUNIO0FBQ0osR0FURDs7QUFXQSxTQUNJTixPQUFPLGdCQUNILDhEQUFDLGtEQUFELGtDQUFXRSxLQUFYO0FBQUEsMkJBQ0k7QUFBRyxlQUFTLEVBQUdILFNBQWY7QUFBMkIsV0FBSyxFQUFHRSxLQUFuQztBQUEyQyxhQUFPLEVBQUdFLGNBQXJEO0FBQXNFLDZCQUF1QixFQUFHSSxvREFBWSxDQUFFUCxPQUFGLENBQTVHO0FBQUEsZ0JBQ01GO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERyxnQkFNSCw4REFBQyxrREFBRCxrQ0FBV0ksS0FBWDtBQUFBLDJCQUNJO0FBQUcsZUFBUyxFQUFHSCxTQUFmO0FBQTJCLFdBQUssRUFBR0UsS0FBbkM7QUFBMkMsYUFBTyxFQUFHRSxjQUFyRDtBQUFBLGdCQUNNTDtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBUFI7QUFhSCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlCRDtDQUdBOztBQUNBLFNBQVNVLFdBQVQsQ0FBdUJOLEtBQXZCLEVBQStCO0FBQzNCLFFBQU07QUFBRU8sV0FBRjtBQUFXQztBQUFYLE1BQXVCUixLQUE3QjtBQUNBLFFBQU1TLFdBQVcsR0FBR0MsNkNBQU0sQ0FBRSxJQUFGLENBQTFCO0FBQ0EsUUFBTUMsY0FBYyxHQUFHO0FBQ25CQyxTQUFLLEVBQUUsQ0FEWTtBQUVuQkMsUUFBSSxFQUFFLEtBRmE7QUFHbkJDLFVBQU0sRUFBRSxDQUhXO0FBSW5CQyxtQkFBZSxFQUFFLE1BSkU7QUFLbkJDLE9BQUcsRUFBRSxJQUxjO0FBTW5CQyxXQUFPLEVBQUUsQ0FBRSwrQkFBRixFQUFtQyxnQ0FBbkMsQ0FOVTtBQU9uQkMsY0FBVSxFQUFFLFFBUE87QUFRbkJDLFFBQUksRUFBRSxJQVJhO0FBU25CQyxjQUFVLEVBQUUsR0FUTztBQVVuQkMsWUFBUSxFQUFFLEtBVlM7QUFXbkJDLGNBQVUsRUFBRSxLQVhPLENBWW5COztBQVptQixHQUF2QjtBQWVBQyxrREFBUyxDQUFFLE1BQU07QUFDYixRQUFLdkIsS0FBSyxDQUFDd0IsV0FBWCxFQUF5QjtBQUNyQnhCLFdBQUssQ0FBQ3dCLFdBQU4sQ0FBbUJmLFdBQW5CO0FBQ0g7QUFDSixHQUpRLEVBSU4sQ0FBRUEsV0FBRixDQUpNLENBQVQ7QUFNQSxNQUFJZ0IsTUFBTSxHQUFHO0FBQ1RDLGdCQUFZLEVBQUUsVUFBV3hCLENBQVgsRUFBZTtBQUN6QixVQUFLLENBQUNBLENBQUMsQ0FBQ3lCLE1BQVIsRUFBaUI7O0FBQ2pCLFVBQUszQixLQUFLLENBQUM0QixhQUFYLEVBQTJCO0FBQ3ZCNUIsYUFBSyxDQUFDNEIsYUFBTixDQUFxQjFCLENBQUMsQ0FBQzJCLElBQUYsQ0FBT0MsS0FBNUI7QUFDSDtBQUNKO0FBTlEsR0FBYjtBQVNBTCxRQUFNLEdBQUdNLE1BQU0sQ0FBQ0MsTUFBUCxDQUFlLEVBQWYsRUFBbUJQLE1BQW5CLEVBQTJCekIsS0FBSyxDQUFDeUIsTUFBakMsQ0FBVDtBQUNBLE1BQUlRLFFBQVEsR0FBR0YsTUFBTSxDQUFDQyxNQUFQLENBQWUsRUFBZixFQUFtQnJCLGNBQW5CLEVBQW1DSCxPQUFuQyxDQUFmO0FBRUEsU0FDSVIsS0FBSyxDQUFDSixRQUFOLEdBQ0lJLEtBQUssQ0FBQ0osUUFBTixDQUFlc0MsTUFBZixHQUF3QixDQUF4QixJQUErQmxDLEtBQUssQ0FBQ0osUUFBTixJQUFrQkksS0FBSyxDQUFDSixRQUFOLENBQWVzQyxNQUFmLEtBQTBCQyxTQUEzRSxnQkFDSSw4REFBQyw0REFBRDtBQUFVLE9BQUcsRUFBRzFCLFdBQWhCO0FBQThCLGFBQVMsRUFBSSxnQkFBZ0JGLE9BQVMsRUFBcEU7QUFBd0UsV0FBTyxFQUFHMEIsUUFBbEY7QUFBNkYsVUFBTSxFQUFHUixNQUF0RztBQUFBLGNBQ016QixLQUFLLENBQUNKO0FBRFo7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKLEdBSU0sRUFMVixHQU1NLEVBUFY7QUFTSDs7QUFFRCw0RUFBZXdDLGlEQUFBLENBQVk5QixXQUFaLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuREE7QUFFQTtBQUVBO0FBRWUsU0FBUytCLFNBQVQsQ0FBcUJyQyxLQUFyQixFQUE2QjtBQUN4QyxRQUFNO0FBQUVzQztBQUFGLE1BQWN0QyxLQUFwQjtBQUVBLHNCQUNJO0FBQUssYUFBUyxFQUFDLGdCQUFmO0FBQUEsMkJBQ0k7QUFBSyxlQUFTLEVBQUMsb0JBQWY7QUFBb0MsV0FBSyxFQUFHO0FBQUV1QyxXQUFHLEVBQUU7QUFBUCxPQUE1QztBQUFBLDhCQUNJO0FBQUcsaUJBQVMsRUFBQyxpQkFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBR0k7QUFBSyxpQkFBUyxFQUFDLDhDQUFmO0FBQUEsZ0NBQ0k7QUFBUSxtQkFBUyxFQUFDLDBCQUFsQjtBQUFBLGlDQUNJLDhEQUFDLHFFQUFEO0FBQU8sZ0JBQUksRUFBSSxvQkFBb0JELE9BQU8sQ0FBQ0UsSUFBTSxFQUFqRDtBQUFBLG1DQUNJO0FBQ0ksaUJBQUcsRUFBR0MsdUJBQUEsR0FBb0NILE9BQU8sQ0FBQ0ksUUFBUixDQUFrQixDQUFsQixFQUFzQkMsR0FEcEU7QUFFSSxpQkFBRyxFQUFDLFNBRlI7QUFHSSxtQkFBSyxFQUFDLElBSFY7QUFJSSxvQkFBTSxFQUFDO0FBSlg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBV0k7QUFBSyxtQkFBUyxFQUFDLGdCQUFmO0FBQUEsa0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFJLG9CQUFvQkwsT0FBTyxDQUFDRSxJQUFNLEVBQWpEO0FBQXFELHFCQUFTLEVBQUMsY0FBL0Q7QUFBQSxzQkFBZ0ZGLE9BQU8sQ0FBQ007QUFBeEY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQUVJO0FBQU0scUJBQVMsRUFBQyxXQUFoQjtBQUFBLG9DQUNJO0FBQU0sdUJBQVMsRUFBQyxrQkFBaEI7QUFBQSx3QkFBcUNOLE9BQU8sQ0FBQ087QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESixlQUVJO0FBQU0sdUJBQVMsRUFBQyxlQUFoQjtBQUFBLDhCQUFtQ0MsaURBQVMsQ0FBRVIsT0FBTyxDQUFDUyxLQUFWLENBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUhKLGVBdUJJO0FBQUssaUJBQVMsRUFBQyxxQkFBZjtBQUFBLGdDQUNJLDhEQUFDLHFFQUFEO0FBQU8sY0FBSSxFQUFDLGFBQVo7QUFBMEIsbUJBQVMsRUFBQyxnREFBcEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFFSSw4REFBQyxxRUFBRDtBQUFPLGNBQUksRUFBQyxpQkFBWjtBQUE4QixtQkFBUyxFQUFDLG9DQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0F2Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBZ0NILEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6Q0Q7QUFDQTtBQUVBLE1BQU1DLE9BQU8sR0FBSSxHQUFHUCx1QkFBb0MsVUFBeEQ7QUFFQSxNQUFNUSxZQUFZLEdBQUcsSUFBSUMscURBQUosQ0FBa0I7QUFDbkNDLEtBQUcsRUFBRUgsT0FEOEI7QUFFbkNJLE9BQUssRUFBRSxJQUFJQyx1REFBSjtBQUY0QixDQUFsQixDQUFyQjtBQUtBLCtEQUFlQyx1REFBVSxDQUFFTCxZQUFGLENBQXpCLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVkE7QUFFTyxNQUFNTSxXQUFXLEdBQUksS0FBckI7QUFFUCxNQUFNQyxjQUFjLEdBQUdDLG9EQUFJO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FoQ0E7QUFrQ0EsTUFBTUMsYUFBYSxHQUFHRCxvREFBSTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQXRCQTtBQXdCTyxNQUFNRSxZQUFZLEdBQUdGLG9EQUFJO0FBQ2hDO0FBQ0EseUJBQXlCRixXQUFhO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFPQyxjQUFnQjtBQUN2QixDQVpPO0FBY0EsTUFBTUksb0JBQW9CLEdBQUdILG9EQUFJO0FBQ3hDO0FBQ0EsZ0NBQWdDRixXQUFhO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFPRyxhQUFlO0FBQ3RCLENBbEJPO0FBb0JBLE1BQU1HLFdBQVcsR0FBR0osb0RBQUk7QUFDL0I7QUFDQSx3QkFBd0JGLFdBQWE7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFPQyxjQUFnQjtBQUN2QixDQTVGTztBQThGQSxNQUFNTSxTQUFTLEdBQUdMLG9EQUFJO0FBQzdCO0FBQ0Esc0JBQXNCRixXQUFhO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FWTztBQVlBLE1BQU1RLHFCQUFxQixHQUFHTixvREFBSTtBQUN6QztBQUNBLGdDQUFnQ0YsV0FBYTtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBNUJPO0FBOEJBLE1BQU1TLFNBQVMsR0FBR1Asb0RBQUk7QUFDN0I7QUFDQSxzQkFBc0JGLFdBQWE7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBbkNPO0FBcUNBLE1BQU1VLFFBQVEsR0FBR1Isb0RBQUk7QUFDNUI7QUFDQSxxQkFBcUJGLFdBQWE7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FuRE87QUFxREEsTUFBTVcscUJBQXFCLEdBQUdULG9EQUFJO0FBQ3pDO0FBQ0EsZ0NBQWdDRixXQUFhO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FuQk87QUFxQkEsTUFBTVksYUFBYSxHQUFHVixvREFBSTtBQUNqQztBQUNBLGdDQUFnQ0YsV0FBYTtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQkEsV0FBYTtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU9DLGNBQWdCO0FBQ3ZCLENBOUNPLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZWUDtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUEsTUFBTVksV0FBVyxHQUFHO0FBQ2hCQyxhQUFXLEVBQUUsYUFERztBQUVoQkMsa0JBQWdCLEVBQUUsa0JBRkY7QUFHaEJDLGFBQVcsRUFBRSxhQUhHO0FBSWhCQyxlQUFhLEVBQUU7QUFKQyxDQUFwQjtBQU9BLE1BQU1DLFlBQVksR0FBRztBQUNqQkMsTUFBSSxFQUFFO0FBRFcsQ0FBckI7O0FBSUEsU0FBU0MsV0FBVCxDQUFzQkMsS0FBSyxHQUFHSCxZQUE5QixFQUE0Q0ksTUFBNUMsRUFBcUQ7QUFDakQsVUFBU0EsTUFBTSxDQUFDQyxJQUFoQjtBQUNJLFNBQUtWLFdBQVcsQ0FBQ0MsV0FBakI7QUFDSSxVQUFJVSxVQUFVLHFCQUFRRixNQUFNLENBQUNHLE9BQVAsQ0FBZTFDLE9BQXZCLENBQWQ7O0FBRUEsVUFBS3NDLEtBQUssQ0FBQ0YsSUFBTixDQUFXTyxTQUFYLENBQXNCcEQsSUFBSSxJQUFJQSxJQUFJLENBQUNlLElBQUwsS0FBY2lDLE1BQU0sQ0FBQ0csT0FBUCxDQUFlMUMsT0FBZixDQUF1Qk0sSUFBbkUsSUFBNEUsQ0FBQyxDQUFsRixFQUFzRjtBQUNsRixZQUFJc0MsT0FBTyxHQUFHTixLQUFLLENBQUNGLElBQU4sQ0FBV1MsTUFBWCxDQUFtQixDQUFFQyxHQUFGLEVBQU9DLEdBQVAsS0FBZ0I7QUFDN0MsY0FBS0EsR0FBRyxDQUFDekMsSUFBSixLQUFhbUMsVUFBVSxDQUFDbkMsSUFBN0IsRUFBb0M7QUFDaEN3QyxlQUFHLENBQUNFLElBQUosaUNBQ09ELEdBRFA7QUFFSXhDLGlCQUFHLEVBQUUwQyxRQUFRLENBQUVGLEdBQUcsQ0FBQ3hDLEdBQU4sQ0FBUixHQUFzQjBDLFFBQVEsQ0FBRVIsVUFBVSxDQUFDbEMsR0FBYjtBQUZ2QztBQUlILFdBTEQsTUFLTztBQUNIdUMsZUFBRyxDQUFDRSxJQUFKLENBQVVELEdBQVY7QUFDSDs7QUFFRCxpQkFBT0QsR0FBUDtBQUNILFNBWGEsRUFXWCxFQVhXLENBQWQ7QUFhQSwrQ0FBWVIsS0FBWjtBQUFtQkYsY0FBSSxFQUFFUTtBQUF6QjtBQUNILE9BZkQsTUFlTztBQUNILCtDQUFZTixLQUFaO0FBQW1CRixjQUFJLEVBQUUsQ0FBRSxHQUFHRSxLQUFLLENBQUNGLElBQVgsRUFBaUJLLFVBQWpCO0FBQXpCO0FBQ0g7O0FBRUwsU0FBS1gsV0FBVyxDQUFDRSxnQkFBakI7QUFDSSxVQUFJa0IsSUFBSSxHQUFHWixLQUFLLENBQUNGLElBQU4sQ0FBV1MsTUFBWCxDQUFtQixDQUFFTSxPQUFGLEVBQVduRCxPQUFYLEtBQXdCO0FBQ2xELFlBQUtBLE9BQU8sQ0FBQ00sSUFBUixLQUFpQmlDLE1BQU0sQ0FBQ0csT0FBUCxDQUFlMUMsT0FBZixDQUF1Qk0sSUFBN0MsRUFBb0Q7QUFDaEQ2QyxpQkFBTyxDQUFDSCxJQUFSLENBQWNoRCxPQUFkO0FBQ0g7O0FBQ0QsZUFBT21ELE9BQVA7QUFDSCxPQUxVLEVBS1IsRUFMUSxDQUFYO0FBT0EsNkNBQVliLEtBQVo7QUFBbUJGLFlBQUksRUFBRWM7QUFBekI7O0FBRUosU0FBS3BCLFdBQVcsQ0FBQ0csV0FBakI7QUFDSSw2Q0FBWUssS0FBWjtBQUFtQkYsWUFBSSxFQUFFRyxNQUFNLENBQUNHLE9BQVAsQ0FBZVU7QUFBeEM7O0FBRUosU0FBS3RCLFdBQVcsQ0FBQ0ksYUFBakI7QUFDSSxhQUFPQyxZQUFQOztBQUVKO0FBQ0ksYUFBT0csS0FBUDtBQXhDUjtBQTBDSDs7QUFFTSxNQUFNZSxXQUFXLEdBQUc7QUFDdkJDLFdBQVMsRUFBRXRELE9BQU8sS0FBTTtBQUFFd0MsUUFBSSxFQUFFVixXQUFXLENBQUNDLFdBQXBCO0FBQWlDVyxXQUFPLEVBQUU7QUFBRTFDO0FBQUY7QUFBMUMsR0FBTixDQURLO0FBRXZCdUQsZ0JBQWMsRUFBRXZELE9BQU8sS0FBTTtBQUFFd0MsUUFBSSxFQUFFVixXQUFXLENBQUNFLGdCQUFwQjtBQUFzQ1UsV0FBTyxFQUFFO0FBQUUxQztBQUFGO0FBQS9DLEdBQU4sQ0FGQTtBQUd2QndELFlBQVUsRUFBRUosUUFBUSxLQUFNO0FBQUVaLFFBQUksRUFBRVYsV0FBVyxDQUFDRyxXQUFwQjtBQUFpQ1MsV0FBTyxFQUFFO0FBQUVVO0FBQUY7QUFBMUMsR0FBTjtBQUhHLENBQXBCO0FBTUEsVUFBVUssUUFBVixHQUFxQjtBQUN4QixRQUFNQyw2REFBUyxDQUFFNUIsV0FBVyxDQUFDQyxXQUFkLEVBQTJCLFVBQVU0QixJQUFWLENBQWdCL0YsQ0FBaEIsRUFBb0I7QUFDMURnRyx5REFBSyxlQUFFLDhEQUFDLG1GQUFEO0FBQVcsYUFBTyxFQUFHaEcsQ0FBQyxDQUFDOEUsT0FBRixDQUFVMUM7QUFBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUFGLENBQUw7QUFDSCxHQUZjLENBQWY7QUFHSDtBQUVELE1BQU02RCxhQUFhLEdBQUc7QUFDbEJDLFdBQVMsRUFBRSxRQURPO0FBRWxCQyxLQUFHLEVBQUUsTUFGYTtBQUdsQkMsU0FBT0E7QUFIVyxDQUF0QjtBQU1BLCtEQUFlQyw2REFBYyxDQUFFSixhQUFGLEVBQWlCeEIsV0FBakIsQ0FBN0IsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pGQTtBQUNBO0FBRUEsTUFBTVAsV0FBVyxHQUFHO0FBQ2hCb0MsWUFBVSxFQUFFLFlBREk7QUFFaEJDLGFBQVcsRUFBRSxhQUZHO0FBR2hCQyxnQkFBYyxFQUFFLGdCQUhBO0FBSWhCQyxpQkFBZSxFQUFFLGlCQUpEO0FBS2hCbkMsZUFBYSxFQUFFO0FBTEMsQ0FBcEI7QUFRQSxNQUFNQyxZQUFZLEdBQUc7QUFDakJLLE1BQUksRUFBRSxPQURXO0FBRWpCOEIsV0FBUyxFQUFFLEtBRk07QUFHakJDLFdBQVMsRUFBRSxLQUhNO0FBSWpCQyxZQUFVLEVBQUU7QUFKSyxDQUFyQjs7QUFPQSxTQUFTQyxZQUFULENBQXVCbkMsS0FBSyxHQUFHSCxZQUEvQixFQUE2Q0ksTUFBN0MsRUFBc0Q7QUFDbEQsVUFBU0EsTUFBTSxDQUFDQyxJQUFoQjtBQUNJLFNBQUtWLFdBQVcsQ0FBQ3NDLGNBQWpCO0FBQ0ksNkNBQ085QixLQURQO0FBRUlpQyxpQkFBUyxFQUFFLElBRmY7QUFHSUMsa0JBQVUsRUFBRWpDLE1BQU0sQ0FBQ0csT0FBUCxDQUFleEM7QUFIL0I7O0FBTUosU0FBSzRCLFdBQVcsQ0FBQ3VDLGVBQWpCO0FBQ0ksNkNBQ08vQixLQURQO0FBRUlpQyxpQkFBUyxFQUFFO0FBRmY7O0FBS0osU0FBS3pDLFdBQVcsQ0FBQ29DLFVBQWpCO0FBQ0ksNkNBQ081QixLQURQO0FBRUlrQyxrQkFBVSxFQUFFakMsTUFBTSxDQUFDRyxPQUFQLENBQWV4QyxJQUYvQjtBQUdJb0UsaUJBQVMsRUFBRTtBQUhmOztBQU1KLFNBQUt4QyxXQUFXLENBQUNxQyxXQUFqQjtBQUNJLDZDQUNPN0IsS0FEUDtBQUVJZ0MsaUJBQVMsRUFBRTtBQUZmOztBQUtKLFNBQUt4QyxXQUFXLENBQUNJLGFBQWpCO0FBQ0ksYUFBT0MsWUFBUDs7QUFFSjtBQUNJLGFBQU9HLEtBQVA7QUEvQlI7QUFpQ0g7O0FBRU0sTUFBTW9DLFlBQVksR0FBRztBQUN4QkosV0FBUyxFQUFFcEUsSUFBSSxLQUFNO0FBQUVzQyxRQUFJLEVBQUVWLFdBQVcsQ0FBQ29DLFVBQXBCO0FBQWdDeEIsV0FBTyxFQUFFO0FBQUV4QztBQUFGO0FBQXpDLEdBQU4sQ0FEUztBQUV4QnlFLFlBQVUsRUFBRUMsU0FBUyxLQUFNO0FBQUVwQyxRQUFJLEVBQUVWLFdBQVcsQ0FBQ3FDLFdBQXBCO0FBQWlDekIsV0FBTyxFQUFFO0FBQUVrQztBQUFGO0FBQTFDLEdBQU4sQ0FGRztBQUd4QkMsZUFBYSxFQUFFM0UsSUFBSSxLQUFNO0FBQUVzQyxRQUFJLEVBQUVWLFdBQVcsQ0FBQ3NDLGNBQXBCO0FBQW9DMUIsV0FBTyxFQUFFO0FBQUV4QztBQUFGO0FBQTdDLEdBQU4sQ0FISztBQUl4QjRFLGdCQUFjLEVBQUUsT0FBUTtBQUFFdEMsUUFBSSxFQUFFVixXQUFXLENBQUN1QztBQUFwQixHQUFSO0FBSlEsQ0FBckI7QUFPUCxNQUFNUixhQUFhLEdBQUc7QUFDbEJDLFdBQVMsRUFBRSxRQURPO0FBRWxCQyxLQUFHLEVBQUUsT0FGYTtBQUdsQkMsU0FBT0E7QUFIVyxDQUF0QjtBQU1BLCtEQUFlQyw2REFBYyxDQUFFSixhQUFGLEVBQWlCWSxZQUFqQixDQUE3QixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkVBO0FBQ0E7QUFFQSxNQUFNM0MsV0FBVyxHQUFHO0FBQ2hCaUQsaUJBQWUsRUFBRSxpQkFERDtBQUVoQkMsc0JBQW9CLEVBQUUsc0JBRk47QUFHaEI5QyxlQUFhLEVBQUU7QUFIQyxDQUFwQjtBQU1BLE1BQU1DLFlBQVksR0FBRztBQUNqQkMsTUFBSSxFQUFFO0FBRFcsQ0FBckI7O0FBSUEsU0FBUzZDLGVBQVQsQ0FBMEIzQyxLQUFLLEdBQUdILFlBQWxDLEVBQWdESSxNQUFoRCxFQUF5RDtBQUNyRCxVQUFTQSxNQUFNLENBQUNDLElBQWhCO0FBQ0ksU0FBS1YsV0FBVyxDQUFDaUQsZUFBakI7QUFDSSxVQUFJdkYsS0FBSyxHQUFHOEMsS0FBSyxDQUFDRixJQUFOLENBQVdPLFNBQVgsQ0FBc0JwRCxJQUFJLElBQUlBLElBQUksQ0FBQ2UsSUFBTCxLQUFjaUMsTUFBTSxDQUFDRyxPQUFQLENBQWUxQyxPQUFmLENBQXVCTSxJQUFuRSxDQUFaO0FBQ0EsVUFBSXNDLE9BQU8sR0FBRyxDQUFFLEdBQUdOLEtBQUssQ0FBQ0YsSUFBWCxDQUFkOztBQUVBLFVBQUs1QyxLQUFLLEtBQUssQ0FBQyxDQUFoQixFQUFvQjtBQUNoQm9ELGVBQU8sQ0FBQ0ksSUFBUixDQUFjVCxNQUFNLENBQUNHLE9BQVAsQ0FBZTFDLE9BQTdCO0FBQ0gsT0FGRCxNQUVPO0FBQ0g0QyxlQUFPLENBQUNzQyxNQUFSLENBQWdCMUYsS0FBaEI7QUFDSDs7QUFFRCw2Q0FBWThDLEtBQVo7QUFBbUJGLFlBQUksRUFBRVE7QUFBekI7O0FBRUosU0FBS2QsV0FBVyxDQUFDa0Qsb0JBQWpCO0FBQ0ksVUFBSUcsUUFBUSxHQUFHN0MsS0FBSyxDQUFDRixJQUFOLENBQVdTLE1BQVgsQ0FBbUIsQ0FBRXVDLFdBQUYsRUFBZXBGLE9BQWYsS0FBNEI7QUFDMUQsWUFBS0EsT0FBTyxDQUFDTSxJQUFSLEtBQWlCaUMsTUFBTSxDQUFDRyxPQUFQLENBQWUxQyxPQUFmLENBQXVCTSxJQUE3QyxFQUFvRDtBQUNoRDhFLHFCQUFXLENBQUNwQyxJQUFaLENBQWtCaEQsT0FBbEI7QUFDSDs7QUFDRCxlQUFPb0YsV0FBUDtBQUNILE9BTGMsRUFLWixFQUxZLENBQWY7QUFPQSw2Q0FBWTlDLEtBQVo7QUFBbUJGLFlBQUksRUFBRStDO0FBQXpCOztBQUVKLFNBQUtyRCxXQUFXLENBQUNJLGFBQWpCO0FBQ0ksYUFBT0MsWUFBUDs7QUFFSjtBQTFCSjs7QUE0QkEsU0FBT0csS0FBUDtBQUNIOztBQUVNLE1BQU0rQyxlQUFlLEdBQUc7QUFDM0JDLGdCQUFjLEVBQUV0RixPQUFPLEtBQU07QUFBRXdDLFFBQUksRUFBRVYsV0FBVyxDQUFDaUQsZUFBcEI7QUFBcUNyQyxXQUFPLEVBQUU7QUFBRTFDO0FBQUY7QUFBOUMsR0FBTixDQURJO0FBRTNCdUYsb0JBQWtCLEVBQUV2RixPQUFPLEtBQU07QUFBRXdDLFFBQUksRUFBRVYsV0FBVyxDQUFDa0Qsb0JBQXBCO0FBQTBDdEMsV0FBTyxFQUFFO0FBQUUxQztBQUFGO0FBQW5ELEdBQU47QUFGQSxDQUF4QjtBQUtQLE1BQU02RCxhQUFhLEdBQUc7QUFDbEJDLFdBQVMsRUFBRSxRQURPO0FBRWxCQyxLQUFHLEVBQUUsVUFGYTtBQUdsQkMsU0FBT0E7QUFIVyxDQUF0QjtBQU1BLCtEQUFlQyw2REFBYyxDQUFFSixhQUFGLEVBQWlCb0IsZUFBakIsQ0FBN0IsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeERPLE1BQU1PLFdBQVcsR0FBRztBQUN2QmxILE9BQUssRUFBRSxDQURnQjtBQUV2QkksS0FBRyxFQUFFLEtBRmtCO0FBR3ZCSCxNQUFJLEVBQUUsS0FIaUI7QUFJdkJNLE1BQUksRUFBRSxJQUppQjtBQUt2QkwsUUFBTSxFQUFFLENBTGU7QUFNdkJpSCxZQUFVLEVBQUU7QUFDUixPQUFHO0FBQ0NuSCxXQUFLLEVBQUU7QUFEUixLQURLO0FBSVIsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQUpHO0FBT1IsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQVBHO0FBVVIsVUFBTTtBQUNGQSxXQUFLLEVBQUUsQ0FETDtBQUVGTyxVQUFJLEVBQUU7QUFGSjtBQVZFO0FBTlcsQ0FBcEI7QUF1QkEsTUFBTTZHLFdBQVcsR0FBRztBQUN2QnBILE9BQUssRUFBRSxDQURnQjtBQUV2QkMsTUFBSSxFQUFFLEtBRmlCO0FBR3ZCRyxLQUFHLEVBQUUsSUFIa0I7QUFJdkJHLE1BQUksRUFBRSxJQUppQjtBQUt2QjRHLFlBQVUsRUFBRTtBQUNSLE9BQUc7QUFDQ25ILFdBQUssRUFBRTtBQURSLEtBREs7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBSkc7QUFPUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROO0FBUEc7QUFMVyxDQUFwQjtBQWtCQSxNQUFNcUgsV0FBVyxHQUFHO0FBQ3ZCM0csWUFBVSxFQUFFLEtBRFc7QUFFdkJILE1BQUksRUFBRSxLQUZpQjtBQUd2QkgsS0FBRyxFQUFFLElBSGtCLENBSXZCOztBQUp1QixDQUFwQjtBQU9BLE1BQU1rSCxXQUFXLEdBQUc7QUFDdkJsSCxLQUFHLEVBQUUsS0FEa0I7QUFFdkJHLE1BQUksRUFBRSxJQUZpQjtBQUd2QlAsT0FBSyxFQUFFLENBSGdCO0FBSXZCRSxRQUFNLEVBQUUsRUFKZTtBQUt2QkQsTUFBSSxFQUFFLEtBTGlCO0FBTXZCc0gsVUFBUSxFQUFFO0FBTmEsQ0FBcEI7QUFTQSxNQUFNQyxXQUFXLEdBQUc7QUFDdkJwSCxLQUFHLEVBQUUsS0FEa0I7QUFFdkJHLE1BQUksRUFBRSxJQUZpQjtBQUd2QlAsT0FBSyxFQUFFLENBSGdCO0FBSXZCRSxRQUFNLEVBQUUsRUFKZTtBQUt2QkQsTUFBSSxFQUFFLEtBTGlCO0FBTXZCa0gsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FQRztBQVVSLFNBQUs7QUFDREEsV0FBSyxFQUFFLENBRE47QUFFRE8sVUFBSSxFQUFFO0FBRkw7QUFWRztBQU5XLENBQXBCO0FBdUJBLE1BQU1rSCxXQUFXLEdBQUc7QUFDdkJySCxLQUFHLEVBQUUsS0FEa0I7QUFFdkJHLE1BQUksRUFBRSxJQUZpQjtBQUd2QlAsT0FBSyxFQUFFLENBSGdCO0FBSXZCRSxRQUFNLEVBQUUsRUFKZTtBQUt2QkQsTUFBSSxFQUFFLEtBTGlCO0FBTXZCa0gsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFLENBRE47QUFFRE8sVUFBSSxFQUFFO0FBRkw7QUFQRztBQU5XLENBQXBCO0FBb0JBLE1BQU1tSCxXQUFXLEdBQUc7QUFDdkIxSCxPQUFLLEVBQUUsQ0FEZ0I7QUFFdkJJLEtBQUcsRUFBRSxJQUZrQjtBQUd2QkcsTUFBSSxFQUFFLEtBSGlCO0FBSXZCZ0gsVUFBUSxFQUFFLElBSmE7QUFLdkJ0SCxNQUFJLEVBQUUsS0FMaUI7QUFNdkJDLFFBQU0sRUFBRTtBQU5lLENBQXBCO0FBU0EsTUFBTXlILFdBQVcsR0FBRztBQUN2QjNILE9BQUssRUFBRSxDQURnQjtBQUV2QkksS0FBRyxFQUFFLEtBRmtCO0FBR3ZCRyxNQUFJLEVBQUUsS0FIaUI7QUFJdkJMLFFBQU0sRUFBRSxDQUplO0FBS3ZCTyxVQUFRLEVBQUUsSUFMYTtBQU12QjBHLFlBQVUsRUFBRTtBQUNSLE9BQUc7QUFDQ25ILFdBQUssRUFBRTtBQURSLEtBREs7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBSkc7QUFPUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBUEc7QUFVUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBVkc7QUFhUixVQUFNO0FBQ0ZBLFdBQUssRUFBRTtBQURMO0FBYkU7QUFOVyxDQUFwQjtBQXlCQSxNQUFNNEgsV0FBVyxHQUFHO0FBQ3ZCNUgsT0FBSyxFQUFFLENBRGdCO0FBRXZCSSxLQUFHLEVBQUUsS0FGa0I7QUFHdkJHLE1BQUksRUFBRSxJQUhpQjtBQUl2QkwsUUFBTSxFQUFFLEVBSmU7QUFLdkJPLFVBQVEsRUFBRSxJQUxhO0FBTXZCMEcsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FQRztBQVVSLFVBQU07QUFDRkEsV0FBSyxFQUFFO0FBREw7QUFWRTtBQU5XLENBQXBCO0FBc0JBLE1BQU02SCxZQUFZLEdBQUc7QUFDeEI3SCxPQUFLLEVBQUUsQ0FEaUI7QUFFeEJFLFFBQU0sRUFBRSxFQUZnQjtBQUd4QkQsTUFBSSxFQUFFLEtBSGtCO0FBSXhCRyxLQUFHLEVBQUUsS0FKbUI7QUFLeEJHLE1BQUksRUFBRSxJQUxrQjtBQU14QjRHLFlBQVUsRUFBRTtBQUNSLFNBQUs7QUFDRG5ILFdBQUssRUFBRTtBQUROLEtBREc7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROO0FBSkc7QUFOWSxDQUFyQjtBQWdCQSxNQUFNOEgsWUFBWSxHQUFHO0FBQ3hCMUgsS0FBRyxFQUFFLEtBRG1CO0FBRXhCRyxNQUFJLEVBQUUsSUFGa0I7QUFHeEJOLE1BQUksRUFBRSxLQUhrQjtBQUl4QkMsUUFBTSxFQUFFLEVBSmdCO0FBS3hCaUgsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFLENBRE47QUFFRE8sVUFBSSxFQUFFO0FBRkw7QUFQRztBQUxZLENBQXJCO0FBbUJBLE1BQU13SCxZQUFZLEdBQUc7QUFDeEIzSCxLQUFHLEVBQUUsS0FEbUI7QUFFeEJHLE1BQUksRUFBRSxJQUZrQjtBQUd4Qk4sTUFBSSxFQUFFLEtBSGtCO0FBSXhCQyxRQUFNLEVBQUUsRUFKZ0I7QUFLeEJpSCxZQUFVLEVBQUU7QUFDUixPQUFHO0FBQ0NuSCxXQUFLLEVBQUU7QUFEUixLQURLO0FBSVIsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQUpHO0FBT1IsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQVBHO0FBVVIsU0FBSztBQUNEQSxXQUFLLEVBQUUsQ0FETjtBQUVETyxVQUFJLEVBQUU7QUFGTDtBQVZHO0FBTFksQ0FBckI7QUFzQkEsTUFBTXlILFlBQVksR0FBRztBQUN4QmhJLE9BQUssRUFBRSxDQURpQjtBQUV4QkUsUUFBTSxFQUFFLEVBRmdCO0FBR3hCRCxNQUFJLEVBQUUsS0FIa0I7QUFJeEJHLEtBQUcsRUFBRSxLQUptQjtBQUt4QkcsTUFBSSxFQUFFLElBTGtCO0FBTXhCNEcsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFLENBRE47QUFFRE8sVUFBSSxFQUFFO0FBRkw7QUFQRztBQU5ZLENBQXJCO0FBb0JBLE1BQU0wSCxZQUFZLEdBQUc7QUFDeEJqSSxPQUFLLEVBQUUsQ0FEaUI7QUFFeEJJLEtBQUcsRUFBRSxLQUZtQjtBQUd4QkcsTUFBSSxFQUFFLElBSGtCO0FBSXhCTixNQUFJLEVBQUUsS0FKa0I7QUFLeEJDLFFBQU0sRUFBRSxFQUxnQjtBQU14QmlILFlBQVUsRUFBRTtBQUNSLE9BQUc7QUFDQ25ILFdBQUssRUFBRTtBQURSLEtBREs7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRSxDQUROO0FBRURPLFVBQUksRUFBRTtBQUZMO0FBSkc7QUFOWSxDQUFyQjtBQWlCQSxNQUFNMkgsWUFBWSxHQUFHO0FBQ3hCbEksT0FBSyxFQUFFLENBRGlCO0FBRXhCSSxLQUFHLEVBQUUsSUFGbUI7QUFHeEJNLFlBQVUsRUFBRTtBQUhZLENBQXJCO0FBTUEsTUFBTXlILFlBQVksR0FBRztBQUN4Qm5JLE9BQUssRUFBRSxDQURpQjtBQUV4QkksS0FBRyxFQUFFLEtBRm1CO0FBR3hCRyxNQUFJLEVBQUUsS0FIa0I7QUFJeEJMLFFBQU0sRUFBRSxFQUpnQjtBQUt4Qk8sVUFBUSxFQUFFLElBTGM7QUFNeEIySCxpQkFBZSxFQUFFLElBTk87QUFPeEJuSSxNQUFJLEVBQUUsSUFQa0I7QUFReEJrSCxZQUFVLEVBQUU7QUFDUixPQUFHO0FBQ0NuSCxXQUFLLEVBQUU7QUFEUixLQURLO0FBSVIsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQUpHO0FBT1IsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQVBHO0FBVVIsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQVZHO0FBYVIsVUFBTTtBQUNGQSxXQUFLLEVBQUU7QUFETDtBQWJFO0FBUlksQ0FBckI7QUEyQkEsTUFBTXFJLFlBQVksR0FBRztBQUN4QnBJLE1BQUksRUFBRSxLQURrQjtBQUV4QjtBQUNBRyxLQUFHLEVBQUUsS0FIbUI7QUFJeEJGLFFBQU0sRUFBRSxFQUpnQjtBQUt4QmlILFlBQVUsRUFBRTtBQUNSLE9BQUc7QUFDQ25ILFdBQUssRUFBRTtBQURSLEtBREs7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBSkc7QUFPUixTQUFLO0FBQ0RBLFdBQUssRUFBRSxDQUROO0FBRURPLFVBQUksRUFBRSxLQUZMO0FBR0RILFNBQUcsRUFBRTtBQUhKO0FBUEcsR0FMWSxDQWtCeEI7QUFDQTs7QUFuQndCLENBQXJCO0FBc0JBLE1BQU1rSSxZQUFZLEdBQUc7QUFDeEJ0SSxPQUFLLEVBQUUsQ0FEaUI7QUFFeEJDLE1BQUksRUFBRSxLQUZrQjtBQUd4QkcsS0FBRyxFQUFFLElBSG1CO0FBSXhCRyxNQUFJLEVBQUUsS0FKa0I7QUFLeEJMLFFBQU0sRUFBRSxFQUxnQjtBQU14QmlILFlBQVUsRUFBRTtBQUNSLE9BQUc7QUFDQ25ILFdBQUssRUFBRTtBQURSLEtBREs7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBSkc7QUFPUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROO0FBUEc7QUFOWSxDQUFyQjtBQW1CQSxNQUFNdUksWUFBWSxHQUFHO0FBQ3hCbkksS0FBRyxFQUFFLEtBRG1CO0FBRXhCSCxNQUFJLEVBQUUsS0FGa0I7QUFHeEJNLE1BQUksRUFBRSxLQUhrQjtBQUl4QmdILFVBQVEsRUFBRSxLQUpjO0FBS3hCckgsUUFBTSxFQUFFLEVBTGdCO0FBTXhCaUgsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE47QUFQRztBQU5ZLENBQXJCO0FBbUJBLE1BQU13SSxZQUFZLEdBQUc7QUFDeEJwSSxLQUFHLEVBQUUsS0FEbUI7QUFFeEJHLE1BQUksRUFBRTtBQUZrQixDQUFyQixDLENBS1A7O0FBQ08sTUFBTWtJLFdBQVcsR0FBRztBQUN2QnJJLEtBQUcsRUFBRSxLQURrQjtBQUV2QkcsTUFBSSxFQUFFLElBRmlCO0FBR3ZCTixNQUFJLEVBQUUsS0FIaUI7QUFJdkJELE9BQUssRUFBRSxDQUpnQjtBQUt2QlMsVUFBUSxFQUFFO0FBTGEsQ0FBcEI7QUFRQSxNQUFNaUksYUFBYSxHQUFHO0FBQ3pCMUksT0FBSyxFQUFFLENBRGtCO0FBRXpCSSxLQUFHLEVBQUUsS0FGb0I7QUFHekJHLE1BQUksRUFBRSxLQUhtQjtBQUl6QkwsUUFBTSxFQUFFLENBSmlCO0FBS3pCRCxNQUFJLEVBQUUsS0FMbUI7QUFNekJRLFVBQVEsRUFBRSxLQU5lO0FBT3pCMkgsaUJBQWUsRUFBRSxJQVBRO0FBUXpCakIsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFLENBRE47QUFFREMsVUFBSSxFQUFFO0FBRkw7QUFQRztBQVJhLENBQXRCO0FBc0JBLE1BQU0wSSxXQUFXLEdBQUc7QUFDdkIzSSxPQUFLLEVBQUUsQ0FEZ0I7QUFFdkJJLEtBQUcsRUFBRSxLQUZrQjtBQUd2QkcsTUFBSSxFQUFFLEtBSGlCO0FBSXZCRSxVQUFRLEVBQUUsSUFKYTtBQUt2QlIsTUFBSSxFQUFFLEtBTGlCO0FBTXZCQyxRQUFNLEVBQUUsRUFOZTtBQU92QmlILFlBQVUsRUFBRTtBQUNSLE9BQUc7QUFDQ25ILFdBQUssRUFBRTtBQURSLEtBREs7QUFJUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBSkc7QUFPUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBUEc7QUFVUixTQUFLO0FBQ0RBLFdBQUssRUFBRTtBQUROLEtBVkc7QUFhUixVQUFNO0FBQ0ZBLFdBQUssRUFBRTtBQURMO0FBYkU7QUFQVyxDQUFwQjtBQTBCQSxNQUFNNEksYUFBYSxHQUFHO0FBQ3pCNUksT0FBSyxFQUFFLENBRGtCO0FBRXpCSSxLQUFHLEVBQUUsS0FGb0I7QUFHekJHLE1BQUksRUFBRSxJQUhtQjtBQUl6QkUsVUFBUSxFQUFFLEtBSmU7QUFLekJSLE1BQUksRUFBRSxLQUxtQjtBQU16QkMsUUFBTSxFQUFFLEVBTmlCO0FBT3pCaUgsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFLENBRE47QUFFRE8sVUFBSSxFQUFFLEtBRkw7QUFHREgsU0FBRyxFQUFFO0FBSEo7QUFQRztBQVBhLENBQXRCO0FBc0JBLE1BQU15SSxjQUFjLEdBQUc7QUFDMUI3SSxPQUFLLEVBQUUsQ0FEbUI7QUFFMUJJLEtBQUcsRUFBRSxLQUZxQjtBQUcxQkcsTUFBSSxFQUFFLElBSG9CO0FBSTFCRSxVQUFRLEVBQUUsS0FKZ0I7QUFLMUJSLE1BQUksRUFBRSxLQUxvQjtBQU0xQkMsUUFBTSxFQUFFLEVBTmtCO0FBTzFCaUgsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDbkgsV0FBSyxFQUFFO0FBRFIsS0FESztBQUlSLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FKRztBQU9SLFNBQUs7QUFDREEsV0FBSyxFQUFFO0FBRE4sS0FQRztBQVVSLFVBQU07QUFDRkEsV0FBSyxFQUFFLENBREw7QUFFRk8sVUFBSSxFQUFFLEtBRko7QUFHRkgsU0FBRyxFQUFFO0FBSEg7QUFWRTtBQVBjLENBQXZCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzYVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLE1BQU0wSSxZQUFZLEdBQUcsVUFBV2xKLE9BQVgsRUFBcUI7QUFDN0MsTUFBSyxhQUFhLE9BQU9BLE9BQXpCLEVBQW1DO0FBQy9CLFdBQU9tSixJQUFJLENBQUNDLEtBQUwsQ0FBWXBKLE9BQU8sQ0FBQ3FKLE9BQVIsQ0FBaUIsSUFBakIsRUFBdUIsR0FBdkIsRUFBNkJBLE9BQTdCLENBQXNDLEdBQXRDLEVBQTJDLEVBQTNDLENBQVosQ0FBUDtBQUNIOztBQUNELFNBQU8sRUFBUDtBQUNILENBTE07QUFPUDtBQUNBO0FBQ0E7QUFDQTs7QUFDTyxNQUFNQyxXQUFXLEdBQUcsWUFBWTtBQUNuQyxNQUFJQyxNQUFNLEdBQUdDLFNBQVMsQ0FBQ0MsU0FBdkI7O0FBQ0EsTUFBS0YsTUFBTSxDQUFDRyxPQUFQLENBQWdCLFNBQWhCLElBQThCLENBQUMsQ0FBcEMsRUFBd0M7QUFDcEMsV0FBTyxJQUFQO0FBQ0g7O0FBRUQsU0FBTyxLQUFQO0FBQ0gsQ0FQTTtBQVNQO0FBQ0E7QUFDQTtBQUNBOztBQUNPLE1BQU1DLGVBQWUsR0FBRyxZQUFZO0FBQ3ZDLE1BQUlKLE1BQU0sR0FBR0MsU0FBUyxDQUFDQyxTQUF2QjtBQUNBLE1BQUtGLE1BQU0sQ0FBQ0csT0FBUCxDQUFnQixRQUFoQixNQUErQixDQUFDLENBQWhDLElBQXFDSCxNQUFNLENBQUNHLE9BQVAsQ0FBZ0IsUUFBaEIsTUFBK0IsQ0FBQyxDQUExRSxFQUNJLE9BQU8sSUFBUDtBQUNKLFNBQU8sS0FBUDtBQUNILENBTE07QUFPUDtBQUNBO0FBQ0E7QUFDQTs7QUFDTyxNQUFNRSxhQUFhLEdBQUcsWUFBWTtBQUNyQyxNQUFJTCxNQUFNLEdBQUdDLFNBQVMsQ0FBQ0MsU0FBdkI7QUFDQSxNQUFLRixNQUFNLENBQUNHLE9BQVAsQ0FBZ0IsTUFBaEIsSUFBMkIsQ0FBQyxDQUFqQyxFQUNJLE9BQU8sSUFBUDtBQUNKLFNBQU8sS0FBUDtBQUNILENBTE07QUFPUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ08sTUFBTWpGLFNBQVMsR0FBRyxVQUFXb0YsS0FBWCxFQUFrQkMsRUFBbEIsRUFBdUI7QUFDNUMsT0FBTSxJQUFJQyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHRixLQUFLLENBQUNuSSxNQUEzQixFQUFtQ3FJLENBQUMsRUFBcEMsRUFBeUM7QUFDckMsUUFBS0QsRUFBRSxDQUFFRCxLQUFLLENBQUVFLENBQUYsQ0FBUCxDQUFQLEVBQXdCO0FBQ3BCLGFBQU9BLENBQVA7QUFDSDtBQUNKOztBQUNELFNBQU8sQ0FBQyxDQUFSO0FBQ0gsQ0FQTTtBQVNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNPLE1BQU1DLGNBQWMsR0FBRyxVQUFXSCxLQUFYLEVBQWtCSSxXQUFsQixFQUErQkgsRUFBL0IsRUFBb0M7QUFDOUQsT0FBTSxJQUFJQyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHRSxXQUFXLENBQUN2SSxNQUFqQyxFQUF5Q3FJLENBQUMsRUFBMUMsRUFBK0M7QUFDM0MsUUFBS0QsRUFBRSxDQUFFRyxXQUFXLENBQUVGLENBQUYsQ0FBYixDQUFQLEVBQThCO0FBQzFCLGFBQU9BLENBQVA7QUFDSDtBQUNKOztBQUNELFNBQU8sQ0FBQyxDQUFSO0FBQ0gsQ0FQTTtBQVVQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ08sTUFBTWxLLFlBQVksR0FBS3FLLElBQUYsSUFBWTtBQUNwQyxRQUFNQyxZQUFZLEdBQUcscURBQXJCLENBRG9DLENBR3BDOztBQUNBLFNBQVFBLFlBQVksQ0FBQ0MsSUFBYixDQUFtQkYsSUFBbkIsQ0FBUixFQUFvQztBQUNoQ0EsUUFBSSxHQUFHQSxJQUFJLENBQUNiLE9BQUwsQ0FBY2MsWUFBZCxFQUE0QixFQUE1QixDQUFQO0FBQ0gsR0FObUMsQ0FRcEM7OztBQUNBRCxNQUFJLEdBQUdBLElBQUksQ0FBQ2IsT0FBTCxDQUFjLGlCQUFkLEVBQWlDLEVBQWpDLENBQVA7QUFFQSxTQUFPO0FBQ0hnQixVQUFNLEVBQUVIO0FBREwsR0FBUDtBQUdILENBZE07QUFnQlA7QUFDQTtBQUNBOztBQUNPLE1BQU1JLG1CQUFtQixHQUFHLFlBQVk7QUFDM0MsTUFBSXZJLEdBQUcsR0FBR3dJLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixJQUFtQ0QsUUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDQyxTQUFwRSxHQUFnRixHQUExRjtBQUVBLE1BQUlDLFlBQVksR0FBR0gsUUFBUSxDQUFDQyxhQUFULENBQXdCLGdCQUF4QixDQUFuQjtBQUNBLE1BQUlHLE1BQU0sR0FBRyxDQUFiOztBQUVBLE1BQUtELFlBQUwsRUFBb0I7QUFDaEJDLFVBQU0sR0FBR0QsWUFBWSxDQUFDRSxZQUF0QjtBQUNIOztBQUVELE1BQUtDLE1BQU0sQ0FBQ0MsV0FBUCxJQUFzQi9JLEdBQXRCLElBQTZCOEksTUFBTSxDQUFDRSxVQUFQLElBQXFCLEdBQXZELEVBQTZEO0FBQ3pELFFBQUtMLFlBQUwsRUFBb0I7QUFDaEJBLGtCQUFZLENBQUNNLFNBQWIsQ0FBdUJDLEdBQXZCLENBQTRCLE9BQTVCOztBQUNBLFVBQUssQ0FBQ1YsUUFBUSxDQUFDQyxhQUFULENBQXdCLGlCQUF4QixDQUFOLEVBQW9EO0FBQ2hELFlBQUlVLE9BQU8sR0FBR1gsUUFBUSxDQUFDWSxhQUFULENBQXdCLEtBQXhCLENBQWQ7QUFDQUQsZUFBTyxDQUFDN0wsU0FBUixHQUFvQixnQkFBcEI7QUFDQXFMLG9CQUFZLENBQUNVLFVBQWIsQ0FBd0JDLFlBQXhCLENBQXNDSCxPQUF0QyxFQUErQ1IsWUFBL0M7QUFDQUgsZ0JBQVEsQ0FBQ0MsYUFBVCxDQUF3QixpQkFBeEIsRUFBNENjLHFCQUE1QyxDQUFtRSxXQUFuRSxFQUFnRlosWUFBaEY7QUFDQUgsZ0JBQVEsQ0FBQ0MsYUFBVCxDQUF3QixpQkFBeEIsRUFBNENlLFlBQTVDLENBQTBELE9BQTFELEVBQW1FLGFBQWFaLE1BQWIsR0FBc0IsSUFBekY7QUFDSDs7QUFFRCxVQUFLLENBQUNKLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixpQkFBeEIsRUFBNENnQixZQUE1QyxDQUEwRCxPQUExRCxDQUFOLEVBQTRFO0FBQ3hFakIsZ0JBQVEsQ0FBQ0MsYUFBVCxDQUF3QixpQkFBeEIsRUFBNENlLFlBQTVDLENBQTBELE9BQTFELEVBQW1FLGFBQWFaLE1BQWIsR0FBc0IsSUFBekY7QUFDSDtBQUNKO0FBQ0osR0FmRCxNQWVPO0FBQ0gsUUFBS0QsWUFBTCxFQUFvQjtBQUNoQkEsa0JBQVksQ0FBQ00sU0FBYixDQUF1QlMsTUFBdkIsQ0FBK0IsT0FBL0I7QUFDSDs7QUFFRCxRQUFLbEIsUUFBUSxDQUFDQyxhQUFULENBQXdCLGlCQUF4QixDQUFMLEVBQW1EO0FBQy9DRCxjQUFRLENBQUNDLGFBQVQsQ0FBd0IsaUJBQXhCLEVBQTRDa0IsZUFBNUMsQ0FBNkQsT0FBN0Q7QUFDSDtBQUNKOztBQUVELE1BQUtiLE1BQU0sQ0FBQ2MsVUFBUCxJQUFxQixHQUFyQixJQUE0QnBCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ1EsU0FBakMsQ0FBMkNZLFFBQTNDLENBQXFELHNCQUFyRCxDQUFqQyxFQUFpSDtBQUM3R3JCLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ1EsU0FBakMsQ0FBMkNTLE1BQTNDLENBQW1ELHNCQUFuRDtBQUNIO0FBQ0osQ0F0Q007QUF3Q1A7QUFDQTtBQUNBOztBQUNPLE1BQU1JLGFBQWEsR0FBRyxVQUFXQyxLQUFLLEdBQUcsR0FBbkIsRUFBd0JDLEtBQUssR0FBRyxzQkFBaEMsRUFBeUQ7QUFDbEYsTUFBSUMsV0FBVyxHQUFHekIsUUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEtBQW9DRCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNRLFNBQXZGO0FBQ0FnQixhQUFXLEdBQUdBLFdBQVcsQ0FBQ0MsS0FBWixDQUFrQkMsS0FBbEIsQ0FBeUIsR0FBekIsRUFBK0JDLE1BQS9CLENBQXVDOUssSUFBSSxJQUFJQSxJQUFJLEtBQUssTUFBVCxJQUFtQkEsSUFBSSxLQUFLLFFBQTNFLENBQWQ7O0FBQ0EsT0FBTSxJQUFJMEksQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBR2lDLFdBQVcsQ0FBQ3RLLE1BQWpDLEVBQXlDcUksQ0FBQyxFQUExQyxFQUErQztBQUMzQ1EsWUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEtBQW9DRCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNRLFNBQWpDLENBQTJDUyxNQUEzQyxDQUFtRE8sV0FBVyxDQUFFakMsQ0FBRixDQUE5RCxDQUFwQztBQUNIO0FBQ0osQ0FOTTtBQVFQO0FBQ0E7QUFDQTs7QUFDTyxNQUFNcUMsbUJBQW1CLEdBQUcsWUFBWTtBQUMzQyxNQUFJQyxZQUFZLEdBQUc5QixRQUFRLENBQUNDLGFBQVQsQ0FBd0IsZ0JBQXhCLENBQW5CO0FBQ0EsTUFBSXpJLEdBQUcsR0FBR3dJLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixJQUFtQ0QsUUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDQyxTQUFwRSxHQUFnRixHQUExRjtBQUVBLE1BQUlFLE1BQU0sR0FBRyxDQUFiOztBQUVBLE1BQUswQixZQUFMLEVBQW9CO0FBQ2hCMUIsVUFBTSxHQUFHMEIsWUFBWSxDQUFDekIsWUFBdEI7QUFDSDs7QUFFRCxNQUFLQyxNQUFNLENBQUNDLFdBQVAsSUFBc0IvSSxHQUF0QixJQUE2QjhJLE1BQU0sQ0FBQ0UsVUFBUCxHQUFvQixHQUF0RCxFQUE0RDtBQUN4RCxRQUFLc0IsWUFBTCxFQUFvQjtBQUNoQkEsa0JBQVksQ0FBQ3JCLFNBQWIsQ0FBdUJDLEdBQXZCLENBQTRCLE9BQTVCOztBQUNBLFVBQUssQ0FBQ1YsUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFOLEVBQTREO0FBQ3hELFlBQUlVLE9BQU8sR0FBR1gsUUFBUSxDQUFDWSxhQUFULENBQXdCLEtBQXhCLENBQWQ7QUFDQUQsZUFBTyxDQUFDN0wsU0FBUixHQUFvQix3QkFBcEI7QUFDQWdOLG9CQUFZLENBQUNqQixVQUFiLENBQXdCQyxZQUF4QixDQUFzQ0gsT0FBdEMsRUFBK0NtQixZQUEvQztBQUNBOUIsZ0JBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsRUFBb0RjLHFCQUFwRCxDQUEyRSxXQUEzRSxFQUF3RmUsWUFBeEY7QUFDSDs7QUFFRDlCLGNBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsRUFBb0RlLFlBQXBELENBQWtFLE9BQWxFLEVBQTJFLGFBQWFaLE1BQWIsR0FBc0IsSUFBakc7QUFDSDtBQUNKLEdBWkQsTUFZTztBQUNILFFBQUswQixZQUFMLEVBQW9CO0FBQ2hCQSxrQkFBWSxDQUFDckIsU0FBYixDQUF1QlMsTUFBdkIsQ0FBK0IsT0FBL0I7QUFDSDs7QUFFRCxRQUFLbEIsUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFMLEVBQTJEO0FBQ3ZERCxjQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLEVBQW9Ea0IsZUFBcEQsQ0FBcUUsT0FBckU7QUFDSDtBQUNKOztBQUVELE1BQUtiLE1BQU0sQ0FBQ0UsVUFBUCxHQUFvQixHQUFwQixJQUEyQlIsUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFoQyxFQUFzRjtBQUNsRkQsWUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixFQUFvRGpMLEtBQXBELENBQTBEb0wsTUFBMUQsR0FBbUUsTUFBbkU7QUFDSDtBQUNKLENBbkNNO0FBcUNQO0FBQ0E7QUFDQTs7QUFDTyxNQUFNMkIsZUFBZSxHQUFHLFlBQVk7QUFDdkMsTUFBSUMsYUFBYSxHQUFHaEMsUUFBUSxDQUFDaUMsZ0JBQVQsQ0FBMkIsV0FBM0IsQ0FBcEI7O0FBRUEsTUFBS0QsYUFBTCxFQUFxQjtBQUNqQixTQUFNLElBQUl4QyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHd0MsYUFBYSxDQUFDN0ssTUFBbkMsRUFBMkNxSSxDQUFDLEVBQTVDLEVBQWlEO0FBQzdDO0FBQ0EsVUFBSTBDLFFBQVEsR0FBR0YsYUFBYSxDQUFFeEMsQ0FBRixDQUE1QjtBQUFBLFVBQW1DMkMsSUFBbkM7QUFBQSxVQUF5Q0MsYUFBYSxHQUFHLENBQXpEOztBQUVBLFVBQUtGLFFBQVEsQ0FBQ2pCLFlBQVQsQ0FBdUIsYUFBdkIsQ0FBTCxFQUE4QztBQUMxQ21CLHFCQUFhLEdBQUc1SCxRQUFRLENBQUVtRSxZQUFZLENBQUV1RCxRQUFRLENBQUNqQixZQUFULENBQXVCLGFBQXZCLENBQUYsQ0FBWixDQUF1RG9CLEtBQXpELENBQXhCO0FBQ0g7O0FBRURGLFVBQUksR0FBRyxDQUFFRCxRQUFRLENBQUNoQyxTQUFULEdBQXFCSSxNQUFNLENBQUNDLFdBQTlCLElBQThDLEVBQTlDLEdBQW1ENkIsYUFBbkQsR0FBbUVGLFFBQVEsQ0FBQ2hDLFNBQTVFLEdBQXdGLEVBQS9GO0FBRUFnQyxjQUFRLENBQUNsTixLQUFULENBQWVzTixrQkFBZixHQUFvQyxTQUFTSCxJQUFULEdBQWdCLEdBQXBEO0FBQ0g7QUFDSjtBQUNKLENBakJNO0FBbUJQO0FBQ0E7QUFDQTs7QUFDTyxNQUFNSSxvQkFBb0IsR0FBRyxZQUFZO0FBQzVDLE1BQUlDLFNBQVMsR0FBR3hDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixhQUF4QixDQUFoQjs7QUFFQSxNQUFLSyxNQUFNLENBQUNDLFdBQVAsSUFBc0IsR0FBM0IsRUFBaUM7QUFDN0JpQyxhQUFTLENBQUMvQixTQUFWLENBQW9CQyxHQUFwQixDQUF5QixNQUF6QjtBQUNILEdBRkQsTUFFTztBQUNIOEIsYUFBUyxDQUFDL0IsU0FBVixDQUFvQlMsTUFBcEIsQ0FBNEIsTUFBNUI7QUFDSDtBQUNKLENBUk07QUFVUDtBQUNBO0FBQ0E7O0FBQ08sU0FBU3VCLGdCQUFULENBQTJCQyxRQUFRLEdBQUcsSUFBdEMsRUFBNENMLEtBQUssR0FBRyxFQUFwRCxFQUF5RDtBQUM1RCxNQUFJbkMsU0FBUyxHQUFHLENBQWhCOztBQUVBLE1BQUt3QyxRQUFRLElBQUksQ0FBQ3JELGFBQWEsRUFBL0IsRUFBb0M7QUFDaEMsUUFBS1csUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFMLEVBQTJEO0FBQ3ZEQyxlQUFTLEdBQUdGLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsRUFBb0QwQyxxQkFBcEQsR0FBNEVuTCxHQUE1RSxHQUFrRjhJLE1BQU0sQ0FBQ0MsV0FBekYsR0FBdUdQLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixnQkFBeEIsRUFBMkNJLFlBQWxKLEdBQWlLLENBQTdLO0FBQ0g7QUFDSixHQUpELE1BSU87QUFDSEgsYUFBUyxHQUFHLENBQVo7QUFDSDs7QUFFRCxNQUFLZCxlQUFlLE1BQU1DLGFBQWEsRUFBdkMsRUFBNEM7QUFDeEMsUUFBSXVELEdBQUcsR0FBR3RDLE1BQU0sQ0FBQ0MsV0FBakI7QUFDQSxRQUFJc0MsT0FBTyxHQUFHQyxXQUFXLENBQUUsTUFBTTtBQUM3QixVQUFLRixHQUFHLElBQUkxQyxTQUFaLEVBQXdCNkMsYUFBYSxDQUFFRixPQUFGLENBQWI7QUFDeEJ2QyxZQUFNLENBQUMwQyxRQUFQLENBQWlCLENBQWpCLEVBQW9CLENBQUNYLEtBQXJCO0FBQ0FPLFNBQUcsSUFBSVAsS0FBUDtBQUNILEtBSndCLEVBSXRCLENBSnNCLENBQXpCO0FBS0gsR0FQRCxNQU9PO0FBQ0gvQixVQUFNLENBQUMyQyxRQUFQLENBQWlCO0FBQ2J6TCxTQUFHLEVBQUUwSSxTQURRO0FBRWJnRCxjQUFRLEVBQUU7QUFGRyxLQUFqQjtBQUlIO0FBQ0o7QUFFRDtBQUNBO0FBQ0E7O0FBQ08sTUFBTUMsWUFBWSxHQUFLaE8sQ0FBRixJQUFTO0FBQ2pDQSxHQUFDLENBQUNpTyxlQUFGO0FBQ0FqTyxHQUFDLENBQUNELGNBQUY7O0FBRUEsTUFBS0MsQ0FBQyxDQUFDa08sYUFBRixDQUFnQkMsT0FBaEIsQ0FBeUIsYUFBekIsQ0FBTCxFQUFnRDtBQUM1QyxRQUFJQyxLQUFLLEdBQUdwTyxDQUFDLENBQUNrTyxhQUFGLENBQWdCQyxPQUFoQixDQUF5QixhQUF6QixDQUFaOztBQUNBLFFBQUtDLEtBQUssQ0FBQzlDLFNBQU4sQ0FBZ0JZLFFBQWhCLENBQTBCLFNBQTFCLENBQUwsRUFBNkM7QUFDekNrQyxXQUFLLENBQUM5QyxTQUFOLENBQWdCUyxNQUFoQixDQUF3QixTQUF4QjtBQUNBcUMsV0FBSyxDQUFDOUMsU0FBTixDQUFnQkMsR0FBaEIsQ0FBcUIsUUFBckI7QUFDQTZDLFdBQUssQ0FBQ3RELGFBQU4sQ0FBcUIsT0FBckIsRUFBK0J1RCxLQUEvQjtBQUNILEtBSkQsTUFJTztBQUNIRCxXQUFLLENBQUM5QyxTQUFOLENBQWdCQyxHQUFoQixDQUFxQixTQUFyQjtBQUNBNkMsV0FBSyxDQUFDdEQsYUFBTixDQUFxQixPQUFyQixFQUErQndELElBQS9CO0FBQ0g7O0FBRURGLFNBQUssQ0FBQ3RELGFBQU4sQ0FBcUIsT0FBckIsRUFBK0J5RCxnQkFBL0IsQ0FBaUQsT0FBakQsRUFBMEQsWUFBWTtBQUNsRUgsV0FBSyxDQUFDOUMsU0FBTixDQUFnQlMsTUFBaEIsQ0FBd0IsU0FBeEI7QUFDQXFDLFdBQUssQ0FBQzlDLFNBQU4sQ0FBZ0JTLE1BQWhCLENBQXdCLFFBQXhCO0FBQ0gsS0FIRDtBQUlIO0FBQ0osQ0FwQk07QUFzQlA7QUFDQTtBQUNBOztBQUNPLE1BQU15QyxhQUFhLEdBQUdDLFNBQVMsSUFBSTtBQUN0QyxNQUFJQyxLQUFLLEdBQUcsQ0FBWjs7QUFDQSxNQUFLRCxTQUFMLEVBQWlCO0FBQ2IsU0FBTSxJQUFJcEUsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBR29FLFNBQVMsQ0FBQ3pNLE1BQS9CLEVBQXVDcUksQ0FBQyxFQUF4QyxFQUE2QztBQUN6Q3FFLFdBQUssSUFBSUQsU0FBUyxDQUFFcEUsQ0FBRixDQUFULENBQWV4SCxLQUFmLEdBQXVCd0MsUUFBUSxDQUFFb0osU0FBUyxDQUFFcEUsQ0FBRixDQUFULENBQWUxSCxHQUFqQixFQUFzQixFQUF0QixDQUF4QztBQUNIO0FBQ0o7O0FBQ0QsU0FBTytMLEtBQVA7QUFDSCxDQVJNO0FBVVA7QUFDQTtBQUNBOztBQUNPLE1BQU1DLFlBQVksR0FBR0YsU0FBUyxJQUFJO0FBQ3JDLE1BQUlDLEtBQUssR0FBRyxDQUFaOztBQUVBLE9BQU0sSUFBSXJFLENBQUMsR0FBRyxDQUFkLEVBQWlCQSxDQUFDLEdBQUdvRSxTQUFTLENBQUN6TSxNQUEvQixFQUF1Q3FJLENBQUMsRUFBeEMsRUFBNkM7QUFDekNxRSxTQUFLLElBQUlySixRQUFRLENBQUVvSixTQUFTLENBQUVwRSxDQUFGLENBQVQsQ0FBZTFILEdBQWpCLEVBQXNCLEVBQXRCLENBQWpCO0FBQ0g7O0FBRUQsU0FBTytMLEtBQVA7QUFDSCxDQVJNO0FBVVA7QUFDQTtBQUNBOztBQUNPLE1BQU05TCxTQUFTLEdBQUcsQ0FBRUMsS0FBRixFQUFTK0wsVUFBVSxHQUFHLENBQXRCLEtBQTZCO0FBQ2xELFNBQU8vTCxLQUFLLENBQUNnTSxjQUFOLENBQXNCNU0sU0FBdEIsRUFBaUM7QUFBRTZNLHlCQUFxQixFQUFFRixVQUF6QjtBQUFxQ0cseUJBQXFCLEVBQUVIO0FBQTVELEdBQWpDLENBQVA7QUFDSCxDQUZNLEM7Ozs7Ozs7Ozs7QUN4VFAsZSIsImZpbGUiOiJjb21wb25lbnRzX2NvbW1vbl9BcGlfanMtY29tcG9uZW50c19mZWF0dXJlc19vd2wtY2Fyb3VzZWxfanN4LXNlcnZlcl9hcG9sbG9fanMtc2VydmVyX3F1ZXJpZXMtZGFhYjAzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcclxuXHJcbmV4cG9ydCBjb25zdCBBUElfQkFTRV9VUkwgPSBcImh0dHA6Ly8zLjE2LjcuMjE6MzAwMC9kc3F1YXJlL2FwaS9cIjtcclxuZXhwb3J0IGNvbnN0IGdldEFwaVVybCA9IChlbmRwb2ludCkgPT4gQVBJX0JBU0VfVVJMICsgZW5kcG9pbnQ7XHJcbmV4cG9ydCBjb25zdCBTSUdOVVBfQVBJID0gZ2V0QXBpVXJsKFwiY3JlYXRlLWFjY291bnRcIik7XHJcbmV4cG9ydCBjb25zdCBTRU5ET1RQX0FQSSA9IGdldEFwaVVybChcImdlbmVyYXRlLW90cFwiKTtcclxuZXhwb3J0IGNvbnN0IFZFUklGWV9BUEkgPSBnZXRBcGlVcmwoXCJ2ZXJpZnktb3RwXCIpO1xyXG5leHBvcnQgY29uc3QgQUREX0FERFJFU1NfQVBJID0gZ2V0QXBpVXJsKFwiYWRkLWFkZHJlc3NcIik7XHJcbmV4cG9ydCBjb25zdCBHRVRfQUREUkVTU19BUEkgPSBnZXRBcGlVcmwoXCJnZXQtbXktYWRyZXNzXCIpO1xyXG5leHBvcnQgY29uc3QgREVMRVRFX0FERFJFU1NfQVBJID0gZ2V0QXBpVXJsKFwiZGVsZXRlLW15LWFkcmVzc1wiKTtcclxuZXhwb3J0IGNvbnN0IFBST0RVQ1RfQ0FURUdPUllfQVBJID0gZ2V0QXBpVXJsKFwicHJvZHVjdC1jYXRlZ29yaWVzXCIpO1xyXG5cclxuXHJcbiIsImltcG9ydCBMaW5rIGZyb20gXCJuZXh0L2xpbmtcIjtcclxuXHJcbmltcG9ydCB7IHBhcnNlQ29udGVudCB9IGZyb20gJ34vdXRpbHMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQUxpbmsgKCB7IGNoaWxkcmVuLCBjbGFzc05hbWUsIGNvbnRlbnQsIHN0eWxlLCAuLi5wcm9wcyB9ICkge1xyXG5cclxuICAgIGNvbnN0IHByZXZlbnREZWZhdWx0ID0gKCBlICkgPT4ge1xyXG4gICAgICAgIGlmICggcHJvcHMuaHJlZiA9PT0gJyMnICkge1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIHByb3BzLm9uQ2xpY2sgKSB7XHJcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgcHJvcHMub25DbGljaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIGNvbnRlbnQgP1xyXG4gICAgICAgICAgICA8TGluayB7IC4uLnByb3BzIH0gPlxyXG4gICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPXsgY2xhc3NOYW1lIH0gc3R5bGU9eyBzdHlsZSB9IG9uQ2xpY2s9eyBwcmV2ZW50RGVmYXVsdCB9IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXsgcGFyc2VDb250ZW50KCBjb250ZW50ICkgfT5cclxuICAgICAgICAgICAgICAgICAgICB7IGNoaWxkcmVuIH1cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9MaW5rPiA6XHJcbiAgICAgICAgICAgIDxMaW5rIHsgLi4ucHJvcHMgfSA+XHJcbiAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9eyBjbGFzc05hbWUgfSBzdHlsZT17IHN0eWxlIH0gb25DbGljaz17IHByZXZlbnREZWZhdWx0IH0+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBjaGlsZHJlbiB9XHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDwvTGluaz5cclxuICAgIClcclxufSIsImltcG9ydCBSZWFjdCwgeyB1c2VSZWYsIHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IENhcm91c2VsIGZyb20gJ3JlYWN0LW93bC1jYXJvdXNlbDInO1xyXG5cclxuLy8gbGV0IHByZXZQYXRoO1xyXG5mdW5jdGlvbiBPd2xDYXJvdXNlbCAoIHByb3BzICkge1xyXG4gICAgY29uc3QgeyBhZENsYXNzLCBvcHRpb25zIH0gPSBwcm9wcztcclxuICAgIGNvbnN0IGNhcm91c2VsUmVmID0gdXNlUmVmKCBudWxsICk7XHJcbiAgICBjb25zdCBkZWZhdWx0T3B0aW9ucyA9IHtcclxuICAgICAgICBpdGVtczogMSxcclxuICAgICAgICBsb29wOiBmYWxzZSxcclxuICAgICAgICBtYXJnaW46IDAsXHJcbiAgICAgICAgcmVzcG9uc2l2ZUNsYXNzOiBcInRydWVcIixcclxuICAgICAgICBuYXY6IHRydWUsXHJcbiAgICAgICAgbmF2VGV4dDogWyAnPGkgY2xhc3M9XCJkLWljb24tYW5nbGUtbGVmdFwiPicsICc8aSBjbGFzcz1cImQtaWNvbi1hbmdsZS1yaWdodFwiPicgXSxcclxuICAgICAgICBuYXZFbGVtZW50OiBcImJ1dHRvblwiLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgc21hcnRTcGVlZDogNDAwLFxyXG4gICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcclxuICAgICAgICBhdXRvSGVpZ2h0OiBmYWxzZVxyXG4gICAgICAgIC8vIGF1dG9wbGF5VGltZW91dDogNTAwMCxcclxuICAgIH07XHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgaWYgKCBwcm9wcy5vbkNoYW5nZVJlZiApIHtcclxuICAgICAgICAgICAgcHJvcHMub25DaGFuZ2VSZWYoIGNhcm91c2VsUmVmICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgWyBjYXJvdXNlbFJlZiBdIClcclxuXHJcbiAgICBsZXQgZXZlbnRzID0ge1xyXG4gICAgICAgIG9uVHJhbnNsYXRlZDogZnVuY3Rpb24gKCBlICkge1xyXG4gICAgICAgICAgICBpZiAoICFlLnRhcmdldCApIHJldHVybjtcclxuICAgICAgICAgICAgaWYgKCBwcm9wcy5vbkNoYW5nZUluZGV4ICkge1xyXG4gICAgICAgICAgICAgICAgcHJvcHMub25DaGFuZ2VJbmRleCggZS5pdGVtLmluZGV4ICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZXZlbnRzID0gT2JqZWN0LmFzc2lnbigge30sIGV2ZW50cywgcHJvcHMuZXZlbnRzICk7XHJcbiAgICBsZXQgc2V0dGluZ3MgPSBPYmplY3QuYXNzaWduKCB7fSwgZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMgKTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIHByb3BzLmNoaWxkcmVuID9cclxuICAgICAgICAgICAgcHJvcHMuY2hpbGRyZW4ubGVuZ3RoID4gMCB8fCAoIHByb3BzLmNoaWxkcmVuICYmIHByb3BzLmNoaWxkcmVuLmxlbmd0aCA9PT0gdW5kZWZpbmVkICkgP1xyXG4gICAgICAgICAgICAgICAgPENhcm91c2VsIHJlZj17IGNhcm91c2VsUmVmIH0gY2xhc3NOYW1lPXsgYG93bC1jYXJvdXNlbCAkeyBhZENsYXNzIH1gIH0gb3B0aW9ucz17IHNldHRpbmdzIH0gZXZlbnRzPXsgZXZlbnRzIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBwcm9wcy5jaGlsZHJlbiB9XHJcbiAgICAgICAgICAgICAgICA8L0Nhcm91c2VsID5cclxuICAgICAgICAgICAgICAgIDogXCJcIlxyXG4gICAgICAgICAgICA6IFwiXCJcclxuICAgICk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFJlYWN0Lm1lbW8oIE93bENhcm91c2VsICk7IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmltcG9ydCBBTGluayBmcm9tICd+L2NvbXBvbmVudHMvZmVhdHVyZXMvY3VzdG9tLWxpbmsnO1xyXG5cclxuaW1wb3J0IHsgdG9EZWNpbWFsIH0gZnJvbSAnfi91dGlscyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDYXJ0UG9wdXAgKCBwcm9wcyApIHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcHJvcHM7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1pbmlwb3B1cC1hcmVhXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWluaXBvcHVwLWJveCBzaG93XCIgc3R5bGU9eyB7IHRvcDogXCIwXCIgfSB9PlxyXG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwibWluaXBvcHVwLXRpdGxlXCI+U3VjY2Vzc2Z1bGx5IGFkZGVkLjwvcD5cclxuXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QgcHJvZHVjdC1wdXJjaGFzZWQgIHByb2R1Y3QtY2FydCBtYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGZpZ3VyZSBjbGFzc05hbWU9XCJwcm9kdWN0LW1lZGlhIHB1cmUtbWVkaWFcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyBgL3Byb2R1Y3QvZGVmYXVsdC8keyBwcm9kdWN0LnNsdWcgfWAgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcmM9eyBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BU1NFVF9VUkkgKyBwcm9kdWN0LnBpY3R1cmVzWyAwIF0udXJsIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbHQ9XCJwcm9kdWN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjkwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCI5MFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZmlndXJlPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1kZXRhaWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyBgL3Byb2R1Y3QvZGVmYXVsdC8keyBwcm9kdWN0LnNsdWcgfWAgfSBjbGFzc05hbWU9XCJwcm9kdWN0LW5hbWVcIj57IHByb2R1Y3QubmFtZSB9PC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJpY2UtYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwcm9kdWN0LXF1YW50aXR5XCI+eyBwcm9kdWN0LnF0eSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJvZHVjdC1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5wcmljZSApIH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYWN0aW9uLWdyb3VwIGQtZmxleFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiL3BhZ2VzL2NhcnRcIiBjbGFzc05hbWU9XCJidG4gYnRuLXNtIGJ0bi1vdXRsaW5lIGJ0bi1wcmltYXJ5IGJ0bi1yb3VuZGVkXCI+VmlldyBDYXJ0PC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9wYWdlcy9jaGVja291dFwiIGNsYXNzTmFtZT1cImJ0biBidG4tc20gYnRuLXByaW1hcnkgYnRuLXJvdW5kZWRcIj5DaGVjayBPdXQ8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG59IiwiaW1wb3J0IHsgd2l0aEFwb2xsbyB9IGZyb20gJ25leHQtYXBvbGxvJztcclxuaW1wb3J0IEFwb2xsb0NsaWVudCwgeyBJbk1lbW9yeUNhY2hlIH0gZnJvbSAnYXBvbGxvLWJvb3N0JztcclxuXHJcbmNvbnN0IEFQSV9VUkkgPSBgJHsgcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfU0VSVkVSX1VSTCB9L2dyYXBocWxgXHJcblxyXG5jb25zdCBhcG9sbG9DbGllbnQgPSBuZXcgQXBvbGxvQ2xpZW50KCB7XHJcbiAgICB1cmk6IEFQSV9VUkksXHJcbiAgICBjYWNoZTogbmV3IEluTWVtb3J5Q2FjaGUoKVxyXG59ICk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCB3aXRoQXBvbGxvKCBhcG9sbG9DbGllbnQgKTsiLCJpbXBvcnQgZ3FsIGZyb20gJ2dyYXBocWwtdGFnJ1xyXG5cclxuZXhwb3J0IGNvbnN0IGN1cnJlbnREZW1vID0gYFwiMVwiYDtcclxuXHJcbmNvbnN0IFBST0RVQ1RfU0lNUExFID0gZ3FsYFxyXG4gICAgZnJhZ21lbnQgUHJvZHVjdFNpbXBsZSBvbiBQcm9kdWN0IHtcclxuICAgICAgICBuYW1lXHJcbiAgICAgICAgc2x1Z1xyXG4gICAgICAgIHByaWNlXHJcbiAgICAgICAgcmF0aW5nc1xyXG4gICAgICAgIHJldmlld3NcclxuICAgICAgICBzdG9ja1xyXG4gICAgICAgIHNob3J0X2Rlc2NyaXB0aW9uXHJcbiAgICAgICAgaXNfZmVhdHVyZWRcclxuICAgICAgICBpc19uZXdcclxuICAgICAgICB1bnRpbFxyXG4gICAgICAgIGRpc2NvdW50XHJcbiAgICAgICAgcGljdHVyZXMge1xyXG4gICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNtYWxsX3BpY3R1cmVzIHtcclxuICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRlZ29yaWVzIHtcclxuICAgICAgICAgICAgbmFtZVxyXG4gICAgICAgICAgICBzbHVnXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhcmlhbnRzIHtcclxuICAgICAgICAgICAgcHJpY2VcclxuICAgICAgICAgICAgc2FsZV9wcmljZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuYFxyXG5cclxuY29uc3QgUFJPRFVDVF9TTUFMTCA9IGdxbGBcclxuICAgIGZyYWdtZW50IFByb2R1Y3RTbWFsbCBvbiBQcm9kdWN0IHtcclxuICAgICAgICBpZFxyXG4gICAgICAgIG5hbWVcclxuICAgICAgICBzbHVnXHJcbiAgICAgICAgcHJpY2VcclxuICAgICAgICByYXRpbmdzXHJcbiAgICAgICAgcGljdHVyZXMge1xyXG4gICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNtYWxsX3BpY3R1cmVzIHtcclxuICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgIH1cclxuICAgICAgICB2YXJpYW50cyB7XHJcbiAgICAgICAgICAgIHByaWNlXHJcbiAgICAgICAgICAgIHNhbGVfcHJpY2VcclxuICAgICAgICB9XHJcbiAgICB9XHJcbmBcclxuXHJcbmV4cG9ydCBjb25zdCBHRVRfUFJPRFVDVFMgPSBncWxgXHJcbiAgICBxdWVyeSBwcm9kdWN0cygkc2VhcmNoOiBTdHJpbmcsICRjb2xvcnM6IFtTdHJpbmddLCAkc2l6ZXM6IFtTdHJpbmddLCAkYnJhbmRzOiBbU3RyaW5nXSwgJG1pbl9wcmljZTogSW50LCAkbWF4X3ByaWNlOiBJbnQsICRjYXRlZ29yeTogU3RyaW5nLCAkdGFnOiBTdHJpbmcsICRzb3J0Qnk6IFN0cmluZywgJGZyb206IEludCwgJHRvOiBJbnQsICRsaXN0OiBCb29sZWFuID0gZmFsc2UpIHtcclxuICAgICAgICBwcm9kdWN0cyhkZW1vOiAke2N1cnJlbnREZW1vIH0sIHNlYXJjaDogJHNlYXJjaCwgY29sb3JzOiAkY29sb3JzLCBzaXplczogJHNpemVzLCBicmFuZHM6ICRicmFuZHMsIG1pbl9wcmljZTogJG1pbl9wcmljZSwgbWF4X3ByaWNlOiAkbWF4X3ByaWNlLCBjYXRlZ29yeTogJGNhdGVnb3J5LCB0YWc6ICR0YWcsIHNvcnRCeTogJHNvcnRCeSwgZnJvbTogJGZyb20sIHRvOiAkdG8gKSB7XHJcbiAgICAgICAgICAgIGRhdGEge1xyXG4gICAgICAgICAgICAgICAgc2hvcnRfZGVzY3JpcHRpb24gQGluY2x1ZGUoaWY6ICRsaXN0KVxyXG4gICAgICAgICAgICAgICAgLi4uUHJvZHVjdFNpbXBsZVxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0b3RhbFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgICR7IFBST0RVQ1RfU0lNUExFIH1cclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IEdFVF9TUEVDSUFMX1BST0RVQ1RTID0gZ3FsYFxyXG4gICAgcXVlcnkgc3BlY2lhbFByb2R1Y3RzKCRmZWF0dXJlZDogQm9vbGVhbiA9IGZhbHNlLCAkYmVzdFNlbGxpbmc6IEJvb2xlYW4gPSBmYWxzZSwgJHRvcFJhdGVkOiBCb29sZWFuID0gZmFsc2UsICRvblNhbGU6IEJvb2xlYW4gPSBmYWxzZSwgJGNvdW50OiBJbnQpIHtcclxuICAgICAgICBzcGVjaWFsUHJvZHVjdHMoZGVtbzogJHtjdXJyZW50RGVtbyB9LCBmZWF0dXJlZDogJGZlYXR1cmVkLCBiZXN0U2VsbGluZzogJGJlc3RTZWxsaW5nLCB0b3BSYXRlZDogJHRvcFJhdGVkLCBvblNhbGU6ICRvblNhbGUsIGNvdW50OiAkY291bnQpIHtcclxuICAgICAgICAgICAgZmVhdHVyZWQgQGluY2x1ZGUoaWY6ICRmZWF0dXJlZCkge1xyXG4gICAgICAgICAgICAgICAgLi4uUHJvZHVjdFNtYWxsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYmVzdFNlbGxpbmcgQGluY2x1ZGUoaWY6ICRiZXN0U2VsbGluZykge1xyXG4gICAgICAgICAgICAgICAgLi4uUHJvZHVjdFNtYWxsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdG9wUmF0ZWQgQGluY2x1ZGUoaWY6ICR0b3BSYXRlZCkge1xyXG4gICAgICAgICAgICAgICAgLi4uUHJvZHVjdFNtYWxsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGF0ZXN0IEBpbmNsdWRlKGlmOiAkbGF0ZXN0KSB7XHJcbiAgICAgICAgICAgICAgICAuLi5Qcm9kdWN0U21hbGxcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgICR7IFBST0RVQ1RfU01BTEwgfVxyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgR0VUX1BST0RVQ1QgPSBncWxgXHJcbiAgICBxdWVyeSBwcm9kdWN0KCRzbHVnOiBTdHJpbmchLCAkb25seURhdGE6IEJvb2xlYW4gPSBmYWxzZSkge1xyXG4gICAgICAgIHByb2R1Y3QoZGVtbzogJHtjdXJyZW50RGVtbyB9LCBzbHVnOiAkc2x1Zywgb25seURhdGE6ICRvbmx5RGF0YSkge1xyXG4gICAgICAgICAgICBkYXRhIHtcclxuICAgICAgICAgICAgICAgIGlkXHJcbiAgICAgICAgICAgICAgICBzbHVnXHJcbiAgICAgICAgICAgICAgICBuYW1lXHJcbiAgICAgICAgICAgICAgICBwcmljZVxyXG4gICAgICAgICAgICAgICAgZGlzY291bnRcclxuICAgICAgICAgICAgICAgIHNob3J0X2Rlc2NyaXB0aW9uXHJcbiAgICAgICAgICAgICAgICBza3VcclxuICAgICAgICAgICAgICAgIHN0b2NrXHJcbiAgICAgICAgICAgICAgICByYXRpbmdzXHJcbiAgICAgICAgICAgICAgICByZXZpZXdzXHJcbiAgICAgICAgICAgICAgICBzYWxlX2NvdW50XHJcbiAgICAgICAgICAgICAgICBpc19uZXdcclxuICAgICAgICAgICAgICAgIGlzX3RvcFxyXG4gICAgICAgICAgICAgICAgdW50aWxcclxuICAgICAgICAgICAgICAgIHBpY3R1cmVzIHtcclxuICAgICAgICAgICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgc21hbGxfcGljdHVyZXMge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsYXJnZV9waWN0dXJlcyB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNhdGVnb3JpZXMge1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWVcclxuICAgICAgICAgICAgICAgICAgICBzbHVnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0YWdzIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJhbmRzIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdmFyaWFudHMge1xyXG4gICAgICAgICAgICAgICAgICAgIHByaWNlXHJcbiAgICAgICAgICAgICAgICAgICAgc2FsZV9wcmljZVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvclxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHVtYiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBzaXplIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRodW1iIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHByZXYgQHNraXAoaWY6ICRvbmx5RGF0YSkge1xyXG4gICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgbmFtZVxyXG4gICAgICAgICAgICAgICAgcGljdHVyZXMge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmV4dCBAc2tpcChpZjogJG9ubHlEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBzbHVnXHJcbiAgICAgICAgICAgICAgICBuYW1lXHJcbiAgICAgICAgICAgICAgICBwaWN0dXJlcyB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZWxhdGVkIEBza2lwKGlmOiAkb25seURhdGEpIHtcclxuICAgICAgICAgICAgICAgIC4uLlByb2R1Y3RTaW1wbGVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgICR7IFBST0RVQ1RfU0lNUExFIH1cclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IEdFVF9WSURFTyA9IGdxbGBcclxuICAgIHF1ZXJ5IHZpZGVvKCRzbHVnOiBTdHJpbmchKSB7XHJcbiAgICAgICAgdmlkZW8oZGVtbzogJHtjdXJyZW50RGVtbyB9LCBzbHVnOiAkc2x1Zykge1xyXG4gICAgICAgICAgICBkYXRhIHtcclxuICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgR0VUX1NIT1BfU0lERUJBUl9EQVRBID0gZ3FsYFxyXG4gICAgcXVlcnkgc2hvcFNpZGViYXJEYXRhKCRmZWF0dXJlZDogQm9vbGVhbiA9IGZhbHNlKSB7XHJcbiAgICAgICAgc2hvcFNpZGViYXJEYXRhKGRlbW86ICR7Y3VycmVudERlbW8gfSwgZmVhdHVyZWQ6ICRmZWF0dXJlZCkge1xyXG4gICAgICAgICAgICBjYXRlZ29yaWVzIHtcclxuICAgICAgICAgICAgICAgIG5hbWVcclxuICAgICAgICAgICAgICAgIHNsdWdcclxuICAgICAgICAgICAgICAgIGNoaWxkcmVuIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZlYXR1cmVkIEBpbmNsdWRlKGlmOiAkZmVhdHVyZWQpIHtcclxuICAgICAgICAgICAgICAgIHNsdWdcclxuICAgICAgICAgICAgICAgIG5hbWVcclxuICAgICAgICAgICAgICAgIHByaWNlXHJcbiAgICAgICAgICAgICAgICByYXRpbmdzXHJcbiAgICAgICAgICAgICAgICByZXZpZXdzXHJcbiAgICAgICAgICAgICAgICBwaWN0dXJlcyB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHZhcmlhbnRzIHtcclxuICAgICAgICAgICAgICAgICAgICBwcmljZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgR0VUX1BPU1RTID0gZ3FsYFxyXG4gICAgcXVlcnkgcG9zdHMoJGNhdGVnb3J5OiBTdHJpbmcsICRmcm9tOiBJbnQsICR0bzogSW50LCAkY2F0ZWdvcmllczogW1N0cmluZ10pIHtcclxuICAgICAgICBwb3N0cyhkZW1vOiAke2N1cnJlbnREZW1vIH0sIGNhdGVnb3J5OiAkY2F0ZWdvcnksIGZyb206ICRmcm9tLCB0bzogJHRvLCBjYXRlZ29yaWVzOiAkY2F0ZWdvcmllcyApIHtcclxuICAgICAgICAgICAgZGF0YSB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZVxyXG4gICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgZGF0ZVxyXG4gICAgICAgICAgICAgICAgY29tbWVudHNcclxuICAgICAgICAgICAgICAgIGNvbnRlbnRcclxuICAgICAgICAgICAgICAgIHR5cGVcclxuICAgICAgICAgICAgICAgIGF1dGhvclxyXG4gICAgICAgICAgICAgICAgbGFyZ2VfcGljdHVyZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHBpY3R1cmUge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBzbWFsbF9waWN0dXJlIHtcclxuICAgICAgICAgICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdmlkZW8ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdG90YWxcclxuICAgICAgICB9XHJcbiAgICB9XHJcbmBcclxuXHJcbmV4cG9ydCBjb25zdCBHRVRfUE9TVCA9IGdxbGBcclxuICAgIHF1ZXJ5IHBvc3QoJHNsdWc6IFN0cmluZyEpIHtcclxuICAgICAgICBwb3N0KGRlbW86ICR7Y3VycmVudERlbW8gfSwgc2x1ZzogJHNsdWcpIHtcclxuICAgICAgICAgICAgZGF0YSB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZVxyXG4gICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgYXV0aG9yXHJcbiAgICAgICAgICAgICAgICBkYXRlXHJcbiAgICAgICAgICAgICAgICBjb21tZW50c1xyXG4gICAgICAgICAgICAgICAgY29udGVudFxyXG4gICAgICAgICAgICAgICAgdHlwZVxyXG4gICAgICAgICAgICAgICAgbGFyZ2VfcGljdHVyZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHBpY3R1cmUge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2aWRlbyB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNhdGVnb3JpZXMge1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWVcclxuICAgICAgICAgICAgICAgICAgICBzbHVnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmVsYXRlZCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZVxyXG4gICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgZGF0ZVxyXG4gICAgICAgICAgICAgICAgdHlwZVxyXG4gICAgICAgICAgICAgICAgY29tbWVudHNcclxuICAgICAgICAgICAgICAgIGNvbnRlbnRcclxuICAgICAgICAgICAgICAgIHBpY3R1cmUge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2aWRlbyB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IEdFVF9QT1NUX1NJREVCQVJfREFUQSA9IGdxbGBcclxuICAgIHF1ZXJ5IHBvc3RTaWRiYXJEYXRhIHtcclxuICAgICAgICBwb3N0U2lkZWJhckRhdGEoZGVtbzogJHtjdXJyZW50RGVtbyB9KSB7XHJcbiAgICAgICAgICAgIGNhdGVnb3JpZXMge1xyXG4gICAgICAgICAgICAgICAgbmFtZVxyXG4gICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJlY2VudCB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZVxyXG4gICAgICAgICAgICAgICAgc2x1Z1xyXG4gICAgICAgICAgICAgICAgZGF0ZVxyXG4gICAgICAgICAgICAgICAgc21hbGxfcGljdHVyZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IEdFVF9IT01FX0RBVEEgPSBncWxgXHJcbiAgICBxdWVyeSBpbmRleERhdGEoJHByb2R1Y3RzQ291bnQ6IEludCwgJHBvc3RzQ291bnQ6IEludCkge1xyXG4gICAgICAgIHNwZWNpYWxQcm9kdWN0cyhkZW1vOiAke2N1cnJlbnREZW1vIH0sIGZlYXR1cmVkOiB0cnVlLCBiZXN0U2VsbGluZzogdHJ1ZSwgdG9wUmF0ZWQ6IHRydWUsIGxhdGVzdDogdHJ1ZSwgb25TYWxlOiB0cnVlLCBjb3VudDogJHByb2R1Y3RzQ291bnQpIHtcclxuICAgICAgICAgICAgZmVhdHVyZWQge1xyXG4gICAgICAgICAgICAgICAgLi4uUHJvZHVjdFNpbXBsZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGJlc3RTZWxsaW5nIHtcclxuICAgICAgICAgICAgICAgIC4uLlByb2R1Y3RTaW1wbGVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0b3BSYXRlZCB7XHJcbiAgICAgICAgICAgICAgICAuLi5Qcm9kdWN0U2ltcGxlXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGF0ZXN0IHtcclxuICAgICAgICAgICAgICAgIC4uLlByb2R1Y3RTaW1wbGVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvblNhbGUge1xyXG4gICAgICAgICAgICAgICAgLi4uUHJvZHVjdFNpbXBsZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHBvc3RzKGRlbW86ICR7Y3VycmVudERlbW8gfSwgdG86ICRwb3N0c0NvdW50KSB7XHJcbiAgICAgICAgICAgIGRhdGEge1xyXG4gICAgICAgICAgICAgICAgdGl0bGVcclxuICAgICAgICAgICAgICAgIHNsdWdcclxuICAgICAgICAgICAgICAgIGRhdGVcclxuICAgICAgICAgICAgICAgIHR5cGVcclxuICAgICAgICAgICAgICAgIGNvbW1lbnRzXHJcbiAgICAgICAgICAgICAgICBjb250ZW50XHJcbiAgICAgICAgICAgICAgICBwaWN0dXJlIHtcclxuICAgICAgICAgICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdmlkZW8ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsYXJnZV9waWN0dXJlIHtcclxuICAgICAgICAgICAgICAgICAgICB1cmxcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgJHsgUFJPRFVDVF9TSU1QTEUgfVxyXG5gXHJcbiIsImltcG9ydCB7IHBlcnNpc3RSZWR1Y2VyIH0gZnJvbSBcInJlZHV4LXBlcnNpc3RcIjtcclxuaW1wb3J0IHN0b3JhZ2UgZnJvbSAncmVkdXgtcGVyc2lzdC9saWIvc3RvcmFnZSc7XHJcbmltcG9ydCB7IHRvYXN0IH0gZnJvbSAncmVhY3QtdG9hc3RpZnknO1xyXG5pbXBvcnQgeyB0YWtlRXZlcnkgfSBmcm9tICdyZWR1eC1zYWdhL2VmZmVjdHMnO1xyXG5cclxuaW1wb3J0IENhcnRQb3B1cCBmcm9tICd+L2NvbXBvbmVudHMvZmVhdHVyZXMvcHJvZHVjdC9jb21tb24vY2FydC1wb3B1cCc7XHJcblxyXG5jb25zdCBhY3Rpb25UeXBlcyA9IHtcclxuICAgIEFERF9UT19DQVJUOiAnQUREX1RPX0NBUlQnLFxyXG4gICAgUkVNT1ZFX0ZST01fQ0FSVDogJ1JFTU9WRV9GUk9NX0NBUlQnLFxyXG4gICAgVVBEQVRFX0NBUlQ6ICdVUERBVEVfQ0FSVCcsXHJcbiAgICBSRUZSRVNIX1NUT1JFOiAnUkVGUkVTSF9TVE9SRSdcclxufVxyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xyXG4gICAgZGF0YTogW11cclxufVxyXG5cclxuZnVuY3Rpb24gY2FydFJlZHVjZXIoIHN0YXRlID0gaW5pdGlhbFN0YXRlLCBhY3Rpb24gKSB7XHJcbiAgICBzd2l0Y2ggKCBhY3Rpb24udHlwZSApIHtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkFERF9UT19DQVJUOlxyXG4gICAgICAgICAgICBsZXQgdG1wUHJvZHVjdCA9IHsgLi4uYWN0aW9uLnBheWxvYWQucHJvZHVjdCB9O1xyXG5cclxuICAgICAgICAgICAgaWYgKCBzdGF0ZS5kYXRhLmZpbmRJbmRleCggaXRlbSA9PiBpdGVtLm5hbWUgPT09IGFjdGlvbi5wYXlsb2FkLnByb2R1Y3QubmFtZSApID4gLTEgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdG1wRGF0YSA9IHN0YXRlLmRhdGEucmVkdWNlKCAoIGFjYywgY3VyICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICggY3VyLm5hbWUgPT09IHRtcFByb2R1Y3QubmFtZSApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjLnB1c2goIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLmN1cixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF0eTogcGFyc2VJbnQoIGN1ci5xdHkgKSArIHBhcnNlSW50KCB0bXBQcm9kdWN0LnF0eSApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2MucHVzaCggY3VyICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICAgICAgICAgICAgfSwgW10gKVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiB7IC4uLnN0YXRlLCBkYXRhOiB0bXBEYXRhIH07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4geyAuLi5zdGF0ZSwgZGF0YTogWyAuLi5zdGF0ZS5kYXRhLCB0bXBQcm9kdWN0IF0gfTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLlJFTU9WRV9GUk9NX0NBUlQ6XHJcbiAgICAgICAgICAgIGxldCBjYXJ0ID0gc3RhdGUuZGF0YS5yZWR1Y2UoICggY2FydEFjYywgcHJvZHVjdCApID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICggcHJvZHVjdC5uYW1lICE9PSBhY3Rpb24ucGF5bG9hZC5wcm9kdWN0Lm5hbWUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FydEFjYy5wdXNoKCBwcm9kdWN0ICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY2FydEFjYztcclxuICAgICAgICAgICAgfSwgW10gKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB7IC4uLnN0YXRlLCBkYXRhOiBjYXJ0IH07XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuVVBEQVRFX0NBUlQ6XHJcbiAgICAgICAgICAgIHJldHVybiB7IC4uLnN0YXRlLCBkYXRhOiBhY3Rpb24ucGF5bG9hZC5wcm9kdWN0cyB9O1xyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLlJFRlJFU0hfU1RPUkU6XHJcbiAgICAgICAgICAgIHJldHVybiBpbml0aWFsU3RhdGU7XHJcblxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGNhcnRBY3Rpb25zID0ge1xyXG4gICAgYWRkVG9DYXJ0OiBwcm9kdWN0ID0+ICggeyB0eXBlOiBhY3Rpb25UeXBlcy5BRERfVE9fQ0FSVCwgcGF5bG9hZDogeyBwcm9kdWN0IH0gfSApLFxyXG4gICAgcmVtb3ZlRnJvbUNhcnQ6IHByb2R1Y3QgPT4gKCB7IHR5cGU6IGFjdGlvblR5cGVzLlJFTU9WRV9GUk9NX0NBUlQsIHBheWxvYWQ6IHsgcHJvZHVjdCB9IH0gKSxcclxuICAgIHVwZGF0ZUNhcnQ6IHByb2R1Y3RzID0+ICggeyB0eXBlOiBhY3Rpb25UeXBlcy5VUERBVEVfQ0FSVCwgcGF5bG9hZDogeyBwcm9kdWN0cyB9IH0gKVxyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uKiBjYXJ0U2FnYSgpIHtcclxuICAgIHlpZWxkIHRha2VFdmVyeSggYWN0aW9uVHlwZXMuQUREX1RPX0NBUlQsIGZ1bmN0aW9uKiBzYWdhKCBlICkge1xyXG4gICAgICAgIHRvYXN0KCA8Q2FydFBvcHVwIHByb2R1Y3Q9eyBlLnBheWxvYWQucHJvZHVjdCB9IC8+ICk7XHJcbiAgICB9IClcclxufVxyXG5cclxuY29uc3QgcGVyc2lzdENvbmZpZyA9IHtcclxuICAgIGtleVByZWZpeDogXCJyaW9kZS1cIixcclxuICAgIGtleTogXCJjYXJ0XCIsXHJcbiAgICBzdG9yYWdlXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHBlcnNpc3RSZWR1Y2VyKCBwZXJzaXN0Q29uZmlnLCBjYXJ0UmVkdWNlciApOyIsImltcG9ydCB7IHBlcnNpc3RSZWR1Y2VyIH0gZnJvbSBcInJlZHV4LXBlcnNpc3RcIjtcclxuaW1wb3J0IHN0b3JhZ2UgZnJvbSAncmVkdXgtcGVyc2lzdC9saWIvc3RvcmFnZSc7XHJcblxyXG5jb25zdCBhY3Rpb25UeXBlcyA9IHtcclxuICAgIE9QRU5fTU9EQUw6ICdPUEVOX01PREFMJyxcclxuICAgIENMT1NFX01PREFMOiAnQ0xPU0VfTU9EQUwnLFxyXG4gICAgT1BFTl9RVUlDS1ZJRVc6ICdPUEVOX1FVSUNLVklFVycsXHJcbiAgICBDTE9TRV9RVUlDS1ZJRVc6ICdDTE9TRV9RVUlDS1ZJRVcnLFxyXG4gICAgUkVGUkVTSF9TVE9SRTogJ1JFRlJFU0hfU1RPUkUnXHJcbn1cclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZSA9IHtcclxuICAgIHR5cGU6ICd2aWRlbycsXHJcbiAgICBvcGVuTW9kYWw6IGZhbHNlLFxyXG4gICAgcXVpY2t2aWV3OiBmYWxzZSxcclxuICAgIHNpbmdsZVNsdWc6ICcnXHJcbn1cclxuXHJcbmZ1bmN0aW9uIG1vZGFsUmVkdWNlciggc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbiApIHtcclxuICAgIHN3aXRjaCAoIGFjdGlvbi50eXBlICkge1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuT1BFTl9RVUlDS1ZJRVc6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIHF1aWNrdmlldzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHNpbmdsZVNsdWc6IGFjdGlvbi5wYXlsb2FkLnNsdWdcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLkNMT1NFX1FVSUNLVklFVzpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgcXVpY2t2aWV3OiBmYWxzZVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuT1BFTl9NT0RBTDpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgc2luZ2xlU2x1ZzogYWN0aW9uLnBheWxvYWQuc2x1ZyxcclxuICAgICAgICAgICAgICAgIG9wZW5Nb2RhbDogdHJ1ZVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuQ0xPU0VfTU9EQUw6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIG9wZW5Nb2RhbDogZmFsc2VcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLlJFRlJFU0hfU1RPUkU6XHJcbiAgICAgICAgICAgIHJldHVybiBpbml0aWFsU3RhdGU7XHJcblxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1vZGFsQWN0aW9ucyA9IHtcclxuICAgIG9wZW5Nb2RhbDogc2x1ZyA9PiAoIHsgdHlwZTogYWN0aW9uVHlwZXMuT1BFTl9NT0RBTCwgcGF5bG9hZDogeyBzbHVnIH0gfSApLFxyXG4gICAgY2xvc2VNb2RhbDogbW9kYWxUeXBlID0+ICggeyB0eXBlOiBhY3Rpb25UeXBlcy5DTE9TRV9NT0RBTCwgcGF5bG9hZDogeyBtb2RhbFR5cGUgfSB9ICksXHJcbiAgICBvcGVuUXVpY2t2aWV3OiBzbHVnID0+ICggeyB0eXBlOiBhY3Rpb25UeXBlcy5PUEVOX1FVSUNLVklFVywgcGF5bG9hZDogeyBzbHVnIH0gfSApLFxyXG4gICAgY2xvc2VRdWlja3ZpZXc6ICgpID0+ICggeyB0eXBlOiBhY3Rpb25UeXBlcy5DTE9TRV9RVUlDS1ZJRVcgfSApXHJcbn07XHJcblxyXG5jb25zdCBwZXJzaXN0Q29uZmlnID0ge1xyXG4gICAga2V5UHJlZml4OiBcInJpb2RlLVwiLFxyXG4gICAga2V5OiBcIm1vZGFsXCIsXHJcbiAgICBzdG9yYWdlXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHBlcnNpc3RSZWR1Y2VyKCBwZXJzaXN0Q29uZmlnLCBtb2RhbFJlZHVjZXIgKTsiLCJpbXBvcnQgeyBwZXJzaXN0UmVkdWNlciB9IGZyb20gXCJyZWR1eC1wZXJzaXN0XCI7XHJcbmltcG9ydCBzdG9yYWdlIGZyb20gJ3JlZHV4LXBlcnNpc3QvbGliL3N0b3JhZ2UnO1xyXG5cclxuY29uc3QgYWN0aW9uVHlwZXMgPSB7XHJcbiAgICBUT0dHTEVfV0lTSExJU1Q6ICdUT0dHTEVfV0lTSExJU1QnLFxyXG4gICAgUkVNT1ZFX0ZST01fV0lTSExJU1Q6ICdSRU1PVkVfRlJPTV9XSVNITElTVCcsXHJcbiAgICBSRUZSRVNIX1NUT1JFOiAnUkVGUkVTSF9TVE9SRSdcclxufVxyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xyXG4gICAgZGF0YTogW11cclxufVxyXG5cclxuZnVuY3Rpb24gd2lzaGxpc3RSZWR1Y2VyKCBzdGF0ZSA9IGluaXRpYWxTdGF0ZSwgYWN0aW9uICkge1xyXG4gICAgc3dpdGNoICggYWN0aW9uLnR5cGUgKSB7XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5UT0dHTEVfV0lTSExJU1Q6XHJcbiAgICAgICAgICAgIGxldCBpbmRleCA9IHN0YXRlLmRhdGEuZmluZEluZGV4KCBpdGVtID0+IGl0ZW0ubmFtZSA9PT0gYWN0aW9uLnBheWxvYWQucHJvZHVjdC5uYW1lICk7XHJcbiAgICAgICAgICAgIGxldCB0bXBEYXRhID0gWyAuLi5zdGF0ZS5kYXRhIF07XHJcblxyXG4gICAgICAgICAgICBpZiAoIGluZGV4ID09PSAtMSApIHtcclxuICAgICAgICAgICAgICAgIHRtcERhdGEucHVzaCggYWN0aW9uLnBheWxvYWQucHJvZHVjdCApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdG1wRGF0YS5zcGxpY2UoIGluZGV4ICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiB7IC4uLnN0YXRlLCBkYXRhOiB0bXBEYXRhIH07XHJcblxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuUkVNT1ZFX0ZST01fV0lTSExJU1Q6XHJcbiAgICAgICAgICAgIGxldCB3aXNobGlzdCA9IHN0YXRlLmRhdGEucmVkdWNlKCAoIHdpc2hsaXN0QWNjLCBwcm9kdWN0ICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBwcm9kdWN0Lm5hbWUgIT09IGFjdGlvbi5wYXlsb2FkLnByb2R1Y3QubmFtZSApIHtcclxuICAgICAgICAgICAgICAgICAgICB3aXNobGlzdEFjYy5wdXNoKCBwcm9kdWN0ICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gd2lzaGxpc3RBY2M7XHJcbiAgICAgICAgICAgIH0sIFtdICk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4geyAuLi5zdGF0ZSwgZGF0YTogd2lzaGxpc3QgfTtcclxuXHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5SRUZSRVNIX1NUT1JFOlxyXG4gICAgICAgICAgICByZXR1cm4gaW5pdGlhbFN0YXRlO1xyXG5cclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHN0YXRlO1xyXG59XHJcblxyXG5leHBvcnQgY29uc3Qgd2lzaGxpc3RBY3Rpb25zID0ge1xyXG4gICAgdG9nZ2xlV2lzaGxpc3Q6IHByb2R1Y3QgPT4gKCB7IHR5cGU6IGFjdGlvblR5cGVzLlRPR0dMRV9XSVNITElTVCwgcGF5bG9hZDogeyBwcm9kdWN0IH0gfSApLFxyXG4gICAgcmVtb3ZlRnJvbVdpc2hsaXN0OiBwcm9kdWN0ID0+ICggeyB0eXBlOiBhY3Rpb25UeXBlcy5SRU1PVkVfRlJPTV9XSVNITElTVCwgcGF5bG9hZDogeyBwcm9kdWN0IH0gfSApXHJcbn07XHJcblxyXG5jb25zdCBwZXJzaXN0Q29uZmlnID0ge1xyXG4gICAga2V5UHJlZml4OiBcInJpb2RlLVwiLFxyXG4gICAga2V5OiBcIndpc2hsaXN0XCIsXHJcbiAgICBzdG9yYWdlXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHBlcnNpc3RSZWR1Y2VyKCBwZXJzaXN0Q29uZmlnLCB3aXNobGlzdFJlZHVjZXIgKTsiLCJleHBvcnQgY29uc3QgbWFpblNsaWRlcjEgPSB7XHJcbiAgICBpdGVtczogNSxcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBtYXJnaW46IDIsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzXHJcbiAgICAgICAgfSxcclxuICAgICAgICA5OTI6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDRcclxuICAgICAgICB9LFxyXG4gICAgICAgIDEyMDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDUsXHJcbiAgICAgICAgICAgIGRvdHM6IGZhbHNlXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjIgPSB7XHJcbiAgICBpdGVtczogMSxcclxuICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgbmF2OiB0cnVlLFxyXG4gICAgZG90czogdHJ1ZSxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAxXHJcbiAgICAgICAgfSxcclxuICAgICAgICA3Njg6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDJcclxuICAgICAgICB9LFxyXG4gICAgICAgIDk5Mjoge1xyXG4gICAgICAgICAgICBpdGVtczogMVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXIzID0ge1xyXG4gICAgYXV0b0hlaWdodDogZmFsc2UsXHJcbiAgICBkb3RzOiBmYWxzZSxcclxuICAgIG5hdjogdHJ1ZVxyXG4gICAgLy8gZG90c0NvbnRhaW5lcjogXCIucHJvZHVjdC10aHVtYnNcIlxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjQgPSB7XHJcbiAgICBuYXY6IGZhbHNlLFxyXG4gICAgZG90czogdHJ1ZSxcclxuICAgIGl0ZW1zOiAxLFxyXG4gICAgbWFyZ2luOiAyMCxcclxuICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgYXV0b1BsYXk6IHRydWVcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXI1ID0ge1xyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBpdGVtczogNCxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAxXHJcbiAgICAgICAgfSxcclxuICAgICAgICA1NzY6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDJcclxuICAgICAgICB9LFxyXG4gICAgICAgIDc2ODoge1xyXG4gICAgICAgICAgICBpdGVtczogM1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgOTkyOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiA0LFxyXG4gICAgICAgICAgICBkb3RzOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXI2ID0ge1xyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBpdGVtczogMyxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAxXHJcbiAgICAgICAgfSxcclxuICAgICAgICA1NzY6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDJcclxuICAgICAgICB9LFxyXG4gICAgICAgIDk5Mjoge1xyXG4gICAgICAgICAgICBpdGVtczogMyxcclxuICAgICAgICAgICAgZG90czogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBtYWluU2xpZGVyNyA9IHtcclxuICAgIGl0ZW1zOiAxLFxyXG4gICAgbmF2OiB0cnVlLFxyXG4gICAgZG90czogZmFsc2UsXHJcbiAgICBhdXRvUGxheTogdHJ1ZSxcclxuICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgbWFyZ2luOiAyMFxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjggPSB7XHJcbiAgICBpdGVtczogNixcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBkb3RzOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMCxcclxuICAgIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgcmVzcG9uc2l2ZToge1xyXG4gICAgICAgIDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDJcclxuICAgICAgICB9LFxyXG4gICAgICAgIDU3Njoge1xyXG4gICAgICAgICAgICBpdGVtczogM1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiA0XHJcbiAgICAgICAgfSxcclxuICAgICAgICA5OTI6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDVcclxuICAgICAgICB9LFxyXG4gICAgICAgIDEyMDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDZcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBtYWluU2xpZGVyOSA9IHtcclxuICAgIGl0ZW1zOiA1LFxyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBtYXJnaW46IDIwLFxyXG4gICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNTc2OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzXHJcbiAgICAgICAgfSxcclxuICAgICAgICA5OTI6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDRcclxuICAgICAgICB9LFxyXG4gICAgICAgIDEyMDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDVcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBtYWluU2xpZGVyMTAgPSB7XHJcbiAgICBpdGVtczogMSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBkb3RzOiB0cnVlLFxyXG4gICAgcmVzcG9uc2l2ZToge1xyXG4gICAgICAgIDU3Njoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjExID0ge1xyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNTc2OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAyXHJcbiAgICAgICAgfSxcclxuICAgICAgICA3Njg6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDMsXHJcbiAgICAgICAgICAgIGRvdHM6IGZhbHNlXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjEyID0ge1xyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNTc2OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAyXHJcbiAgICAgICAgfSxcclxuICAgICAgICA3Njg6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDNcclxuICAgICAgICB9LFxyXG4gICAgICAgIDk5Mjoge1xyXG4gICAgICAgICAgICBpdGVtczogNCxcclxuICAgICAgICAgICAgZG90czogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBtYWluU2xpZGVyMTMgPSB7XHJcbiAgICBpdGVtczogMyxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBkb3RzOiB0cnVlLFxyXG4gICAgcmVzcG9uc2l2ZToge1xyXG4gICAgICAgIDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDFcclxuICAgICAgICB9LFxyXG4gICAgICAgIDc2ODoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgOTkyOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzLFxyXG4gICAgICAgICAgICBkb3RzOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXIxNCA9IHtcclxuICAgIGl0ZW1zOiAyLFxyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgOTkyOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAyLFxyXG4gICAgICAgICAgICBkb3RzOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXIxNSA9IHtcclxuICAgIGl0ZW1zOiA0LFxyXG4gICAgbmF2OiB0cnVlLFxyXG4gICAgYXV0b0hlaWdodDogZmFsc2VcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXIxNiA9IHtcclxuICAgIGl0ZW1zOiA3LFxyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgbWFyZ2luOiAyMCxcclxuICAgIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgYXV0b3BsYXlUaW1lb3V0OiAzMDAwLFxyXG4gICAgbG9vcDogdHJ1ZSxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAyXHJcbiAgICAgICAgfSxcclxuICAgICAgICA1NzY6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDNcclxuICAgICAgICB9LFxyXG4gICAgICAgIDc2ODoge1xyXG4gICAgICAgICAgICBpdGVtczogNFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgOTkyOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiA1XHJcbiAgICAgICAgfSxcclxuICAgICAgICAxMjAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiA2XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjE3ID0ge1xyXG4gICAgbG9vcDogZmFsc2UsXHJcbiAgICAvLyBkb3RzOiB0cnVlLFxyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzXHJcbiAgICAgICAgfSxcclxuICAgICAgICA5OTI6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDQsXHJcbiAgICAgICAgICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgICAgICAgICBuYXY6IGZhbHNlXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLFxyXG4gICAgLy8gZG90c0NvbnRhaW5lcjogXCIucHJvZHVjdC10aHVtYnNcIlxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgbWFpblNsaWRlcjE4ID0ge1xyXG4gICAgaXRlbXM6IDMsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG5hdjogdHJ1ZSxcclxuICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgbWFyZ2luOiAyMCxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAxXHJcbiAgICAgICAgfSxcclxuICAgICAgICA1NzY6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDJcclxuICAgICAgICB9LFxyXG4gICAgICAgIDc2ODoge1xyXG4gICAgICAgICAgICBpdGVtczogM1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXIxOSA9IHtcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgYXV0b1BsYXk6IGZhbHNlLFxyXG4gICAgbWFyZ2luOiAyMCxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAyXHJcbiAgICAgICAgfSxcclxuICAgICAgICA3Njg6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDNcclxuICAgICAgICB9LFxyXG4gICAgICAgIDk5Mjoge1xyXG4gICAgICAgICAgICBpdGVtczogNFxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IG1haW5TbGlkZXIyMCA9IHtcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBkb3RzOiB0cnVlXHJcbn1cclxuXHJcbi8vIGhvbWUgcGFnZVxyXG5leHBvcnQgY29uc3QgaW50cm9TbGlkZXIgPSB7XHJcbiAgICBuYXY6IGZhbHNlLFxyXG4gICAgZG90czogdHJ1ZSxcclxuICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgaXRlbXM6IDEsXHJcbiAgICBhdXRvcGxheTogZmFsc2VcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IHNlcnZpY2VTbGlkZXIgPSB7XHJcbiAgICBpdGVtczogMyxcclxuICAgIG5hdjogZmFsc2UsXHJcbiAgICBkb3RzOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMCxcclxuICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgYXV0b3BsYXk6IGZhbHNlLFxyXG4gICAgYXV0b3BsYXlUaW1lb3V0OiA1MDAwLFxyXG4gICAgcmVzcG9uc2l2ZToge1xyXG4gICAgICAgIDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDFcclxuICAgICAgICB9LFxyXG4gICAgICAgIDU3Njoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzLFxyXG4gICAgICAgICAgICBsb29wOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGJyYW5kU2xpZGVyID0ge1xyXG4gICAgaXRlbXM6IDYsXHJcbiAgICBuYXY6IGZhbHNlLFxyXG4gICAgZG90czogZmFsc2UsXHJcbiAgICBhdXRvcGxheTogdHJ1ZSxcclxuICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgbWFyZ2luOiAyMCxcclxuICAgIHJlc3BvbnNpdmU6IHtcclxuICAgICAgICAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAyXHJcbiAgICAgICAgfSxcclxuICAgICAgICA1NzY6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDNcclxuICAgICAgICB9LFxyXG4gICAgICAgIDc2ODoge1xyXG4gICAgICAgICAgICBpdGVtczogNFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgOTkyOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiA1XHJcbiAgICAgICAgfSxcclxuICAgICAgICAxMjAwOiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiA2XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgcHJvZHVjdFNsaWRlciA9IHtcclxuICAgIGl0ZW1zOiA1LFxyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBhdXRvcGxheTogZmFsc2UsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzXHJcbiAgICAgICAgfSxcclxuICAgICAgICA5OTI6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDQsXHJcbiAgICAgICAgICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgICAgICAgICBuYXY6IHRydWVcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBwcm9kdWN0U2xpZGVyMiA9IHtcclxuICAgIGl0ZW1zOiA1LFxyXG4gICAgbmF2OiBmYWxzZSxcclxuICAgIGRvdHM6IHRydWUsXHJcbiAgICBhdXRvcGxheTogZmFsc2UsXHJcbiAgICBsb29wOiBmYWxzZSxcclxuICAgIG1hcmdpbjogMjAsXHJcbiAgICByZXNwb25zaXZlOiB7XHJcbiAgICAgICAgMDoge1xyXG4gICAgICAgICAgICBpdGVtczogMlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgNzY4OiB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAzXHJcbiAgICAgICAgfSxcclxuICAgICAgICA5OTI6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDRcclxuICAgICAgICB9LFxyXG4gICAgICAgIDEyMDA6IHtcclxuICAgICAgICAgICAgaXRlbXM6IDUsXHJcbiAgICAgICAgICAgIGRvdHM6IGZhbHNlLFxyXG4gICAgICAgICAgICBuYXY6IHRydWVcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIvKipcclxuICogdXRpbHMgdG8gcGFyc2Ugb3B0aW9ucyBzdHJpbmcgdG8gb2JqZWN0XHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBvcHRpb25zIFxyXG4gKiBAcmV0dXJuIHtvYmplY3R9XHJcbiAqL1xyXG5leHBvcnQgY29uc3QgcGFyc2VPcHRpb25zID0gZnVuY3Rpb24gKCBvcHRpb25zICkge1xyXG4gICAgaWYgKCBcInN0cmluZ1wiID09PSB0eXBlb2Ygb3B0aW9ucyApIHtcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZSggb3B0aW9ucy5yZXBsYWNlKCAvJy9nLCAnXCInICkucmVwbGFjZSggJzsnLCAnJyApICk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge307XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBkZWN0ZWN0IElFIGJyb3dzZXJcclxuICogQHJldHVybiB7Ym9vbH1cclxuICovXHJcbmV4cG9ydCBjb25zdCBpc0lFQnJvd3NlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBzVXNyQWcgPSBuYXZpZ2F0b3IudXNlckFnZW50O1xyXG4gICAgaWYgKCBzVXNyQWcuaW5kZXhPZiggXCJUcmlkZW50XCIgKSA+IC0xICkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBmYWxzZTtcclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIGRldGVjdCBzYWZhcmkgYnJvd3NlclxyXG4gKiBAcmV0dXJuIHtib29sfVxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IGlzU2FmYXJpQnJvd3NlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBzVXNyQWcgPSBuYXZpZ2F0b3IudXNlckFnZW50O1xyXG4gICAgaWYgKCBzVXNyQWcuaW5kZXhPZiggJ1NhZmFyaScgKSAhPT0gLTEgJiYgc1VzckFnLmluZGV4T2YoICdDaHJvbWUnICkgPT09IC0xIClcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIHJldHVybiBmYWxzZTtcclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIGRldGVjdCBFZGdlIGJyb3dzZXJcclxuICogQHJldHVybiB7Ym9vbH1cclxuICovXHJcbmV4cG9ydCBjb25zdCBpc0VkZ2VCcm93c2VyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IHNVc3JBZyA9IG5hdmlnYXRvci51c2VyQWdlbnQ7XHJcbiAgICBpZiAoIHNVc3JBZy5pbmRleE9mKCBcIkVkZ2VcIiApID4gLTEgKVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG59XHJcblxyXG4vKipcclxuICogdXRpbHMgdG8gZmluZCBpbmRleCBpbiBhcnJheVxyXG4gKiBAcGFyYW0ge2FycmF5fSBhcnJheVxyXG4gKiBAcGFyYW0ge2NhbGxiYWNrfSBjYlxyXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBpbmRleFxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IGZpbmRJbmRleCA9IGZ1bmN0aW9uICggYXJyYXksIGNiICkge1xyXG4gICAgZm9yICggbGV0IGkgPSAwOyBpIDwgYXJyYXkubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgaWYgKCBjYiggYXJyYXlbIGkgXSApICkge1xyXG4gICAgICAgICAgICByZXR1cm4gaTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gLTE7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBnZXQgdGhlIHBvc2l0aW9uIG9mIHRoZSBmaXJzdCBlbGVtZW50IG9mIHNlYXJjaCBhcnJheSBpbiBhcnJheVxyXG4gKiBAcGFyYW0ge2FycmF5fSBhcnJheVxyXG4gKiBAcGFyYW0ge2FycmF5fSBzZWFyY2hBcnJheVxyXG4gKiBAcGFyYW0ge2NhbGxiYWNrfSBjYlxyXG4gKiBAcmV0dXJucyB7bnVtYmVyfSBpbmRleFxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IGZpbmRBcnJheUluZGV4ID0gZnVuY3Rpb24gKCBhcnJheSwgc2VhcmNoQXJyYXksIGNiICkge1xyXG4gICAgZm9yICggbGV0IGkgPSAwOyBpIDwgc2VhcmNoQXJyYXkubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgaWYgKCBjYiggc2VhcmNoQXJyYXlbIGkgXSApICkge1xyXG4gICAgICAgICAgICByZXR1cm4gaTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gLTE7XHJcbn1cclxuXHJcblxyXG4vKipcclxuICogdXRpbHMgdG8gcmVtb3ZlIGFsbCBYU1MgIGF0dGFja3MgcG90ZW50aWFsXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBodG1sXHJcbiAqIEByZXR1cm4ge09iamVjdH1cclxuICovXHJcbmV4cG9ydCBjb25zdCBwYXJzZUNvbnRlbnQgPSAoIGh0bWwgKSA9PiB7XHJcbiAgICBjb25zdCBTQ1JJUFRfUkVHRVggPSAvPHNjcmlwdFxcYltePF0qKD86KD8hPFxcL3NjcmlwdD4pPFtePF0qKSo8XFwvc2NyaXB0Pi9naTtcclxuXHJcbiAgICAvL1JlbW92aW5nIHRoZSA8c2NyaXB0PiB0YWdzXHJcbiAgICB3aGlsZSAoIFNDUklQVF9SRUdFWC50ZXN0KCBodG1sICkgKSB7XHJcbiAgICAgICAgaHRtbCA9IGh0bWwucmVwbGFjZSggU0NSSVBUX1JFR0VYLCAnJyApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vUmVtb3ZpbmcgYWxsIGV2ZW50cyBmcm9tIHRhZ3MuLi5cclxuICAgIGh0bWwgPSBodG1sLnJlcGxhY2UoIC8gb25cXHcrPVwiW15cIl0qXCIvZywgJycgKTtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIF9faHRtbDogaHRtbFxyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogQXBwbHkgc3RpY2t5IGhlYWRlclxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHN0aWNreUhlYWRlckhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgdG9wID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ21haW4nICkgPyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnbWFpbicgKS5vZmZzZXRUb3AgOiAzMDA7XHJcblxyXG4gICAgbGV0IHN0aWNreUhlYWRlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWhlYWRlcicgKTtcclxuICAgIGxldCBoZWlnaHQgPSAwO1xyXG5cclxuICAgIGlmICggc3RpY2t5SGVhZGVyICkge1xyXG4gICAgICAgIGhlaWdodCA9IHN0aWNreUhlYWRlci5vZmZzZXRIZWlnaHQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCB3aW5kb3cucGFnZVlPZmZzZXQgPj0gdG9wICYmIHdpbmRvdy5pbm5lcldpZHRoID49IDk5MiApIHtcclxuICAgICAgICBpZiAoIHN0aWNreUhlYWRlciApIHtcclxuICAgICAgICAgICAgc3RpY2t5SGVhZGVyLmNsYXNzTGlzdC5hZGQoICdmaXhlZCcgKTtcclxuICAgICAgICAgICAgaWYgKCAhZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktd3JhcHBlcicgKSApIHtcclxuICAgICAgICAgICAgICAgIGxldCBuZXdOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCggXCJkaXZcIiApO1xyXG4gICAgICAgICAgICAgICAgbmV3Tm9kZS5jbGFzc05hbWUgPSBcInN0aWNreS13cmFwcGVyXCI7XHJcbiAgICAgICAgICAgICAgICBzdGlja3lIZWFkZXIucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoIG5ld05vZGUsIHN0aWNreUhlYWRlciApO1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktd3JhcHBlcicgKS5pbnNlcnRBZGphY2VudEVsZW1lbnQoICdiZWZvcmVlbmQnLCBzdGlja3lIZWFkZXIgKTtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LXdyYXBwZXInICkuc2V0QXR0cmlidXRlKCBcInN0eWxlXCIsIFwiaGVpZ2h0OiBcIiArIGhlaWdodCArIFwicHhcIiApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoICFkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS13cmFwcGVyJyApLmdldEF0dHJpYnV0ZSggXCJzdHlsZVwiICkgKSB7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS13cmFwcGVyJyApLnNldEF0dHJpYnV0ZSggXCJzdHlsZVwiLCBcImhlaWdodDogXCIgKyBoZWlnaHQgKyBcInB4XCIgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKCBzdGlja3lIZWFkZXIgKSB7XHJcbiAgICAgICAgICAgIHN0aWNreUhlYWRlci5jbGFzc0xpc3QucmVtb3ZlKCAnZml4ZWQnICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LXdyYXBwZXInICkgKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LXdyYXBwZXInICkucmVtb3ZlQXR0cmlidXRlKCBcInN0eWxlXCIgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCB3aW5kb3cub3V0ZXJXaWR0aCA+PSA5OTIgJiYgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ2JvZHknICkuY2xhc3NMaXN0LmNvbnRhaW5zKCAncmlnaHQtc2lkZWJhci1hY3RpdmUnICkgKSB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ2JvZHknICkuY2xhc3NMaXN0LnJlbW92ZSggJ3JpZ2h0LXNpZGViYXItYWN0aXZlJyApO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogQWRkIG9yIHJlbW92ZSBzZXR0aW5ncyB3aGVuIHRoZSB3aW5kb3cgaXMgcmVzaXplZFxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHJlc2l6ZUhhbmRsZXIgPSBmdW5jdGlvbiAoIHdpZHRoID0gOTkyLCBhdHRyaSA9ICdyaWdodC1zaWRlYmFyLWFjdGl2ZScgKSB7XHJcbiAgICBsZXQgYm9keUNsYXNzZXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcImJvZHlcIiApICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiYm9keVwiICkuY2xhc3NMaXN0O1xyXG4gICAgYm9keUNsYXNzZXMgPSBib2R5Q2xhc3Nlcy52YWx1ZS5zcGxpdCggJyAnICkuZmlsdGVyKCBpdGVtID0+IGl0ZW0gIT09ICdob21lJyAmJiBpdGVtICE9PSAnbG9hZGVkJyApO1xyXG4gICAgZm9yICggbGV0IGkgPSAwOyBpIDwgYm9keUNsYXNzZXMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCJib2R5XCIgKSAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnYm9keScgKS5jbGFzc0xpc3QucmVtb3ZlKCBib2R5Q2xhc3Nlc1sgaSBdICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBBcHBseSBzdGlja3kgZm9vdGVyXHJcbiAqL1xyXG5leHBvcnQgY29uc3Qgc3RpY2t5Rm9vdGVySGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBzdGlja3lGb290ZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1mb290ZXInICk7XHJcbiAgICBsZXQgdG9wID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ21haW4nICkgPyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnbWFpbicgKS5vZmZzZXRUb3AgOiAzMDA7XHJcblxyXG4gICAgbGV0IGhlaWdodCA9IDA7XHJcblxyXG4gICAgaWYgKCBzdGlja3lGb290ZXIgKSB7XHJcbiAgICAgICAgaGVpZ2h0ID0gc3RpY2t5Rm9vdGVyLm9mZnNldEhlaWdodDtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIHdpbmRvdy5wYWdlWU9mZnNldCA+PSB0b3AgJiYgd2luZG93LmlubmVyV2lkdGggPCA3NjggKSB7XHJcbiAgICAgICAgaWYgKCBzdGlja3lGb290ZXIgKSB7XHJcbiAgICAgICAgICAgIHN0aWNreUZvb3Rlci5jbGFzc0xpc3QuYWRkKCAnZml4ZWQnICk7XHJcbiAgICAgICAgICAgIGlmICggIWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKSApIHtcclxuICAgICAgICAgICAgICAgIGxldCBuZXdOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCggXCJkaXZcIiApO1xyXG4gICAgICAgICAgICAgICAgbmV3Tm9kZS5jbGFzc05hbWUgPSBcInN0aWNreS1jb250ZW50LXdyYXBwZXJcIjtcclxuICAgICAgICAgICAgICAgIHN0aWNreUZvb3Rlci5wYXJlbnROb2RlLmluc2VydEJlZm9yZSggbmV3Tm9kZSwgc3RpY2t5Rm9vdGVyICk7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1jb250ZW50LXdyYXBwZXInICkuaW5zZXJ0QWRqYWNlbnRFbGVtZW50KCAnYmVmb3JlZW5kJywgc3RpY2t5Rm9vdGVyICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKS5zZXRBdHRyaWJ1dGUoIFwic3R5bGVcIiwgXCJoZWlnaHQ6IFwiICsgaGVpZ2h0ICsgXCJweFwiICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoIHN0aWNreUZvb3RlciApIHtcclxuICAgICAgICAgICAgc3RpY2t5Rm9vdGVyLmNsYXNzTGlzdC5yZW1vdmUoICdmaXhlZCcgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApICkge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1jb250ZW50LXdyYXBwZXInICkucmVtb3ZlQXR0cmlidXRlKCBcInN0eWxlXCIgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCB3aW5kb3cuaW5uZXJXaWR0aCA+IDc2OCAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1jb250ZW50LXdyYXBwZXInICkgKSB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLnN0eWxlLmhlaWdodCA9ICdhdXRvJztcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIG1ha2UgYmFja2dyb3VuZCBwYXJhbGxheFxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHBhcmFsbGF4SGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBwYXJhbGxheEl0ZW1zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCggJy5wYXJhbGxheCcgKTtcclxuXHJcbiAgICBpZiAoIHBhcmFsbGF4SXRlbXMgKSB7XHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgcGFyYWxsYXhJdGVtcy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgLy8gY2FsY3VsYXRlIGJhY2tncm91bmQgeSBQb3NpdGlvbjtcclxuICAgICAgICAgICAgbGV0IHBhcmFsbGF4ID0gcGFyYWxsYXhJdGVtc1sgaSBdLCB5UG9zLCBwYXJhbGxheFNwZWVkID0gMTtcclxuXHJcbiAgICAgICAgICAgIGlmICggcGFyYWxsYXguZ2V0QXR0cmlidXRlKCAnZGF0YS1vcHRpb24nICkgKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbGxheFNwZWVkID0gcGFyc2VJbnQoIHBhcnNlT3B0aW9ucyggcGFyYWxsYXguZ2V0QXR0cmlidXRlKCAnZGF0YS1vcHRpb24nICkgKS5zcGVlZCApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB5UG9zID0gKCBwYXJhbGxheC5vZmZzZXRUb3AgLSB3aW5kb3cucGFnZVlPZmZzZXQgKSAqIDUwICogcGFyYWxsYXhTcGVlZCAvIHBhcmFsbGF4Lm9mZnNldFRvcCArIDUwO1xyXG5cclxuICAgICAgICAgICAgcGFyYWxsYXguc3R5bGUuYmFja2dyb3VuZFBvc2l0aW9uID0gXCI1MCUgXCIgKyB5UG9zICsgXCIlXCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogdXRpbHMgdG8gc2hvdyBzY3JvbGxUb3AgYnV0dG9uXHJcbiAqL1xyXG5leHBvcnQgY29uc3Qgc2hvd1Njcm9sbFRvcEhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgc2Nyb2xsVG9wID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIuc2Nyb2xsLXRvcFwiICk7XHJcblxyXG4gICAgaWYgKCB3aW5kb3cucGFnZVlPZmZzZXQgPj0gNzY4ICkge1xyXG4gICAgICAgIHNjcm9sbFRvcC5jbGFzc0xpc3QuYWRkKCBcInNob3dcIiApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBzY3JvbGxUb3AuY2xhc3NMaXN0LnJlbW92ZSggXCJzaG93XCIgKTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIHNjcm9sbCB0byB0b3BcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBzY3JvbGxUb3BIYW5kbGVyKCBpc0N1c3RvbSA9IHRydWUsIHNwZWVkID0gMTUgKSB7XHJcbiAgICBsZXQgb2Zmc2V0VG9wID0gMDtcclxuXHJcbiAgICBpZiAoIGlzQ3VzdG9tICYmICFpc0VkZ2VCcm93c2VyKCkgKSB7XHJcbiAgICAgICAgaWYgKCBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLm1haW4gLmNvbnRhaW5lciA+IC5yb3cnICkgKSB7XHJcbiAgICAgICAgICAgIG9mZnNldFRvcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcubWFpbiAuY29udGFpbmVyID4gLnJvdycgKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3AgKyB3aW5kb3cucGFnZVlPZmZzZXQgLSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1oZWFkZXInICkub2Zmc2V0SGVpZ2h0ICsgMjtcclxuICAgICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIG9mZnNldFRvcCA9IDA7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCBpc1NhZmFyaUJyb3dzZXIoKSB8fCBpc0VkZ2VCcm93c2VyKCkgKSB7XHJcbiAgICAgICAgbGV0IHBvcyA9IHdpbmRvdy5wYWdlWU9mZnNldDtcclxuICAgICAgICBsZXQgdGltZXJJZCA9IHNldEludGVydmFsKCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICggcG9zIDw9IG9mZnNldFRvcCApIGNsZWFySW50ZXJ2YWwoIHRpbWVySWQgKTtcclxuICAgICAgICAgICAgd2luZG93LnNjcm9sbEJ5KCAwLCAtc3BlZWQgKTtcclxuICAgICAgICAgICAgcG9zIC09IHNwZWVkO1xyXG4gICAgICAgIH0sIDEgKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgd2luZG93LnNjcm9sbFRvKCB7XHJcbiAgICAgICAgICAgIHRvcDogb2Zmc2V0VG9wLFxyXG4gICAgICAgICAgICBiZWhhdmlvcjogJ3Ntb290aCdcclxuICAgICAgICB9ICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBwbGF5IGFuZCBwYXVzZSB2aWRlb1xyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHZpZGVvSGFuZGxlciA9ICggZSApID0+IHtcclxuICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgaWYgKCBlLmN1cnJlbnRUYXJnZXQuY2xvc2VzdCggJy5wb3N0LXZpZGVvJyApICkge1xyXG4gICAgICAgIGxldCB2aWRlbyA9IGUuY3VycmVudFRhcmdldC5jbG9zZXN0KCAnLnBvc3QtdmlkZW8nICk7XHJcbiAgICAgICAgaWYgKCB2aWRlby5jbGFzc0xpc3QuY29udGFpbnMoICdwbGF5aW5nJyApICkge1xyXG4gICAgICAgICAgICB2aWRlby5jbGFzc0xpc3QucmVtb3ZlKCAncGxheWluZycgKTtcclxuICAgICAgICAgICAgdmlkZW8uY2xhc3NMaXN0LmFkZCggJ3BhdXNlZCcgKTtcclxuICAgICAgICAgICAgdmlkZW8ucXVlcnlTZWxlY3RvciggJ3ZpZGVvJyApLnBhdXNlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmlkZW8uY2xhc3NMaXN0LmFkZCggJ3BsYXlpbmcnICk7XHJcbiAgICAgICAgICAgIHZpZGVvLnF1ZXJ5U2VsZWN0b3IoICd2aWRlbycgKS5wbGF5KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2aWRlby5xdWVyeVNlbGVjdG9yKCAndmlkZW8nICkuYWRkRXZlbnRMaXN0ZW5lciggJ2VuZGVkJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2aWRlby5jbGFzc0xpc3QucmVtb3ZlKCAncGxheWluZycgKTtcclxuICAgICAgICAgICAgdmlkZW8uY2xhc3NMaXN0LnJlbW92ZSggJ3BhdXNlZCcgKTtcclxuICAgICAgICB9IClcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIGdldCB0b3RhbCBQcmljZSBvZiBwcm9kdWN0cyBpbiBjYXJ0LlxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IGdldFRvdGFsUHJpY2UgPSBjYXJ0SXRlbXMgPT4ge1xyXG4gICAgbGV0IHRvdGFsID0gMDtcclxuICAgIGlmICggY2FydEl0ZW1zICkge1xyXG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGNhcnRJdGVtcy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgdG90YWwgKz0gY2FydEl0ZW1zWyBpIF0ucHJpY2UgKiBwYXJzZUludCggY2FydEl0ZW1zWyBpIF0ucXR5LCAxMCApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0b3RhbDtcclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIGdldCBudW1iZXIgb2YgcHJvZHVjdHMgaW4gY2FydFxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IGdldENhcnRDb3VudCA9IGNhcnRJdGVtcyA9PiB7XHJcbiAgICBsZXQgdG90YWwgPSAwO1xyXG5cclxuICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGNhcnRJdGVtcy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICB0b3RhbCArPSBwYXJzZUludCggY2FydEl0ZW1zWyBpIF0ucXR5LCAxMCApO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0b3RhbDtcclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIHNob3cgbnVtYmVyIHRvIG4gcGxhY2VzIG9mIGRlY2ltYWxzXHJcbiAqL1xyXG5leHBvcnQgY29uc3QgdG9EZWNpbWFsID0gKCBwcmljZSwgZml4ZWRDb3VudCA9IDIgKSA9PiB7XHJcbiAgICByZXR1cm4gcHJpY2UudG9Mb2NhbGVTdHJpbmcoIHVuZGVmaW5lZCwgeyBtaW5pbXVtRnJhY3Rpb25EaWdpdHM6IGZpeGVkQ291bnQsIG1heGltdW1GcmFjdGlvbkRpZ2l0czogZml4ZWRDb3VudCB9ICk7XHJcbn0iLCIvKiAoaWdub3JlZCkgKi8iXSwic291cmNlUm9vdCI6IiJ9