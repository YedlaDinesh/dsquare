(function() {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./components/common/footer.jsx":
/*!**************************************!*\
  !*** ./components/common/footer.jsx ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Footer; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");

var _jsxFileName = "D:\\dsquare\\components\\common\\footer.jsx";

function Footer() {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("footer", {
    className: "footer",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "container",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "footer-top",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "row align-items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "col-lg-3",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
              href: "/",
              className: "logo-footer",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                src: "/images/logo.png",
                alt: "logo-footer",
                width: "154",
                height: "43"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 11,
                columnNumber: 33
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 10,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 9,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "col-lg-9",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "widget widget-newsletter form-wrapper form-wrapper-inline",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "newsletter-info mx-auto mr-lg-2 ml-lg-4",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
                  className: "widget-title",
                  children: "Subscribe to our Newsletter"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 19,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "Get all the latest information, Sales and Offers."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 20,
                  columnNumber: 37
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 18,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
                action: "#",
                className: "input-wrapper input-wrapper-inline",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                  type: "email",
                  className: "form-control",
                  name: "email",
                  id: "email",
                  placeholder: "Email address here...",
                  required: true
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 23,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
                  className: "btn btn-primary btn-rounded btn-md ml-2",
                  type: "submit",
                  children: ["subscribe", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
                    className: "d-icon-arrow-right"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 25,
                    columnNumber: 120
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 25,
                  columnNumber: 37
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 22,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 17,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 16,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 8,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "footer-middle",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "row",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "col-lg-3 col-md-6",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "widget widget-info",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
                className: "widget-title",
                children: "Contact Info"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 38,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
                className: "widget-body",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                    children: "Phone: "
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 41,
                    columnNumber: 41
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "tel:#",
                    children: "Toll Free (123) 456-7890"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 42,
                    columnNumber: 41
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 40,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                    children: "Email: "
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 45,
                    columnNumber: 41
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "mail@dsqaure.com"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 46,
                    columnNumber: 41
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 44,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                    children: "Address: "
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 49,
                    columnNumber: 41
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "123 Street Name, City, India"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 50,
                    columnNumber: 41
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 48,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                    children: "WORKING DAYS / HOURS: "
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 53,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 52,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Mon - Sun / 9:00 AM - 8:00 PM"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 56,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 55,
                  columnNumber: 37
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 39,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 36,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "col-lg-3 col-md-6",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "widget ml-lg-4",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
                className: "widget-title",
                children: "My Account"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 64,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
                className: "widget-body",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "/pages/about-us",
                    children: "About Us"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 67,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 66,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Order History"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 70,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 69,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Returns"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 73,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 72,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Custom Service"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 76,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 75,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Terms & Condition"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 79,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 78,
                  columnNumber: 37
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 65,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 63,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 62,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "col-lg-3 col-md-6",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "widget ml-lg-4",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
                className: "widget-title",
                children: "Contact Info"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 87,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
                className: "widget-body",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "/pages/login",
                    children: "Sign in"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 90,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 89,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "/pages/cart",
                    children: "View Cart"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 93,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 92,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "My Wishlist"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 96,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 95,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Track My Order"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 99,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 98,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                    href: "#",
                    children: "Help"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 102,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 101,
                  columnNumber: 37
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 88,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 86,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 85,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "col-lg-3 col-md-6",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "widget widget-instagram",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
                className: "widget-title",
                children: "Instagram"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 110,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("figure", {
                className: "widget-body row",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/01.jpg",
                    alt: "instagram 1",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 113,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 112,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/02.jpg",
                    alt: "instagram 2",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 116,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 115,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/03.jpg",
                    alt: "instagram 3",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 119,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 118,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/04.jpg",
                    alt: "instagram 4",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 122,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 121,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/05.jpg",
                    alt: "instagram 5",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 125,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 124,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/06.jpg",
                    alt: "instagram 6",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 128,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 127,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/07.jpg",
                    alt: "instagram 7",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 131,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 130,
                  columnNumber: 37
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "col-3",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                    src: "/images/instagram/08.jpg",
                    alt: "instagram 8",
                    width: "64",
                    height: "64"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 134,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 133,
                  columnNumber: 37
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 111,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 109,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 108,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "footer-bottom",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "footer-left",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("figure", {
            className: "payment",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
              src: "/images/payment.png",
              alt: "payment",
              width: "159",
              height: "29"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 145,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 143,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "footer-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "copyright ls-normal",
            children: "Dsqaure eCommerce \xA9 2021. All Rights Reserved"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 149,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 148,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "footer-right",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "social-links",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
              href: "#",
              className: "social-link social-facebook fab fa-facebook-f"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 153,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
              href: "#",
              className: "social-link social-twitter fab fa-twitter"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 154,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
              href: "#",
              className: "social-link social-linkedin fab fa-linkedin-in"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 155,
              columnNumber: 29
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 152,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 151,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 142,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 13
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 5,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/common/header.jsx":
/*!**************************************!*\
  !*** ./components/common/header.jsx ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Header; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _components_common_partials_cart_menu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/components/common/partials/cart-menu */ "./components/common/partials/cart-menu.jsx");
/* harmony import */ var _components_common_partials_main_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/common/partials/main-menu */ "./components/common/partials/main-menu.jsx");
/* harmony import */ var _components_common_partials_search_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/components/common/partials/search-box */ "./components/common/partials/search-box.jsx");
/* harmony import */ var _components_features_modals_login_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/components/features/modals/login-modal */ "./components/features/modals/login-modal.jsx");
/* harmony import */ var _utils_data_menu__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/utils/data/menu */ "./utils/data/menu.js");

var _jsxFileName = "D:\\dsquare\\components\\common\\header.jsx";








function Header(props) {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    let header = document.querySelector('header');

    if (header) {
      if (_utils_data_menu__WEBPACK_IMPORTED_MODULE_8__.headerBorderRemoveList.includes(router.pathname) && header.classList.contains('header-border')) header.classList.remove('header-border');else if (!_utils_data_menu__WEBPACK_IMPORTED_MODULE_8__.headerBorderRemoveList.includes(router.pathname)) document.querySelector('header').classList.add('header-border');
    }
  }, [router.pathname]);

  const showMobileMenu = () => {
    document.querySelector('body').classList.add('mmenu-active');
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("header", {
    className: "header header-border",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "header-top",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "container",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "header-left",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
            className: "welcome-msg",
            children: "Welcome to Dsqaure store!"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "header-right",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "dropdown",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
              href: "#",
              children: "USD"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
              className: "dropdown-box",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: "#",
                  children: "USD"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 39,
                  columnNumber: 37
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 39,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: "#",
                  children: "EUR"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 40,
                  columnNumber: 37
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 40,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 38,
              columnNumber: 29
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 36,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "dropdown ml-5",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
              href: "#",
              children: "ENG"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 45,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
              className: "dropdown-box",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: "#",
                  children: "ENG"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 48,
                  columnNumber: 37
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 47,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: "#",
                  children: "FRH"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 51,
                  columnNumber: 37
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 50,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 46,
              columnNumber: 29
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 44,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "divider"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 56,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "/pages/contact-us",
            className: "contact d-lg-show",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
              className: "d-icon-map"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 57,
              columnNumber: 87
            }, this), "Contact"]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 57,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "#",
            className: "help d-lg-show",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
              className: "d-icon-info"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 58,
              columnNumber: 68
            }, this), " Need Help"]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 58,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_modals_login_modal__WEBPACK_IMPORTED_MODULE_7__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 59,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "header-middle sticky-header fix-top sticky-content",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "container",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "header-left",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "#",
            className: "mobile-menu-toggle",
            onClick: showMobileMenu,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
              className: "d-icon-bars2"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 68,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 67,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "/",
            className: "logo",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
              src: "/images/logo.png",
              alt: "logo",
              width: "153",
              height: "44"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 72,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 71,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_partials_search_box__WEBPACK_IMPORTED_MODULE_6__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 76,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 66,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "header-right",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "tel:#",
            className: "icon-box icon-box-side",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "icon-box-icon mr-0 mr-lg-2",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
                className: "d-icon-phone"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 82,
                columnNumber: 33
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 81,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "icon-box-content d-lg-show",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
                className: "icon-box-title",
                children: "Call Us Now:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 85,
                columnNumber: 33
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                children: "0(800) 123-456"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 86,
                columnNumber: 33
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 84,
              columnNumber: 29
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 80,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "divider"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 89,
            columnNumber: 25
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_partials_cart_menu__WEBPACK_IMPORTED_MODULE_4__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 90,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 79,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "header-bottom d-lg-show",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "container",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "header-left",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_partials_main_menu__WEBPACK_IMPORTED_MODULE_5__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 98,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 97,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "header-right",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "#",
            className: "btn btn-sm btn-primary btn-rounded btn-icon-right",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
              className: "fas fa-cloud-upload-alt"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 102,
              columnNumber: 103
            }, this), "Upload"]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 102,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 101,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 96,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 29,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/common/partials/cart-menu.jsx":
/*!**************************************************!*\
  !*** ./components/common/partials/cart-menu.jsx ***!
  \**************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _store_cart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/store/cart */ "./store/cart.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");


var _jsxFileName = "D:\\dsquare\\components\\common\\partials\\cart-menu.jsx";







function CartMenu(props) {
  const {
    cartList,
    removeFromCart
  } = props;
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    hideCartMenu();
  }, [router.asPath]);

  const showCartMenu = e => {
    e.preventDefault();
    e.currentTarget.closest('.cart-dropdown').classList.add('opened');
  };

  const hideCartMenu = () => {
    if (document.querySelector('.cart-dropdown').classList.contains('opened')) document.querySelector('.cart-dropdown').classList.remove('opened');
  };

  const removeCart = item => {
    removeFromCart(item);
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "dropdown cart-dropdown type2 cart-offcanvas mr-0 mr-lg-2",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      href: "#",
      className: "cart-toggle label-block link",
      onClick: showCartMenu,
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "cart-label d-lg-show",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "cart-name",
          children: "Shopping Cart:"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "cart-price",
          children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_6__.toDecimal)((0,_utils__WEBPACK_IMPORTED_MODULE_6__.getTotalPrice)(cartList))]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-bag",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "cart-count",
          children: (0,_utils__WEBPACK_IMPORTED_MODULE_6__.getCartCount)(cartList)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 43
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "cart-overlay",
      onClick: hideCartMenu
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "dropdown-box",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "cart-header",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
          className: "cart-title",
          children: "Shopping Cart"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 45,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
          href: "#",
          className: "btn btn-dark btn-link btn-icon-right btn-close",
          onClick: hideCartMenu,
          children: ["close", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
            className: "d-icon-arrow-right"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 46,
            columnNumber: 126
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "sr-only",
            children: "Cart"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 47,
            columnNumber: 60
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 46,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 17
      }, this), cartList.length > 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "products scrollable",
          children: cartList.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "product product-cart",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("figure", {
              className: "product-media pure-media",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
                href: '/product/default/' + item.slug,
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                  src: "http://localhost:4000" + item.pictures[0].url,
                  alt: "product",
                  width: "80",
                  height: "88"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 58,
                  columnNumber: 53
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 57,
                columnNumber: 49
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
                className: "btn btn-link btn-close",
                onClick: () => {
                  removeCart(item);
                },
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
                  className: "fas fa-times"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 62,
                  columnNumber: 53
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                  className: "sr-only",
                  children: "Close"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 62,
                  columnNumber: 85
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 61,
                columnNumber: 49
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 56,
              columnNumber: 45
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "product-detail",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
                href: '/product/default/' + item.slug,
                className: "product-name",
                children: item.name
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 66,
                columnNumber: 49
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "price-box",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                  className: "product-quantity",
                  children: item.qty
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 68,
                  columnNumber: 53
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                  className: "product-price",
                  children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_6__.toDecimal)(item.price)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 69,
                  columnNumber: 53
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 67,
                columnNumber: 49
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 65,
              columnNumber: 45
            }, this)]
          }, 'cart-menu-product-' + index, true, {
            fileName: _jsxFileName,
            lineNumber: 55,
            columnNumber: 41
          }, this))
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 52,
          columnNumber: 29
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "cart-total",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
            children: "Subtotal:"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 77,
            columnNumber: 33
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "price",
            children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_6__.toDecimal)((0,_utils__WEBPACK_IMPORTED_MODULE_6__.getTotalPrice)(cartList))]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 78,
            columnNumber: 33
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 29
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "cart-action",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
            href: "/pages/cart",
            className: "btn btn-dark btn-link",
            onClick: hideCartMenu,
            children: "View Cart"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 82,
            columnNumber: 33
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
            href: "/pages/checkout",
            className: "btn btn-dark",
            onClick: hideCartMenu,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              children: "Go To Checkout"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 83,
              columnNumber: 113
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 83,
            columnNumber: 33
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 81,
          columnNumber: 29
        }, this)]
      }, void 0, true) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
        className: "mt-4 text-center font-weight-semi-bold ls-normal text-body",
        children: "No products in the cart."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 25
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 34,
    columnNumber: 9
  }, this);
}

function mapStateToProps(state) {
  return {
    cartList: state.cart.data
  };
}

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_3__.connect)(mapStateToProps, {
  removeFromCart: _store_cart__WEBPACK_IMPORTED_MODULE_5__.cartActions.removeFromCart
})(CartMenu));

/***/ }),

/***/ "./components/common/partials/footer-search-box.jsx":
/*!**********************************************************!*\
  !*** ./components/common/partials/footer-search-box.jsx ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @apollo/react-hooks */ "@apollo/react-hooks");
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-lazy-load-image-component */ "react-lazy-load-image-component");
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _server_queries__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/server/queries */ "./server/queries.js");
/* harmony import */ var _server_apollo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/server/apollo */ "./server/apollo.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");


var _jsxFileName = "D:\\dsquare\\components\\common\\partials\\footer-search-box.jsx";









function SearchForm() {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const {
    0: search,
    1: setSearch
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const [searchProducts, {
    data
  }] = (0,_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__.useLazyQuery)(_server_queries__WEBPACK_IMPORTED_MODULE_6__.GET_PRODUCTS);
  const {
    0: timer,
    1: setTimer
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    document.querySelector("body").addEventListener("click", onBodyClick);
    return () => {
      document.querySelector("body").removeEventListener("click", onBodyClick);
    };
  }, []);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    setSearch("");
  }, [router.query.slug]);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (search.length > 2) {
      if (timer) clearTimeout(timer);
      let timerId = setTimeout(() => {
        searchProducts({
          variables: {
            search: search
          }
        });
        setTimer(null);
        ;
      }, 500);
      setTimer(timerId);
    }
  }, [search]);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    document.querySelector('.header-search.show-results') && document.querySelector('.header-search.show-results').classList.remove('show-results');
  }, [router.pathname]);

  function removeXSSAttacks(html) {
    const SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi; // Removing the <script> tags

    while (SCRIPT_REGEX.test(html)) {
      html = html.replace(SCRIPT_REGEX, "");
    } // Removing all events from tags...


    html = html.replace(/ on\w+="[^"]*"/g, "");
    return {
      __html: html
    };
  }

  function matchEmphasize(name) {
    let regExp = new RegExp(search, "i");
    return name.replace(regExp, match => "<strong>" + match + "</strong>");
  }

  function onSearchClick(e) {
    e.preventDefault();
    e.stopPropagation();
    e.currentTarget.parentNode.classList.toggle('show');
  }

  function onBodyClick(e) {
    if (e.target.closest('.header-search')) return e.target.closest('.header-search').classList.contains('show-results') || e.target.closest('.header-search').classList.add('show-results');
    document.querySelector('.header-search.show') && document.querySelector('.header-search.show').classList.remove('show');
    document.querySelector('.header-search.show-results') && document.querySelector('.header-search.show-results').classList.remove('show-results');
  }

  function onSearchChange(e) {
    setSearch(e.target.value);
  }

  function onSubmitSearchForm(e) {
    e.preventDefault();
    router.push({
      pathname: '/shop',
      query: {
        search: search
      }
    });
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "header-search hs-toggle dir-up",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      href: "#",
      className: "search-toggle sticky-link",
      role: "button",
      onClick: onSearchClick,
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-search"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 101,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "Search"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
      action: "#",
      method: "get",
      onSubmit: onSubmitSearchForm,
      className: "input-wrapper",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        type: "text",
        className: "form-control",
        name: "search",
        autoComplete: "off",
        value: search,
        onChange: onSearchChange,
        placeholder: "Search...",
        required: true
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 105,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        className: "btn btn-search",
        type: "submit",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
          className: "d-icon-search"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 109,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 108,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "live-search-list bg-white",
        children: search.length > 2 && data && data.products.data.map((product, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
          href: `/product/default/${product.slug}`,
          className: "autocomplete-suggestion",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4__.LazyLoadImage, {
            effect: "opacity",
            src: "http://localhost:4000" + product.pictures[0].url,
            width: 40,
            height: 40,
            alt: "product"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 115,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "search-name",
            dangerouslySetInnerHTML: removeXSSAttacks(matchEmphasize(product.name))
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 116,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "search-price",
            children: product.price[0] !== product.price[1] ? product.variants.length === 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "new-price mr-1",
                children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[0])]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 123,
                columnNumber: 49
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "old-price",
                children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[1])]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 124,
                columnNumber: 49
              }, this)]
            }, void 0, true) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "new-price",
              children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[0]), " \u2013 $", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[1])]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 127,
              columnNumber: 45
            }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "new-price",
              children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[0])]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 128,
              columnNumber: 43
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 118,
            columnNumber: 29
          }, this)]
        }, `search-result-${index}`, true, {
          fileName: _jsxFileName,
          lineNumber: 114,
          columnNumber: 25
        }, this))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 112,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 99,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = ((0,_server_apollo__WEBPACK_IMPORTED_MODULE_7__.default)({
  ssr: true
})(SearchForm));

/***/ }),

/***/ "./components/common/partials/main-menu.jsx":
/*!**************************************************!*\
  !*** ./components/common/partials/main-menu.jsx ***!
  \**************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _utils_data_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/utils/data/menu */ "./utils/data/menu.js");
/* harmony import */ var _Api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Api */ "./components/common/Api.js");

var _jsxFileName = "D:\\dsquare\\components\\common\\partials\\main-menu.jsx";







function MainMenu() {
  const pathname = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)().pathname;
  const {
    0: allCategory,
    1: setCategory
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)([]);

  const GetAllCategory = async () => {
    const config = {
      headers: {
        accessToken: 'a'
      }
    };

    try {
      const {
        data: {
          body,
          error
        }
      } = await axios__WEBPACK_IMPORTED_MODULE_1___default().get(_Api__WEBPACK_IMPORTED_MODULE_6__.PRODUCT_CATEGORY_API, config);
      setCategory(body);
    } catch (err) {}
  }; //   console.log(allCategory,"rrrrrrrrrrrr");


  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {
    GetAllCategory();
  }, []);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("nav", {
    className: "main-nav",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
      className: "menu",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        id: "menu-home",
        className: pathname === '/' ? 'active' : '',
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
          href: "/",
          children: "Home"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        className: `submenu  ${pathname.includes('/shop') ? 'active' : ''}`,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
          href: "#",
          children: "Categories"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
          children: allCategory.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
              href: '/' + item.categoryName,
              children: [item.categoryName, item.hot ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "tip tip-hot",
                children: "Hot"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 50,
                columnNumber: 66
              }, this) : ""]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 48,
              columnNumber: 49
            }, this)
          }, `shop-${item.id}`, false, {
            fileName: _jsxFileName,
            lineNumber: 47,
            columnNumber: 45
          }, this))
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 26
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
          href: "#",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
            className: "d-icon-card"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 78,
            columnNumber: 33
          }, this), "Special Offers"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 78,
          columnNumber: 17
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
          href: "/pages/contact-us",
          children: "Contact Us"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 81,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_4__.default, {
          href: "/pages/about-us",
          children: "About Us"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 84,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 13
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 34,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (MainMenu);

/***/ }),

/***/ "./components/common/partials/mobile-menu.jsx":
/*!****************************************************!*\
  !*** ./components/common/partials/mobile-menu.jsx ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/components/features/accordion/card */ "./components/features/accordion/card.jsx");
/* harmony import */ var _utils_data_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/utils/data/menu */ "./utils/data/menu.js");

var _jsxFileName = "D:\\dsquare\\components\\common\\partials\\mobile-menu.jsx";






function MobileMenu(props) {
  const {
    0: search,
    1: setSearch
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    0: timer,
    1: setTimer
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    window.addEventListener('resize', hideMobileMenuHandler);
    document.querySelector("body").addEventListener("click", onBodyClick);
    return () => {
      window.removeEventListener('resize', hideMobileMenuHandler);
      document.querySelector("body").removeEventListener("click", onBodyClick);
    };
  }, []);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    setSearch("");
  }, [router.query.slug]);

  const hideMobileMenuHandler = () => {
    if (window.innerWidth > 991) {
      document.querySelector('body').classList.remove('mmenu-active');
    }
  };

  const hideMobileMenu = () => {
    document.querySelector('body').classList.remove('mmenu-active');
  };

  function onSearchChange(e) {
    setSearch(e.target.value);
  }

  function onBodyClick(e) {
    if (e.target.closest('.header-search')) return e.target.closest('.header-search').classList.contains('show-results') || e.target.closest('.header-search').classList.add('show-results');
    document.querySelector('.header-search.show') && document.querySelector('.header-search.show').classList.remove('show');
    document.querySelector('.header-search.show-results') && document.querySelector('.header-search.show-results').classList.remove('show-results');
  }

  function onSubmitSearchForm(e) {
    e.preventDefault();
    router.push({
      pathname: '/shop',
      query: {
        search: search
      }
    });
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "mobile-menu-wrapper",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "mobile-menu-overlay",
      onClick: hideMobileMenu
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
      className: "mobile-menu-close",
      href: "#",
      onClick: hideMobileMenu,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-times"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 86
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "mobile-menu-container scrollable",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
        action: "#",
        className: "input-wrapper",
        onSubmit: onSubmitSearchForm,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
          type: "text",
          className: "form-control",
          name: "search",
          autoComplete: "off",
          value: search,
          onChange: onSearchChange,
          placeholder: "Search your keyword...",
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
          className: "btn btn-search",
          type: "submit",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
            className: "d-icon-search"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 71,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
        className: "mobile-menu mmenu-anim",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: "/",
            children: "Home"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 77,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__.default, {
            title: "categories",
            type: "mobile",
            url: "/shop",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__.default, {
                  title: "Variations 1",
                  type: "mobile",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
                    children: _utils_data_menu__WEBPACK_IMPORTED_MODULE_5__.mainMenu.shop.variation1.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                        href: '/' + item.url,
                        children: [item.title, item.hot ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                          className: "tip tip-hot",
                          children: "Hot"
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 91,
                          columnNumber: 74
                        }, this) : ""]
                      }, void 0, true, {
                        fileName: _jsxFileName,
                        lineNumber: 89,
                        columnNumber: 57
                      }, this)
                    }, `shop-${item.title}`, false, {
                      fileName: _jsxFileName,
                      lineNumber: 88,
                      columnNumber: 53
                    }, this))
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 85,
                    columnNumber: 41
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 84,
                  columnNumber: 37
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 83,
                columnNumber: 33
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 82,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 81,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 80,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__.default, {
            title: "Pages",
            type: "mobile",
            url: "/pages/about-us",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
              children: _utils_data_menu__WEBPACK_IMPORTED_MODULE_5__.mainMenu.other.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: '/' + item.url,
                  children: [item.title, item.new ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                    className: "tip tip-new",
                    children: "New"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 114,
                    columnNumber: 62
                  }, this) : ""]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 112,
                  columnNumber: 45
                }, this)
              }, `other-${item.title}`, false, {
                fileName: _jsxFileName,
                lineNumber: 111,
                columnNumber: 41
              }, this))
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 108,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 107,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 106,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__.default, {
            title: "Blog",
            type: "mobile",
            url: "/blog/classic",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
              children: _utils_data_menu__WEBPACK_IMPORTED_MODULE_5__.mainMenu.blog.map((item, index) => item.subPages ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__.default, {
                  title: item.title,
                  url: '/' + item.url,
                  type: "mobile",
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
                    children: item.subPages.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                        href: '/' + item.url,
                        children: item.title
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 135,
                        columnNumber: 69
                      }, this)
                    }, `blog-${item.title}`, false, {
                      fileName: _jsxFileName,
                      lineNumber: 134,
                      columnNumber: 65
                    }, this))
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 131,
                    columnNumber: 53
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 130,
                  columnNumber: 49
                }, this)
              }, "blog" + item.title, false, {
                fileName: _jsxFileName,
                lineNumber: 129,
                columnNumber: 45
              }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                className: item.subPages ? "submenu" : "",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: '/' + item.url,
                  children: item.title
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 146,
                  columnNumber: 49
                }, this)
              }, "blog" + item.title, false, {
                fileName: _jsxFileName,
                lineNumber: 145,
                columnNumber: 45
              }, this))
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 125,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 124,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 123,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_accordion_card__WEBPACK_IMPORTED_MODULE_4__.default, {
            title: "elements",
            type: "mobile",
            url: "/elements",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
              children: _utils_data_menu__WEBPACK_IMPORTED_MODULE_5__.mainMenu.element.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
                  href: '/' + item.url,
                  children: item.title
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 162,
                  columnNumber: 45
                }, this)
              }, `elements-${item.title}`, false, {
                fileName: _jsxFileName,
                lineNumber: 161,
                columnNumber: 41
              }, this))
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 158,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 157,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 156,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: '/pages/account',
            children: "Login"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 174,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 174,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: '/pages/cart',
            children: "My Cart"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 175,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 175,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_3__.default, {
            href: '/pages/wishlist',
            children: "Wishlist"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 176,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 176,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 60,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(MobileMenu));

/***/ }),

/***/ "./components/common/partials/search-box.jsx":
/*!***************************************************!*\
  !*** ./components/common/partials/search-box.jsx ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @apollo/react-hooks */ "@apollo/react-hooks");
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-lazy-load-image-component */ "react-lazy-load-image-component");
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _server_queries__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/server/queries */ "./server/queries.js");
/* harmony import */ var _server_apollo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/server/apollo */ "./server/apollo.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");


var _jsxFileName = "D:\\dsquare\\components\\common\\partials\\search-box.jsx";









function SearchForm() {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const {
    0: search,
    1: setSearch
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const [searchProducts, {
    data
  }] = (0,_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__.useLazyQuery)(_server_queries__WEBPACK_IMPORTED_MODULE_6__.GET_PRODUCTS);
  const {
    0: timer,
    1: setTimer
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    document.querySelector("body").addEventListener("click", onBodyClick);
    return () => {
      document.querySelector("body").removeEventListener("click", onBodyClick);
    };
  }, []);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    setSearch("");
  }, [router.query.slug]);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (search.length > 2) {
      if (timer) clearTimeout(timer);
      let timerId = setTimeout(() => {
        searchProducts({
          variables: {
            search: search
          }
        });
        setTimer(null);
        ;
      }, 500);
      setTimer(timerId);
    }
  }, [search]);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    document.querySelector('.header-search.show-results') && document.querySelector('.header-search.show-results').classList.remove('show-results');
  }, [router.pathname]);

  function removeXSSAttacks(html) {
    const SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi; // Removing the <script> tags

    while (SCRIPT_REGEX.test(html)) {
      html = html.replace(SCRIPT_REGEX, "");
    } // Removing all events from tags...


    html = html.replace(/ on\w+="[^"]*"/g, "");
    return {
      __html: html
    };
  }

  function matchEmphasize(name) {
    let regExp = new RegExp(search, "i");
    return name.replace(regExp, match => "<strong>" + match + "</strong>");
  }

  function onSearchClick(e) {
    e.preventDefault();
    e.stopPropagation();
    e.currentTarget.parentNode.classList.toggle('show');
  }

  function onBodyClick(e) {
    if (e.target.closest('.header-search')) return e.target.closest('.header-search').classList.contains('show-results') || e.target.closest('.header-search').classList.add('show-results');
    document.querySelector('.header-search.show') && document.querySelector('.header-search.show').classList.remove('show');
    document.querySelector('.header-search.show-results') && document.querySelector('.header-search.show-results').classList.remove('show-results');
  }

  function onSearchChange(e) {
    setSearch(e.target.value);
  }

  function onSubmitSearchForm(e) {
    e.preventDefault();
    router.push({
      pathname: '/shop',
      query: {
        search: search
      }
    });
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "header-search hs-simple",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      href: "#",
      className: "search-toggle",
      role: "button",
      onClick: onSearchClick,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "icon-search-3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 100,
        columnNumber: 91
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
      action: "#",
      method: "get",
      onSubmit: onSubmitSearchForm,
      className: "input-wrapper",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        type: "text",
        className: "form-control",
        name: "search",
        autoComplete: "off",
        value: search,
        onChange: onSearchChange,
        placeholder: "Search...",
        required: true
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        className: "btn btn-search",
        type: "submit",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
          className: "d-icon-search"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 106,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 105,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "live-search-list bg-white scrollable",
        children: search.length > 2 && data && data.products.data.map((product, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
          href: `/product/default/${product.slug}`,
          className: "autocomplete-suggestion",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_4__.LazyLoadImage, {
            effect: "opacity",
            src: "http://localhost:4000" + product.pictures[0].url,
            width: 40,
            height: 40,
            alt: "product"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 112,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "search-name",
            dangerouslySetInnerHTML: removeXSSAttacks(matchEmphasize(product.name))
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 113,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "search-price",
            children: product.price[0] !== product.price[1] ? product.variants.length === 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "new-price mr-1",
                children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[0])]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 119,
                columnNumber: 49
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "old-price",
                children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[1])]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 120,
                columnNumber: 49
              }, this)]
            }, void 0, true) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "new-price",
              children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[0]), " \u2013 $", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[1])]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 123,
              columnNumber: 45
            }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "new-price",
              children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_8__.toDecimal)(product.price[0])]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 124,
              columnNumber: 43
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 114,
            columnNumber: 29
          }, this)]
        }, `search-result-${index}`, true, {
          fileName: _jsxFileName,
          lineNumber: 111,
          columnNumber: 25
        }, this))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 109,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 101,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 99,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = ((0,_server_apollo__WEBPACK_IMPORTED_MODULE_7__.default)({
  ssr: true
})(SearchForm));

/***/ }),

/***/ "./components/common/sticky-footer.jsx":
/*!*********************************************!*\
  !*** ./components/common/sticky-footer.jsx ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ StickyFooter; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _components_common_partials_footer_search_box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/components/common/partials/footer-search-box */ "./components/common/partials/footer-search-box.jsx");

var _jsxFileName = "D:\\dsquare\\components\\common\\sticky-footer.jsx";



function StickyFooter() {
  let tmp = 0;
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    window.addEventListener('scroll', stickyFooterHandler);
    return () => {
      window.removeEventListener('scroll', stickyFooterHandler);
    };
  }, []);

  const stickyFooterHandler = e => {
    let top = document.querySelector('.page-content') ? document.querySelector('.page-content').offsetTop + document.querySelector('header').offsetHeight + 100 : 600;
    let stickyFooter = document.querySelector('.sticky-footer.sticky-content');
    let height = 0;

    if (stickyFooter) {
      height = stickyFooter.offsetHeight;
    }

    if (window.pageYOffset >= top && window.innerWidth < 768 && e.currentTarget.scrollY >= tmp) {
      if (stickyFooter) {
        stickyFooter.classList.add('fixed');
        stickyFooter.setAttribute('style', "margin-bottom: 0");

        if (!document.querySelector('.sticky-content-wrapper')) {
          let newNode = document.createElement("div");
          newNode.className = "sticky-content-wrapper";
          stickyFooter.parentNode.insertBefore(newNode, stickyFooter);
          document.querySelector('.sticky-content-wrapper').insertAdjacentElement('beforeend', stickyFooter);
          document.querySelector('.sticky-content-wrapper').setAttribute("style", "height: " + height + "px");
        }

        if (!document.querySelector('.sticky-content-wrapper').getAttribute("style")) {
          document.querySelector('.sticky-content-wrapper').setAttribute("style", "height: " + height + "px");
        }
      }
    } else {
      if (stickyFooter) {
        stickyFooter.classList.remove('fixed');
        stickyFooter.setAttribute('style', `margin-bottom: -${height}px`);
      }

      if (document.querySelector('.sticky-content-wrapper')) {
        document.querySelector('.sticky-content-wrapper').removeAttribute("style");
      }
    }

    if (window.innerWidth > 767 && document.querySelector('.sticky-content-wrapper')) {
      document.querySelector('.sticky-content-wrapper').style.height = 'auto';
    }

    tmp = e.currentTarget.scrollY;
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "sticky-footer sticky-content fix-bottom",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
      href: "/",
      className: "sticky-link active",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-home"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "Home"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
      href: "/shop",
      className: "sticky-link",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-volume"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "Categories"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
      href: "/pages/wishlist",
      className: "sticky-link",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-heart"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "Wishlist"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
      href: "/pages/account",
      className: "sticky-link",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-user"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "Account"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_partials_footer_search_box__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 62,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/features/accordion/card.jsx":
/*!************************************************!*\
  !*** ./components/features/accordion/card.jsx ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Card; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var react_slide_toggle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-slide-toggle */ "react-slide-toggle");
/* harmony import */ var react_slide_toggle__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_slide_toggle__WEBPACK_IMPORTED_MODULE_3__);


var _jsxFileName = "D:\\dsquare\\components\\features\\accordion\\card.jsx";



function Card(props) {
  const {
    title,
    expanded = false,
    adClass,
    iconClass,
    type = "normal",
    url
  } = props;
  return "normal" === type ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_slide_toggle__WEBPACK_IMPORTED_MODULE_3___default()), {
    collapsed: expanded ? false : true,
    children: ({
      onToggle,
      setCollapsibleElement,
      toggleState
    }) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: `card ${adClass}`,
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: `card-header`,
        onClick: onToggle,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
          href: "#",
          className: `toggle-button ${toggleState.toLowerCase()}`,
          children: [iconClass ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
            className: iconClass
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 20,
            columnNumber: 41
          }, this) : "", title ? title : ""]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 17,
          columnNumber: 29
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 25
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        ref: setCollapsibleElement,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "card-body overflow-hidden",
          children: props.children
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 29,
          columnNumber: 29
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 28,
        columnNumber: 25
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 21
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 13
  }, this) : "parse" === type ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_slide_toggle__WEBPACK_IMPORTED_MODULE_3___default()), {
    collapsed: expanded ? false : true,
    children: ({
      onToggle,
      setCollapsibleElement,
      toggleState
    }) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
        href: url ? url : '#',
        content: title,
        className: `parse-content ${toggleState.toLowerCase()}`,
        onClick: e => {
          onToggle();
        }
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 29
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        ref: setCollapsibleElement,
        className: "overflow-hidden",
        children: props.children
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 49,
        columnNumber: 29
      }, this)]
    }, void 0, true)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 38,
    columnNumber: 17
  }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_slide_toggle__WEBPACK_IMPORTED_MODULE_3___default()), {
    collapsed: expanded ? false : true,
    children: ({
      onToggle,
      setCollapsibleElement,
      toggleState
    }) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_2__.default, {
        href: url ? url : "#",
        children: [title, /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: `toggle-btn ${toggleState.toLowerCase()}`,
          onClick: e => {
            onToggle();
            e.preventDefault();
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 62,
          columnNumber: 33
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 29
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        ref: setCollapsibleElement,
        className: "overflow-hidden",
        children: props.children
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 29
      }, this)]
    }, void 0, true)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 55,
    columnNumber: 17
  }, this);
  return '';
}

/***/ }),

/***/ "./components/features/countdown.jsx":
/*!*******************************************!*\
  !*** ./components/features/countdown.jsx ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ ProductCountDown; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-countdown */ "react-countdown");
/* harmony import */ var react_countdown__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_countdown__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "D:\\dsquare\\components\\features\\countdown.jsx";

function ProductCountDown(props) {
  const {
    date = "2021-08-20",
    type = 1,
    adClass = ''
  } = props;

  const renderer = ({
    days,
    hours,
    minutes,
    seconds,
    completed
  }) => {
    if (completed) {
      return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "Product Selling Finished!"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 20
      }, this);
    } else {
      return type === 1 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: `countdown ${adClass}`,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "countdown-row countdown-show4",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: (0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(days)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 15,
              columnNumber: 33
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-period",
              children: "DAYS"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 16,
              columnNumber: 33
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 14,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: (0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(hours)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 20,
              columnNumber: 33
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-period",
              children: "HOURS"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 21,
              columnNumber: 33
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: (0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(minutes)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 25,
              columnNumber: 33
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-period",
              children: "MINUTES"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 26,
              columnNumber: 33
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 24,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: (0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(seconds)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 29,
              columnNumber: 33
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-period",
              children: "SECONDS"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 30,
              columnNumber: 33
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 28,
            columnNumber: 29
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 13,
          columnNumber: 25
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 21
      }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "product-countdown-container font-weight-semi-bold",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "product-countdown-title",
          children: "Offer Ends In:\xA0"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 25
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "product-countdown countdown-compact",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section days",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: [(0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(days), " "]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 40,
              columnNumber: 33
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-period",
              children: "days,\xA0"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 41,
              columnNumber: 33
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section hours",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: [(0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(hours), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "mr-1 ml-1",
                children: ":"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 45,
                columnNumber: 88
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 45,
              columnNumber: 33
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 44,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section minutes",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: [(0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(minutes), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "mr-1 ml-1",
                children: ":"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 49,
                columnNumber: 90
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 49,
              columnNumber: 33
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 48,
            columnNumber: 29
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "countdown-section seconds",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
              className: "countdown-amount",
              children: (0,react_countdown__WEBPACK_IMPORTED_MODULE_1__.zeroPad)(seconds)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 53,
              columnNumber: 33
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 52,
            columnNumber: 29
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 25
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 21
      }, this);
    }
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_countdown__WEBPACK_IMPORTED_MODULE_1___default()), {
    date: new Date(date),
    renderer: renderer
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 62,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/features/modals/login-modal.jsx":
/*!****************************************************!*\
  !*** ./components/features/modals/login-modal.jsx ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_tabs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-tabs */ "react-tabs");
/* harmony import */ var react_tabs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_tabs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-modal */ "react-modal");
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _components_common_Api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/components/common/Api */ "./components/common/Api.js");


var _jsxFileName = "D:\\dsquare\\components\\features\\modals\\login-modal.jsx";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


 // import { useHistory } from 'react-router'





const customStyles = {
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    display: "flex"
  }
};
let index = 0;
react_modal__WEBPACK_IMPORTED_MODULE_4___default().setAppElement("#__next");

function LoginModal() {
  const {
    0: open,
    1: setOpen
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const {
    0: phoneNumber,
    1: setPhone
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');
  const {
    0: step,
    1: setStep
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
  const {
    0: OTP,
    1: setOtp
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');
  const {
    0: vertifyOtp,
    1: setvertifyData
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(''); // const history = useHistory();
  // Getting form value here

  const {
    0: form,
    1: setForm
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({
    userName: "",
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    age: ""
  });
  const {
    0: formAddress,
    1: setAddressForm
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({
    addressId: "",
    name: "",
    locality: "",
    area: "",
    street: "",
    pincodeId: "",
    cityId: "",
    stateId: "",
    addressType: ""
  });

  function closeModal() {
    document.querySelector(".ReactModal__Overlay").classList.add('removed');
    document.querySelector(".login-popup.ReactModal__Content").classList.remove("ReactModal__Content--after-open");
    document.querySelector(".login-popup-overlay.ReactModal__Overlay").classList.remove("ReactModal__Overlay--after-open");
    setTimeout(() => {
      setOpen(false);
    }, 330);
  }

  function openModal(e, loginIndex = 0) {
    e.preventDefault();
    index = loginIndex;
    setOpen(true);
  }

  const config = {
    headers: {
      accessToken: 'a'
    }
  }; // Only numbers allowed

  const handlePhoneChange = e => {
    const value = e.target.value.replace(/\D/g, "");
    setPhone(value);
  }; // Send otp 


  const sendOtp = e => {
    e.preventDefault();
    const bodyParameters = {
      phoneNumber: phoneNumber,
      deviceId: "314145165185"
    };
    axios__WEBPACK_IMPORTED_MODULE_2___default().post(_components_common_Api__WEBPACK_IMPORTED_MODULE_6__.SENDOTP_API, bodyParameters, config).then(response => {
      setStep(step + 1);
    }, error => {});
  }; // Verify OTp


  const verifyOtp = e => {
    e.preventDefault();
    const bodyParameters = {
      phoneNumber: phoneNumber,
      otp: parseInt(OTP),
      deviceId: "314145165185"
    };
    axios__WEBPACK_IMPORTED_MODULE_2___default().post(_components_common_Api__WEBPACK_IMPORTED_MODULE_6__.VERIFY_API, bodyParameters, config).then(response => {
      setvertifyData(response.data);
      localStorage.setItem('session_id', response.data.token);
      setStep(step + 1);
    }, error => {});
  }; //   Get all form fields at once


  const handleChange = e => {
    setForm(_objectSpread(_objectSpread({}, form), {}, {
      [e.target.name]: e.target.value
    }));
  };

  const handleAddress = e => {
    setAddressForm(_objectSpread(_objectSpread({}, formAddress), {}, {
      [e.target.name]: e.target.value
    }));
  }; // Create or register account 


  const createAccount = e => {
    e.preventDefault();
    const config = {
      headers: {
        Authorization: `${vertifyOtp.token}`
      }
    };
    const bodyParameters = {
      userName: form.userName,
      firstName: form.firstName,
      lastName: form.lastName,
      email: form.email,
      password: form.password,
      age: parseInt(form.age)
    };
    axios__WEBPACK_IMPORTED_MODULE_2___default().post(_components_common_Api__WEBPACK_IMPORTED_MODULE_6__.SIGNUP_API, bodyParameters, config).then(response => {
      setStep(step + 1);
    }, error => {});
  }; //  Address Field enter 


  const addAddress = e => {
    e.preventDefault();
    const config = {
      headers: {
        Authorization: `${vertifyOtp.token}`
      }
    };
    const bodyParameters = {
      addressId: formAddress.addressId,
      name: formAddress.name,
      locality: formAddress.locality,
      area: formAddress.area,
      street: formAddress.street,
      pincodeId: parseInt(formAddress.pincodeId),
      cityId: parseInt(formAddress.cityId),
      stateId: parseInt(formAddress.stateId),
      addressType: formAddress.addressType
    };
    axios__WEBPACK_IMPORTED_MODULE_2___default().post(_components_common_Api__WEBPACK_IMPORTED_MODULE_6__.ADD_ADDRESS_API, bodyParameters, config).then(response => {
      console.log(response, "aaaaaaaaaaa");
      setStep(step + 1);
    }, error => {});
  };

  const tabScreen = () => {
    switch (step) {
      case 1:
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.TabList, {
            className: "nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.Tab, {
              className: "nav-item",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "nav-link border-no lh-1 ls-normal",
                children: "Dsquare"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 208,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 207,
              columnNumber: 13
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 206,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
            action: "#",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group mb-3",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                className: "form-control",
                name: "phone_number",
                id: "phone_number",
                maxlength: "19",
                type: "text",
                placeholder: "Enter Phone Number *",
                value: phoneNumber,
                onChange: handlePhoneChange,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 214,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 213,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
              className: "btn btn-dark btn-block btn-rounded",
              type: "submit",
              onClick: sendOtp,
              children: "Send OTP"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 217,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 212,
            columnNumber: 13
          }, this)]
        }, void 0, true);

      case 2:
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.TabList, {
            className: "nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.Tab, {
              className: "nav-item",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "nav-link border-no lh-1 ls-normal",
                children: "Enter Code"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 226,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 225,
              columnNumber: 13
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 224,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
            action: "#",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group mb-3",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                className: "form-control",
                name: "otp_number",
                id: "otp_number",
                maxlength: "6",
                type: "text",
                placeholder: "Enter OTP Number *",
                value: OTP,
                onChange: e => setOtp(e.target.value),
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 233,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 232,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
              className: "btn btn-dark btn-block btn-rounded",
              type: "submit",
              onClick: verifyOtp,
              children: "Verify OTP"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 236,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 231,
            columnNumber: 13
          }, this)]
        }, void 0, true);

      case 3:
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.TabList, {
            className: "nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.Tab, {
              className: "nav-item",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "nav-link border-no lh-1 ls-normal",
                children: "Create your profile"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 245,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 244,
              columnNumber: 13
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 243,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
            action: "#",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Username:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 252,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "userName",
                name: "userName",
                placeholder: "Enter username *",
                value: form.userName,
                onChange: handleChange,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 253,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 251,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "First Name:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 256,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "firstName",
                name: "firstName",
                placeholder: "Enter first name",
                value: form.firstName,
                onChange: handleChange
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 257,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 255,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Last Name:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 260,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "lastName",
                name: "lastName",
                placeholder: "Enter last name",
                value: form.lastName,
                onChange: handleChange
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 261,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 259,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Your email address:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 264,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "email",
                className: "form-control",
                id: "email",
                name: "email",
                placeholder: "Your Email address *",
                value: form.email,
                onChange: handleChange,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 265,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 263,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "Password:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 268,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "password",
                className: "form-control",
                id: "password",
                name: "password",
                placeholder: "Password *",
                value: form.password,
                onChange: handleChange,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 269,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 267,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "Password:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 272,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "number",
                className: "form-control",
                id: "age",
                name: "age",
                placeholder: "age",
                value: form.age,
                onChange: handleChange
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 273,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 271,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-footer",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "form-checkbox",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                  type: "checkbox",
                  className: "custom-checkbox",
                  id: "register-agree",
                  name: "register-agree",
                  required: true
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 278,
                  columnNumber: 21
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                  className: "form-control-label",
                  htmlFor: "register-agree",
                  children: "I agree to the privacy policy"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 280,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 277,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 276,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
              className: "btn btn-dark btn-block btn-rounded",
              type: "submit",
              onClick: createAccount,
              children: "Create account"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 283,
              columnNumber: 13
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 250,
            columnNumber: 13
          }, this)]
        }, void 0, true);

      case 4:
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.TabList, {
            className: "nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.Tab, {
              className: "nav-item",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                className: "nav-link border-no lh-1 ls-normal",
                children: "Add your address"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 292,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 291,
              columnNumber: 13
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 290,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
            action: "#",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Address ID:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 299,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "addressId",
                name: "addressId",
                placeholder: "Enter address id *",
                value: formAddress.addressId,
                onChange: handleAddress,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 300,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 298,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Name:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 303,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "name",
                name: "name",
                placeholder: "Enter name",
                value: formAddress.name,
                onChange: handleAddress
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 304,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 302,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Locality:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 307,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "locality",
                name: "locality",
                placeholder: "Enter your locality",
                value: formAddress.locality,
                onChange: handleAddress
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 308,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 306,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-email",
                children: "Area:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 311,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "area",
                name: "area",
                placeholder: "Enter your area *",
                value: formAddress.area,
                onChange: handleAddress,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 312,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 310,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "street:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 315,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "street",
                name: "street",
                placeholder: "street ",
                value: formAddress.street,
                onChange: handleAddress
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 316,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 314,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "Pincode ID:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 319,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "number",
                className: "form-control",
                id: "pincodeId",
                name: "pincodeId",
                placeholder: "Pincode Id *",
                value: formAddress.pincodeId,
                onChange: handleAddress,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 320,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 318,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "City ID:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 324,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "number",
                className: "form-control",
                id: "cityId",
                name: "cityId",
                placeholder: "City Id",
                value: formAddress.cityId,
                onChange: handleAddress
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 325,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 323,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "State ID:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 328,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "number",
                className: "form-control",
                id: "stateId",
                name: "stateId",
                placeholder: "State Id",
                value: formAddress.stateId,
                onChange: handleAddress
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 329,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 327,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-group",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                htmlFor: "singin-password",
                children: "Address Type:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 332,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                type: "text",
                className: "form-control",
                id: "addressType",
                name: "addressType",
                placeholder: "Address Type",
                value: formAddress.addressType,
                onChange: handleAddress
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 333,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 331,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "form-footer",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "form-checkbox",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
                  type: "checkbox",
                  className: "custom-checkbox",
                  id: "register-agree",
                  name: "register-agree",
                  required: true
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 338,
                  columnNumber: 21
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                  className: "form-control-label",
                  htmlFor: "register-agree",
                  children: "I agree to the privacy policy"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 340,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 337,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 336,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
              className: "btn btn-dark btn-block btn-rounded",
              type: "submit",
              onClick: addAddress,
              children: "Add Address"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 343,
              columnNumber: 13
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 297,
            columnNumber: 13
          }, this)]
        }, void 0, true);

      default:
        return 'foo';
    }
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      className: "login-link d-lg-show",
      href: "#",
      onClick: openModal,
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-user"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 359,
        columnNumber: 17
      }, this), "Sign in"]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 358,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
      className: "delimiter",
      children: "/"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 360,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      className: "register-link ml-0",
      onClick: e => openModal(e, 1),
      href: "#",
      children: "Register"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 361,
      columnNumber: 13
    }, this), open ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_modal__WEBPACK_IMPORTED_MODULE_4___default()), {
      isOpen: open,
      onRequestClose: closeModal,
      style: customStyles,
      contentLabel: "Login Modal",
      className: "login-popup",
      overlayClassName: "login-popup-overlay",
      shouldReturnFocusAfterClose: false,
      id: "login-modal",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "form-box",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "tab tab-nav-simple tab-nav-boxed form-tab",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_tabs__WEBPACK_IMPORTED_MODULE_3__.Tabs, {
            selectedTabClassName: "active",
            selectedTabPanelClassName: "active",
            defaultIndex: index,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "tab-content",
              children: tabScreen()
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 379,
              columnNumber: 37
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 377,
            columnNumber: 33
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 376,
          columnNumber: 29
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 375,
        columnNumber: 25
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        title: "Close (Esc)",
        type: "button",
        className: "mfp-close",
        onClick: closeModal,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          children: "\xD7"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 388,
          columnNumber: 112
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 388,
        columnNumber: 25
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 365,
      columnNumber: 21
    }, this) : '']
  }, void 0, true);
}

/* harmony default export */ __webpack_exports__["default"] = (LoginModal);

/***/ }),

/***/ "./components/features/modals/video-modal.jsx":
/*!****************************************************!*\
  !*** ./components/features/modals/video-modal.jsx ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-modal */ "react-modal");
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _store_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/store/modal */ "./store/modal.js");

var _jsxFileName = "D:\\dsquare\\components\\features\\modals\\video-modal.jsx";





const customStyles = {
  content: {
    position: "relative"
  },
  overlay: {
    background: 'rgba(0,0,0,.4)',
    overflowX: 'hidden',
    display: 'flex',
    overflowY: 'auto',
    opacity: 0
  }
};
react_modal__WEBPACK_IMPORTED_MODULE_4___default().setAppElement('#__next');

function VideoModal(props) {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();
  const {
    isOpen,
    closeModal,
    singleSlug
  } = props;
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    closeModal();
    router.events.on('routeChangeStart', closeModal);
    return () => {
      router.events.off('routeChangeStart', closeModal);
    };
  }, []);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (isOpen) setTimeout(() => {
      document.querySelector(".ReactModal__Overlay").classList.add('opened');
    }, 100);
  }, [isOpen]);

  const closeVideo = () => {
    document.querySelector(".ReactModal__Overlay").classList.add('removed');
    document.querySelector(".ReactModal__Overlay").classList.remove('opened');
    document.querySelector(".video-modal.ReactModal__Content").classList.remove("ReactModal__Content--after-open");
    document.querySelector(".video-modal-overlay.ReactModal__Overlay").classList.remove("ReactModal__Overlay--after-open");
    setTimeout(() => {
      closeModal();
    }, 330);
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_modal__WEBPACK_IMPORTED_MODULE_4___default()), {
    isOpen: isOpen,
    contentLabel: "VideoModal",
    onRequestClose: closeVideo,
    shouldFocusAfterRender: false,
    style: customStyles,
    overlayClassName: "video-modal-overlay",
    className: "row video-modal",
    id: "video-modal",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("video", {
      src: "http://localhost:4000" + singleSlug,
      autoPlay: true,
      loop: true,
      controls: true,
      className: "p-0"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
      title: "Close (Esc)",
      type: "button",
      className: "mfp-close",
      onClick: closeModal,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        children: "\xD7"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 101
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 56,
    columnNumber: 9
  }, this);
}

function mapStateToProps(state) {
  return {
    isOpen: state.modal.openModal,
    singleSlug: state.modal.singleSlug
  };
}

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_2__.connect)(mapStateToProps, {
  closeModal: _store_modal__WEBPACK_IMPORTED_MODULE_5__.modalActions.closeModal
})(VideoModal));

/***/ }),

/***/ "./components/features/product/common/quickview-modal.jsx":
/*!****************************************************************!*\
  !*** ./components/features/product/common/quickview-modal.jsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @apollo/react-hooks */ "@apollo/react-hooks");
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_image_magnifiers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-image-magnifiers */ "react-image-magnifiers");
/* harmony import */ var react_image_magnifiers__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_image_magnifiers__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-modal */ "react-modal");
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! imagesloaded */ "imagesloaded");
/* harmony import */ var imagesloaded__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(imagesloaded__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _server_queries__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/server/queries */ "./server/queries.js");
/* harmony import */ var _server_apollo__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/server/apollo */ "./server/apollo.js");
/* harmony import */ var _components_features_owl_carousel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ~/components/features/owl-carousel */ "./components/features/owl-carousel.jsx");
/* harmony import */ var _components_partials_product_detail_detail_one__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ~/components/partials/product/detail/detail-one */ "./components/partials/product/detail/detail-one.jsx");
/* harmony import */ var _store_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ~/store/modal */ "./store/modal.js");
/* harmony import */ var _utils_data_carousel__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ~/utils/data/carousel */ "./utils/data/carousel.js");


var _jsxFileName = "D:\\dsquare\\components\\features\\product\\common\\quickview-modal.jsx";












const customStyles = {
  content: {
    position: "relative"
  },
  overlay: {
    background: 'rgba(0,0,0,.4)',
    zIndex: '10000',
    overflowX: 'hidden',
    overflowY: 'auto'
  }
};
react_modal__WEBPACK_IMPORTED_MODULE_5___default().setAppElement('#__next');

function Quickview(props) {
  const {
    slug,
    closeQuickview,
    isOpen
  } = props;
  if (!isOpen) return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {}, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 36,
    columnNumber: 27
  }, this);
  const {
    0: loaded,
    1: setLoadingState
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const {
    data,
    loading
  } = (0,_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_3__.useQuery)(_server_queries__WEBPACK_IMPORTED_MODULE_7__.GET_PRODUCT, {
    variables: {
      slug,
      onlyData: true
    }
  });
  const product = data && data.product;
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    setTimeout(() => {
      if (!loading && data && isOpen && document.querySelector('.quickview-modal')) imagesloaded__WEBPACK_IMPORTED_MODULE_6___default()('.quickview-modal').on('done', function () {
        setLoadingState(true);
        window.jQuery('.quickview-modal .product-single-carousel').trigger('refresh.owl.carousel');
      }).on('progress', function () {
        setLoadingState(false);
      });
    }, 200);
  }, [data, isOpen]);
  if (slug === '' || !product || !product.data) return '';

  const closeQuick = () => {
    document.querySelector(".ReactModal__Overlay").classList.add('removed');
    document.querySelector('.quickview-modal').classList.add('removed');
    setLoadingState(false);
    setTimeout(() => {
      closeQuickview();
    }, 330);
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_modal__WEBPACK_IMPORTED_MODULE_5___default()), {
    isOpen: isOpen,
    contentLabel: "QuickView",
    onRequestClose: closeQuick,
    shouldFocusAfterRender: false,
    style: customStyles,
    className: "product product-single row product-popup quickview-modal",
    id: "product-quickview",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: `row p-0 m-0 ${loaded ? '' : 'd-none'}`,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "col-md-6",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "product-gallery mb-md-0 pb-0",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "product-label-group",
              children: [product.data.is_new ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                className: "product-label label-new",
                children: "New"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 80,
                columnNumber: 57
              }, this) : '', product.data.is_top ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                className: "product-label label-top",
                children: "Top"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 81,
                columnNumber: 57
              }, this) : '', product.data.discount > 0 ? product.data.variants.length === 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                className: "product-label label-sale",
                children: [product.data.discount, "% OFF"]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 85,
                columnNumber: 45
              }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
                className: "product-label label-sale",
                children: "Sale"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 86,
                columnNumber: 47
              }, this) : '']
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 79,
              columnNumber: 29
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_owl_carousel__WEBPACK_IMPORTED_MODULE_9__.default, {
              adClass: "product-single-carousel owl-theme owl-nav-inner",
              options: _utils_data_carousel__WEBPACK_IMPORTED_MODULE_12__.mainSlider3,
              children: product && product.data && product.data.large_pictures.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_image_magnifiers__WEBPACK_IMPORTED_MODULE_4__.Magnifier, {
                imageSrc: "http://localhost:4000" + item.url,
                imageAlt: "magnifier",
                largeImageSrc: "http://localhost:4000" + item.url,
                dragToMove: false,
                mouseActivation: "hover",
                cursorStyleActive: "crosshair",
                className: "product-image large-image"
              }, 'quickview-image-' + index, false, {
                fileName: _jsxFileName,
                lineNumber: 94,
                columnNumber: 41
              }, this))
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 91,
              columnNumber: 29
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 78,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "col-md-6",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_product_detail_detail_one__WEBPACK_IMPORTED_MODULE_10__.default, {
            data: data,
            adClass: "scrollable pr-3",
            isNav: false
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 111,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 110,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        title: "Close (Esc)",
        type: "button",
        className: "mfp-close p-0",
        onClick: closeQuick,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          children: "\xD7"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 115,
          columnNumber: 109
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 115,
        columnNumber: 17
      }, this)]
    }, void 0, true), loaded ? '' : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "product row p-0 m-0 skeleton-body mfp-product",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "col-md-6",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "skel-pro-gallery"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 120,
          columnNumber: 25
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 119,
        columnNumber: 21
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "col-md-6",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "skel-pro-summary"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 125,
          columnNumber: 25
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 124,
        columnNumber: 21
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 31
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 67,
    columnNumber: 9
  }, this);
}

function mapStateToProps(state) {
  return {
    slug: state.modal.singleSlug,
    isOpen: state.modal.quickview
  };
}

/* harmony default export */ __webpack_exports__["default"] = ((0,_server_apollo__WEBPACK_IMPORTED_MODULE_8__.default)({
  ssr: true
})((0,react_redux__WEBPACK_IMPORTED_MODULE_2__.connect)(mapStateToProps, {
  closeQuickview: _store_modal__WEBPACK_IMPORTED_MODULE_11__.modalActions.closeQuickview
})(Quickview)));

/***/ }),

/***/ "./components/features/quantity.jsx":
/*!******************************************!*\
  !*** ./components/features/quantity.jsx ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Quantity; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "D:\\dsquare\\components\\features\\quantity.jsx";

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }


function Quantity(_ref) {
  let {
    qty = 1
  } = _ref,
      props = _objectWithoutProperties(_ref, ["qty"]);

  const {
    adClass = 'mr-2 input-group'
  } = props;
  const {
    0: quantity,
    1: setQuantity
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(parseInt(qty));
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    setQuantity(qty);
  }, [props.product]);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    props.onChangeQty && props.onChangeQty(quantity);
  }, [quantity]);

  function minusQuantity() {
    if (quantity > 1) {
      setQuantity(parseInt(quantity) - 1);
    }
  }

  function plusQuantity() {
    if (quantity < props.max) {
      setQuantity(parseInt(quantity) + 1);
    }
  }

  function changeQty(e) {
    let newQty;

    if (e.currentTarget.value !== '') {
      newQty = Math.min(parseInt(e.currentTarget.value), props.max);
      newQty = Math.max(newQty, 1);
      setQuantity(newQty);
    }
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: adClass,
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
      className: "quantity-minus d-icon-minus",
      onClick: minusQuantity
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
      className: "quantity form-control",
      type: "number",
      min: "1",
      max: props.max,
      value: quantity,
      onChange: changeQty
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
      className: "quantity-plus d-icon-plus",
      onClick: plusQuantity
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 38,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/layout.jsx":
/*!*******************************!*\
  !*** ./components/layout.jsx ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_toastify_dist_ReactToastify_min_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-toastify/dist/ReactToastify.min.css */ "./node_modules/react-toastify/dist/ReactToastify.min.css");
/* harmony import */ var react_toastify_dist_ReactToastify_min_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_toastify_dist_ReactToastify_min_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_image_lightbox_style_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-image-lightbox/style.css */ "./node_modules/react-image-lightbox/style.css");
/* harmony import */ var react_image_lightbox_style_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_image_lightbox_style_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_input_range_lib_css_index_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-input-range/lib/css/index.css */ "./node_modules/react-input-range/lib/css/index.css");
/* harmony import */ var react_input_range_lib_css_index_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_input_range_lib_css_index_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _components_common_header__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ~/components/common/header */ "./components/common/header.jsx");
/* harmony import */ var _components_common_footer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ~/components/common/footer */ "./components/common/footer.jsx");
/* harmony import */ var _components_common_sticky_footer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ~/components/common/sticky-footer */ "./components/common/sticky-footer.jsx");
/* harmony import */ var _components_features_product_common_quickview_modal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ~/components/features/product/common/quickview-modal */ "./components/features/product/common/quickview-modal.jsx");
/* harmony import */ var _components_features_modals_video_modal__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ~/components/features/modals/video-modal */ "./components/features/modals/video-modal.jsx");
/* harmony import */ var _components_common_partials_mobile_menu__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ~/components/common/partials/mobile-menu */ "./components/common/partials/mobile-menu.jsx");
/* harmony import */ var _store_modal__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ~/store/modal */ "./store/modal.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");


var _jsxFileName = "D:\\dsquare\\components\\layout.jsx";

















function Layout({
  children,
  closeQuickview
}) {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_4__.useRouter)();
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useLayoutEffect)(() => {
    document.querySelector('body') && document.querySelector('body').classList.remove('loaded');
  }, [router.pathname]);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    window.addEventListener('scroll', _utils__WEBPACK_IMPORTED_MODULE_16__.showScrollTopHandler, true);
    window.addEventListener('scroll', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyHeaderHandler, true);
    window.addEventListener('scroll', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyFooterHandler, true);
    window.addEventListener('resize', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyHeaderHandler);
    window.addEventListener('resize', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyFooterHandler);
    window.addEventListener('resize', _utils__WEBPACK_IMPORTED_MODULE_16__.resizeHandler);
    return () => {
      window.removeEventListener('scroll', _utils__WEBPACK_IMPORTED_MODULE_16__.showScrollTopHandler, true);
      window.removeEventListener('scroll', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyHeaderHandler, true);
      window.removeEventListener('scroll', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyFooterHandler, true);
      window.removeEventListener('resize', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyHeaderHandler);
      window.removeEventListener('resize', _utils__WEBPACK_IMPORTED_MODULE_16__.stickyFooterHandler);
      window.removeEventListener('resize', _utils__WEBPACK_IMPORTED_MODULE_16__.resizeHandler);
    };
  }, []);
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    closeQuickview();
    let bodyClasses = [...document.querySelector("body").classList];

    for (let i = 0; i < bodyClasses.length; i++) {
      document.querySelector('body').classList.remove(bodyClasses[i]);
    }

    setTimeout(() => {
      document.querySelector('body').classList.add('loaded');
    }, 50);
  }, [router.pathname]);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "page-wrapper",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_header__WEBPACK_IMPORTED_MODULE_9__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 17
      }, this), children, /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_footer__WEBPACK_IMPORTED_MODULE_10__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_sticky_footer__WEBPACK_IMPORTED_MODULE_11__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_8__.default, {
      id: "scroll-top",
      href: "#",
      title: "Top",
      role: "button",
      className: "scroll-top",
      onClick: () => (0,_utils__WEBPACK_IMPORTED_MODULE_16__.scrollTopHandler)(false),
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
        className: "d-icon-arrow-up"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 138
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_common_partials_mobile_menu__WEBPACK_IMPORTED_MODULE_14__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_toastify__WEBPACK_IMPORTED_MODULE_3__.ToastContainer, {
      autoClose: 3000,
      duration: 300,
      newestOnTo: true,
      className: "toast-container",
      position: "bottom-left",
      closeButton: false,
      hideProgressBar: true,
      newestOnTop: true
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_product_common_quickview_modal__WEBPACK_IMPORTED_MODULE_12__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_modals_video_modal__WEBPACK_IMPORTED_MODULE_13__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 13
    }, this)]
  }, void 0, true);
}

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_2__.connect)(null, {
  closeQuickview: _store_modal__WEBPACK_IMPORTED_MODULE_15__.modalActions.closeQuickview
})(Layout));

/***/ }),

/***/ "./components/partials/product/detail/detail-one.jsx":
/*!***********************************************************!*\
  !*** ./components/partials/product/detail/detail-one.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/collapse */ "react-bootstrap/collapse");
/* harmony import */ var react_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var _components_features_countdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/components/features/countdown */ "./components/features/countdown.jsx");
/* harmony import */ var _components_features_quantity__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/components/features/quantity */ "./components/features/quantity.jsx");
/* harmony import */ var _components_partials_product_product_nav__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/components/partials/product/product-nav */ "./components/partials/product/product-nav.jsx");
/* harmony import */ var _store_wishlist__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ~/store/wishlist */ "./store/wishlist.js");
/* harmony import */ var _store_cart__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ~/store/cart */ "./store/cart.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");


var _jsxFileName = "D:\\dsquare\\components\\partials\\product\\detail\\detail-one.jsx";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }













function DetailOne(props) {
  let router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const {
    data,
    isStickyCart = false,
    adClass = '',
    isNav = true
  } = props;
  const {
    toggleWishlist,
    addToCart,
    wishlist
  } = props;
  const {
    0: curColor,
    1: setCurColor
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)('null');
  const {
    0: curSize,
    1: setCurSize
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)('null');
  const {
    0: curIndex,
    1: setCurIndex
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(-1);
  const {
    0: cartActive,
    1: setCartActive
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false);
  const {
    0: quantity,
    1: setQauntity
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(1);
  let product = data && data.product; // decide if the product is wishlisted

  let isWishlisted,
      colors = [],
      sizes = [];
  isWishlisted = wishlist.findIndex(item => item.slug === product.data.slug) > -1 ? true : false;

  if (product.data && product.data.variants.length > 0) {
    if (product.data.variants[0].size) product.data.variants.forEach(item => {
      if (sizes.findIndex(size => size.name === item.size.name) === -1) {
        sizes.push({
          name: item.size.name,
          value: item.size.size
        });
      }
    });

    if (product.data.variants[0].color) {
      product.data.variants.forEach(item => {
        if (colors.findIndex(color => color.name === item.color.name) === -1) colors.push({
          name: item.color.name,
          value: item.color.color
        });
      });
    }
  }

  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {
    return () => {
      setCurIndex(-1);
      resetValueHandler();
    };
  }, [product]);
  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {
    if (product.data.variants.length > 0) {
      if (curSize !== 'null' && curColor !== 'null' || curSize === 'null' && product.data.variants[0].size === null && curColor !== 'null' || curColor === 'null' && product.data.variants[0].color === null && curSize !== 'null') {
        setCartActive(true);
        setCurIndex(product.data.variants.findIndex(item => item.size !== null && item.color !== null && item.color.name === curColor && item.size.name === curSize || item.size === null && item.color.name === curColor || item.color === null && item.size.name === curSize));
      } else {
        setCartActive(false);
      }
    } else {
      setCartActive(true);
    }

    if (product.stock === 0) {
      setCartActive(false);
    }
  }, [curColor, curSize, product]);

  const wishlistHandler = e => {
    e.preventDefault();

    if (toggleWishlist && !isWishlisted) {
      let currentTarget = e.currentTarget;
      currentTarget.classList.add('load-more-overlay', 'loading');
      toggleWishlist(product.data);
      setTimeout(() => {
        currentTarget.classList.remove('load-more-overlay', 'loading');
      }, 1000);
    } else {
      router.push('/pages/wishlist');
    }
  };

  const setColorHandler = e => {
    setCurColor(e.target.value);
  };

  const setSizeHandler = e => {
    setCurSize(e.target.value);
  };

  const addToCartHandler = () => {
    if (product.data.stock > 0 && cartActive) {
      if (product.data.variants.length > 0) {
        let tmpName = product.data.name,
            tmpPrice;
        tmpName += curColor !== 'null' ? '-' + curColor : '';
        tmpName += curSize !== 'null' ? '-' + curSize : '';

        if (product.data.price[0] === product.data.price[1]) {
          tmpPrice = product.data.price[0];
        } else if (!product.data.variants[0].price && product.data.discount > 0) {
          tmpPrice = product.data.price[0];
        } else {
          tmpPrice = product.data.variants[curIndex].sale_price ? product.data.variants[curIndex].sale_price : product.data.variants[curIndex].price;
        }

        addToCart(_objectSpread(_objectSpread({}, product.data), {}, {
          name: tmpName,
          qty: quantity,
          price: tmpPrice
        }));
      } else {
        addToCart(_objectSpread(_objectSpread({}, product.data), {}, {
          qty: quantity,
          price: product.data.price[0]
        }));
      }
    }
  };

  const resetValueHandler = e => {
    setCurColor('null');
    setCurSize('null');
  };

  function isDisabled(color, size) {
    if (color === 'null' || size === 'null') return false;

    if (sizes.length === 0) {
      return product.data.variants.findIndex(item => item.color.name === curColor) === -1;
    }

    if (colors.length === 0) {
      return product.data.variants.findIndex(item => item.size.name === curSize) === -1;
    }

    return product.data.variants.findIndex(item => item.color.name === color && item.size.name === size) === -1;
  }

  function changeQty(qty) {
    setQauntity(qty);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "product-details " + adClass,
    children: [isNav ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "product-navigation",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
        className: "breadcrumb breadcrumb-lg",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
            href: "/",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
              className: "d-icon-home"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 147,
              columnNumber: 49
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 147,
            columnNumber: 33
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 147,
          columnNumber: 29
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
            href: "#",
            className: "active",
            children: "Products"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 148,
            columnNumber: 33
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 148,
          columnNumber: 29
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: "Detail"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 149,
          columnNumber: 29
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 146,
        columnNumber: 25
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_partials_product_product_nav__WEBPACK_IMPORTED_MODULE_8__.default, {
        product: product
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 152,
        columnNumber: 25
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 145,
      columnNumber: 21
    }, this) : '', /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
      className: "product-name",
      children: product.data.name
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 156,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "product-meta",
      children: ["SKU: ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        className: "product-sku",
        children: product.data.sku
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 159,
        columnNumber: 22
      }, this), "CATEGORIES: ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        className: "product-brand",
        children: product.data.categories.map((item, index) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react__WEBPACK_IMPORTED_MODULE_3___default().Fragment), {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
            href: {
              pathname: '/shop',
              query: {
                category: item.slug
              }
            },
            children: item.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 164,
            columnNumber: 33
          }, this), index < product.data.categories.length - 1 ? ', ' : '']
        }, item.name + '-' + index, true, {
          fileName: _jsxFileName,
          lineNumber: 163,
          columnNumber: 29
        }, this))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 160,
        columnNumber: 29
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 158,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "product-price mb-2",
      children: product.data.price[0] !== product.data.price[1] ? product.data.variants.length === 0 || product.data.variants.length > 0 && !product.data.variants[0].price ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
          className: "new-price",
          children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[0])]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 178,
          columnNumber: 33
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("del", {
          className: "old-price",
          children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[1])]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 179,
          columnNumber: 33
        }, this)]
      }, void 0, true) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("del", {
        className: "new-price",
        children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[0]), " \u2013 $", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[1])]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 182,
        columnNumber: 29
      }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
        className: "new-price",
        children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[0])]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 183,
        columnNumber: 27
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 173,
      columnNumber: 13
    }, this), product.data.price[0] !== product.data.price[1] && product.data.variants.length === 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_countdown__WEBPACK_IMPORTED_MODULE_6__.default, {
      type: 2
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 189,
      columnNumber: 21
    }, this) : '', /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "ratings-container",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "ratings-full",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "ratings",
          style: {
            width: 20 * product.data.ratings + '%'
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 194,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "tooltiptext tooltip-top",
          children: (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.ratings)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 195,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 193,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
        href: "#",
        className: "rating-reviews",
        children: ["( ", product.data.reviews, " reviews )"]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 198,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 192,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
      className: "product-short-desc",
      children: product.data.short_description
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 201,
      columnNumber: 13
    }, this), product && product.data.variants.length > 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [product.data.variants[0].color ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "product-form product-variations product-color",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
          children: "Color:"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 209,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "select-box",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("select", {
            name: "color",
            className: "form-control select-color",
            onChange: setColorHandler,
            value: curColor,
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("option", {
              value: "null",
              children: "Choose an option"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 212,
              columnNumber: 45
            }, this), colors.map(item => !isDisabled(item.name, curSize) ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("option", {
              value: item.name,
              children: item.name
            }, "color-" + item.name, false, {
              fileName: _jsxFileName,
              lineNumber: 216,
              columnNumber: 57
            }, this) : '')]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 211,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 210,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 208,
        columnNumber: 33
      }, this) : "", product.data.variants[0].size ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "product-form product-variations product-size mb-0 pb-2",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
          children: "Size:"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 227,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "product-form-group",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "select-box",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("select", {
              name: "size",
              className: "form-control select-size",
              onChange: setSizeHandler,
              value: curSize,
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("option", {
                value: "null",
                children: "Choose an option"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 231,
                columnNumber: 49
              }, this), sizes.map(item => !isDisabled(curColor, item.name) ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("option", {
                value: item.name,
                children: item.name
              }, "size-" + item.name, false, {
                fileName: _jsxFileName,
                lineNumber: 235,
                columnNumber: 61
              }, this) : '')]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 230,
              columnNumber: 45
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 229,
            columnNumber: 41
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_4___default()), {
            in: 'null' !== curColor || 'null' !== curSize,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "card-wrapper overflow-hidden reset-value-button w-100 mb-0",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
                href: "#",
                className: "product-variation-clean",
                onClick: resetValueHandler,
                children: "Clean All"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 243,
                columnNumber: 49
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 242,
              columnNumber: 45
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 241,
            columnNumber: 41
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 228,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 226,
        columnNumber: 33
      }, this) : "", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "product-variation-price",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_4___default()), {
          in: cartActive && curIndex > -1,
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "card-wrapper",
            children: curIndex > -1 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "single-product-price",
              children: product.data.variants[curIndex].price ? product.data.variants[curIndex].sale_price ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "product-price mb-0",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
                  className: "new-price",
                  children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.variants[curIndex].sale_price)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 260,
                  columnNumber: 65
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("del", {
                  className: "old-price",
                  children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.variants[curIndex].price)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 261,
                  columnNumber: 65
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 259,
                columnNumber: 61
              }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "product-price mb-0",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
                  className: "new-price",
                  children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.variants[curIndex].price)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 264,
                  columnNumber: 65
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 263,
                columnNumber: 63
              }, this) : ""
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 255,
              columnNumber: 45
            }, this) : ''
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 252,
            columnNumber: 33
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 251,
          columnNumber: 29
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 250,
        columnNumber: 25
      }, this)]
    }, void 0, true) : '', /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("hr", {
      className: "product-divider"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 276,
      columnNumber: 13
    }, this), isStickyCart ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "sticky-content fix-top product-sticky-content",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "container",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "sticky-product-details",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("figure", {
            className: "product-image",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
              href: '/product/default/' + product.data.slug,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                src: "http://localhost:4000" + product.data.pictures[0].url,
                width: "90",
                height: "90",
                alt: "Product"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 285,
                columnNumber: 41
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 284,
              columnNumber: 37
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 283,
            columnNumber: 33
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
              className: "product-title",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
                href: '/product/default/' + product.data.slug,
                children: product.data.name
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 290,
                columnNumber: 67
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 290,
              columnNumber: 37
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
              className: "product-info",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "product-price mb-0",
                children: curIndex > -1 && product.data.variants[0] ? product.data.variants[curIndex].price ? product.data.variants[curIndex].sale_price ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
                    className: "new-price",
                    children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.variants[curIndex].sale_price)]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 298,
                    columnNumber: 65
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("del", {
                    className: "old-price",
                    children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.variants[curIndex].price)]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 299,
                    columnNumber: 65
                  }, this)]
                }, void 0, true) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
                    className: "new-price",
                    children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.variants[curIndex].price)]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 303,
                    columnNumber: 65
                  }, this)
                }, void 0, false) : "" : product.data.price[0] !== product.data.price[1] ? product.data.variants.length === 0 ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
                    className: "new-price",
                    children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[0])]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 310,
                    columnNumber: 65
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("del", {
                    className: "old-price",
                    children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[1])]
                  }, void 0, true, {
                    fileName: _jsxFileName,
                    lineNumber: 311,
                    columnNumber: 65
                  }, this)]
                }, void 0, true) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("del", {
                  className: "new-price",
                  children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[0]), " \u2013 $", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[1])]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 314,
                  columnNumber: 61
                }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ins", {
                  className: "new-price",
                  children: ["$", (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.price[0])]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 315,
                  columnNumber: 59
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 292,
                columnNumber: 41
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "ratings-container mb-0",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                  className: "ratings-full",
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                    className: "ratings",
                    style: {
                      width: 20 * product.data.ratings + '%'
                    }
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 321,
                    columnNumber: 49
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                    className: "tooltiptext tooltip-top",
                    children: (0,_utils__WEBPACK_IMPORTED_MODULE_11__.toDecimal)(product.data.ratings)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 322,
                    columnNumber: 49
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 320,
                  columnNumber: 45
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
                  href: "#",
                  className: "rating-reviews",
                  children: ["( ", product.data.reviews, " reviews )"]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 325,
                  columnNumber: 45
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 319,
                columnNumber: 41
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 291,
              columnNumber: 37
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 289,
            columnNumber: 33
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 282,
          columnNumber: 29
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "product-form product-qty pb-0",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
            className: "d-none",
            children: "QTY:"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 331,
            columnNumber: 33
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "product-form-group",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_quantity__WEBPACK_IMPORTED_MODULE_7__.default, {
              max: product.data.stock,
              product: product,
              onChangeQty: changeQty
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 333,
              columnNumber: 37
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
              className: `btn-product btn-cart text-normal ls-normal font-weight-semi-bold ${cartActive ? '' : 'disabled'}`,
              onClick: addToCartHandler,
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
                className: "d-icon-bag"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 334,
                columnNumber: 189
              }, this), "Add to Cart"]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 334,
              columnNumber: 37
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 332,
            columnNumber: 33
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 330,
          columnNumber: 29
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 281,
        columnNumber: 25
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 280,
      columnNumber: 21
    }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "product-form product-qty pb-0",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("label", {
        className: "d-none",
        children: "QTY:"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 341,
        columnNumber: 25
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "product-form-group",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_quantity__WEBPACK_IMPORTED_MODULE_7__.default, {
          max: product.data.stock,
          product: product,
          onChangeQty: changeQty
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 343,
          columnNumber: 29
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
          className: `btn-product btn-cart text-normal ls-normal font-weight-semi-bold ${cartActive ? '' : 'disabled'}`,
          onClick: addToCartHandler,
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
            className: "d-icon-bag"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 344,
            columnNumber: 181
          }, this), "Add to Cart"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 344,
          columnNumber: 29
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 342,
        columnNumber: 25
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 340,
      columnNumber: 21
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("hr", {
      className: "product-divider mb-3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 349,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "product-footer",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "social-links mr-4",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
          href: "#",
          className: "social-link social-facebook fab fa-facebook-f"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 353,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
          href: "#",
          className: "social-link social-twitter fab fa-twitter"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 354,
          columnNumber: 21
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_5__.default, {
          href: "#",
          className: "social-link social-pinterest fab fa-pinterest-p"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 355,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 352,
        columnNumber: 17
      }, this), " ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
        className: "divider d-lg-show"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 356,
        columnNumber: 24
      }, this), " ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
        href: "#",
        className: `btn-product btn-wishlist`,
        title: isWishlisted ? 'Browse wishlist' : 'Add to wishlist',
        onClick: wishlistHandler,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
          className: isWishlisted ? "d-icon-heart-full" : "d-icon-heart"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 357,
          columnNumber: 21
        }, this), " ", isWishlisted ? 'Browse wishlist' : 'Add to Wishlist']
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 356,
        columnNumber: 68
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 351,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 142,
    columnNumber: 9
  }, this);
}

function mapStateToProps(state) {
  return {
    wishlist: state.wishlist.data ? state.wishlist.data : []
  };
}

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_1__.connect)(mapStateToProps, {
  toggleWishlist: _store_wishlist__WEBPACK_IMPORTED_MODULE_9__.wishlistActions.toggleWishlist,
  addToCart: _store_cart__WEBPACK_IMPORTED_MODULE_10__.cartActions.addToCart
})(DetailOne));

/***/ }),

/***/ "./components/partials/product/product-nav.jsx":
/*!*****************************************************!*\
  !*** ./components/partials/product/product-nav.jsx ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ ProductNav; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ~/components/features/custom-link */ "./components/features/custom-link.jsx");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "D:\\dsquare\\components\\partials\\product\\product-nav.jsx";


function ProductNav(props) {
  const {
    product
  } = props;
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
    className: "product-nav",
    children: [product.prev ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
      className: `product-nav-${product.next ? 'prev' : 'next no-next'}`,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
        href: {
          pathname: router.pathname,
          query: {
            slug: product.prev.slug
          }
        },
        scroll: false,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
          className: "d-icon-arrow-left"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 15,
          columnNumber: 29
        }, this), " Prev", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "product-nav-popup",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            src: "http://localhost:4000" + product.prev.pictures[0].url,
            alt: "product thumbnail",
            width: "110",
            height: "123"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 17,
            columnNumber: 33
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "product-name",
            children: product.prev.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 33
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 14,
        columnNumber: 25
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 21
    }, this) : "", product.next ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
      className: "product-nav-next",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_features_custom_link__WEBPACK_IMPORTED_MODULE_1__.default, {
        href: {
          pathname: router.pathname,
          query: {
            slug: product.next.slug
          }
        },
        scroll: false,
        children: ["Next ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("i", {
          className: "d-icon-arrow-right"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 29,
          columnNumber: 34
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
          className: "product-nav-popup",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            src: "http://localhost:4000" + product.next.pictures[0].url,
            alt: "product thumbnail",
            width: "110",
            height: "123"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 31,
            columnNumber: 33
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "product-name",
            children: product.next.name
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 33
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 30,
          columnNumber: 29
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 28,
        columnNumber: 25
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 21
    }, this) : ""]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 10,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var redux_persist_integration_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! redux-persist/integration/react */ "redux-persist/integration/react");
/* harmony import */ var redux_persist_integration_react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(redux_persist_integration_react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-helmet */ "react-helmet");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/components/layout */ "./components/layout.jsx");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/store */ "./store/index.js");
/* harmony import */ var _store_demo__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ~/store/demo */ "./store/demo.js");
/* harmony import */ var _server_queries__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ~/server/queries */ "./server/queries.js");
/* harmony import */ var _public_sass_style_scss__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ~/public/sass/style.scss */ "./public/sass/style.scss");
/* harmony import */ var _public_sass_style_scss__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_public_sass_style_scss__WEBPACK_IMPORTED_MODULE_10__);

var _jsxFileName = "D:\\dsquare\\pages\\_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












const App = ({
  Component,
  pageProps,
  store
}) => {
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (store.getState().demo.current !== _server_queries__WEBPACK_IMPORTED_MODULE_9__.currentDemo) {
      store.dispatch(_store_demo__WEBPACK_IMPORTED_MODULE_8__.demoActions.refreshStore(_server_queries__WEBPACK_IMPORTED_MODULE_9__.currentDemo));
    }
  }, []);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_3__.Provider, {
    store: store,
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(redux_persist_integration_react__WEBPACK_IMPORTED_MODULE_4__.PersistGate, {
      persistor: store.__persistor,
      loading: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "loading-overlay",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "bounce-loader",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "bounce1"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 29,
            columnNumber: 25
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "bounce2"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 30,
            columnNumber: 25
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "bounce3"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 31,
            columnNumber: 25
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "bounce4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 25
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 28,
          columnNumber: 21
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 26
      }, undefined),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react_helmet__WEBPACK_IMPORTED_MODULE_5___default()), {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("meta", {
          charSet: "UTF-8"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("meta", {
          "http-equiv": "X-UA-Compatible",
          content: "IE=edge"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("meta", {
          name: "viewport",
          content: "width=device-width, initial-scale=1, shrink-to-fit=no"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("title", {
          children: "Riode - React eCommerce Template"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("meta", {
          name: "keywords",
          content: "React Template"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 42,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("meta", {
          name: "description",
          content: "Riode - React eCommerce Template"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("meta", {
          name: "author",
          content: "D-THEMES"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 21
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 17
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_layout__WEBPACK_IMPORTED_MODULE_6__.default, {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, _objectSpread({}, pageProps), void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 48,
          columnNumber: 21
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 17
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 13
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 24,
    columnNumber: 9
  }, undefined);
};

App.getInitialProps = async ({
  Component,
  ctx
}) => {
  let pageProps = {};

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  return {
    pageProps
  };
};

/* harmony default export */ __webpack_exports__["default"] = (next_redux_wrapper__WEBPACK_IMPORTED_MODULE_2___default()(_store__WEBPACK_IMPORTED_MODULE_7__.default)(App));

/***/ }),

/***/ "./store/demo.js":
/*!***********************!*\
  !*** ./store/demo.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "actionTypes": function() { return /* binding */ actionTypes; },
/* harmony export */   "demoActions": function() { return /* binding */ demoActions; }
/* harmony export */ });
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-persist */ "redux-persist");
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_persist__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-persist/lib/storage */ "redux-persist/lib/storage");
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const actionTypes = {
  RefreshStore: "REFRESH_STORE"
};
let initialState = {
  current: 1
};

const demoReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.RefreshStore:
      return _objectSpread(_objectSpread({}, state), {}, {
        current: action.payload.current
      });

    default:
      return state;
  }
};

const demoActions = {
  refreshStore: current => ({
    type: actionTypes.RefreshStore,
    payload: {
      current
    }
  })
};
const persistConfig = {
  keyPrefix: "riode-",
  key: "demo",
  storage: (redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default())
};
/* harmony default export */ __webpack_exports__["default"] = ((0,redux_persist__WEBPACK_IMPORTED_MODULE_0__.persistReducer)(persistConfig, demoReducer));

/***/ }),

/***/ "./store/index.js":
/*!************************!*\
  !*** ./store/index.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga */ "redux-saga");
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-persist */ "redux-persist");
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_persist__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _store_root_saga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ~/store/root-saga */ "./store/root-saga.js");
/* harmony import */ var _store_cart__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ~/store/cart */ "./store/cart.js");
/* harmony import */ var _store_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ~/store/modal */ "./store/modal.js");
/* harmony import */ var _store_wishlist__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ~/store/wishlist */ "./store/wishlist.js");
/* harmony import */ var _store_demo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ~/store/demo */ "./store/demo.js");








const sagaMiddleware = redux_saga__WEBPACK_IMPORTED_MODULE_1___default()();
const rootReducers = (0,redux__WEBPACK_IMPORTED_MODULE_0__.combineReducers)({
  cart: _store_cart__WEBPACK_IMPORTED_MODULE_4__.default,
  modal: _store_modal__WEBPACK_IMPORTED_MODULE_5__.default,
  wishlist: _store_wishlist__WEBPACK_IMPORTED_MODULE_6__.default,
  demo: _store_demo__WEBPACK_IMPORTED_MODULE_7__.default
});

const makeStore = (initialState, options) => {
  const store = (0,redux__WEBPACK_IMPORTED_MODULE_0__.createStore)(rootReducers, (0,redux__WEBPACK_IMPORTED_MODULE_0__.applyMiddleware)(sagaMiddleware));
  store.sagaTask = sagaMiddleware.run(_store_root_saga__WEBPACK_IMPORTED_MODULE_3__.default); // store.sagaTask = sagaMiddleware.run( rootSaga );

  store.__persistor = (0,redux_persist__WEBPACK_IMPORTED_MODULE_2__.persistStore)(store);
  return store;
};

/* harmony default export */ __webpack_exports__["default"] = (makeStore);

/***/ }),

/***/ "./store/root-saga.js":
/*!****************************!*\
  !*** ./store/root-saga.js ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ rootSaga; }
/* harmony export */ });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store_cart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ~/store/cart */ "./store/cart.js");


function* rootSaga() {
  yield (0,redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__.all)([(0,_store_cart__WEBPACK_IMPORTED_MODULE_1__.cartSaga)()]);
}

/***/ }),

/***/ "./utils/data/menu.js":
/*!****************************!*\
  !*** ./utils/data/menu.js ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "mainMenu": function() { return /* binding */ mainMenu; },
/* harmony export */   "elementsList": function() { return /* binding */ elementsList; },
/* harmony export */   "headerBorderRemoveList": function() { return /* binding */ headerBorderRemoveList; }
/* harmony export */ });
const mainMenu = {
  "shop": {
    "variation1": [{
      "title": "Infinite Ajaxscroll",
      "url": "shop/infinite-scroll"
    }]
  },
  "product": {
    "pages": [{
      "title": "Simple Product",
      "url": "product/default/fashionable-overnight-bag"
    }],
    "layout": [{
      "title": "Grid Images",
      "url": "product/grid/fashionable-leather-satchel",
      "new": true
    }]
  },
  "other": [{
    "title": "About",
    "url": "pages/about-us"
  }, {
    "title": "Contact Us",
    "url": "pages/contact-us"
  }, {
    "title": "My Account",
    "url": "pages/account"
  }, {
    "title": "FAQs",
    "url": "pages/faqs"
  }, {
    "title": "Error 404",
    "url": "pages/404"
  }, {
    "title": "Coming Soon",
    "url": "pages/coming-soon"
  }],
  "blog": [{
    "title": "Classic",
    "url": "blog/classic"
  }, {
    "title": "Listing",
    "url": "blog/listing"
  }, {
    "title": "Grid",
    "url": "blog/grid/2cols",
    "subPages": [{
      "title": "Grid 2 columns",
      "url": "blog/grid/2cols"
    }, {
      "title": "Grid 3 columns",
      "url": "blog/grid/3cols"
    }, {
      "title": "Grid 4 columns",
      "url": "blog/grid/4cols"
    }, {
      "title": "Grid sidebar",
      "url": "blog/grid/sidebar"
    }]
  }, {
    "title": "Masonry",
    "url": "blog/masonry/2cols",
    "subPages": [{
      "title": "Masonry 2 columns",
      "url": "blog/masonry/2cols"
    }, {
      "title": "Masonry 3 columns",
      "url": "blog/masonry/3cols"
    }, {
      "title": "Masonry 4 columns",
      "url": "blog/masonry/4cols"
    }, {
      "title": "Masonry sidebar",
      "url": "blog/masonry/sidebar"
    }]
  }, {
    "title": "Mask",
    "url": "blog/mask/grid",
    "subPages": [{
      "title": "Blog mask grid",
      "url": "blog/mask/grid"
    }, {
      "title": "Blog mask masonry",
      "url": "blog/mask/masonry"
    }]
  }, {
    "title": "Single Post",
    "url": "blog/single/pellentesque-fusce-suscipit"
  }],
  "element": [{
    "title": "Products",
    "url": "elements/products"
  }, {
    "title": "Typography",
    "url": "elements/typography"
  }, {
    "title": "Titles",
    "url": "elements/titles"
  }, {
    "title": "Product Category",
    "url": "elements/product-category"
  }, {
    "title": "Buttons",
    "url": "elements/buttons"
  }, {
    "title": "Accordions",
    "url": "elements/accordions"
  }, {
    "title": "Alert & Notification",
    "url": "elements/alerts"
  }, {
    "title": "Tabs",
    "url": "elements/tabs"
  }, {
    "title": "Testimonials",
    "url": "elements/testimonials"
  }, {
    "title": "Blog Posts",
    "url": "elements/blog-posts"
  }, {
    "title": "Instagrams",
    "url": "elements/instagrams"
  }, {
    "title": "Call to Action",
    "url": "elements/cta"
  }, {
    "title": "Icon Boxes",
    "url": "elements/icon-boxes"
  }, {
    "title": "Icons",
    "url": "elements/icons"
  }]
};
const elementsList = [{
  "url": "accordions",
  "class": "element-accordian",
  "title": "accordions"
}, {
  "url": "blog-posts",
  "class": "element-blog",
  "title": "blog posts"
}, {
  "url": "buttons",
  "class": "element-button",
  "title": "buttons"
}, {
  "url": "cta",
  "class": "element-cta",
  "title": "call to action"
}, {
  "url": "icon-boxes",
  "class": "element-icon-box",
  "title": "icon boxes"
}, {
  "url": "icons",
  "class": "element-icon",
  "title": "Icons"
}, {
  "url": "instagrams",
  "class": "element-portfolio",
  "title": "instagrams"
}, {
  "url": "categories",
  "class": "element-category",
  "title": "product categories"
}, {
  "url": "products",
  "class": "element-product",
  "title": "products"
}, {
  "url": "tabs",
  "class": "element-tab",
  "title": "tabs"
}, {
  "url": "testimonials",
  "class": "element-testimonial",
  "title": "testimonials"
}, {
  "url": "titles",
  "class": "element-title",
  "title": "titles"
}, {
  "url": "typography",
  "class": "element-typography",
  "title": "typography"
}, {
  "url": "alerts",
  "class": "element-video",
  "title": "Notification"
}];
const headerBorderRemoveList = ["/", "/shop", "/shop/infinite-scroll", "/shop/horizontal-filter", "/shop/navigation-filter", "/shop/off-canvas-filter", "/shop/right-sidebar", "/shop/grid/[grid]", "/pages/404", "/elements", "/elements/products", "/elements/typography", "/elements/titles", "/elements/product-category", "/elements/buttons", "/elements/accordions", "/elements/alerts", "/elements/tabs", "/elements/testimonials", "/elements/blog-posts", "/elements/instagrams", "/elements/cta", "/elements/icon-boxes", "/elements/icons"];

/***/ }),

/***/ "./node_modules/react-image-lightbox/style.css":
/*!*****************************************************!*\
  !*** ./node_modules/react-image-lightbox/style.css ***!
  \*****************************************************/
/***/ (function() {



/***/ }),

/***/ "./node_modules/react-input-range/lib/css/index.css":
/*!**********************************************************!*\
  !*** ./node_modules/react-input-range/lib/css/index.css ***!
  \**********************************************************/
/***/ (function() {



/***/ }),

/***/ "./node_modules/react-toastify/dist/ReactToastify.min.css":
/*!****************************************************************!*\
  !*** ./node_modules/react-toastify/dist/ReactToastify.min.css ***!
  \****************************************************************/
/***/ (function() {



/***/ }),

/***/ "./public/sass/style.scss":
/*!********************************!*\
  !*** ./public/sass/style.scss ***!
  \********************************/
/***/ (function() {



/***/ }),

/***/ "@apollo/react-hooks":
/*!**************************************!*\
  !*** external "@apollo/react-hooks" ***!
  \**************************************/
/***/ (function(module) {

"use strict";
module.exports = require("@apollo/react-hooks");;

/***/ }),

/***/ "apollo-boost":
/*!*******************************!*\
  !*** external "apollo-boost" ***!
  \*******************************/
/***/ (function(module) {

"use strict";
module.exports = require("apollo-boost");;

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "graphql-tag":
/*!******************************!*\
  !*** external "graphql-tag" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("graphql-tag");;

/***/ }),

/***/ "imagesloaded":
/*!*******************************!*\
  !*** external "imagesloaded" ***!
  \*******************************/
/***/ (function(module) {

"use strict";
module.exports = require("imagesloaded");;

/***/ }),

/***/ "next-apollo":
/*!******************************!*\
  !*** external "next-apollo" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("next-apollo");;

/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/***/ (function(module) {

"use strict";
module.exports = require("next-redux-wrapper");;

/***/ }),

/***/ "../next-server/lib/router-context":
/*!**************************************************************!*\
  !*** external "next/dist/next-server/lib/router-context.js" ***!
  \**************************************************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router-context.js");;

/***/ }),

/***/ "../next-server/lib/router/utils/get-asset-path-from-route":
/*!**************************************************************************************!*\
  !*** external "next/dist/next-server/lib/router/utils/get-asset-path-from-route.js" ***!
  \**************************************************************************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router/utils/get-asset-path-from-route.js");;

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react-bootstrap/collapse":
/*!*******************************************!*\
  !*** external "react-bootstrap/collapse" ***!
  \*******************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-bootstrap/collapse");;

/***/ }),

/***/ "react-countdown":
/*!**********************************!*\
  !*** external "react-countdown" ***!
  \**********************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-countdown");;

/***/ }),

/***/ "react-helmet":
/*!*******************************!*\
  !*** external "react-helmet" ***!
  \*******************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-helmet");;

/***/ }),

/***/ "react-image-magnifiers":
/*!*****************************************!*\
  !*** external "react-image-magnifiers" ***!
  \*****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-image-magnifiers");;

/***/ }),

/***/ "react-is":
/*!***************************!*\
  !*** external "react-is" ***!
  \***************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-is");;

/***/ }),

/***/ "react-lazy-load-image-component":
/*!**************************************************!*\
  !*** external "react-lazy-load-image-component" ***!
  \**************************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-lazy-load-image-component");;

/***/ }),

/***/ "react-modal":
/*!******************************!*\
  !*** external "react-modal" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-modal");;

/***/ }),

/***/ "react-owl-carousel2":
/*!**************************************!*\
  !*** external "react-owl-carousel2" ***!
  \**************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-owl-carousel2");;

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ "react-slide-toggle":
/*!*************************************!*\
  !*** external "react-slide-toggle" ***!
  \*************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-slide-toggle");;

/***/ }),

/***/ "react-tabs":
/*!*****************************!*\
  !*** external "react-tabs" ***!
  \*****************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-tabs");;

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-toastify");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("redux");;

/***/ }),

/***/ "redux-persist":
/*!********************************!*\
  !*** external "redux-persist" ***!
  \********************************/
/***/ (function(module) {

"use strict";
module.exports = require("redux-persist");;

/***/ }),

/***/ "redux-persist/integration/react":
/*!**************************************************!*\
  !*** external "redux-persist/integration/react" ***!
  \**************************************************/
/***/ (function(module) {

"use strict";
module.exports = require("redux-persist/integration/react");;

/***/ }),

/***/ "redux-persist/lib/storage":
/*!********************************************!*\
  !*** external "redux-persist/lib/storage" ***!
  \********************************************/
/***/ (function(module) {

"use strict";
module.exports = require("redux-persist/lib/storage");;

/***/ }),

/***/ "redux-saga":
/*!*****************************!*\
  !*** external "redux-saga" ***!
  \*****************************/
/***/ (function(module) {

"use strict";
module.exports = require("redux-saga");;

/***/ }),

/***/ "redux-saga/effects":
/*!*************************************!*\
  !*** external "redux-saga/effects" ***!
  \*************************************/
/***/ (function(module) {

"use strict";
module.exports = require("redux-saga/effects");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = __webpack_require__.X(0, ["vendors-node_modules_next_link_js","components_common_Api_js-components_features_owl-carousel_jsx-server_apollo_js-server_queries-daab03"], function() { return __webpack_exec__("./pages/_app.js"); });
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvY29tbW9uL2Zvb3Rlci5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2NvbW1vbi9oZWFkZXIuanN4Iiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vY29tcG9uZW50cy9jb21tb24vcGFydGlhbHMvY2FydC1tZW51LmpzeCIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvY29tbW9uL3BhcnRpYWxzL2Zvb3Rlci1zZWFyY2gtYm94LmpzeCIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvY29tbW9uL3BhcnRpYWxzL21haW4tbWVudS5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2NvbW1vbi9wYXJ0aWFscy9tb2JpbGUtbWVudS5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2NvbW1vbi9wYXJ0aWFscy9zZWFyY2gtYm94LmpzeCIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvY29tbW9uL3N0aWNreS1mb290ZXIuanN4Iiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vY29tcG9uZW50cy9mZWF0dXJlcy9hY2NvcmRpb24vY2FyZC5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2ZlYXR1cmVzL2NvdW50ZG93bi5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2ZlYXR1cmVzL21vZGFscy9sb2dpbi1tb2RhbC5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2ZlYXR1cmVzL21vZGFscy92aWRlby1tb2RhbC5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2ZlYXR1cmVzL3Byb2R1Y3QvY29tbW9uL3F1aWNrdmlldy1tb2RhbC5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL2ZlYXR1cmVzL3F1YW50aXR5LmpzeCIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvbGF5b3V0LmpzeCIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvcGFydGlhbHMvcHJvZHVjdC9kZXRhaWwvZGV0YWlsLW9uZS5qc3giLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9jb21wb25lbnRzL3BhcnRpYWxzL3Byb2R1Y3QvcHJvZHVjdC1uYXYuanN4Iiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vcGFnZXMvX2FwcC5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3N0b3JlL2RlbW8uanMiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvLi9zdG9yZS9pbmRleC5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3N0b3JlL3Jvb3Qtc2FnYS5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL3V0aWxzL2RhdGEvbWVudS5qcyIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcIkBhcG9sbG8vcmVhY3QtaG9va3NcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcImFwb2xsby1ib29zdFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwiYXhpb3NcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcImdyYXBocWwtdGFnXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJpbWFnZXNsb2FkZWRcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcIm5leHQtYXBvbGxvXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJuZXh0LXJlZHV4LXdyYXBwZXJcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcIm5leHQvZGlzdC9uZXh0LXNlcnZlci9saWIvcm91dGVyLWNvbnRleHQuanNcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcIm5leHQvZGlzdC9uZXh0LXNlcnZlci9saWIvcm91dGVyL3V0aWxzL2dldC1hc3NldC1wYXRoLWZyb20tcm91dGUuanNcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcIm5leHQvcm91dGVyXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVhY3QtYm9vdHN0cmFwL2NvbGxhcHNlXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWFjdC1jb3VudGRvd25cIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcInJlYWN0LWhlbG1ldFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVhY3QtaW1hZ2UtbWFnbmlmaWVyc1wiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVhY3QtaXNcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcInJlYWN0LWxhenktbG9hZC1pbWFnZS1jb21wb25lbnRcIiIsIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC9leHRlcm5hbCBcInJlYWN0LW1vZGFsXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWFjdC1vd2wtY2Fyb3VzZWwyXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWFjdC1yZWR1eFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVhY3Qtc2xpZGUtdG9nZ2xlXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWFjdC10YWJzXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWFjdC10b2FzdGlmeVwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvZXh0ZXJuYWwgXCJyZWR1eFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVkdXgtcGVyc2lzdFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVkdXgtcGVyc2lzdC9pbnRlZ3JhdGlvbi9yZWFjdFwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVkdXgtcGVyc2lzdC9saWIvc3RvcmFnZVwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVkdXgtc2FnYVwiIiwid2VicGFjazovL3Jpb2RlLXJlYWN0L2V4dGVybmFsIFwicmVkdXgtc2FnYS9lZmZlY3RzXCIiXSwibmFtZXMiOlsiRm9vdGVyIiwiSGVhZGVyIiwicHJvcHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ1c2VFZmZlY3QiLCJoZWFkZXIiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJoZWFkZXJCb3JkZXJSZW1vdmVMaXN0IiwicGF0aG5hbWUiLCJjbGFzc0xpc3QiLCJjb250YWlucyIsInJlbW92ZSIsImFkZCIsInNob3dNb2JpbGVNZW51IiwiQ2FydE1lbnUiLCJjYXJ0TGlzdCIsInJlbW92ZUZyb21DYXJ0IiwiaGlkZUNhcnRNZW51IiwiYXNQYXRoIiwic2hvd0NhcnRNZW51IiwiZSIsInByZXZlbnREZWZhdWx0IiwiY3VycmVudFRhcmdldCIsImNsb3Nlc3QiLCJyZW1vdmVDYXJ0IiwiaXRlbSIsInRvRGVjaW1hbCIsImdldFRvdGFsUHJpY2UiLCJnZXRDYXJ0Q291bnQiLCJsZW5ndGgiLCJtYXAiLCJpbmRleCIsInNsdWciLCJwcm9jZXNzIiwicGljdHVyZXMiLCJ1cmwiLCJuYW1lIiwicXR5IiwicHJpY2UiLCJtYXBTdGF0ZVRvUHJvcHMiLCJzdGF0ZSIsImNhcnQiLCJkYXRhIiwiY29ubmVjdCIsImNhcnRBY3Rpb25zIiwiU2VhcmNoRm9ybSIsInNlYXJjaCIsInNldFNlYXJjaCIsInVzZVN0YXRlIiwic2VhcmNoUHJvZHVjdHMiLCJ1c2VMYXp5UXVlcnkiLCJHRVRfUFJPRFVDVFMiLCJ0aW1lciIsInNldFRpbWVyIiwiYWRkRXZlbnRMaXN0ZW5lciIsIm9uQm9keUNsaWNrIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsInF1ZXJ5IiwiY2xlYXJUaW1lb3V0IiwidGltZXJJZCIsInNldFRpbWVvdXQiLCJ2YXJpYWJsZXMiLCJyZW1vdmVYU1NBdHRhY2tzIiwiaHRtbCIsIlNDUklQVF9SRUdFWCIsInRlc3QiLCJyZXBsYWNlIiwiX19odG1sIiwibWF0Y2hFbXBoYXNpemUiLCJyZWdFeHAiLCJSZWdFeHAiLCJtYXRjaCIsIm9uU2VhcmNoQ2xpY2siLCJzdG9wUHJvcGFnYXRpb24iLCJwYXJlbnROb2RlIiwidG9nZ2xlIiwidGFyZ2V0Iiwib25TZWFyY2hDaGFuZ2UiLCJ2YWx1ZSIsIm9uU3VibWl0U2VhcmNoRm9ybSIsInB1c2giLCJwcm9kdWN0cyIsInByb2R1Y3QiLCJ2YXJpYW50cyIsIndpdGhBcG9sbG8iLCJzc3IiLCJNYWluTWVudSIsImFsbENhdGVnb3J5Iiwic2V0Q2F0ZWdvcnkiLCJHZXRBbGxDYXRlZ29yeSIsImNvbmZpZyIsImhlYWRlcnMiLCJhY2Nlc3NUb2tlbiIsImJvZHkiLCJlcnJvciIsImF4aW9zIiwiUFJPRFVDVF9DQVRFR09SWV9BUEkiLCJlcnIiLCJpbmNsdWRlcyIsImNhdGVnb3J5TmFtZSIsImhvdCIsImlkIiwiTW9iaWxlTWVudSIsIndpbmRvdyIsImhpZGVNb2JpbGVNZW51SGFuZGxlciIsImlubmVyV2lkdGgiLCJoaWRlTW9iaWxlTWVudSIsIm1haW5NZW51IiwidGl0bGUiLCJuZXciLCJzdWJQYWdlcyIsIlJlYWN0IiwiU3RpY2t5Rm9vdGVyIiwidG1wIiwic3RpY2t5Rm9vdGVySGFuZGxlciIsInRvcCIsIm9mZnNldFRvcCIsIm9mZnNldEhlaWdodCIsInN0aWNreUZvb3RlciIsImhlaWdodCIsInBhZ2VZT2Zmc2V0Iiwic2Nyb2xsWSIsInNldEF0dHJpYnV0ZSIsIm5ld05vZGUiLCJjcmVhdGVFbGVtZW50IiwiY2xhc3NOYW1lIiwiaW5zZXJ0QmVmb3JlIiwiaW5zZXJ0QWRqYWNlbnRFbGVtZW50IiwiZ2V0QXR0cmlidXRlIiwicmVtb3ZlQXR0cmlidXRlIiwic3R5bGUiLCJDYXJkIiwiZXhwYW5kZWQiLCJhZENsYXNzIiwiaWNvbkNsYXNzIiwidHlwZSIsIm9uVG9nZ2xlIiwic2V0Q29sbGFwc2libGVFbGVtZW50IiwidG9nZ2xlU3RhdGUiLCJ0b0xvd2VyQ2FzZSIsImNoaWxkcmVuIiwiUHJvZHVjdENvdW50RG93biIsImRhdGUiLCJyZW5kZXJlciIsImRheXMiLCJob3VycyIsIm1pbnV0ZXMiLCJzZWNvbmRzIiwiY29tcGxldGVkIiwiemVyb1BhZCIsIkRhdGUiLCJjdXN0b21TdHlsZXMiLCJvdmVybGF5IiwiYmFja2dyb3VuZENvbG9yIiwiZGlzcGxheSIsIk1vZGFsIiwiTG9naW5Nb2RhbCIsIm9wZW4iLCJzZXRPcGVuIiwicGhvbmVOdW1iZXIiLCJzZXRQaG9uZSIsInN0ZXAiLCJzZXRTdGVwIiwiT1RQIiwic2V0T3RwIiwidmVydGlmeU90cCIsInNldHZlcnRpZnlEYXRhIiwiZm9ybSIsInNldEZvcm0iLCJ1c2VyTmFtZSIsImZpcnN0TmFtZSIsImxhc3ROYW1lIiwiZW1haWwiLCJwYXNzd29yZCIsImFnZSIsImZvcm1BZGRyZXNzIiwic2V0QWRkcmVzc0Zvcm0iLCJhZGRyZXNzSWQiLCJsb2NhbGl0eSIsImFyZWEiLCJzdHJlZXQiLCJwaW5jb2RlSWQiLCJjaXR5SWQiLCJzdGF0ZUlkIiwiYWRkcmVzc1R5cGUiLCJjbG9zZU1vZGFsIiwib3Blbk1vZGFsIiwibG9naW5JbmRleCIsImhhbmRsZVBob25lQ2hhbmdlIiwic2VuZE90cCIsImJvZHlQYXJhbWV0ZXJzIiwiZGV2aWNlSWQiLCJTRU5ET1RQX0FQSSIsInRoZW4iLCJyZXNwb25zZSIsInZlcmlmeU90cCIsIm90cCIsInBhcnNlSW50IiwiVkVSSUZZX0FQSSIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJ0b2tlbiIsImhhbmRsZUNoYW5nZSIsImhhbmRsZUFkZHJlc3MiLCJjcmVhdGVBY2NvdW50IiwiQXV0aG9yaXphdGlvbiIsIlNJR05VUF9BUEkiLCJhZGRBZGRyZXNzIiwiQUREX0FERFJFU1NfQVBJIiwiY29uc29sZSIsImxvZyIsInRhYlNjcmVlbiIsImNvbnRlbnQiLCJwb3NpdGlvbiIsImJhY2tncm91bmQiLCJvdmVyZmxvd1giLCJvdmVyZmxvd1kiLCJvcGFjaXR5IiwiVmlkZW9Nb2RhbCIsImlzT3BlbiIsInNpbmdsZVNsdWciLCJldmVudHMiLCJvbiIsIm9mZiIsImNsb3NlVmlkZW8iLCJtb2RhbCIsIm1vZGFsQWN0aW9ucyIsInpJbmRleCIsIlF1aWNrdmlldyIsImNsb3NlUXVpY2t2aWV3IiwibG9hZGVkIiwic2V0TG9hZGluZ1N0YXRlIiwibG9hZGluZyIsInVzZVF1ZXJ5IiwiR0VUX1BST0RVQ1QiLCJvbmx5RGF0YSIsImltYWdlc0xvYWRlZCIsImpRdWVyeSIsInRyaWdnZXIiLCJjbG9zZVF1aWNrIiwiaXNfbmV3IiwiaXNfdG9wIiwiZGlzY291bnQiLCJtYWluU2xpZGVyMyIsImxhcmdlX3BpY3R1cmVzIiwicXVpY2t2aWV3IiwiUXVhbnRpdHkiLCJxdWFudGl0eSIsInNldFF1YW50aXR5Iiwib25DaGFuZ2VRdHkiLCJtaW51c1F1YW50aXR5IiwicGx1c1F1YW50aXR5IiwibWF4IiwiY2hhbmdlUXR5IiwibmV3UXR5IiwiTWF0aCIsIm1pbiIsIkxheW91dCIsInVzZUxheW91dEVmZmVjdCIsInNob3dTY3JvbGxUb3BIYW5kbGVyIiwic3RpY2t5SGVhZGVySGFuZGxlciIsInJlc2l6ZUhhbmRsZXIiLCJib2R5Q2xhc3NlcyIsImkiLCJzY3JvbGxUb3BIYW5kbGVyIiwiRGV0YWlsT25lIiwiaXNTdGlja3lDYXJ0IiwiaXNOYXYiLCJ0b2dnbGVXaXNobGlzdCIsImFkZFRvQ2FydCIsIndpc2hsaXN0IiwiY3VyQ29sb3IiLCJzZXRDdXJDb2xvciIsImN1clNpemUiLCJzZXRDdXJTaXplIiwiY3VySW5kZXgiLCJzZXRDdXJJbmRleCIsImNhcnRBY3RpdmUiLCJzZXRDYXJ0QWN0aXZlIiwic2V0UWF1bnRpdHkiLCJpc1dpc2hsaXN0ZWQiLCJjb2xvcnMiLCJzaXplcyIsImZpbmRJbmRleCIsInNpemUiLCJmb3JFYWNoIiwiY29sb3IiLCJyZXNldFZhbHVlSGFuZGxlciIsInN0b2NrIiwid2lzaGxpc3RIYW5kbGVyIiwic2V0Q29sb3JIYW5kbGVyIiwic2V0U2l6ZUhhbmRsZXIiLCJhZGRUb0NhcnRIYW5kbGVyIiwidG1wTmFtZSIsInRtcFByaWNlIiwic2FsZV9wcmljZSIsImlzRGlzYWJsZWQiLCJza3UiLCJjYXRlZ29yaWVzIiwiY2F0ZWdvcnkiLCJ3aWR0aCIsInJhdGluZ3MiLCJyZXZpZXdzIiwic2hvcnRfZGVzY3JpcHRpb24iLCJ3aXNobGlzdEFjdGlvbnMiLCJQcm9kdWN0TmF2IiwicHJldiIsIm5leHQiLCJBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiLCJzdG9yZSIsImdldFN0YXRlIiwiZGVtbyIsImN1cnJlbnQiLCJjdXJyZW50RGVtbyIsImRpc3BhdGNoIiwiZGVtb0FjdGlvbnMiLCJfX3BlcnNpc3RvciIsImdldEluaXRpYWxQcm9wcyIsImN0eCIsIndpdGhSZWR1eCIsIm1ha2VTdG9yZSIsImFjdGlvblR5cGVzIiwiUmVmcmVzaFN0b3JlIiwiaW5pdGlhbFN0YXRlIiwiZGVtb1JlZHVjZXIiLCJhY3Rpb24iLCJwYXlsb2FkIiwicmVmcmVzaFN0b3JlIiwicGVyc2lzdENvbmZpZyIsImtleVByZWZpeCIsImtleSIsInN0b3JhZ2UiLCJwZXJzaXN0UmVkdWNlciIsInNhZ2FNaWRkbGV3YXJlIiwiY3JlYXRlU2FnYU1pZGRsZVdhcmUiLCJyb290UmVkdWNlcnMiLCJjb21iaW5lUmVkdWNlcnMiLCJjYXJ0UmVkdWNlciIsIm1vZGFsUmVkdWNlciIsIndpc2hsaXN0UmVkdWNlciIsIm9wdGlvbnMiLCJjcmVhdGVTdG9yZSIsImFwcGx5TWlkZGxld2FyZSIsInNhZ2FUYXNrIiwicnVuIiwicm9vdFNhZ2EiLCJwZXJzaXN0U3RvcmUiLCJhbGwiLCJjYXJ0U2FnYSIsImVsZW1lbnRzTGlzdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBRWUsU0FBU0EsTUFBVCxHQUFtQjtBQUM5QixzQkFDSTtBQUFRLGFBQVMsRUFBQyxRQUFsQjtBQUFBLDJCQUNJO0FBQUssZUFBUyxFQUFDLFdBQWY7QUFBQSw4QkFDSTtBQUFLLGlCQUFTLEVBQUMsWUFBZjtBQUFBLCtCQUNJO0FBQUssbUJBQVMsRUFBQyx3QkFBZjtBQUFBLGtDQUNJO0FBQUsscUJBQVMsRUFBQyxVQUFmO0FBQUEsbUNBQ0ksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFDLEdBQVo7QUFBZ0IsdUJBQVMsRUFBQyxhQUExQjtBQUFBLHFDQUNJO0FBQUssbUJBQUcsRUFBQyxrQkFBVDtBQUE0QixtQkFBRyxFQUFDLGFBQWhDO0FBQThDLHFCQUFLLEVBQUMsS0FBcEQ7QUFBMEQsc0JBQU0sRUFBQztBQUFqRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREosZUFRSTtBQUFLLHFCQUFTLEVBQUMsVUFBZjtBQUFBLG1DQUNJO0FBQUssdUJBQVMsRUFBQywyREFBZjtBQUFBLHNDQUNJO0FBQUsseUJBQVMsRUFBQyx5Q0FBZjtBQUFBLHdDQUNJO0FBQUksMkJBQVMsRUFBQyxjQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURKLGVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBS0k7QUFBTSxzQkFBTSxFQUFDLEdBQWI7QUFBaUIseUJBQVMsRUFBQyxvQ0FBM0I7QUFBQSx3Q0FDSTtBQUFPLHNCQUFJLEVBQUMsT0FBWjtBQUFvQiwyQkFBUyxFQUFDLGNBQTlCO0FBQTZDLHNCQUFJLEVBQUMsT0FBbEQ7QUFBMEQsb0JBQUUsRUFBQyxPQUE3RDtBQUNJLDZCQUFXLEVBQUMsdUJBRGhCO0FBQ3dDLDBCQUFRO0FBRGhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREosZUFHSTtBQUFRLDJCQUFTLEVBQUMseUNBQWxCO0FBQTRELHNCQUFJLEVBQUMsUUFBakU7QUFBQSx1REFBbUY7QUFDL0UsNkJBQVMsRUFBQztBQURxRTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUFuRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUxKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBNEJJO0FBQUssaUJBQVMsRUFBQyxlQUFmO0FBQUEsK0JBQ0k7QUFBSyxtQkFBUyxFQUFDLEtBQWY7QUFBQSxrQ0FDSTtBQUFLLHFCQUFTLEVBQUMsbUJBQWY7QUFBQSxtQ0FDSTtBQUFLLHVCQUFTLEVBQUMsb0JBQWY7QUFBQSxzQ0FDSTtBQUFJLHlCQUFTLEVBQUMsY0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQUkseUJBQVMsRUFBQyxhQUFkO0FBQUEsd0NBQ0k7QUFBQSwwQ0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFESixlQUVJLDhEQUFDLHFFQUFEO0FBQU8sd0JBQUksRUFBQyxPQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFESixlQUtJO0FBQUEsMENBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBREosZUFFSSw4REFBQyxxRUFBRDtBQUFPLHdCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTEosZUFTSTtBQUFBLDBDQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURKLGVBRUksOERBQUMscUVBQUQ7QUFBTyx3QkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVRKLGVBYUk7QUFBQSx5Q0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBYkosZUFnQkk7QUFBQSx5Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHdCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBaEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLGVBMkJJO0FBQUsscUJBQVMsRUFBQyxtQkFBZjtBQUFBLG1DQUNJO0FBQUssdUJBQVMsRUFBQyxnQkFBZjtBQUFBLHNDQUNJO0FBQUkseUJBQVMsRUFBQyxjQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBSSx5QkFBUyxFQUFDLGFBQWQ7QUFBQSx3Q0FDSTtBQUFBLHlDQUNJLDhEQUFDLHFFQUFEO0FBQU8sd0JBQUksRUFBQyxpQkFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREosZUFJSTtBQUFBLHlDQUNJLDhEQUFDLHFFQUFEO0FBQU8sd0JBQUksRUFBQyxHQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFKSixlQU9JO0FBQUEseUNBQ0ksOERBQUMscUVBQUQ7QUFBTyx3QkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVBKLGVBVUk7QUFBQSx5Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHdCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBVkosZUFhSTtBQUFBLHlDQUNJLDhEQUFDLHFFQUFEO0FBQU8sd0JBQUksRUFBQyxHQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFiSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkEzQkosZUFrREk7QUFBSyxxQkFBUyxFQUFDLG1CQUFmO0FBQUEsbUNBQ0k7QUFBSyx1QkFBUyxFQUFDLGdCQUFmO0FBQUEsc0NBQ0k7QUFBSSx5QkFBUyxFQUFDLGNBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFJLHlCQUFTLEVBQUMsYUFBZDtBQUFBLHdDQUNJO0FBQUEseUNBQ0ksOERBQUMscUVBQUQ7QUFBTyx3QkFBSSxFQUFDLGNBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURKLGVBSUk7QUFBQSx5Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHdCQUFJLEVBQUMsYUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSkosZUFPSTtBQUFBLHlDQUNJLDhEQUFDLHFFQUFEO0FBQU8sd0JBQUksRUFBQyxHQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFQSixlQVVJO0FBQUEseUNBQ0ksOERBQUMscUVBQUQ7QUFBTyx3QkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVZKLGVBYUk7QUFBQSx5Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHdCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBYko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBbERKLGVBeUVJO0FBQUsscUJBQVMsRUFBQyxtQkFBZjtBQUFBLG1DQUNJO0FBQUssdUJBQVMsRUFBQyx5QkFBZjtBQUFBLHNDQUNJO0FBQUkseUJBQVMsRUFBQyxjQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBUSx5QkFBUyxFQUFDLGlCQUFsQjtBQUFBLHdDQUNJO0FBQUssMkJBQVMsRUFBQyxPQUFmO0FBQUEseUNBQ0k7QUFBSyx1QkFBRyxFQUFDLDBCQUFUO0FBQW9DLHVCQUFHLEVBQUMsYUFBeEM7QUFBc0QseUJBQUssRUFBQyxJQUE1RDtBQUFpRSwwQkFBTSxFQUFDO0FBQXhFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURKLGVBSUk7QUFBSywyQkFBUyxFQUFDLE9BQWY7QUFBQSx5Q0FDSTtBQUFLLHVCQUFHLEVBQUMsMEJBQVQ7QUFBb0MsdUJBQUcsRUFBQyxhQUF4QztBQUFzRCx5QkFBSyxFQUFDLElBQTVEO0FBQWlFLDBCQUFNLEVBQUM7QUFBeEU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSkosZUFPSTtBQUFLLDJCQUFTLEVBQUMsT0FBZjtBQUFBLHlDQUNJO0FBQUssdUJBQUcsRUFBQywwQkFBVDtBQUFvQyx1QkFBRyxFQUFDLGFBQXhDO0FBQXNELHlCQUFLLEVBQUMsSUFBNUQ7QUFBaUUsMEJBQU0sRUFBQztBQUF4RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFQSixlQVVJO0FBQUssMkJBQVMsRUFBQyxPQUFmO0FBQUEseUNBQ0k7QUFBSyx1QkFBRyxFQUFDLDBCQUFUO0FBQW9DLHVCQUFHLEVBQUMsYUFBeEM7QUFBc0QseUJBQUssRUFBQyxJQUE1RDtBQUFpRSwwQkFBTSxFQUFDO0FBQXhFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVZKLGVBYUk7QUFBSywyQkFBUyxFQUFDLE9BQWY7QUFBQSx5Q0FDSTtBQUFLLHVCQUFHLEVBQUMsMEJBQVQ7QUFBb0MsdUJBQUcsRUFBQyxhQUF4QztBQUFzRCx5QkFBSyxFQUFDLElBQTVEO0FBQWlFLDBCQUFNLEVBQUM7QUFBeEU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBYkosZUFnQkk7QUFBSywyQkFBUyxFQUFDLE9BQWY7QUFBQSx5Q0FDSTtBQUFLLHVCQUFHLEVBQUMsMEJBQVQ7QUFBb0MsdUJBQUcsRUFBQyxhQUF4QztBQUFzRCx5QkFBSyxFQUFDLElBQTVEO0FBQWlFLDBCQUFNLEVBQUM7QUFBeEU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBaEJKLGVBbUJJO0FBQUssMkJBQVMsRUFBQyxPQUFmO0FBQUEseUNBQ0k7QUFBSyx1QkFBRyxFQUFDLDBCQUFUO0FBQW9DLHVCQUFHLEVBQUMsYUFBeEM7QUFBc0QseUJBQUssRUFBQyxJQUE1RDtBQUFpRSwwQkFBTSxFQUFDO0FBQXhFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQW5CSixlQXNCSTtBQUFLLDJCQUFTLEVBQUMsT0FBZjtBQUFBLHlDQUNJO0FBQUssdUJBQUcsRUFBQywwQkFBVDtBQUFvQyx1QkFBRyxFQUFDLGFBQXhDO0FBQXNELHlCQUFLLEVBQUMsSUFBNUQ7QUFBaUUsMEJBQU0sRUFBQztBQUF4RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkF0Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBekVKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0E1QkosZUF3SUk7QUFBSyxpQkFBUyxFQUFDLGVBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsYUFBZjtBQUFBLGlDQUNJO0FBQVEscUJBQVMsRUFBQyxTQUFsQjtBQUFBLG1DQUNJO0FBQUssaUJBQUcsRUFBQyxxQkFBVDtBQUErQixpQkFBRyxFQUFDLFNBQW5DO0FBQTZDLG1CQUFLLEVBQUMsS0FBbkQ7QUFBeUQsb0JBQU0sRUFBQztBQUFoRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFNSTtBQUFLLG1CQUFTLEVBQUMsZUFBZjtBQUFBLGlDQUNJO0FBQUcscUJBQVMsRUFBQyxxQkFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkosZUFTSTtBQUFLLG1CQUFTLEVBQUMsY0FBZjtBQUFBLGlDQUNJO0FBQUsscUJBQVMsRUFBQyxjQUFmO0FBQUEsb0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFDLEdBQVo7QUFBZ0IsdUJBQVMsRUFBQztBQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURKLGVBRUksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFDLEdBQVo7QUFBZ0IsdUJBQVMsRUFBQztBQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKLGVBR0ksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFDLEdBQVo7QUFBZ0IsdUJBQVMsRUFBQztBQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBVEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBeElKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQThKSCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pLRDtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRWUsU0FBU0MsTUFBVCxDQUFpQkMsS0FBakIsRUFBeUI7QUFDcEMsUUFBTUMsTUFBTSxHQUFHQyxzREFBUyxFQUF4QjtBQUVBQyxrREFBUyxDQUFFLE1BQU07QUFDYixRQUFJQyxNQUFNLEdBQUdDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixRQUF4QixDQUFiOztBQUNBLFFBQUtGLE1BQUwsRUFBYztBQUNWLFVBQUtHLDZFQUFBLENBQWlDTixNQUFNLENBQUNPLFFBQXhDLEtBQXNESixNQUFNLENBQUNLLFNBQVAsQ0FBaUJDLFFBQWpCLENBQTJCLGVBQTNCLENBQTNELEVBQTBHTixNQUFNLENBQUNLLFNBQVAsQ0FBaUJFLE1BQWpCLENBQXlCLGVBQXpCLEVBQTFHLEtBQ0ssSUFBSyxDQUFDSiw2RUFBQSxDQUFpQ04sTUFBTSxDQUFDTyxRQUF4QyxDQUFOLEVBQTJESCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsUUFBeEIsRUFBbUNHLFNBQW5DLENBQTZDRyxHQUE3QyxDQUFrRCxlQUFsRDtBQUNuRTtBQUNKLEdBTlEsRUFNTixDQUFFWCxNQUFNLENBQUNPLFFBQVQsQ0FOTSxDQUFUOztBQVFBLFFBQU1LLGNBQWMsR0FBRyxNQUFNO0FBQ3pCUixZQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNHLFNBQWpDLENBQTJDRyxHQUEzQyxDQUFnRCxjQUFoRDtBQUNILEdBRkQ7O0FBSUEsc0JBQ0k7QUFBUSxhQUFTLEVBQUMsc0JBQWxCO0FBQUEsNEJBQ0k7QUFBSyxlQUFTLEVBQUMsWUFBZjtBQUFBLDZCQUNJO0FBQUssaUJBQVMsRUFBQyxXQUFmO0FBQUEsZ0NBQ0k7QUFBSyxtQkFBUyxFQUFDLGFBQWY7QUFBQSxpQ0FDSTtBQUFHLHFCQUFTLEVBQUMsYUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFJSTtBQUFLLG1CQUFTLEVBQUMsY0FBZjtBQUFBLGtDQUNJO0FBQUsscUJBQVMsRUFBQyxVQUFmO0FBQUEsb0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFJLHVCQUFTLEVBQUMsY0FBZDtBQUFBLHNDQUNJO0FBQUEsdUNBQUksOERBQUMscUVBQUQ7QUFBTyxzQkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBSjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBQSx1Q0FBSSw4REFBQyxxRUFBRDtBQUFPLHNCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFKO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQVNJO0FBQUsscUJBQVMsRUFBQyxlQUFmO0FBQUEsb0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFJLHVCQUFTLEVBQUMsY0FBZDtBQUFBLHNDQUNJO0FBQUEsdUNBQ0ksOERBQUMscUVBQUQ7QUFBTyxzQkFBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBSUk7QUFBQSx1Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHNCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFUSixlQXFCSTtBQUFNLHFCQUFTLEVBQUM7QUFBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFyQkosZUFzQkksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFDLG1CQUFaO0FBQWdDLHFCQUFTLEVBQUMsbUJBQTFDO0FBQUEsb0NBQThEO0FBQUcsdUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBQTlEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkF0QkosZUF1QkksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFDLEdBQVo7QUFBZ0IscUJBQVMsRUFBQyxnQkFBMUI7QUFBQSxvQ0FBMkM7QUFBRyx1QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQXZCSixlQXdCSSw4REFBQyw0RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQXhCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLGVBbUNJO0FBQUssZUFBUyxFQUFDLG9EQUFmO0FBQUEsNkJBQ0k7QUFBSyxpQkFBUyxFQUFDLFdBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsYUFBZjtBQUFBLGtDQUNJLDhEQUFDLHFFQUFEO0FBQU8sZ0JBQUksRUFBQyxHQUFaO0FBQWdCLHFCQUFTLEVBQUMsb0JBQTFCO0FBQStDLG1CQUFPLEVBQUdDLGNBQXpEO0FBQUEsbUNBQ0k7QUFBRyx1QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREosZUFLSSw4REFBQyxxRUFBRDtBQUFPLGdCQUFJLEVBQUMsR0FBWjtBQUFnQixxQkFBUyxFQUFDLE1BQTFCO0FBQUEsbUNBQ0k7QUFBSyxpQkFBRyxFQUFDLGtCQUFUO0FBQTRCLGlCQUFHLEVBQUMsTUFBaEM7QUFBdUMsbUJBQUssRUFBQyxLQUE3QztBQUFtRCxvQkFBTSxFQUFDO0FBQTFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUxKLGVBVUksOERBQUMsMkVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFWSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFjSTtBQUFLLG1CQUFTLEVBQUMsY0FBZjtBQUFBLGtDQUNJLDhEQUFDLHFFQUFEO0FBQU8sZ0JBQUksRUFBQyxPQUFaO0FBQW9CLHFCQUFTLEVBQUMsd0JBQTlCO0FBQUEsb0NBQ0k7QUFBSyx1QkFBUyxFQUFDLDRCQUFmO0FBQUEscUNBQ0k7QUFBRyx5QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFJSTtBQUFLLHVCQUFTLEVBQUMsNEJBQWY7QUFBQSxzQ0FDSTtBQUFJLHlCQUFTLEVBQUMsZ0JBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLGVBVUk7QUFBTSxxQkFBUyxFQUFDO0FBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBVkosZUFXSSw4REFBQywwRUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQVhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFkSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBbkNKLGVBa0VJO0FBQUssZUFBUyxFQUFDLHlCQUFmO0FBQUEsNkJBQ0k7QUFBSyxpQkFBUyxFQUFDLFdBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsYUFBZjtBQUFBLGlDQUNJLDhEQUFDLDBFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBS0k7QUFBSyxtQkFBUyxFQUFDLGNBQWY7QUFBQSxpQ0FDSSw4REFBQyxxRUFBRDtBQUFPLGdCQUFJLEVBQUMsR0FBWjtBQUFnQixxQkFBUyxFQUFDLG1EQUExQjtBQUFBLG9DQUE4RTtBQUFHLHVCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUxKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFsRUo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFnRkgsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzR0Q7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBOztBQUVBLFNBQVNDLFFBQVQsQ0FBbUJkLEtBQW5CLEVBQTJCO0FBQ3ZCLFFBQU07QUFBRWUsWUFBRjtBQUFZQztBQUFaLE1BQStCaEIsS0FBckM7QUFDQSxRQUFNQyxNQUFNLEdBQUdDLHNEQUFTLEVBQXhCO0FBRUFDLGtEQUFTLENBQUUsTUFBTTtBQUNiYyxnQkFBWTtBQUNmLEdBRlEsRUFFTixDQUFFaEIsTUFBTSxDQUFDaUIsTUFBVCxDQUZNLENBQVQ7O0FBSUEsUUFBTUMsWUFBWSxHQUFLQyxDQUFGLElBQVM7QUFDMUJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBRCxLQUFDLENBQUNFLGFBQUYsQ0FBZ0JDLE9BQWhCLENBQXlCLGdCQUF6QixFQUE0Q2QsU0FBNUMsQ0FBc0RHLEdBQXRELENBQTJELFFBQTNEO0FBQ0gsR0FIRDs7QUFLQSxRQUFNSyxZQUFZLEdBQUcsTUFBTTtBQUN2QixRQUFLWixRQUFRLENBQUNDLGFBQVQsQ0FBd0IsZ0JBQXhCLEVBQTJDRyxTQUEzQyxDQUFxREMsUUFBckQsQ0FBK0QsUUFBL0QsQ0FBTCxFQUNJTCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsZ0JBQXhCLEVBQTJDRyxTQUEzQyxDQUFxREUsTUFBckQsQ0FBNkQsUUFBN0Q7QUFDUCxHQUhEOztBQUtBLFFBQU1hLFVBQVUsR0FBS0MsSUFBRixJQUFZO0FBQzNCVCxrQkFBYyxDQUFFUyxJQUFGLENBQWQ7QUFDSCxHQUZEOztBQUlBLHNCQUNJO0FBQUssYUFBUyxFQUFDLDBEQUFmO0FBQUEsNEJBQ0k7QUFBRyxVQUFJLEVBQUMsR0FBUjtBQUFZLGVBQVMsRUFBQyw4QkFBdEI7QUFBcUQsYUFBTyxFQUFHTixZQUEvRDtBQUFBLDhCQUNJO0FBQUssaUJBQVMsRUFBQyxzQkFBZjtBQUFBLGdDQUNJO0FBQU0sbUJBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQUVJO0FBQU0sbUJBQVMsRUFBQyxZQUFoQjtBQUFBLDBCQUFnQ08saURBQVMsQ0FBRUMscURBQWEsQ0FBRVosUUFBRixDQUFmLENBQXpDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQUtJO0FBQUcsaUJBQVMsRUFBQyxZQUFiO0FBQUEsK0JBQTBCO0FBQU0sbUJBQVMsRUFBQyxZQUFoQjtBQUFBLG9CQUErQmEsb0RBQVksQ0FBRWIsUUFBRjtBQUEzQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTFCO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FMSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFESixlQVFJO0FBQUssZUFBUyxFQUFDLGNBQWY7QUFBOEIsYUFBTyxFQUFHRTtBQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBLFlBUkosZUFTSTtBQUFLLGVBQVMsRUFBQyxjQUFmO0FBQUEsOEJBQ0k7QUFBSyxpQkFBUyxFQUFDLGFBQWY7QUFBQSxnQ0FDSTtBQUFJLG1CQUFTLEVBQUMsWUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQUVJLDhEQUFDLHFFQUFEO0FBQU8sY0FBSSxFQUFDLEdBQVo7QUFBZ0IsbUJBQVMsRUFBQyxnREFBMUI7QUFBMkUsaUJBQU8sRUFBR0EsWUFBckY7QUFBQSwyQ0FBeUc7QUFDckcscUJBQVMsRUFBQztBQUQyRjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUF6RyxlQUN1QztBQUFNLHFCQUFTLEVBQUMsU0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRHZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixFQU9RRixRQUFRLENBQUNjLE1BQVQsR0FBa0IsQ0FBbEIsZ0JBQ0k7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMscUJBQWY7QUFBQSxvQkFFUWQsUUFBUSxDQUFDZSxHQUFULENBQWMsQ0FBRUwsSUFBRixFQUFRTSxLQUFSLGtCQUNWO0FBQUsscUJBQVMsRUFBQyxzQkFBZjtBQUFBLG9DQUNJO0FBQVEsdUJBQVMsRUFBQywwQkFBbEI7QUFBQSxzQ0FDSSw4REFBQyxxRUFBRDtBQUFPLG9CQUFJLEVBQUcsc0JBQXNCTixJQUFJLENBQUNPLElBQXpDO0FBQUEsdUNBQ0k7QUFBSyxxQkFBRyxFQUFHQyx1QkFBQSxHQUFvQ1IsSUFBSSxDQUFDUyxRQUFMLENBQWUsQ0FBZixFQUFtQkMsR0FBbEU7QUFBd0UscUJBQUcsRUFBQyxTQUE1RTtBQUFzRix1QkFBSyxFQUFDLElBQTVGO0FBQ0ksd0JBQU0sRUFBQztBQURYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBS0k7QUFBUSx5QkFBUyxFQUFDLHdCQUFsQjtBQUEyQyx1QkFBTyxFQUFHLE1BQU07QUFBRVgsNEJBQVUsQ0FBRUMsSUFBRixDQUFWO0FBQW9CLGlCQUFqRjtBQUFBLHdDQUNJO0FBQUcsMkJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREosZUFDb0M7QUFBTSwyQkFBUyxFQUFDLFNBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURwQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBTEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURKLGVBVUk7QUFBSyx1QkFBUyxFQUFDLGdCQUFmO0FBQUEsc0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxvQkFBSSxFQUFHLHNCQUFzQkEsSUFBSSxDQUFDTyxJQUF6QztBQUFnRCx5QkFBUyxFQUFDLGNBQTFEO0FBQUEsMEJBQTJFUCxJQUFJLENBQUNXO0FBQWhGO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFLLHlCQUFTLEVBQUMsV0FBZjtBQUFBLHdDQUNJO0FBQU0sMkJBQVMsRUFBQyxrQkFBaEI7QUFBQSw0QkFBcUNYLElBQUksQ0FBQ1k7QUFBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFESixlQUVJO0FBQU0sMkJBQVMsRUFBQyxlQUFoQjtBQUFBLGtDQUFtQ1gsaURBQVMsQ0FBRUQsSUFBSSxDQUFDYSxLQUFQLENBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQVZKO0FBQUEsYUFBNEMsdUJBQXVCUCxLQUFuRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQXlCSTtBQUFLLG1CQUFTLEVBQUMsWUFBZjtBQUFBLGtDQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLGVBRUk7QUFBTSxxQkFBUyxFQUFDLE9BQWhCO0FBQUEsNEJBQTJCTCxpREFBUyxDQUFFQyxxREFBYSxDQUFFWixRQUFGLENBQWYsQ0FBcEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkF6QkosZUE4Qkk7QUFBSyxtQkFBUyxFQUFDLGFBQWY7QUFBQSxrQ0FDSSw4REFBQyxxRUFBRDtBQUFPLGdCQUFJLEVBQUMsYUFBWjtBQUEwQixxQkFBUyxFQUFDLHVCQUFwQztBQUE0RCxtQkFBTyxFQUFHRSxZQUF0RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQUVJLDhEQUFDLHFFQUFEO0FBQU8sZ0JBQUksRUFBQyxpQkFBWjtBQUE4QixxQkFBUyxFQUFDLGNBQXhDO0FBQXVELG1CQUFPLEVBQUdBLFlBQWpFO0FBQUEsbUNBQWdGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWhGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQTlCSjtBQUFBLHNCQURKLGdCQW9DSTtBQUFHLGlCQUFTLEVBQUMsNERBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0EzQ1o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBVEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUEwREg7O0FBRUQsU0FBU3NCLGVBQVQsQ0FBMEJDLEtBQTFCLEVBQWtDO0FBQzlCLFNBQU87QUFDSHpCLFlBQVEsRUFBRXlCLEtBQUssQ0FBQ0MsSUFBTixDQUFXQztBQURsQixHQUFQO0FBR0g7O0FBRUQsK0RBQWVDLG9EQUFPLENBQUVKLGVBQUYsRUFBbUI7QUFBRXZCLGdCQUFjLEVBQUU0QixtRUFBMEI1QjtBQUE1QyxDQUFuQixDQUFQLENBQTRFRixRQUE1RSxDQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7O0FBRUEsU0FBUytCLFVBQVQsR0FBc0I7QUFDbEIsUUFBTTVDLE1BQU0sR0FBR0Msc0RBQVMsRUFBeEI7QUFDQSxRQUFNO0FBQUEsT0FBRTRDLE1BQUY7QUFBQSxPQUFVQztBQUFWLE1BQXdCQywrQ0FBUSxDQUFFLEVBQUYsQ0FBdEM7QUFDQSxRQUFNLENBQUVDLGNBQUYsRUFBa0I7QUFBRVA7QUFBRixHQUFsQixJQUErQlEsaUVBQVksQ0FBRUMseURBQUYsQ0FBakQ7QUFDQSxRQUFNO0FBQUEsT0FBRUMsS0FBRjtBQUFBLE9BQVNDO0FBQVQsTUFBc0JMLCtDQUFRLENBQUUsSUFBRixDQUFwQztBQUVBN0Msa0RBQVMsQ0FBRSxNQUFNO0FBQ2JFLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ2dELGdCQUFqQyxDQUFtRCxPQUFuRCxFQUE0REMsV0FBNUQ7QUFFQSxXQUFTLE1BQU07QUFDWGxELGNBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ2tELG1CQUFqQyxDQUFzRCxPQUF0RCxFQUErREQsV0FBL0Q7QUFDSCxLQUZEO0FBR0gsR0FOUSxFQU1OLEVBTk0sQ0FBVDtBQVFBcEQsa0RBQVMsQ0FBRSxNQUFNO0FBQ2I0QyxhQUFTLENBQUUsRUFBRixDQUFUO0FBQ0gsR0FGUSxFQUVOLENBQUU5QyxNQUFNLENBQUN3RCxLQUFQLENBQWF6QixJQUFmLENBRk0sQ0FBVDtBQUlBN0Isa0RBQVMsQ0FBRSxNQUFNO0FBQ2IsUUFBSzJDLE1BQU0sQ0FBQ2pCLE1BQVAsR0FBZ0IsQ0FBckIsRUFBeUI7QUFDckIsVUFBS3VCLEtBQUwsRUFBYU0sWUFBWSxDQUFFTixLQUFGLENBQVo7QUFDYixVQUFJTyxPQUFPLEdBQUdDLFVBQVUsQ0FBRSxNQUFNO0FBQzVCWCxzQkFBYyxDQUFFO0FBQUVZLG1CQUFTLEVBQUU7QUFBRWYsa0JBQU0sRUFBRUE7QUFBVjtBQUFiLFNBQUYsQ0FBZDtBQUNBTyxnQkFBUSxDQUFFLElBQUYsQ0FBUjtBQUFpQjtBQUNwQixPQUh1QixFQUdyQixHQUhxQixDQUF4QjtBQUtBQSxjQUFRLENBQUVNLE9BQUYsQ0FBUjtBQUNIO0FBQ0osR0FWUSxFQVVOLENBQUViLE1BQUYsQ0FWTSxDQUFUO0FBWUEzQyxrREFBUyxDQUFFLE1BQU07QUFDYkUsWUFBUSxDQUFDQyxhQUFULENBQXdCLDZCQUF4QixLQUEyREQsUUFBUSxDQUFDQyxhQUFULENBQXdCLDZCQUF4QixFQUF3REcsU0FBeEQsQ0FBa0VFLE1BQWxFLENBQTBFLGNBQTFFLENBQTNEO0FBQ0gsR0FGUSxFQUVOLENBQUVWLE1BQU0sQ0FBQ08sUUFBVCxDQUZNLENBQVQ7O0FBSUEsV0FBU3NELGdCQUFULENBQTJCQyxJQUEzQixFQUFrQztBQUM5QixVQUFNQyxZQUFZLEdBQUcscURBQXJCLENBRDhCLENBRzlCOztBQUNBLFdBQVFBLFlBQVksQ0FBQ0MsSUFBYixDQUFtQkYsSUFBbkIsQ0FBUixFQUFvQztBQUNoQ0EsVUFBSSxHQUFHQSxJQUFJLENBQUNHLE9BQUwsQ0FBY0YsWUFBZCxFQUE0QixFQUE1QixDQUFQO0FBQ0gsS0FONkIsQ0FROUI7OztBQUNBRCxRQUFJLEdBQUdBLElBQUksQ0FBQ0csT0FBTCxDQUFjLGlCQUFkLEVBQWlDLEVBQWpDLENBQVA7QUFFQSxXQUFPO0FBQ0hDLFlBQU0sRUFBRUo7QUFETCxLQUFQO0FBR0g7O0FBRUQsV0FBU0ssY0FBVCxDQUF5QmhDLElBQXpCLEVBQWdDO0FBQzVCLFFBQUlpQyxNQUFNLEdBQUcsSUFBSUMsTUFBSixDQUFZeEIsTUFBWixFQUFvQixHQUFwQixDQUFiO0FBQ0EsV0FBT1YsSUFBSSxDQUFDOEIsT0FBTCxDQUNIRyxNQURHLEVBRURFLEtBQUYsSUFBYSxhQUFhQSxLQUFiLEdBQXFCLFdBRi9CLENBQVA7QUFJSDs7QUFFRCxXQUFTQyxhQUFULENBQXdCcEQsQ0FBeEIsRUFBNEI7QUFDeEJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBRCxLQUFDLENBQUNxRCxlQUFGO0FBQ0FyRCxLQUFDLENBQUNFLGFBQUYsQ0FBZ0JvRCxVQUFoQixDQUEyQmpFLFNBQTNCLENBQXFDa0UsTUFBckMsQ0FBNkMsTUFBN0M7QUFDSDs7QUFFRCxXQUFTcEIsV0FBVCxDQUFzQm5DLENBQXRCLEVBQTBCO0FBQ3RCLFFBQUtBLENBQUMsQ0FBQ3dELE1BQUYsQ0FBU3JELE9BQVQsQ0FBa0IsZ0JBQWxCLENBQUwsRUFBNEMsT0FBT0gsQ0FBQyxDQUFDd0QsTUFBRixDQUFTckQsT0FBVCxDQUFrQixnQkFBbEIsRUFBcUNkLFNBQXJDLENBQStDQyxRQUEvQyxDQUF5RCxjQUF6RCxLQUE2RVUsQ0FBQyxDQUFDd0QsTUFBRixDQUFTckQsT0FBVCxDQUFrQixnQkFBbEIsRUFBcUNkLFNBQXJDLENBQStDRyxHQUEvQyxDQUFvRCxjQUFwRCxDQUFwRjtBQUU1Q1AsWUFBUSxDQUFDQyxhQUFULENBQXdCLHFCQUF4QixLQUFtREQsUUFBUSxDQUFDQyxhQUFULENBQXdCLHFCQUF4QixFQUFnREcsU0FBaEQsQ0FBMERFLE1BQTFELENBQWtFLE1BQWxFLENBQW5EO0FBQ0FOLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3Qiw2QkFBeEIsS0FBMkRELFFBQVEsQ0FBQ0MsYUFBVCxDQUF3Qiw2QkFBeEIsRUFBd0RHLFNBQXhELENBQWtFRSxNQUFsRSxDQUEwRSxjQUExRSxDQUEzRDtBQUNIOztBQUVELFdBQVNrRSxjQUFULENBQXlCekQsQ0FBekIsRUFBNkI7QUFDekIyQixhQUFTLENBQUUzQixDQUFDLENBQUN3RCxNQUFGLENBQVNFLEtBQVgsQ0FBVDtBQUNIOztBQUVELFdBQVNDLGtCQUFULENBQTZCM0QsQ0FBN0IsRUFBaUM7QUFDN0JBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBcEIsVUFBTSxDQUFDK0UsSUFBUCxDQUFhO0FBQ1R4RSxjQUFRLEVBQUUsT0FERDtBQUVUaUQsV0FBSyxFQUFFO0FBQ0hYLGNBQU0sRUFBRUE7QUFETDtBQUZFLEtBQWI7QUFNSDs7QUFFRCxzQkFDSTtBQUFLLGFBQVMsRUFBQyxnQ0FBZjtBQUFBLDRCQUNJO0FBQUcsVUFBSSxFQUFDLEdBQVI7QUFBWSxlQUFTLEVBQUMsMkJBQXRCO0FBQWtELFVBQUksRUFBQyxRQUF2RDtBQUFnRSxhQUFPLEVBQUcwQixhQUExRTtBQUFBLDhCQUNJO0FBQUcsaUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFLSTtBQUFNLFlBQU0sRUFBQyxHQUFiO0FBQWlCLFlBQU0sRUFBQyxLQUF4QjtBQUE4QixjQUFRLEVBQUdPLGtCQUF6QztBQUE4RCxlQUFTLEVBQUMsZUFBeEU7QUFBQSw4QkFDSTtBQUFPLFlBQUksRUFBQyxNQUFaO0FBQW1CLGlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsWUFBSSxFQUFDLFFBQWpEO0FBQTBELG9CQUFZLEVBQUMsS0FBdkU7QUFBNkUsYUFBSyxFQUFHakMsTUFBckY7QUFBOEYsZ0JBQVEsRUFBRytCLGNBQXpHO0FBQ0ksbUJBQVcsRUFBQyxXQURoQjtBQUM0QixnQkFBUTtBQURwQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFJSTtBQUFRLGlCQUFTLEVBQUMsZ0JBQWxCO0FBQW1DLFlBQUksRUFBQyxRQUF4QztBQUFBLCtCQUNJO0FBQUcsbUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSkosZUFRSTtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxrQkFDTS9CLE1BQU0sQ0FBQ2pCLE1BQVAsR0FBZ0IsQ0FBaEIsSUFBcUJhLElBQXJCLElBQTZCQSxJQUFJLENBQUN1QyxRQUFMLENBQWN2QyxJQUFkLENBQW1CWixHQUFuQixDQUF3QixDQUFFb0QsT0FBRixFQUFXbkQsS0FBWCxrQkFDbkQsOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUksb0JBQW9CbUQsT0FBTyxDQUFDbEQsSUFBTSxFQUFqRDtBQUFxRCxtQkFBUyxFQUFDLHlCQUEvRDtBQUFBLGtDQUNJLDhEQUFDLDBFQUFEO0FBQWUsa0JBQU0sRUFBQyxTQUF0QjtBQUFnQyxlQUFHLEVBQUdDLHVCQUFBLEdBQW9DaUQsT0FBTyxDQUFDaEQsUUFBUixDQUFrQixDQUFsQixFQUFzQkMsR0FBaEc7QUFBc0csaUJBQUssRUFBRyxFQUE5RztBQUFtSCxrQkFBTSxFQUFHLEVBQTVIO0FBQWlJLGVBQUcsRUFBQztBQUFySTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLGVBRUk7QUFBSyxxQkFBUyxFQUFDLGFBQWY7QUFBNkIsbUNBQXVCLEVBQUcyQixnQkFBZ0IsQ0FBRU0sY0FBYyxDQUFFYyxPQUFPLENBQUM5QyxJQUFWLENBQWhCO0FBQXZFO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkosZUFJSTtBQUFNLHFCQUFTLEVBQUMsY0FBaEI7QUFBQSxzQkFFUThDLE9BQU8sQ0FBQzVDLEtBQVIsQ0FBZSxDQUFmLE1BQXVCNEMsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBdkIsR0FDSTRDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQnRELE1BQWpCLEtBQTRCLENBQTVCLGdCQUNJO0FBQUEsc0NBQ0k7QUFBTSx5QkFBUyxFQUFDLGdCQUFoQjtBQUFBLGdDQUFvQ0gsaURBQVMsQ0FBRXdELE9BQU8sQ0FBQzVDLEtBQVIsQ0FBZSxDQUFmLENBQUYsQ0FBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBTSx5QkFBUyxFQUFDLFdBQWhCO0FBQUEsZ0NBQStCWixpREFBUyxDQUFFd0QsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBRixDQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQSw0QkFESixnQkFNSTtBQUFPLHVCQUFTLEVBQUMsV0FBakI7QUFBQSw4QkFBZ0NaLGlEQUFTLENBQUV3RCxPQUFPLENBQUM1QyxLQUFSLENBQWUsQ0FBZixDQUFGLENBQXpDLGVBQXVFWixpREFBUyxDQUFFd0QsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBRixDQUFoRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBUFIsZ0JBUU07QUFBTSx1QkFBUyxFQUFDLFdBQWhCO0FBQUEsOEJBQStCWixpREFBUyxDQUFFd0QsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBRixDQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUpKO0FBQUEsV0FBZ0csaUJBQWlCUCxLQUFPLEVBQXhIO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRDJCO0FBRG5DO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFMSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQXdDSDs7QUFFRCwrREFBZXFELHVEQUFVLENBQUU7QUFBRUMsS0FBRztBQUFMLENBQUYsQ0FBVixDQUFzRHhDLFVBQXRELENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU3lDLFFBQVQsR0FBb0I7QUFDaEIsUUFBTTlFLFFBQVEsR0FBR04sc0RBQVMsR0FBR00sUUFBN0I7QUFDQSxRQUFNO0FBQUEsT0FBQytFLFdBQUQ7QUFBQSxPQUFjQztBQUFkLE1BQTZCeEMsK0NBQVEsQ0FBQyxFQUFELENBQTNDOztBQUdDLFFBQU15QyxjQUFjLEdBQUcsWUFBWTtBQUVoQyxVQUFNQyxNQUFNLEdBQUc7QUFDWEMsYUFBTyxFQUFFO0FBQUVDLG1CQUFXLEVBQUU7QUFBZjtBQURFLEtBQWY7O0FBSUEsUUFBSTtBQUNGLFlBQU07QUFBRWxELFlBQUksRUFBRTtBQUFFbUQsY0FBRjtBQUFRQztBQUFSO0FBQVIsVUFBNEIsTUFBTUMsZ0RBQUEsQ0FBVUMsc0RBQVYsRUFBK0JOLE1BQS9CLENBQXhDO0FBQ0VGLGlCQUFXLENBQUNLLElBQUQsQ0FBWDtBQUNILEtBSEQsQ0FJQSxPQUFPSSxHQUFQLEVBQVksQ0FFWDtBQUNGLEdBYkYsQ0FMZSxDQW9CaEI7OztBQUNDOUYsa0RBQVMsQ0FBQyxNQUFLO0FBQ1pzRixrQkFBYztBQUNoQixHQUZRLEVBRVAsRUFGTyxDQUFUO0FBSUQsc0JBQ0k7QUFBSyxhQUFTLEVBQUMsVUFBZjtBQUFBLDJCQUNJO0FBQUksZUFBUyxFQUFDLE1BQWQ7QUFBQSw4QkFDSTtBQUFJLFVBQUUsRUFBQyxXQUFQO0FBQW1CLGlCQUFTLEVBQUdqRixRQUFRLEtBQUssR0FBYixHQUFtQixRQUFuQixHQUE4QixFQUE3RDtBQUFBLCtCQUNJLDhEQUFDLHFFQUFEO0FBQU8sY0FBSSxFQUFDLEdBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFLSTtBQUFJLGlCQUFTLEVBQUksWUFBWUEsUUFBUSxDQUFDMEYsUUFBVCxDQUFtQixPQUFuQixJQUErQixRQUEvQixHQUEwQyxFQUFJLEVBQTNFO0FBQUEsZ0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQUlTO0FBQUEsb0JBRWVYLFdBQVcsQ0FBQ3pELEdBQVosQ0FBaUIsQ0FBRUwsSUFBRixFQUFRTSxLQUFSLGtCQUNiO0FBQUEsbUNBQ0ksOERBQUMscUVBQUQ7QUFBTyxrQkFBSSxFQUFHLE1BQU1OLElBQUksQ0FBQzBFLFlBQXpCO0FBQUEseUJBQ00xRSxJQUFJLENBQUMwRSxZQURYLEVBRU0xRSxJQUFJLENBQUMyRSxHQUFMLGdCQUFXO0FBQU0seUJBQVMsRUFBQyxhQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFBWCxHQUFzRCxFQUY1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESixhQUFXLFFBQVEzRSxJQUFJLENBQUM0RSxFQUFJLEVBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREo7QUFGZjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUpUO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUxKLGVBMENJO0FBQUEsK0JBQ0EsOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUMsR0FBWjtBQUFBLGtDQUFnQjtBQUFHLHFCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBMUNKLGVBNkNJO0FBQUEsK0JBQ0ksOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUMsbUJBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBN0NKLGVBZ0RJO0FBQUEsK0JBQ0ksOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUMsaUJBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBaERKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQXdESDs7QUFFRCwrREFBZWYsUUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFGQTtBQUNBO0FBRUE7QUFDQTtBQUVBOztBQUVBLFNBQVNnQixVQUFULENBQXFCdEcsS0FBckIsRUFBNkI7QUFDekIsUUFBTTtBQUFBLE9BQUU4QyxNQUFGO0FBQUEsT0FBVUM7QUFBVixNQUF3QkMsK0NBQVEsQ0FBRSxFQUFGLENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUVJLEtBQUY7QUFBQSxPQUFTQztBQUFULE1BQXNCTCwrQ0FBUSxDQUFFLElBQUYsQ0FBcEM7QUFDQSxRQUFNL0MsTUFBTSxHQUFHQyxzREFBUyxFQUF4QjtBQUVBQyxrREFBUyxDQUFFLE1BQU07QUFDYm9HLFVBQU0sQ0FBQ2pELGdCQUFQLENBQXlCLFFBQXpCLEVBQW1Da0QscUJBQW5DO0FBQ0FuRyxZQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNnRCxnQkFBakMsQ0FBbUQsT0FBbkQsRUFBNERDLFdBQTVEO0FBRUEsV0FBTyxNQUFNO0FBQ1RnRCxZQUFNLENBQUMvQyxtQkFBUCxDQUE0QixRQUE1QixFQUFzQ2dELHFCQUF0QztBQUNBbkcsY0FBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDa0QsbUJBQWpDLENBQXNELE9BQXRELEVBQStERCxXQUEvRDtBQUNILEtBSEQ7QUFJSCxHQVJRLEVBUU4sRUFSTSxDQUFUO0FBVUFwRCxrREFBUyxDQUFFLE1BQU07QUFDYjRDLGFBQVMsQ0FBRSxFQUFGLENBQVQ7QUFDSCxHQUZRLEVBRU4sQ0FBRTlDLE1BQU0sQ0FBQ3dELEtBQVAsQ0FBYXpCLElBQWYsQ0FGTSxDQUFUOztBQUlBLFFBQU13RSxxQkFBcUIsR0FBRyxNQUFNO0FBQ2hDLFFBQUtELE1BQU0sQ0FBQ0UsVUFBUCxHQUFvQixHQUF6QixFQUErQjtBQUMzQnBHLGNBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ0csU0FBakMsQ0FBMkNFLE1BQTNDLENBQW1ELGNBQW5EO0FBQ0g7QUFDSixHQUpEOztBQU1BLFFBQU0rRixjQUFjLEdBQUcsTUFBTTtBQUN6QnJHLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ0csU0FBakMsQ0FBMkNFLE1BQTNDLENBQW1ELGNBQW5EO0FBQ0gsR0FGRDs7QUFJQSxXQUFTa0UsY0FBVCxDQUF5QnpELENBQXpCLEVBQTZCO0FBQ3pCMkIsYUFBUyxDQUFFM0IsQ0FBQyxDQUFDd0QsTUFBRixDQUFTRSxLQUFYLENBQVQ7QUFDSDs7QUFFRCxXQUFTdkIsV0FBVCxDQUFzQm5DLENBQXRCLEVBQTBCO0FBQ3RCLFFBQUtBLENBQUMsQ0FBQ3dELE1BQUYsQ0FBU3JELE9BQVQsQ0FBa0IsZ0JBQWxCLENBQUwsRUFBNEMsT0FBT0gsQ0FBQyxDQUFDd0QsTUFBRixDQUFTckQsT0FBVCxDQUFrQixnQkFBbEIsRUFBcUNkLFNBQXJDLENBQStDQyxRQUEvQyxDQUF5RCxjQUF6RCxLQUE2RVUsQ0FBQyxDQUFDd0QsTUFBRixDQUFTckQsT0FBVCxDQUFrQixnQkFBbEIsRUFBcUNkLFNBQXJDLENBQStDRyxHQUEvQyxDQUFvRCxjQUFwRCxDQUFwRjtBQUU1Q1AsWUFBUSxDQUFDQyxhQUFULENBQXdCLHFCQUF4QixLQUFtREQsUUFBUSxDQUFDQyxhQUFULENBQXdCLHFCQUF4QixFQUFnREcsU0FBaEQsQ0FBMERFLE1BQTFELENBQWtFLE1BQWxFLENBQW5EO0FBQ0FOLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3Qiw2QkFBeEIsS0FBMkRELFFBQVEsQ0FBQ0MsYUFBVCxDQUF3Qiw2QkFBeEIsRUFBd0RHLFNBQXhELENBQWtFRSxNQUFsRSxDQUEwRSxjQUExRSxDQUEzRDtBQUNIOztBQUVELFdBQVNvRSxrQkFBVCxDQUE2QjNELENBQTdCLEVBQWlDO0FBQzdCQSxLQUFDLENBQUNDLGNBQUY7QUFDQXBCLFVBQU0sQ0FBQytFLElBQVAsQ0FBYTtBQUNUeEUsY0FBUSxFQUFFLE9BREQ7QUFFVGlELFdBQUssRUFBRTtBQUNIWCxjQUFNLEVBQUVBO0FBREw7QUFGRSxLQUFiO0FBTUg7O0FBRUQsc0JBQ0k7QUFBSyxhQUFTLEVBQUMscUJBQWY7QUFBQSw0QkFDSTtBQUFLLGVBQVMsRUFBQyxxQkFBZjtBQUFxQyxhQUFPLEVBQUc0RDtBQUEvQztBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFJSSw4REFBQyxxRUFBRDtBQUFPLGVBQVMsRUFBQyxtQkFBakI7QUFBcUMsVUFBSSxFQUFDLEdBQTFDO0FBQThDLGFBQU8sRUFBR0EsY0FBeEQ7QUFBQSw2QkFBeUU7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF6RTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkosZUFNSTtBQUFLLGVBQVMsRUFBQyxrQ0FBZjtBQUFBLDhCQUNJO0FBQU0sY0FBTSxFQUFDLEdBQWI7QUFBaUIsaUJBQVMsRUFBQyxlQUEzQjtBQUEyQyxnQkFBUSxFQUFHM0Isa0JBQXREO0FBQUEsZ0NBQ0k7QUFBTyxjQUFJLEVBQUMsTUFBWjtBQUFtQixtQkFBUyxFQUFDLGNBQTdCO0FBQTRDLGNBQUksRUFBQyxRQUFqRDtBQUEwRCxzQkFBWSxFQUFDLEtBQXZFO0FBQTZFLGVBQUssRUFBR2pDLE1BQXJGO0FBQThGLGtCQUFRLEVBQUcrQixjQUF6RztBQUNJLHFCQUFXLEVBQUMsd0JBRGhCO0FBQ3lDLGtCQUFRO0FBRGpEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFHSTtBQUFRLG1CQUFTLEVBQUMsZ0JBQWxCO0FBQW1DLGNBQUksRUFBQyxRQUF4QztBQUFBLGlDQUNJO0FBQUcscUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBU0k7QUFBSSxpQkFBUyxFQUFDLHdCQUFkO0FBQUEsZ0NBQ0k7QUFBQSxpQ0FDSSw4REFBQyxxRUFBRDtBQUFPLGdCQUFJLEVBQUMsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFLSTtBQUFBLGlDQUNJLDhEQUFDLHdFQUFEO0FBQU0saUJBQUssRUFBQyxZQUFaO0FBQXlCLGdCQUFJLEVBQUMsUUFBOUI7QUFBdUMsZUFBRyxFQUFDLE9BQTNDO0FBQUEsbUNBQ0k7QUFBQSxxQ0FDSTtBQUFBLHVDQUNJLDhEQUFDLHdFQUFEO0FBQU0sdUJBQUssRUFBQyxjQUFaO0FBQTJCLHNCQUFJLEVBQUMsUUFBaEM7QUFBQSx5Q0FDSTtBQUFBLDhCQUVROEIsMEVBQUEsQ0FBOEIsQ0FBRWxGLElBQUYsRUFBUU0sS0FBUixrQkFDMUI7QUFBQSw2Q0FDSSw4REFBQyxxRUFBRDtBQUFPLDRCQUFJLEVBQUcsTUFBTU4sSUFBSSxDQUFDVSxHQUF6QjtBQUFBLG1DQUNNVixJQUFJLENBQUNtRixLQURYLEVBRU1uRixJQUFJLENBQUMyRSxHQUFMLGdCQUFXO0FBQU0sbUNBQVMsRUFBQyxhQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQ0FBWCxHQUFzRCxFQUY1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESix1QkFBVyxRQUFRM0UsSUFBSSxDQUFDbUYsS0FBTyxFQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQURKO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUxKLGVBK0JJO0FBQUEsaUNBQ0ksOERBQUMsd0VBQUQ7QUFBTSxpQkFBSyxFQUFDLE9BQVo7QUFBb0IsZ0JBQUksRUFBQyxRQUF6QjtBQUFrQyxlQUFHLEVBQUMsaUJBQXRDO0FBQUEsbUNBQ0k7QUFBQSx3QkFFUUQsZ0VBQUEsQ0FBb0IsQ0FBRWxGLElBQUYsRUFBUU0sS0FBUixrQkFDaEI7QUFBQSx1Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHNCQUFJLEVBQUcsTUFBTU4sSUFBSSxDQUFDVSxHQUF6QjtBQUFBLDZCQUNNVixJQUFJLENBQUNtRixLQURYLEVBRU1uRixJQUFJLENBQUNvRixHQUFMLGdCQUFXO0FBQU0sNkJBQVMsRUFBQyxhQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFBWCxHQUFzRCxFQUY1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESixpQkFBVyxTQUFTcEYsSUFBSSxDQUFDbUYsS0FBTyxFQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQS9CSixlQWdESTtBQUFBLGlDQUNJLDhEQUFDLHdFQUFEO0FBQU0saUJBQUssRUFBQyxNQUFaO0FBQW1CLGdCQUFJLEVBQUMsUUFBeEI7QUFBaUMsZUFBRyxFQUFDLGVBQXJDO0FBQUEsbUNBQ0k7QUFBQSx3QkFFUUQsK0RBQUEsQ0FBbUIsQ0FBRWxGLElBQUYsRUFBUU0sS0FBUixLQUNmTixJQUFJLENBQUNxRixRQUFMLGdCQUNJO0FBQUEsdUNBQ0ksOERBQUMsd0VBQUQ7QUFBTSx1QkFBSyxFQUFHckYsSUFBSSxDQUFDbUYsS0FBbkI7QUFBMkIscUJBQUcsRUFBRyxNQUFNbkYsSUFBSSxDQUFDVSxHQUE1QztBQUFrRCxzQkFBSSxFQUFDLFFBQXZEO0FBQUEseUNBQ0k7QUFBQSw4QkFFUVYsSUFBSSxDQUFDcUYsUUFBTCxDQUFjaEYsR0FBZCxDQUFtQixDQUFFTCxJQUFGLEVBQVFNLEtBQVIsa0JBQ2Y7QUFBQSw2Q0FDSSw4REFBQyxxRUFBRDtBQUFPLDRCQUFJLEVBQUcsTUFBTU4sSUFBSSxDQUFDVSxHQUF6QjtBQUFBLGtDQUNNVixJQUFJLENBQUNtRjtBQURYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESix1QkFBVyxRQUFRbkYsSUFBSSxDQUFDbUYsS0FBTyxFQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDRCQURKO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESixpQkFBVSxTQUFTbkYsSUFBSSxDQUFDbUYsS0FBeEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixnQkFpQkk7QUFBZ0MseUJBQVMsRUFBR25GLElBQUksQ0FBQ3FGLFFBQUwsR0FBZ0IsU0FBaEIsR0FBNEIsRUFBeEU7QUFBQSx1Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHNCQUFJLEVBQUcsTUFBTXJGLElBQUksQ0FBQ1UsR0FBekI7QUFBQSw0QkFDTVYsSUFBSSxDQUFDbUY7QUFEWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREosaUJBQVUsU0FBU25GLElBQUksQ0FBQ21GLEtBQXhCO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBbEJSO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWhESixlQWlGSTtBQUFBLGlDQUNJLDhEQUFDLHdFQUFEO0FBQU0saUJBQUssRUFBQyxVQUFaO0FBQXVCLGdCQUFJLEVBQUMsUUFBNUI7QUFBcUMsZUFBRyxFQUFDLFdBQXpDO0FBQUEsbUNBQ0k7QUFBQSx3QkFFUUQsa0VBQUEsQ0FBc0IsQ0FBRWxGLElBQUYsRUFBUU0sS0FBUixrQkFDbEI7QUFBQSx1Q0FDSSw4REFBQyxxRUFBRDtBQUFPLHNCQUFJLEVBQUcsTUFBTU4sSUFBSSxDQUFDVSxHQUF6QjtBQUFBLDRCQUNNVixJQUFJLENBQUNtRjtBQURYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESixpQkFBVyxZQUFZbkYsSUFBSSxDQUFDbUYsS0FBTyxFQUFuQztBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWpGSixlQW1HSTtBQUFBLGlDQUFJLDhEQUFDLHFFQUFEO0FBQU8sZ0JBQUksRUFBRyxnQkFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBbkdKLGVBb0dJO0FBQUEsaUNBQUksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFHLGFBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBSjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQXBHSixlQXFHSTtBQUFBLGlDQUFJLDhEQUFDLHFFQUFEO0FBQU8sZ0JBQUksRUFBRyxpQkFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBckdKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBMEhIOztBQUVELDRFQUFlRyxpREFBQSxDQUFZVCxVQUFaLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0TEE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTs7QUFFQSxTQUFTekQsVUFBVCxHQUFzQjtBQUNsQixRQUFNNUMsTUFBTSxHQUFHQyxzREFBUyxFQUF4QjtBQUNBLFFBQU07QUFBQSxPQUFFNEMsTUFBRjtBQUFBLE9BQVVDO0FBQVYsTUFBd0JDLCtDQUFRLENBQUUsRUFBRixDQUF0QztBQUNBLFFBQU0sQ0FBRUMsY0FBRixFQUFrQjtBQUFFUDtBQUFGLEdBQWxCLElBQStCUSxpRUFBWSxDQUFFQyx5REFBRixDQUFqRDtBQUNBLFFBQU07QUFBQSxPQUFFQyxLQUFGO0FBQUEsT0FBU0M7QUFBVCxNQUFzQkwsK0NBQVEsQ0FBRSxJQUFGLENBQXBDO0FBRUE3QyxrREFBUyxDQUFFLE1BQU07QUFDYkUsWUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDZ0QsZ0JBQWpDLENBQW1ELE9BQW5ELEVBQTREQyxXQUE1RDtBQUVBLFdBQVMsTUFBTTtBQUNYbEQsY0FBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDa0QsbUJBQWpDLENBQXNELE9BQXRELEVBQStERCxXQUEvRDtBQUNILEtBRkQ7QUFHSCxHQU5RLEVBTU4sRUFOTSxDQUFUO0FBUUFwRCxrREFBUyxDQUFFLE1BQU07QUFDYjRDLGFBQVMsQ0FBRSxFQUFGLENBQVQ7QUFDSCxHQUZRLEVBRU4sQ0FBRTlDLE1BQU0sQ0FBQ3dELEtBQVAsQ0FBYXpCLElBQWYsQ0FGTSxDQUFUO0FBSUE3QixrREFBUyxDQUFFLE1BQU07QUFDYixRQUFLMkMsTUFBTSxDQUFDakIsTUFBUCxHQUFnQixDQUFyQixFQUF5QjtBQUNyQixVQUFLdUIsS0FBTCxFQUFhTSxZQUFZLENBQUVOLEtBQUYsQ0FBWjtBQUNiLFVBQUlPLE9BQU8sR0FBR0MsVUFBVSxDQUFFLE1BQU07QUFDNUJYLHNCQUFjLENBQUU7QUFBRVksbUJBQVMsRUFBRTtBQUFFZixrQkFBTSxFQUFFQTtBQUFWO0FBQWIsU0FBRixDQUFkO0FBQ0FPLGdCQUFRLENBQUUsSUFBRixDQUFSO0FBQWlCO0FBQ3BCLE9BSHVCLEVBR3JCLEdBSHFCLENBQXhCO0FBS0FBLGNBQVEsQ0FBRU0sT0FBRixDQUFSO0FBQ0g7QUFDSixHQVZRLEVBVU4sQ0FBRWIsTUFBRixDQVZNLENBQVQ7QUFZQTNDLGtEQUFTLENBQUUsTUFBTTtBQUNiRSxZQUFRLENBQUNDLGFBQVQsQ0FBd0IsNkJBQXhCLEtBQTJERCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsNkJBQXhCLEVBQXdERyxTQUF4RCxDQUFrRUUsTUFBbEUsQ0FBMEUsY0FBMUUsQ0FBM0Q7QUFDSCxHQUZRLEVBRU4sQ0FBRVYsTUFBTSxDQUFDTyxRQUFULENBRk0sQ0FBVDs7QUFJQSxXQUFTc0QsZ0JBQVQsQ0FBMkJDLElBQTNCLEVBQWtDO0FBQzlCLFVBQU1DLFlBQVksR0FBRyxxREFBckIsQ0FEOEIsQ0FHOUI7O0FBQ0EsV0FBUUEsWUFBWSxDQUFDQyxJQUFiLENBQW1CRixJQUFuQixDQUFSLEVBQW9DO0FBQ2hDQSxVQUFJLEdBQUdBLElBQUksQ0FBQ0csT0FBTCxDQUFjRixZQUFkLEVBQTRCLEVBQTVCLENBQVA7QUFDSCxLQU42QixDQVE5Qjs7O0FBQ0FELFFBQUksR0FBR0EsSUFBSSxDQUFDRyxPQUFMLENBQWMsaUJBQWQsRUFBaUMsRUFBakMsQ0FBUDtBQUVBLFdBQU87QUFDSEMsWUFBTSxFQUFFSjtBQURMLEtBQVA7QUFHSDs7QUFFRCxXQUFTSyxjQUFULENBQXlCaEMsSUFBekIsRUFBZ0M7QUFDNUIsUUFBSWlDLE1BQU0sR0FBRyxJQUFJQyxNQUFKLENBQVl4QixNQUFaLEVBQW9CLEdBQXBCLENBQWI7QUFDQSxXQUFPVixJQUFJLENBQUM4QixPQUFMLENBQ0hHLE1BREcsRUFFREUsS0FBRixJQUFhLGFBQWFBLEtBQWIsR0FBcUIsV0FGL0IsQ0FBUDtBQUlIOztBQUVELFdBQVNDLGFBQVQsQ0FBd0JwRCxDQUF4QixFQUE0QjtBQUN4QkEsS0FBQyxDQUFDQyxjQUFGO0FBQ0FELEtBQUMsQ0FBQ3FELGVBQUY7QUFDQXJELEtBQUMsQ0FBQ0UsYUFBRixDQUFnQm9ELFVBQWhCLENBQTJCakUsU0FBM0IsQ0FBcUNrRSxNQUFyQyxDQUE2QyxNQUE3QztBQUNIOztBQUVELFdBQVNwQixXQUFULENBQXNCbkMsQ0FBdEIsRUFBMEI7QUFDdEIsUUFBS0EsQ0FBQyxDQUFDd0QsTUFBRixDQUFTckQsT0FBVCxDQUFrQixnQkFBbEIsQ0FBTCxFQUE0QyxPQUFPSCxDQUFDLENBQUN3RCxNQUFGLENBQVNyRCxPQUFULENBQWtCLGdCQUFsQixFQUFxQ2QsU0FBckMsQ0FBK0NDLFFBQS9DLENBQXlELGNBQXpELEtBQTZFVSxDQUFDLENBQUN3RCxNQUFGLENBQVNyRCxPQUFULENBQWtCLGdCQUFsQixFQUFxQ2QsU0FBckMsQ0FBK0NHLEdBQS9DLENBQW9ELGNBQXBELENBQXBGO0FBRTVDUCxZQUFRLENBQUNDLGFBQVQsQ0FBd0IscUJBQXhCLEtBQW1ERCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IscUJBQXhCLEVBQWdERyxTQUFoRCxDQUEwREUsTUFBMUQsQ0FBa0UsTUFBbEUsQ0FBbkQ7QUFDQU4sWUFBUSxDQUFDQyxhQUFULENBQXdCLDZCQUF4QixLQUEyREQsUUFBUSxDQUFDQyxhQUFULENBQXdCLDZCQUF4QixFQUF3REcsU0FBeEQsQ0FBa0VFLE1BQWxFLENBQTBFLGNBQTFFLENBQTNEO0FBQ0g7O0FBRUQsV0FBU2tFLGNBQVQsQ0FBeUJ6RCxDQUF6QixFQUE2QjtBQUN6QjJCLGFBQVMsQ0FBRTNCLENBQUMsQ0FBQ3dELE1BQUYsQ0FBU0UsS0FBWCxDQUFUO0FBQ0g7O0FBRUQsV0FBU0Msa0JBQVQsQ0FBNkIzRCxDQUE3QixFQUFpQztBQUM3QkEsS0FBQyxDQUFDQyxjQUFGO0FBQ0FwQixVQUFNLENBQUMrRSxJQUFQLENBQWE7QUFDVHhFLGNBQVEsRUFBRSxPQUREO0FBRVRpRCxXQUFLLEVBQUU7QUFDSFgsY0FBTSxFQUFFQTtBQURMO0FBRkUsS0FBYjtBQU1IOztBQUVELHNCQUNJO0FBQUssYUFBUyxFQUFDLHlCQUFmO0FBQUEsNEJBQ0k7QUFBRyxVQUFJLEVBQUMsR0FBUjtBQUFZLGVBQVMsRUFBQyxlQUF0QjtBQUFzQyxVQUFJLEVBQUMsUUFBM0M7QUFBb0QsYUFBTyxFQUFHMEIsYUFBOUQ7QUFBQSw2QkFBOEU7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFFSTtBQUFNLFlBQU0sRUFBQyxHQUFiO0FBQWlCLFlBQU0sRUFBQyxLQUF4QjtBQUE4QixjQUFRLEVBQUdPLGtCQUF6QztBQUE4RCxlQUFTLEVBQUMsZUFBeEU7QUFBQSw4QkFDSTtBQUFPLFlBQUksRUFBQyxNQUFaO0FBQW1CLGlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsWUFBSSxFQUFDLFFBQWpEO0FBQTBELG9CQUFZLEVBQUMsS0FBdkU7QUFBNkUsYUFBSyxFQUFHakMsTUFBckY7QUFBOEYsZ0JBQVEsRUFBRytCLGNBQXpHO0FBQ0ksbUJBQVcsRUFBQyxXQURoQjtBQUM0QixnQkFBUTtBQURwQztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFJSTtBQUFRLGlCQUFTLEVBQUMsZ0JBQWxCO0FBQW1DLFlBQUksRUFBQyxRQUF4QztBQUFBLCtCQUNJO0FBQUcsbUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSkosZUFRSTtBQUFLLGlCQUFTLEVBQUMsc0NBQWY7QUFBQSxrQkFDTS9CLE1BQU0sQ0FBQ2pCLE1BQVAsR0FBZ0IsQ0FBaEIsSUFBcUJhLElBQXJCLElBQTZCQSxJQUFJLENBQUN1QyxRQUFMLENBQWN2QyxJQUFkLENBQW1CWixHQUFuQixDQUF3QixDQUFFb0QsT0FBRixFQUFXbkQsS0FBWCxrQkFDbkQsOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUksb0JBQW9CbUQsT0FBTyxDQUFDbEQsSUFBTSxFQUFqRDtBQUFxRCxtQkFBUyxFQUFDLHlCQUEvRDtBQUFBLGtDQUNJLDhEQUFDLDBFQUFEO0FBQWUsa0JBQU0sRUFBQyxTQUF0QjtBQUFnQyxlQUFHLEVBQUdDLHVCQUFBLEdBQW9DaUQsT0FBTyxDQUFDaEQsUUFBUixDQUFrQixDQUFsQixFQUFzQkMsR0FBaEc7QUFBc0csaUJBQUssRUFBRyxFQUE5RztBQUFtSCxrQkFBTSxFQUFHLEVBQTVIO0FBQWlJLGVBQUcsRUFBQztBQUFySTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLGVBRUk7QUFBSyxxQkFBUyxFQUFDLGFBQWY7QUFBNkIsbUNBQXVCLEVBQUcyQixnQkFBZ0IsQ0FBRU0sY0FBYyxDQUFFYyxPQUFPLENBQUM5QyxJQUFWLENBQWhCO0FBQXZFO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkosZUFHSTtBQUFNLHFCQUFTLEVBQUMsY0FBaEI7QUFBQSxzQkFFUThDLE9BQU8sQ0FBQzVDLEtBQVIsQ0FBZSxDQUFmLE1BQXVCNEMsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBdkIsR0FDSTRDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQnRELE1BQWpCLEtBQTRCLENBQTVCLGdCQUNJO0FBQUEsc0NBQ0k7QUFBTSx5QkFBUyxFQUFDLGdCQUFoQjtBQUFBLGdDQUFvQ0gsaURBQVMsQ0FBRXdELE9BQU8sQ0FBQzVDLEtBQVIsQ0FBZSxDQUFmLENBQUYsQ0FBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBTSx5QkFBUyxFQUFDLFdBQWhCO0FBQUEsZ0NBQStCWixpREFBUyxDQUFFd0QsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBRixDQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQSw0QkFESixnQkFNSTtBQUFPLHVCQUFTLEVBQUMsV0FBakI7QUFBQSw4QkFBZ0NaLGlEQUFTLENBQUV3RCxPQUFPLENBQUM1QyxLQUFSLENBQWUsQ0FBZixDQUFGLENBQXpDLGVBQXVFWixpREFBUyxDQUFFd0QsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBRixDQUFoRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBUFIsZ0JBUU07QUFBTSx1QkFBUyxFQUFDLFdBQWhCO0FBQUEsOEJBQStCWixpREFBUyxDQUFFd0QsT0FBTyxDQUFDNUMsS0FBUixDQUFlLENBQWYsQ0FBRixDQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUhKO0FBQUEsV0FBZ0csaUJBQWlCUCxLQUFPLEVBQXhIO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRDJCO0FBRG5DO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQW9DSDs7QUFFRCwrREFBZXFELHVEQUFVLENBQUU7QUFBRUMsS0FBRztBQUFMLENBQUYsQ0FBVixDQUFzRHhDLFVBQXRELENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2SUE7QUFFQTtBQUVBO0FBRWUsU0FBU21FLFlBQVQsR0FBd0I7QUFDbkMsTUFBSUMsR0FBRyxHQUFHLENBQVY7QUFFQTlHLGtEQUFTLENBQUUsTUFBTTtBQUNib0csVUFBTSxDQUFDakQsZ0JBQVAsQ0FBeUIsUUFBekIsRUFBbUM0RCxtQkFBbkM7QUFFQSxXQUFPLE1BQU07QUFDVFgsWUFBTSxDQUFDL0MsbUJBQVAsQ0FBNEIsUUFBNUIsRUFBc0MwRCxtQkFBdEM7QUFDSCxLQUZEO0FBR0gsR0FOUSxFQU1OLEVBTk0sQ0FBVDs7QUFRQSxRQUFNQSxtQkFBbUIsR0FBSzlGLENBQUYsSUFBUztBQUNqQyxRQUFJK0YsR0FBRyxHQUFHOUcsUUFBUSxDQUFDQyxhQUFULENBQXdCLGVBQXhCLElBQTRDRCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsZUFBeEIsRUFBMEM4RyxTQUExQyxHQUFzRC9HLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixRQUF4QixFQUFtQytHLFlBQXpGLEdBQXdHLEdBQXBKLEdBQTBKLEdBQXBLO0FBQ0EsUUFBSUMsWUFBWSxHQUFHakgsUUFBUSxDQUFDQyxhQUFULENBQXdCLCtCQUF4QixDQUFuQjtBQUNBLFFBQUlpSCxNQUFNLEdBQUcsQ0FBYjs7QUFFQSxRQUFLRCxZQUFMLEVBQW9CO0FBQ2hCQyxZQUFNLEdBQUdELFlBQVksQ0FBQ0QsWUFBdEI7QUFDSDs7QUFFRCxRQUFLZCxNQUFNLENBQUNpQixXQUFQLElBQXNCTCxHQUF0QixJQUE2QlosTUFBTSxDQUFDRSxVQUFQLEdBQW9CLEdBQWpELElBQXdEckYsQ0FBQyxDQUFDRSxhQUFGLENBQWdCbUcsT0FBaEIsSUFBMkJSLEdBQXhGLEVBQThGO0FBQzFGLFVBQUtLLFlBQUwsRUFBb0I7QUFDaEJBLG9CQUFZLENBQUM3RyxTQUFiLENBQXVCRyxHQUF2QixDQUE0QixPQUE1QjtBQUNBMEcsb0JBQVksQ0FBQ0ksWUFBYixDQUEyQixPQUEzQixFQUFvQyxrQkFBcEM7O0FBQ0EsWUFBSyxDQUFDckgsUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFOLEVBQTREO0FBQ3hELGNBQUlxSCxPQUFPLEdBQUd0SCxRQUFRLENBQUN1SCxhQUFULENBQXdCLEtBQXhCLENBQWQ7QUFDQUQsaUJBQU8sQ0FBQ0UsU0FBUixHQUFvQix3QkFBcEI7QUFDQVAsc0JBQVksQ0FBQzVDLFVBQWIsQ0FBd0JvRCxZQUF4QixDQUFzQ0gsT0FBdEMsRUFBK0NMLFlBQS9DO0FBQ0FqSCxrQkFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixFQUFvRHlILHFCQUFwRCxDQUEyRSxXQUEzRSxFQUF3RlQsWUFBeEY7QUFDQWpILGtCQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLEVBQW9Eb0gsWUFBcEQsQ0FBa0UsT0FBbEUsRUFBMkUsYUFBYUgsTUFBYixHQUFzQixJQUFqRztBQUNIOztBQUVELFlBQUssQ0FBQ2xILFFBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsRUFBb0QwSCxZQUFwRCxDQUFrRSxPQUFsRSxDQUFOLEVBQW9GO0FBQ2hGM0gsa0JBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsRUFBb0RvSCxZQUFwRCxDQUFrRSxPQUFsRSxFQUEyRSxhQUFhSCxNQUFiLEdBQXNCLElBQWpHO0FBQ0g7QUFDSjtBQUNKLEtBaEJELE1BZ0JPO0FBQ0gsVUFBS0QsWUFBTCxFQUFvQjtBQUNoQkEsb0JBQVksQ0FBQzdHLFNBQWIsQ0FBdUJFLE1BQXZCLENBQStCLE9BQS9CO0FBQ0EyRyxvQkFBWSxDQUFDSSxZQUFiLENBQTJCLE9BQTNCLEVBQXFDLG1CQUFtQkgsTUFBUSxJQUFoRTtBQUNIOztBQUVELFVBQUtsSCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLENBQUwsRUFBMkQ7QUFDdkRELGdCQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLEVBQW9EMkgsZUFBcEQsQ0FBcUUsT0FBckU7QUFDSDtBQUNKOztBQUVELFFBQUsxQixNQUFNLENBQUNFLFVBQVAsR0FBb0IsR0FBcEIsSUFBMkJwRyxRQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLENBQWhDLEVBQXNGO0FBQ2xGRCxjQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLEVBQW9ENEgsS0FBcEQsQ0FBMERYLE1BQTFELEdBQW1FLE1BQW5FO0FBQ0g7O0FBRUROLE9BQUcsR0FBRzdGLENBQUMsQ0FBQ0UsYUFBRixDQUFnQm1HLE9BQXRCO0FBQ0gsR0F6Q0Q7O0FBMkNBLHNCQUNJO0FBQUssYUFBUyxFQUFDLHlDQUFmO0FBQUEsNEJBQ0ksOERBQUMscUVBQUQ7QUFBTyxVQUFJLEVBQUMsR0FBWjtBQUFnQixlQUFTLEVBQUMsb0JBQTFCO0FBQUEsOEJBQ0k7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFESixlQUtJLDhEQUFDLHFFQUFEO0FBQU8sVUFBSSxFQUFDLE9BQVo7QUFBb0IsZUFBUyxFQUFDLGFBQTlCO0FBQUEsOEJBQ0k7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFMSixlQVNJLDhEQUFDLHFFQUFEO0FBQU8sVUFBSSxFQUFDLGlCQUFaO0FBQThCLGVBQVMsRUFBQyxhQUF4QztBQUFBLDhCQUNJO0FBQUcsaUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBVEosZUFhSSw4REFBQyxxRUFBRDtBQUFPLFVBQUksRUFBQyxnQkFBWjtBQUE2QixlQUFTLEVBQUMsYUFBdkM7QUFBQSw4QkFDSTtBQUFHLGlCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFFSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQWJKLGVBa0JJLDhEQUFDLGtGQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFsQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFzQkgsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xGRDtBQUdBO0FBRUE7QUFFZSxTQUFTVSxJQUFULENBQWVuSSxLQUFmLEVBQXVCO0FBQ2xDLFFBQU07QUFBRTRHLFNBQUY7QUFBU3dCLFlBQVEsR0FBRyxLQUFwQjtBQUEyQkMsV0FBM0I7QUFBb0NDLGFBQXBDO0FBQStDQyxRQUFJLEdBQUcsUUFBdEQ7QUFBZ0VwRztBQUFoRSxNQUF3RW5DLEtBQTlFO0FBRUEsU0FDSSxhQUFhdUksSUFBYixnQkFDSSw4REFBRSwyREFBRjtBQUFjLGFBQVMsRUFBR0gsUUFBUSxHQUFHLEtBQUgsR0FBVyxJQUE3QztBQUFBLGNBQ00sQ0FBRTtBQUFFSSxjQUFGO0FBQVlDLDJCQUFaO0FBQW1DQztBQUFuQyxLQUFGLGtCQUNFO0FBQUssZUFBUyxFQUFJLFFBQVFMLE9BQVMsRUFBbkM7QUFBQSw4QkFDSTtBQUFLLGlCQUFTLEVBQUksYUFBbEI7QUFBaUMsZUFBTyxFQUFHRyxRQUEzQztBQUFBLCtCQUNJLDhEQUFDLHFFQUFEO0FBQU8sY0FBSSxFQUFDLEdBQVo7QUFBZ0IsbUJBQVMsRUFBSSxpQkFBaUJFLFdBQVcsQ0FBQ0MsV0FBWixFQUEyQixFQUF6RTtBQUFBLHFCQUVRTCxTQUFTLGdCQUNMO0FBQUcscUJBQVMsRUFBR0E7QUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURLLEdBQzZCLEVBSDlDLEVBS00xQixLQUFLLEdBQ0hBLEtBREcsR0FDSyxFQU5oQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFhSTtBQUFLLFdBQUcsRUFBRzZCLHFCQUFYO0FBQUEsK0JBQ0k7QUFBSyxtQkFBUyxFQUFDLDJCQUFmO0FBQUEsb0JBQ016SSxLQUFLLENBQUM0STtBQURaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBYko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKLEdBeUJJLFlBQVlMLElBQVosZ0JBQ0ksOERBQUMsMkRBQUQ7QUFBYSxhQUFTLEVBQUdILFFBQVEsR0FBRyxLQUFILEdBQVcsSUFBNUM7QUFBQSxjQUNNLENBQUU7QUFBRUksY0FBRjtBQUFZQywyQkFBWjtBQUFtQ0M7QUFBbkMsS0FBRixrQkFDRTtBQUFBLDhCQUNJLDhEQUFDLHFFQUFEO0FBQ0ksWUFBSSxFQUFHdkcsR0FBRyxHQUFHQSxHQUFILEdBQVMsR0FEdkI7QUFFSSxlQUFPLEVBQUd5RSxLQUZkO0FBR0ksaUJBQVMsRUFBSSxpQkFBaUI4QixXQUFXLENBQUNDLFdBQVosRUFBMkIsRUFIN0Q7QUFJSSxlQUFPLEVBQUt2SCxDQUFGLElBQVM7QUFBRW9ILGtCQUFRO0FBQUs7QUFKdEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBU0k7QUFBSyxXQUFHLEVBQUdDLHFCQUFYO0FBQW1DLGlCQUFTLEVBQUMsaUJBQTdDO0FBQUEsa0JBQ016SSxLQUFLLENBQUM0STtBQURaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FUSjtBQUFBO0FBRlI7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKLGdCQWtCSSw4REFBQywyREFBRDtBQUFhLGFBQVMsRUFBR1IsUUFBUSxHQUFHLEtBQUgsR0FBVyxJQUE1QztBQUFBLGNBQ00sQ0FBRTtBQUFFSSxjQUFGO0FBQVlDLDJCQUFaO0FBQW1DQztBQUFuQyxLQUFGLGtCQUNFO0FBQUEsOEJBQ0ksOERBQUMscUVBQUQ7QUFBTyxZQUFJLEVBQUd2RyxHQUFHLEdBQUdBLEdBQUgsR0FBUyxHQUExQjtBQUFBLG1CQUVReUUsS0FGUixlQUlJO0FBQU0sbUJBQVMsRUFBSSxjQUFjOEIsV0FBVyxDQUFDQyxXQUFaLEVBQTJCLEVBQTVEO0FBQWdFLGlCQUFPLEVBQUt2SCxDQUFGLElBQVM7QUFBRW9ILG9CQUFRO0FBQUlwSCxhQUFDLENBQUNDLGNBQUY7QUFBcUI7QUFBdEg7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQVFJO0FBQUssV0FBRyxFQUFHb0gscUJBQVg7QUFBbUMsaUJBQVMsRUFBQyxpQkFBN0M7QUFBQSxrQkFDTXpJLEtBQUssQ0FBQzRJO0FBRFo7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVJKO0FBQUE7QUFGUjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBNUNaO0FBNkRBLFNBQU8sRUFBUDtBQUNILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hFRDtBQUVlLFNBQVNDLGdCQUFULENBQTRCN0ksS0FBNUIsRUFBb0M7QUFDL0MsUUFBTTtBQUFFOEksUUFBSSxHQUFHLFlBQVQ7QUFBdUJQLFFBQUksR0FBRyxDQUE5QjtBQUFpQ0YsV0FBTyxHQUFHO0FBQTNDLE1BQWtEckksS0FBeEQ7O0FBRUEsUUFBTStJLFFBQVEsR0FBRyxDQUFFO0FBQUVDLFFBQUY7QUFBUUMsU0FBUjtBQUFlQyxXQUFmO0FBQXdCQyxXQUF4QjtBQUFpQ0M7QUFBakMsR0FBRixLQUFvRDtBQUNqRSxRQUFLQSxTQUFMLEVBQWlCO0FBQ2IsMEJBQU87QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQ0liLElBQUksS0FBSyxDQUFULGdCQUNJO0FBQUssaUJBQVMsRUFBSSxhQUFhRixPQUFTLEVBQXhDO0FBQUEsK0JBQ0k7QUFBSyxtQkFBUyxFQUFDLCtCQUFmO0FBQUEsa0NBQ0k7QUFBTSxxQkFBUyxFQUFDLG1CQUFoQjtBQUFBLG9DQUNJO0FBQU0sdUJBQVMsRUFBQyxrQkFBaEI7QUFBQSx3QkFBcUNnQix3REFBTyxDQUFFTCxJQUFGO0FBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQU1JO0FBQU0scUJBQVMsRUFBQyxtQkFBaEI7QUFBQSxvQ0FDSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUEsd0JBQXFDSyx3REFBTyxDQUFFSixLQUFGO0FBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFOSixlQVdJO0FBQU0scUJBQVMsRUFBQyxtQkFBaEI7QUFBQSxvQ0FDSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUEsd0JBQXFDSSx3REFBTyxDQUFFSCxPQUFGO0FBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFYSixlQWVJO0FBQU0scUJBQVMsRUFBQyxtQkFBaEI7QUFBQSxvQ0FDSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUEsd0JBQXFDRyx3REFBTyxDQUFFRixPQUFGO0FBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZ0JBd0JJO0FBQUssaUJBQVMsRUFBQyxtREFBZjtBQUFBLGdDQUNJO0FBQU0sbUJBQVMsRUFBQyx5QkFBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFHSTtBQUFLLG1CQUFTLEVBQUMscUNBQWY7QUFBQSxrQ0FDSTtBQUFNLHFCQUFTLEVBQUMsd0JBQWhCO0FBQUEsb0NBQ0k7QUFBTSx1QkFBUyxFQUFDLGtCQUFoQjtBQUFBLHlCQUFxQ0Usd0RBQU8sQ0FBRUwsSUFBRixDQUE1QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQU1JO0FBQU0scUJBQVMsRUFBQyx5QkFBaEI7QUFBQSxtQ0FDSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUEseUJBQXFDSyx3REFBTyxDQUFFSixLQUFGLENBQTVDLGVBQXVEO0FBQU0seUJBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFBdkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFOSixlQVVJO0FBQU0scUJBQVMsRUFBQywyQkFBaEI7QUFBQSxtQ0FDSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUEseUJBQXFDSSx3REFBTyxDQUFFSCxPQUFGLENBQTVDLGVBQXlEO0FBQU0seUJBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFWSixlQWNJO0FBQU0scUJBQVMsRUFBQywyQkFBaEI7QUFBQSxtQ0FDSTtBQUFNLHVCQUFTLEVBQUMsa0JBQWhCO0FBQUEsd0JBQXFDRyx3REFBTyxDQUFFRixPQUFGO0FBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQWRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0F6QlI7QUFnREg7QUFDSixHQXJERDs7QUF1REEsc0JBQ0ksOERBQUMsd0RBQUQ7QUFDSSxRQUFJLEVBQUcsSUFBSUcsSUFBSixDQUFVUixJQUFWLENBRFg7QUFFSSxZQUFRLEVBQUdDO0FBRmY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBTUgsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xFRDtDQUVBOztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUEsTUFBTVEsWUFBWSxHQUFHO0FBQ2pCQyxTQUFPLEVBQUU7QUFDTEMsbUJBQWUsRUFBRSxpQkFEWjtBQUVMQyxXQUFPLEVBQUU7QUFGSjtBQURRLENBQXJCO0FBT0EsSUFBSTNILEtBQUssR0FBRyxDQUFaO0FBRUE0SCxnRUFBQSxDQUFxQixTQUFyQjs7QUFFQSxTQUFTQyxVQUFULEdBQXNCO0FBQ2xCLFFBQU07QUFBQSxPQUFFQyxJQUFGO0FBQUEsT0FBUUM7QUFBUixNQUFvQjlHLCtDQUFRLENBQUUsS0FBRixDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDK0csV0FBRDtBQUFBLE9BQWNDO0FBQWQsTUFBMEJoSCwrQ0FBUSxDQUFDLEVBQUQsQ0FBeEM7QUFDQSxRQUFNO0FBQUEsT0FBQ2lILElBQUQ7QUFBQSxPQUFPQztBQUFQLE1BQWtCbEgsK0NBQVEsQ0FBQyxDQUFELENBQWhDO0FBQ0EsUUFBTTtBQUFBLE9BQUNtSCxHQUFEO0FBQUEsT0FBTUM7QUFBTixNQUFnQnBILCtDQUFRLENBQUMsRUFBRCxDQUE5QjtBQUNBLFFBQU07QUFBQSxPQUFDcUgsVUFBRDtBQUFBLE9BQWNDO0FBQWQsTUFBZ0N0SCwrQ0FBUSxDQUFDLEVBQUQsQ0FBOUMsQ0FMa0IsQ0FPbEI7QUFDQTs7QUFDRixRQUFNO0FBQUEsT0FBQ3VILElBQUQ7QUFBQSxPQUFPQztBQUFQLE1BQWtCeEgsK0NBQVEsQ0FBQztBQUUvQnlILFlBQVEsRUFBRSxFQUZxQjtBQUcvQkMsYUFBUyxFQUFFLEVBSG9CO0FBSS9CQyxZQUFRLEVBQUUsRUFKcUI7QUFLL0JDLFNBQUssRUFBRSxFQUx3QjtBQU0vQkMsWUFBUSxFQUFFLEVBTnFCO0FBTy9CQyxPQUFHLEVBQUU7QUFQMEIsR0FBRCxDQUFoQztBQVVBLFFBQU07QUFBQSxPQUFDQyxXQUFEO0FBQUEsT0FBY0M7QUFBZCxNQUFnQ2hJLCtDQUFRLENBQUM7QUFFN0NpSSxhQUFTLEVBQUUsRUFGa0M7QUFHN0M3SSxRQUFJLEVBQUUsRUFIdUM7QUFJN0M4SSxZQUFRLEVBQUUsRUFKbUM7QUFLN0NDLFFBQUksRUFBRSxFQUx1QztBQU03Q0MsVUFBTSxFQUFFLEVBTnFDO0FBTzdDQyxhQUFTLEVBQUUsRUFQa0M7QUFRN0NDLFVBQU0sRUFBRSxFQVJxQztBQVM3Q0MsV0FBTyxFQUFFLEVBVG9DO0FBVTdDQyxlQUFXLEVBQUU7QUFWZ0MsR0FBRCxDQUE5Qzs7QUFnQkUsV0FBU0MsVUFBVCxHQUFzQjtBQUNsQnBMLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QixzQkFBeEIsRUFBaURHLFNBQWpELENBQTJERyxHQUEzRCxDQUFnRSxTQUFoRTtBQUNBUCxZQUFRLENBQUNDLGFBQVQsQ0FBd0Isa0NBQXhCLEVBQTZERyxTQUE3RCxDQUF1RUUsTUFBdkUsQ0FBK0UsaUNBQS9FO0FBQ0FOLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QiwwQ0FBeEIsRUFBcUVHLFNBQXJFLENBQStFRSxNQUEvRSxDQUF1RixpQ0FBdkY7QUFDQWlELGNBQVUsQ0FBRSxNQUFNO0FBQ2RrRyxhQUFPLENBQUUsS0FBRixDQUFQO0FBQ0gsS0FGUyxFQUVQLEdBRk8sQ0FBVjtBQUdIOztBQUVELFdBQVM0QixTQUFULENBQW9CdEssQ0FBcEIsRUFBdUJ1SyxVQUFVLEdBQUcsQ0FBcEMsRUFBd0M7QUFDcEN2SyxLQUFDLENBQUNDLGNBQUY7QUFDQVUsU0FBSyxHQUFHNEosVUFBUjtBQUNBN0IsV0FBTyxDQUFFLElBQUYsQ0FBUDtBQUNIOztBQUVDLFFBQU1wRSxNQUFNLEdBQUc7QUFDZkMsV0FBTyxFQUFFO0FBQUVDLGlCQUFXLEVBQUU7QUFBZjtBQURNLEdBQWYsQ0FsRGdCLENBeURwQjs7QUFDQSxRQUFNZ0csaUJBQWlCLEdBQUl4SyxDQUFELElBQU87QUFDL0IsVUFBTTBELEtBQUssR0FBRzFELENBQUMsQ0FBQ3dELE1BQUYsQ0FBU0UsS0FBVCxDQUFlWixPQUFmLENBQXVCLEtBQXZCLEVBQThCLEVBQTlCLENBQWQ7QUFDQThGLFlBQVEsQ0FBQ2xGLEtBQUQsQ0FBUjtBQUNELEdBSEQsQ0ExRG9CLENBK0RyQjs7O0FBQ0MsUUFBTStHLE9BQU8sR0FBSXpLLENBQUQsSUFBTTtBQUVwQkEsS0FBQyxDQUFDQyxjQUFGO0FBQ0EsVUFBTXlLLGNBQWMsR0FBRztBQUNuQi9CLGlCQUFXLEVBQUVBLFdBRE07QUFFbkJnQyxjQUFRLEVBQUU7QUFGUyxLQUF2QjtBQUtFaEcscURBQUEsQ0FBV2lHLCtEQUFYLEVBQXdCRixjQUF4QixFQUF1Q3BHLE1BQXZDLEVBQ0d1RyxJQURILENBQ1NDLFFBQUQsSUFBYztBQUNsQmhDLGFBQU8sQ0FBQ0QsSUFBSSxHQUFHLENBQVIsQ0FBUDtBQUVELEtBSkgsRUFJTW5FLEtBQUQsSUFBVyxDQUViLENBTkg7QUFPSCxHQWZELENBaEVvQixDQWlGckI7OztBQUNBLFFBQU1xRyxTQUFTLEdBQUkvSyxDQUFELElBQU07QUFFckJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBLFVBQU15SyxjQUFjLEdBQUc7QUFDbkIvQixpQkFBVyxFQUFFQSxXQURNO0FBRW5CcUMsU0FBRyxFQUFFQyxRQUFRLENBQUNsQyxHQUFELENBRk07QUFHbkI0QixjQUFRLEVBQUU7QUFIUyxLQUF2QjtBQU9FaEcscURBQUEsQ0FBV3VHLDhEQUFYLEVBQXVCUixjQUF2QixFQUFzQ3BHLE1BQXRDLEVBQ0d1RyxJQURILENBQ1NDLFFBQUQsSUFBYztBQUNqQjVCLG9CQUFjLENBQUM0QixRQUFRLENBQUN4SixJQUFWLENBQWQ7QUFDQTZKLGtCQUFZLENBQUNDLE9BQWIsQ0FBcUIsWUFBckIsRUFBbUNOLFFBQVEsQ0FBQ3hKLElBQVQsQ0FBYytKLEtBQWpEO0FBQ0F2QyxhQUFPLENBQUNELElBQUksR0FBRyxDQUFSLENBQVA7QUFFRixLQU5ILEVBTU1uRSxLQUFELElBQVcsQ0FHYixDQVRIO0FBVUgsR0FwQkYsQ0FsRnFCLENBMEd0Qjs7O0FBRUEsUUFBTTRHLFlBQVksR0FBR3RMLENBQUMsSUFBSTtBQUN0Qm9KLFdBQU8saUNBQ0ZELElBREU7QUFFTCxPQUFDbkosQ0FBQyxDQUFDd0QsTUFBRixDQUFTeEMsSUFBVixHQUFpQmhCLENBQUMsQ0FBQ3dELE1BQUYsQ0FBU0U7QUFGckIsT0FBUDtBQUlELEdBTEg7O0FBUUMsUUFBTTZILGFBQWEsR0FBR3ZMLENBQUMsSUFBRztBQUN0QjRKLGtCQUFjLGlDQUNQRCxXQURPO0FBRVYsT0FBQzNKLENBQUMsQ0FBQ3dELE1BQUYsQ0FBU3hDLElBQVYsR0FBaUJoQixDQUFDLENBQUN3RCxNQUFGLENBQVNFO0FBRmhCLE9BQWQ7QUFJSCxHQUxELENBcEhxQixDQTBIckI7OztBQUNBLFFBQU04SCxhQUFhLEdBQUl4TCxDQUFELElBQU07QUFDekJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBLFVBQU1xRSxNQUFNLEdBQUc7QUFDWEMsYUFBTyxFQUFFO0FBQUVrSCxxQkFBYSxFQUFHLEdBQUV4QyxVQUFVLENBQUNvQyxLQUFNO0FBQXJDO0FBREUsS0FBZjtBQUlBLFVBQU1YLGNBQWMsR0FBRztBQUNuQnJCLGNBQVEsRUFBRUYsSUFBSSxDQUFDRSxRQURJO0FBRW5CQyxlQUFTLEVBQUVILElBQUksQ0FBQ0csU0FGRztBQUduQkMsY0FBUSxFQUFFSixJQUFJLENBQUNJLFFBSEk7QUFJbkJDLFdBQUssRUFBRUwsSUFBSSxDQUFDSyxLQUpPO0FBS25CQyxjQUFRLEVBQUVOLElBQUksQ0FBQ00sUUFMSTtBQU1uQkMsU0FBRyxFQUFFdUIsUUFBUSxDQUFDOUIsSUFBSSxDQUFDTyxHQUFOO0FBTk0sS0FBdkI7QUFTRS9FLHFEQUFBLENBQVcrRyw4REFBWCxFQUFzQmhCLGNBQXRCLEVBQXFDcEcsTUFBckMsRUFDQ3VHLElBREQsQ0FDT0MsUUFBRCxJQUFhO0FBQ2pCaEMsYUFBTyxDQUFDRCxJQUFJLEdBQUcsQ0FBUixDQUFQO0FBQ0QsS0FIRCxFQUdJbkUsS0FBRCxJQUFVLENBRVosQ0FMRDtBQU1KLEdBdEJELENBM0hxQixDQW1KdEI7OztBQUNBLFFBQU1pSCxVQUFVLEdBQUkzTCxDQUFELElBQU07QUFDckJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBLFVBQU1xRSxNQUFNLEdBQUc7QUFDWEMsYUFBTyxFQUFFO0FBQUVrSCxxQkFBYSxFQUFHLEdBQUV4QyxVQUFVLENBQUNvQyxLQUFNO0FBQXJDO0FBREUsS0FBZjtBQUtBLFVBQU1YLGNBQWMsR0FBRztBQUNuQmIsZUFBUyxFQUFFRixXQUFXLENBQUNFLFNBREo7QUFFbkI3SSxVQUFJLEVBQUUySSxXQUFXLENBQUMzSSxJQUZDO0FBR25COEksY0FBUSxFQUFFSCxXQUFXLENBQUNHLFFBSEg7QUFJbkJDLFVBQUksRUFBRUosV0FBVyxDQUFDSSxJQUpDO0FBS25CQyxZQUFNLEVBQUVMLFdBQVcsQ0FBQ0ssTUFMRDtBQU1uQkMsZUFBUyxFQUFFZ0IsUUFBUSxDQUFDdEIsV0FBVyxDQUFDTSxTQUFiLENBTkE7QUFPbkJDLFlBQU0sRUFBRWUsUUFBUSxDQUFDdEIsV0FBVyxDQUFDTyxNQUFiLENBUEc7QUFRbkJDLGFBQU8sRUFBRWMsUUFBUSxDQUFDdEIsV0FBVyxDQUFDUSxPQUFiLENBUkU7QUFTbkJDLGlCQUFXLEVBQUVULFdBQVcsQ0FBQ1M7QUFUTixLQUF2QjtBQWFFekYscURBQUEsQ0FBV2lILG1FQUFYLEVBQTJCbEIsY0FBM0IsRUFBMENwRyxNQUExQyxFQUNDdUcsSUFERCxDQUNPQyxRQUFELElBQWE7QUFFakJlLGFBQU8sQ0FBQ0MsR0FBUixDQUFZaEIsUUFBWixFQUFxQixhQUFyQjtBQUNBaEMsYUFBTyxDQUFDRCxJQUFJLEdBQUcsQ0FBUixDQUFQO0FBQ0QsS0FMRCxFQUtJbkUsS0FBRCxJQUFVLENBRVosQ0FQRDtBQVFKLEdBN0JGOztBQStCRSxRQUFNcUgsU0FBUyxHQUFHLE1BQU07QUFFdEIsWUFBUWxELElBQVI7QUFDRSxXQUFLLENBQUw7QUFDRSw0QkFDSTtBQUFBLGtDQUNBLDhEQUFDLCtDQUFEO0FBQVMscUJBQVMsRUFBQyxnRkFBbkI7QUFBQSxtQ0FDQSw4REFBQywyQ0FBRDtBQUFLLHVCQUFTLEVBQUMsVUFBZjtBQUFBLHFDQUNJO0FBQU0seUJBQVMsRUFBQyxtQ0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFEQSxlQU9BO0FBQU0sa0JBQU0sRUFBQyxHQUFiO0FBQUEsb0NBQ0E7QUFBSyx1QkFBUyxFQUFDLGlCQUFmO0FBQUEscUNBQ1E7QUFBTyx5QkFBUyxFQUFDLGNBQWpCO0FBQWdDLG9CQUFJLEVBQUMsY0FBckM7QUFBb0Qsa0JBQUUsRUFBQyxjQUF2RDtBQUFzRSx5QkFBUyxFQUFDLElBQWhGO0FBQXFGLG9CQUFJLEVBQUMsTUFBMUY7QUFBaUcsMkJBQVcsRUFBQyxzQkFBN0c7QUFBb0kscUJBQUssRUFBRUYsV0FBM0k7QUFBd0osd0JBQVEsRUFBRTZCLGlCQUFsSztBQUFxTCx3QkFBUTtBQUE3TDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRFI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFEQSxlQUtJO0FBQVEsdUJBQVMsRUFBQyxvQ0FBbEI7QUFBdUQsa0JBQUksRUFBQyxRQUE1RDtBQUFxRSxxQkFBTyxFQUFFQyxPQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFMSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUEE7QUFBQSx3QkFESjs7QUFpQkYsV0FBSyxDQUFMO0FBQ0UsNEJBQ0k7QUFBQSxrQ0FDQSw4REFBQywrQ0FBRDtBQUFTLHFCQUFTLEVBQUMsZ0ZBQW5CO0FBQUEsbUNBQ0EsOERBQUMsMkNBQUQ7QUFBSyx1QkFBUyxFQUFDLFVBQWY7QUFBQSxxQ0FDSTtBQUFNLHlCQUFTLEVBQUMsbUNBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREEsZUFRQTtBQUFNLGtCQUFNLEVBQUMsR0FBYjtBQUFBLG9DQUNBO0FBQUssdUJBQVMsRUFBQyxpQkFBZjtBQUFBLHFDQUNRO0FBQU8seUJBQVMsRUFBQyxjQUFqQjtBQUFnQyxvQkFBSSxFQUFDLFlBQXJDO0FBQWtELGtCQUFFLEVBQUMsWUFBckQ7QUFBa0UseUJBQVMsRUFBQyxHQUE1RTtBQUFnRixvQkFBSSxFQUFDLE1BQXJGO0FBQTRGLDJCQUFXLEVBQUMsb0JBQXhHO0FBQTZILHFCQUFLLEVBQUUxQixHQUFwSTtBQUF5SSx3QkFBUSxFQUFFL0ksQ0FBQyxJQUFJZ0osTUFBTSxDQUFDaEosQ0FBQyxDQUFDd0QsTUFBRixDQUFTRSxLQUFWLENBQTlKO0FBQWdMLHdCQUFRO0FBQXhMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEUjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURBLGVBS0k7QUFBUSx1QkFBUyxFQUFDLG9DQUFsQjtBQUF1RCxrQkFBSSxFQUFDLFFBQTVEO0FBQXFFLHFCQUFPLEVBQUVxSCxTQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFMSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUkE7QUFBQSx3QkFESjs7QUFrQkYsV0FBSyxDQUFMO0FBQ0UsNEJBQ0k7QUFBQSxrQ0FDQSw4REFBQywrQ0FBRDtBQUFTLHFCQUFTLEVBQUMsZ0ZBQW5CO0FBQUEsbUNBQ0EsOERBQUMsMkNBQUQ7QUFBSyx1QkFBUyxFQUFDLFVBQWY7QUFBQSxxQ0FDSTtBQUFNLHlCQUFTLEVBQUMsbUNBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREEsZUFRQTtBQUFNLGtCQUFNLEVBQUMsR0FBYjtBQUFBLG9DQUNBO0FBQUssdUJBQVMsRUFBQyxZQUFmO0FBQUEsc0NBQ0k7QUFBTyx1QkFBTyxFQUFDLGNBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFPLG9CQUFJLEVBQUMsTUFBWjtBQUFtQix5QkFBUyxFQUFDLGNBQTdCO0FBQTRDLGtCQUFFLEVBQUMsVUFBL0M7QUFBMEQsb0JBQUksRUFBQyxVQUEvRDtBQUEwRSwyQkFBVyxFQUFDLGtCQUF0RjtBQUEwRyxxQkFBSyxFQUFFNUIsSUFBSSxDQUFDRSxRQUF0SDtBQUFnSSx3QkFBUSxFQUFFaUMsWUFBMUk7QUFBd0osd0JBQVE7QUFBaEs7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREEsZUFLQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxjQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBTyxvQkFBSSxFQUFDLE1BQVo7QUFBbUIseUJBQVMsRUFBQyxjQUE3QjtBQUE0QyxrQkFBRSxFQUFDLFdBQS9DO0FBQTJELG9CQUFJLEVBQUMsV0FBaEU7QUFBNEUsMkJBQVcsRUFBQyxrQkFBeEY7QUFBMkcscUJBQUssRUFBRW5DLElBQUksQ0FBQ0csU0FBdkg7QUFBa0ksd0JBQVEsRUFBRWdDO0FBQTVJO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUxBLGVBU0E7QUFBSyx1QkFBUyxFQUFDLFlBQWY7QUFBQSxzQ0FDSTtBQUFPLHVCQUFPLEVBQUMsY0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxNQUFaO0FBQW1CLHlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsa0JBQUUsRUFBQyxVQUEvQztBQUEwRCxvQkFBSSxFQUFDLFVBQS9EO0FBQTBFLDJCQUFXLEVBQUMsaUJBQXRGO0FBQXdHLHFCQUFLLEVBQUVuQyxJQUFJLENBQUNJLFFBQXBIO0FBQThILHdCQUFRLEVBQUUrQjtBQUF4STtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFUQSxlQWFBO0FBQUssdUJBQVMsRUFBQyxZQUFmO0FBQUEsc0NBQ0k7QUFBTyx1QkFBTyxFQUFDLGNBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFPLG9CQUFJLEVBQUMsT0FBWjtBQUFvQix5QkFBUyxFQUFDLGNBQTlCO0FBQTZDLGtCQUFFLEVBQUMsT0FBaEQ7QUFBd0Qsb0JBQUksRUFBQyxPQUE3RDtBQUFxRSwyQkFBVyxFQUFDLHNCQUFqRjtBQUF3RyxxQkFBSyxFQUFFbkMsSUFBSSxDQUFDSyxLQUFwSDtBQUEySCx3QkFBUSxFQUFFOEIsWUFBckk7QUFBbUosd0JBQVE7QUFBM0o7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBYkEsZUFpQkE7QUFBSyx1QkFBUyxFQUFDLFlBQWY7QUFBQSxzQ0FDSTtBQUFPLHVCQUFPLEVBQUMsaUJBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFPLG9CQUFJLEVBQUMsVUFBWjtBQUF1Qix5QkFBUyxFQUFDLGNBQWpDO0FBQWdELGtCQUFFLEVBQUMsVUFBbkQ7QUFBOEQsb0JBQUksRUFBQyxVQUFuRTtBQUE4RSwyQkFBVyxFQUFDLFlBQTFGO0FBQXVHLHFCQUFLLEVBQUVuQyxJQUFJLENBQUNNLFFBQW5IO0FBQTZILHdCQUFRLEVBQUU2QixZQUF2STtBQUFxSix3QkFBUTtBQUE3SjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFqQkEsZUFxQkE7QUFBSyx1QkFBUyxFQUFDLFlBQWY7QUFBQSxzQ0FDSTtBQUFPLHVCQUFPLEVBQUMsaUJBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFPLG9CQUFJLEVBQUMsUUFBWjtBQUFxQix5QkFBUyxFQUFDLGNBQS9CO0FBQThDLGtCQUFFLEVBQUMsS0FBakQ7QUFBdUQsb0JBQUksRUFBQyxLQUE1RDtBQUFrRSwyQkFBVyxFQUFDLEtBQTlFO0FBQW9GLHFCQUFLLEVBQUVuQyxJQUFJLENBQUNPLEdBQWhHO0FBQXFHLHdCQUFRLEVBQUU0QjtBQUEvRztBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFyQkEsZUEwQkE7QUFBSyx1QkFBUyxFQUFDLGFBQWY7QUFBQSxxQ0FDSTtBQUFLLHlCQUFTLEVBQUMsZUFBZjtBQUFBLHdDQUNJO0FBQU8sc0JBQUksRUFBQyxVQUFaO0FBQXVCLDJCQUFTLEVBQUMsaUJBQWpDO0FBQW1ELG9CQUFFLEVBQUMsZ0JBQXREO0FBQXVFLHNCQUFJLEVBQUMsZ0JBQTVFO0FBQ0ksMEJBQVE7QUFEWjtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURKLGVBR0k7QUFBTywyQkFBUyxFQUFDLG9CQUFqQjtBQUFzQyx5QkFBTyxFQUFDLGdCQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQTFCQSxlQWlDQTtBQUFRLHVCQUFTLEVBQUMsb0NBQWxCO0FBQXVELGtCQUFJLEVBQUMsUUFBNUQ7QUFBcUUscUJBQU8sRUFBR0UsYUFBL0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBakNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFSQTtBQUFBLHdCQURKOztBQThDRixXQUFLLENBQUw7QUFDRSw0QkFDSTtBQUFBLGtDQUNBLDhEQUFDLCtDQUFEO0FBQVMscUJBQVMsRUFBQyxnRkFBbkI7QUFBQSxtQ0FDQSw4REFBQywyQ0FBRDtBQUFLLHVCQUFTLEVBQUMsVUFBZjtBQUFBLHFDQUNJO0FBQU0seUJBQVMsRUFBQyxtQ0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFEQSxlQVFBO0FBQU0sa0JBQU0sRUFBQyxHQUFiO0FBQUEsb0NBQ0E7QUFBSyx1QkFBUyxFQUFDLFlBQWY7QUFBQSxzQ0FDSTtBQUFPLHVCQUFPLEVBQUMsY0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxNQUFaO0FBQW1CLHlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsa0JBQUUsRUFBQyxXQUEvQztBQUEyRCxvQkFBSSxFQUFDLFdBQWhFO0FBQTRFLDJCQUFXLEVBQUMsb0JBQXhGO0FBQThHLHFCQUFLLEVBQUU3QixXQUFXLENBQUNFLFNBQWpJO0FBQTRJLHdCQUFRLEVBQUUwQixhQUF0SjtBQUFxSyx3QkFBUTtBQUE3SztBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFEQSxlQUtBO0FBQUssdUJBQVMsRUFBQyxZQUFmO0FBQUEsc0NBQ0k7QUFBTyx1QkFBTyxFQUFDLGNBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZUFFSTtBQUFPLG9CQUFJLEVBQUMsTUFBWjtBQUFtQix5QkFBUyxFQUFDLGNBQTdCO0FBQTRDLGtCQUFFLEVBQUMsTUFBL0M7QUFBc0Qsb0JBQUksRUFBQyxNQUEzRDtBQUFrRSwyQkFBVyxFQUFDLFlBQTlFO0FBQTJGLHFCQUFLLEVBQUU1QixXQUFXLENBQUMzSSxJQUE5RztBQUFvSCx3QkFBUSxFQUFFdUs7QUFBOUg7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBTEEsZUFTQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxjQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBRUk7QUFBTyxvQkFBSSxFQUFDLE1BQVo7QUFBbUIseUJBQVMsRUFBQyxjQUE3QjtBQUE0QyxrQkFBRSxFQUFDLFVBQS9DO0FBQTBELG9CQUFJLEVBQUMsVUFBL0Q7QUFBMEUsMkJBQVcsRUFBQyxxQkFBdEY7QUFBNEcscUJBQUssRUFBRTVCLFdBQVcsQ0FBQ0csUUFBL0g7QUFBeUksd0JBQVEsRUFBRXlCO0FBQW5KO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQVRBLGVBYUE7QUFBSyx1QkFBUyxFQUFDLFlBQWY7QUFBQSxzQ0FDSTtBQUFPLHVCQUFPLEVBQUMsY0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxNQUFaO0FBQW1CLHlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsa0JBQUUsRUFBQyxNQUEvQztBQUFzRCxvQkFBSSxFQUFDLE1BQTNEO0FBQWtFLDJCQUFXLEVBQUMsbUJBQTlFO0FBQWtHLHFCQUFLLEVBQUU1QixXQUFXLENBQUNJLElBQXJIO0FBQTJILHdCQUFRLEVBQUV3QixhQUFySTtBQUFvSix3QkFBUTtBQUE1SjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFiQSxlQWlCQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxpQkFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxNQUFaO0FBQW1CLHlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsa0JBQUUsRUFBQyxRQUEvQztBQUF3RCxvQkFBSSxFQUFDLFFBQTdEO0FBQXNFLDJCQUFXLEVBQUMsU0FBbEY7QUFBNEYscUJBQUssRUFBRTVCLFdBQVcsQ0FBQ0ssTUFBL0c7QUFBdUgsd0JBQVEsRUFBRXVCO0FBQWpJO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWpCQSxlQXFCQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxpQkFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxRQUFaO0FBQXFCLHlCQUFTLEVBQUMsY0FBL0I7QUFBOEMsa0JBQUUsRUFBQyxXQUFqRDtBQUE2RCxvQkFBSSxFQUFDLFdBQWxFO0FBQThFLDJCQUFXLEVBQUMsY0FBMUY7QUFBeUcscUJBQUssRUFBRTVCLFdBQVcsQ0FBQ00sU0FBNUg7QUFBdUksd0JBQVEsRUFBRXNCLGFBQWpKO0FBQWdLLHdCQUFRO0FBQXhLO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQXJCQSxlQTBCQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxpQkFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxRQUFaO0FBQXFCLHlCQUFTLEVBQUMsY0FBL0I7QUFBOEMsa0JBQUUsRUFBQyxRQUFqRDtBQUEwRCxvQkFBSSxFQUFDLFFBQS9EO0FBQXdFLDJCQUFXLEVBQUMsU0FBcEY7QUFBOEYscUJBQUssRUFBRTVCLFdBQVcsQ0FBQ08sTUFBakg7QUFBeUgsd0JBQVEsRUFBRXFCO0FBQW5JO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQTFCQSxlQThCQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxpQkFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxRQUFaO0FBQXFCLHlCQUFTLEVBQUMsY0FBL0I7QUFBOEMsa0JBQUUsRUFBQyxTQUFqRDtBQUEyRCxvQkFBSSxFQUFDLFNBQWhFO0FBQTBFLDJCQUFXLEVBQUMsVUFBdEY7QUFBaUcscUJBQUssRUFBRTVCLFdBQVcsQ0FBQ1EsT0FBcEg7QUFBNkgsd0JBQVEsRUFBRW9CO0FBQXZJO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQTlCQSxlQWtDQTtBQUFLLHVCQUFTLEVBQUMsWUFBZjtBQUFBLHNDQUNJO0FBQU8sdUJBQU8sRUFBQyxpQkFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQUVJO0FBQU8sb0JBQUksRUFBQyxNQUFaO0FBQW1CLHlCQUFTLEVBQUMsY0FBN0I7QUFBNEMsa0JBQUUsRUFBQyxhQUEvQztBQUE2RCxvQkFBSSxFQUFDLGFBQWxFO0FBQWdGLDJCQUFXLEVBQUMsY0FBNUY7QUFBMkcscUJBQUssRUFBRTVCLFdBQVcsQ0FBQ1MsV0FBOUg7QUFBMkksd0JBQVEsRUFBRW1CO0FBQXJKO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWxDQSxlQXVDQTtBQUFLLHVCQUFTLEVBQUMsYUFBZjtBQUFBLHFDQUNJO0FBQUsseUJBQVMsRUFBQyxlQUFmO0FBQUEsd0NBQ0k7QUFBTyxzQkFBSSxFQUFDLFVBQVo7QUFBdUIsMkJBQVMsRUFBQyxpQkFBakM7QUFBbUQsb0JBQUUsRUFBQyxnQkFBdEQ7QUFBdUUsc0JBQUksRUFBQyxnQkFBNUU7QUFDSSwwQkFBUTtBQURaO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREosZUFHSTtBQUFPLDJCQUFTLEVBQUMsb0JBQWpCO0FBQXNDLHlCQUFPLEVBQUMsZ0JBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBdkNBLGVBOENBO0FBQVEsdUJBQVMsRUFBQyxvQ0FBbEI7QUFBdUQsa0JBQUksRUFBQyxRQUE1RDtBQUFxRSxxQkFBTyxFQUFHSSxVQUEvRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkE5Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQVJBO0FBQUEsd0JBREo7O0FBNERGO0FBQ0UsZUFBTyxLQUFQO0FBbkpKO0FBc0pELEdBeEpEOztBQTRKRSxzQkFDSTtBQUFBLDRCQUNJO0FBQUcsZUFBUyxFQUFDLHNCQUFiO0FBQW9DLFVBQUksRUFBQyxHQUF6QztBQUE2QyxhQUFPLEVBQUdyQixTQUF2RDtBQUFBLDhCQUNJO0FBQUcsaUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFESixlQUdJO0FBQU0sZUFBUyxFQUFDLFdBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSEosZUFJSTtBQUFHLGVBQVMsRUFBQyxvQkFBYjtBQUFrQyxhQUFPLEVBQUt0SyxDQUFGLElBQVNzSyxTQUFTLENBQUV0SyxDQUFGLEVBQUssQ0FBTCxDQUE5RDtBQUF5RSxVQUFJLEVBQUMsR0FBOUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFKSixFQU9ReUksSUFBSSxnQkFDQSw4REFBQyxvREFBRDtBQUNJLFlBQU0sRUFBR0EsSUFEYjtBQUVJLG9CQUFjLEVBQUc0QixVQUZyQjtBQUdJLFdBQUssRUFBR2xDLFlBSFo7QUFJSSxrQkFBWSxFQUFDLGFBSmpCO0FBS0ksZUFBUyxFQUFDLGFBTGQ7QUFNSSxzQkFBZ0IsRUFBQyxxQkFOckI7QUFPSSxpQ0FBMkIsRUFBRyxLQVBsQztBQVFJLFFBQUUsRUFBQyxhQVJQO0FBQUEsOEJBVUk7QUFBSyxpQkFBUyxFQUFDLFVBQWY7QUFBQSwrQkFDSTtBQUFLLG1CQUFTLEVBQUMsMkNBQWY7QUFBQSxpQ0FDSSw4REFBQyw0Q0FBRDtBQUFNLGdDQUFvQixFQUFDLFFBQTNCO0FBQW9DLHFDQUF5QixFQUFDLFFBQTlEO0FBQXVFLHdCQUFZLEVBQUd4SCxLQUF0RjtBQUFBLG1DQUVJO0FBQUssdUJBQVMsRUFBQyxhQUFmO0FBQUEsd0JBQ0NvTCxTQUFTO0FBRFY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVZKLGVBdUJJO0FBQVEsYUFBSyxFQUFDLGFBQWQ7QUFBNEIsWUFBSSxFQUFDLFFBQWpDO0FBQTBDLGlCQUFTLEVBQUMsV0FBcEQ7QUFBZ0UsZUFBTyxFQUFHMUIsVUFBMUU7QUFBQSwrQkFBdUY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBdkY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXZCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFEQSxHQXlCVyxFQWhDdkI7QUFBQSxrQkFESjtBQXFDSDs7QUFFRCwrREFBaUI3QixVQUFqQixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMVlBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQSxNQUFNTCxZQUFZLEdBQUc7QUFDakI2RCxTQUFPLEVBQUU7QUFDTEMsWUFBUSxFQUFFO0FBREwsR0FEUTtBQUlqQjdELFNBQU8sRUFBRTtBQUNMOEQsY0FBVSxFQUFFLGdCQURQO0FBRUxDLGFBQVMsRUFBRSxRQUZOO0FBR0w3RCxXQUFPLEVBQUUsTUFISjtBQUlMOEQsYUFBUyxFQUFFLE1BSk47QUFLTEMsV0FBTyxFQUFFO0FBTEo7QUFKUSxDQUFyQjtBQWFBOUQsZ0VBQUEsQ0FBcUIsU0FBckI7O0FBRUEsU0FBUytELFVBQVQsQ0FBc0IxTixLQUF0QixFQUE4QjtBQUMxQixRQUFNQyxNQUFNLEdBQUdDLHNEQUFTLEVBQXhCO0FBQ0EsUUFBTTtBQUFFeU4sVUFBRjtBQUFVbEMsY0FBVjtBQUFzQm1DO0FBQXRCLE1BQXFDNU4sS0FBM0M7QUFFQUcsa0RBQVMsQ0FBRSxNQUFNO0FBQ2JzTCxjQUFVO0FBRVZ4TCxVQUFNLENBQUM0TixNQUFQLENBQWNDLEVBQWQsQ0FBa0Isa0JBQWxCLEVBQXNDckMsVUFBdEM7QUFFQSxXQUFPLE1BQU07QUFDVHhMLFlBQU0sQ0FBQzROLE1BQVAsQ0FBY0UsR0FBZCxDQUFtQixrQkFBbkIsRUFBdUN0QyxVQUF2QztBQUNILEtBRkQ7QUFHSCxHQVJRLEVBUU4sRUFSTSxDQUFUO0FBVUF0TCxrREFBUyxDQUFFLE1BQU07QUFDYixRQUFLd04sTUFBTCxFQUNJL0osVUFBVSxDQUFFLE1BQU07QUFDZHZELGNBQVEsQ0FBQ0MsYUFBVCxDQUF3QixzQkFBeEIsRUFBaURHLFNBQWpELENBQTJERyxHQUEzRCxDQUFnRSxRQUFoRTtBQUNILEtBRlMsRUFFUCxHQUZPLENBQVY7QUFHUCxHQUxRLEVBS04sQ0FBRStNLE1BQUYsQ0FMTSxDQUFUOztBQU9BLFFBQU1LLFVBQVUsR0FBRyxNQUFNO0FBQ3JCM04sWUFBUSxDQUFDQyxhQUFULENBQXdCLHNCQUF4QixFQUFpREcsU0FBakQsQ0FBMkRHLEdBQTNELENBQWdFLFNBQWhFO0FBQ0FQLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QixzQkFBeEIsRUFBaURHLFNBQWpELENBQTJERSxNQUEzRCxDQUFtRSxRQUFuRTtBQUNBTixZQUFRLENBQUNDLGFBQVQsQ0FBd0Isa0NBQXhCLEVBQTZERyxTQUE3RCxDQUF1RUUsTUFBdkUsQ0FBK0UsaUNBQS9FO0FBQ0FOLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QiwwQ0FBeEIsRUFBcUVHLFNBQXJFLENBQStFRSxNQUEvRSxDQUF1RixpQ0FBdkY7QUFFQWlELGNBQVUsQ0FBRSxNQUFNO0FBQ2Q2SCxnQkFBVTtBQUNiLEtBRlMsRUFFUCxHQUZPLENBQVY7QUFHSCxHQVREOztBQVdBLHNCQUNJLDhEQUFFLG9EQUFGO0FBQ0ksVUFBTSxFQUFHa0MsTUFEYjtBQUVJLGdCQUFZLEVBQUMsWUFGakI7QUFHSSxrQkFBYyxFQUFHSyxVQUhyQjtBQUlJLDBCQUFzQixFQUFHLEtBSjdCO0FBS0ksU0FBSyxFQUFHekUsWUFMWjtBQU1JLG9CQUFnQixFQUFDLHFCQU5yQjtBQU9JLGFBQVMsRUFBQyxpQkFQZDtBQU9nQyxNQUFFLEVBQUMsYUFQbkM7QUFBQSw0QkFTSTtBQUFPLFNBQUcsRUFBR3RILHVCQUFBLEdBQW9DMkwsVUFBakQ7QUFBOEQsY0FBUSxFQUFHLElBQXpFO0FBQWdGLFVBQUksRUFBRyxJQUF2RjtBQUE4RixjQUFRLEVBQUcsSUFBekc7QUFBZ0gsZUFBUyxFQUFDO0FBQTFIO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFUSixlQVdJO0FBQVEsV0FBSyxFQUFDLGFBQWQ7QUFBNEIsVUFBSSxFQUFDLFFBQWpDO0FBQTBDLGVBQVMsRUFBQyxXQUFwRDtBQUFnRSxhQUFPLEVBQUduQyxVQUExRTtBQUFBLDZCQUF3RjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF4RjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBWEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFlSDs7QUFFRCxTQUFTbEosZUFBVCxDQUEyQkMsS0FBM0IsRUFBbUM7QUFDL0IsU0FBTztBQUNIbUwsVUFBTSxFQUFFbkwsS0FBSyxDQUFDeUwsS0FBTixDQUFZdkMsU0FEakI7QUFFSGtDLGNBQVUsRUFBRXBMLEtBQUssQ0FBQ3lMLEtBQU4sQ0FBWUw7QUFGckIsR0FBUDtBQUlIOztBQUVELCtEQUFlakwsb0RBQU8sQ0FBRUosZUFBRixFQUFtQjtBQUFFa0osWUFBVSxFQUFFeUMsaUVBQXVCekM7QUFBckMsQ0FBbkIsQ0FBUCxDQUFxRWlDLFVBQXJFLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFFQTtBQUVBO0FBRUEsTUFBTW5FLFlBQVksR0FBRztBQUNqQjZELFNBQU8sRUFBRTtBQUNMQyxZQUFRLEVBQUU7QUFETCxHQURRO0FBSWpCN0QsU0FBTyxFQUFFO0FBQ0w4RCxjQUFVLEVBQUUsZ0JBRFA7QUFFTGEsVUFBTSxFQUFFLE9BRkg7QUFHTFosYUFBUyxFQUFFLFFBSE47QUFJTEMsYUFBUyxFQUFFO0FBSk47QUFKUSxDQUFyQjtBQVlBN0QsZ0VBQUEsQ0FBcUIsU0FBckI7O0FBRUEsU0FBU3lFLFNBQVQsQ0FBb0JwTyxLQUFwQixFQUE0QjtBQUN4QixRQUFNO0FBQUVnQyxRQUFGO0FBQVFxTSxrQkFBUjtBQUF3QlY7QUFBeEIsTUFBbUMzTixLQUF6QztBQUVBLE1BQUssQ0FBQzJOLE1BQU4sRUFBZSxvQkFBTztBQUFBO0FBQUE7QUFBQTtBQUFBLFVBQVA7QUFFZixRQUFNO0FBQUEsT0FBRVcsTUFBRjtBQUFBLE9BQVVDO0FBQVYsTUFBOEJ2TCwrQ0FBUSxDQUFFLEtBQUYsQ0FBNUM7QUFFQSxRQUFNO0FBQUVOLFFBQUY7QUFBUThMO0FBQVIsTUFBb0JDLDZEQUFRLENBQUVDLHdEQUFGLEVBQWU7QUFBRTdLLGFBQVMsRUFBRTtBQUFFN0IsVUFBRjtBQUFRMk0sY0FBUSxFQUFFO0FBQWxCO0FBQWIsR0FBZixDQUFsQztBQUNBLFFBQU16SixPQUFPLEdBQUd4QyxJQUFJLElBQUlBLElBQUksQ0FBQ3dDLE9BQTdCO0FBRUEvRSxrREFBUyxDQUFFLE1BQU07QUFDYnlELGNBQVUsQ0FBRSxNQUFNO0FBQ2QsVUFBSyxDQUFDNEssT0FBRCxJQUFZOUwsSUFBWixJQUFvQmlMLE1BQXBCLElBQThCdE4sUUFBUSxDQUFDQyxhQUFULENBQXdCLGtCQUF4QixDQUFuQyxFQUNJc08sbURBQVksQ0FBRSxrQkFBRixDQUFaLENBQW1DZCxFQUFuQyxDQUF1QyxNQUF2QyxFQUErQyxZQUFZO0FBQ3ZEUyx1QkFBZSxDQUFFLElBQUYsQ0FBZjtBQUNBaEksY0FBTSxDQUFDc0ksTUFBUCxDQUFlLDJDQUFmLEVBQTZEQyxPQUE3RCxDQUFzRSxzQkFBdEU7QUFDSCxPQUhELEVBR0loQixFQUhKLENBR1EsVUFIUixFQUdvQixZQUFZO0FBQzVCUyx1QkFBZSxDQUFFLEtBQUYsQ0FBZjtBQUNILE9BTEQ7QUFNUCxLQVJTLEVBUVAsR0FSTyxDQUFWO0FBU0gsR0FWUSxFQVVOLENBQUU3TCxJQUFGLEVBQVFpTCxNQUFSLENBVk0sQ0FBVDtBQVlBLE1BQUszTCxJQUFJLEtBQUssRUFBVCxJQUFlLENBQUNrRCxPQUFoQixJQUEyQixDQUFDQSxPQUFPLENBQUN4QyxJQUF6QyxFQUFnRCxPQUFPLEVBQVA7O0FBRWhELFFBQU1xTSxVQUFVLEdBQUcsTUFBTTtBQUNyQjFPLFlBQVEsQ0FBQ0MsYUFBVCxDQUF3QixzQkFBeEIsRUFBaURHLFNBQWpELENBQTJERyxHQUEzRCxDQUFnRSxTQUFoRTtBQUNBUCxZQUFRLENBQUNDLGFBQVQsQ0FBd0Isa0JBQXhCLEVBQTZDRyxTQUE3QyxDQUF1REcsR0FBdkQsQ0FBNEQsU0FBNUQ7QUFDQTJOLG1CQUFlLENBQUUsS0FBRixDQUFmO0FBQ0EzSyxjQUFVLENBQUUsTUFBTTtBQUNkeUssb0JBQWM7QUFDakIsS0FGUyxFQUVQLEdBRk8sQ0FBVjtBQUdILEdBUEQ7O0FBU0Esc0JBQ0ksOERBQUMsb0RBQUQ7QUFDSSxVQUFNLEVBQUdWLE1BRGI7QUFFSSxnQkFBWSxFQUFDLFdBRmpCO0FBR0ksa0JBQWMsRUFBR29CLFVBSHJCO0FBSUksMEJBQXNCLEVBQUcsS0FKN0I7QUFLSSxTQUFLLEVBQUd4RixZQUxaO0FBTUksYUFBUyxFQUFDLDBEQU5kO0FBTXlFLE1BQUUsRUFBQyxtQkFONUU7QUFBQSw0QkFRSTtBQUFBLDhCQUNJO0FBQUssaUJBQVMsRUFBSSxlQUFlK0UsTUFBTSxHQUFHLEVBQUgsR0FBUSxRQUFVLEVBQXpEO0FBQUEsZ0NBQ0k7QUFBSyxtQkFBUyxFQUFDLFVBQWY7QUFBQSxpQ0FDSTtBQUFLLHFCQUFTLEVBQUMsOEJBQWY7QUFBQSxvQ0FDSTtBQUFLLHVCQUFTLEVBQUMscUJBQWY7QUFBQSx5QkFDTXBKLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXNNLE1BQWIsZ0JBQXNCO0FBQU8seUJBQVMsRUFBQyx5QkFBakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBQXRCLEdBQStFLEVBRHJGLEVBRU05SixPQUFPLENBQUN4QyxJQUFSLENBQWF1TSxNQUFiLGdCQUFzQjtBQUFPLHlCQUFTLEVBQUMseUJBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUF0QixHQUErRSxFQUZyRixFQUlRL0osT0FBTyxDQUFDeEMsSUFBUixDQUFhd00sUUFBYixHQUF3QixDQUF4QixHQUNJaEssT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUFzQnRELE1BQXRCLEtBQWlDLENBQWpDLGdCQUNJO0FBQU8seUJBQVMsRUFBQywwQkFBakI7QUFBQSwyQkFBOENxRCxPQUFPLENBQUN4QyxJQUFSLENBQWF3TSxRQUEzRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREosZ0JBRU07QUFBTyx5QkFBUyxFQUFDLDBCQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFIVixHQUlNLEVBUmQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURKLGVBYUksOERBQUMsc0VBQUQ7QUFBYSxxQkFBTyxFQUFDLGlEQUFyQjtBQUF1RSxxQkFBTyxFQUFHQyw4REFBakY7QUFBQSx3QkFFUWpLLE9BQU8sSUFBSUEsT0FBTyxDQUFDeEMsSUFBbkIsSUFBMkJ3QyxPQUFPLENBQUN4QyxJQUFSLENBQWEwTSxjQUFiLENBQTRCdE4sR0FBNUIsQ0FBaUMsQ0FBRUwsSUFBRixFQUFRTSxLQUFSLGtCQUN4RCw4REFBQyw2REFBRDtBQUVJLHdCQUFRLEVBQUdFLHVCQUFBLEdBQW9DUixJQUFJLENBQUNVLEdBRnhEO0FBR0ksd0JBQVEsRUFBQyxXQUhiO0FBSUksNkJBQWEsRUFBR0YsdUJBQUEsR0FBb0NSLElBQUksQ0FBQ1UsR0FKN0Q7QUFLSSwwQkFBVSxFQUFHLEtBTGpCO0FBTUksK0JBQWUsRUFBQyxPQU5wQjtBQU9JLGlDQUFpQixFQUFDLFdBUHRCO0FBUUkseUJBQVMsRUFBQztBQVJkLGlCQUNVLHFCQUFxQkosS0FEL0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFEdUI7QUFGbkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFiSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBa0NJO0FBQUssbUJBQVMsRUFBQyxVQUFmO0FBQUEsaUNBQ0ksOERBQUMsb0ZBQUQ7QUFBVyxnQkFBSSxFQUFHVyxJQUFsQjtBQUF5QixtQkFBTyxFQUFDLGlCQUFqQztBQUFtRCxpQkFBSyxFQUFHO0FBQTNEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWxDSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQXdDSTtBQUFRLGFBQUssRUFBQyxhQUFkO0FBQTRCLFlBQUksRUFBQyxRQUFqQztBQUEwQyxpQkFBUyxFQUFDLGVBQXBEO0FBQW9FLGVBQU8sRUFBR3FNLFVBQTlFO0FBQUEsK0JBQTRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTVGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0F4Q0o7QUFBQSxvQkFSSixFQW1EUVQsTUFBTSxHQUFHLEVBQUgsZ0JBQVE7QUFBSyxlQUFTLEVBQUMsK0NBQWY7QUFBQSw4QkFDVjtBQUFLLGlCQUFTLEVBQUMsVUFBZjtBQUFBLCtCQUNJO0FBQUssbUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRFUsZUFNVjtBQUFLLGlCQUFTLEVBQUMsVUFBZjtBQUFBLCtCQUNJO0FBQUssbUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBTlU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBbkR0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQWlFSDs7QUFFRCxTQUFTL0wsZUFBVCxDQUEwQkMsS0FBMUIsRUFBa0M7QUFDOUIsU0FBTztBQUNIUixRQUFJLEVBQUVRLEtBQUssQ0FBQ3lMLEtBQU4sQ0FBWUwsVUFEZjtBQUVIRCxVQUFNLEVBQUVuTCxLQUFLLENBQUN5TCxLQUFOLENBQVlvQjtBQUZqQixHQUFQO0FBSUg7O0FBRUQsK0RBQWVqSyx1REFBVSxDQUFFO0FBQUVDLEtBQUc7QUFBTCxDQUFGLENBQVYsQ0FBc0QxQyxvREFBTyxDQUFFSixlQUFGLEVBQW1CO0FBQUU4TCxnQkFBYyxFQUFFSCxzRUFBMkJHO0FBQTdDLENBQW5CLENBQVAsQ0FBNkVELFNBQTdFLENBQXRELENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzSUE7QUFFZSxTQUFTa0IsUUFBVCxPQUEyQztBQUFBLE1BQXhCO0FBQUVqTixPQUFHLEdBQUc7QUFBUixHQUF3QjtBQUFBLE1BQVZyQyxLQUFVOztBQUN0RCxRQUFNO0FBQUVxSSxXQUFPLEdBQUc7QUFBWixNQUFtQ3JJLEtBQXpDO0FBQ0EsUUFBTTtBQUFBLE9BQUV1UCxRQUFGO0FBQUEsT0FBWUM7QUFBWixNQUE0QnhNLCtDQUFRLENBQUVxSixRQUFRLENBQUVoSyxHQUFGLENBQVYsQ0FBMUM7QUFFQWxDLGtEQUFTLENBQUUsTUFBTTtBQUNicVAsZUFBVyxDQUFFbk4sR0FBRixDQUFYO0FBQ0gsR0FGUSxFQUVOLENBQUVyQyxLQUFLLENBQUNrRixPQUFSLENBRk0sQ0FBVDtBQUlBL0Usa0RBQVMsQ0FBRSxNQUFNO0FBQ2JILFNBQUssQ0FBQ3lQLFdBQU4sSUFBcUJ6UCxLQUFLLENBQUN5UCxXQUFOLENBQW1CRixRQUFuQixDQUFyQjtBQUNILEdBRlEsRUFFTixDQUFFQSxRQUFGLENBRk0sQ0FBVDs7QUFJQSxXQUFTRyxhQUFULEdBQXlCO0FBQ3JCLFFBQUtILFFBQVEsR0FBRyxDQUFoQixFQUFvQjtBQUNoQkMsaUJBQVcsQ0FBRW5ELFFBQVEsQ0FBRWtELFFBQUYsQ0FBUixHQUF1QixDQUF6QixDQUFYO0FBQ0g7QUFDSjs7QUFFRCxXQUFTSSxZQUFULEdBQXdCO0FBQ3BCLFFBQUtKLFFBQVEsR0FBR3ZQLEtBQUssQ0FBQzRQLEdBQXRCLEVBQTRCO0FBQ3hCSixpQkFBVyxDQUFFbkQsUUFBUSxDQUFFa0QsUUFBRixDQUFSLEdBQXVCLENBQXpCLENBQVg7QUFDSDtBQUNKOztBQUVELFdBQVNNLFNBQVQsQ0FBb0J6TyxDQUFwQixFQUF3QjtBQUNwQixRQUFJME8sTUFBSjs7QUFFQSxRQUFLMU8sQ0FBQyxDQUFDRSxhQUFGLENBQWdCd0QsS0FBaEIsS0FBMEIsRUFBL0IsRUFBb0M7QUFDaENnTCxZQUFNLEdBQUdDLElBQUksQ0FBQ0MsR0FBTCxDQUFVM0QsUUFBUSxDQUFFakwsQ0FBQyxDQUFDRSxhQUFGLENBQWdCd0QsS0FBbEIsQ0FBbEIsRUFBNkM5RSxLQUFLLENBQUM0UCxHQUFuRCxDQUFUO0FBQ0FFLFlBQU0sR0FBR0MsSUFBSSxDQUFDSCxHQUFMLENBQVVFLE1BQVYsRUFBa0IsQ0FBbEIsQ0FBVDtBQUNBTixpQkFBVyxDQUFFTSxNQUFGLENBQVg7QUFDSDtBQUNKOztBQUVELHNCQUNJO0FBQUssYUFBUyxFQUFHekgsT0FBakI7QUFBQSw0QkFDSTtBQUFRLGVBQVMsRUFBQyw2QkFBbEI7QUFBZ0QsYUFBTyxFQUFHcUg7QUFBMUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLGVBRUk7QUFBTyxlQUFTLEVBQUMsdUJBQWpCO0FBQXlDLFVBQUksRUFBQyxRQUE5QztBQUF1RCxTQUFHLEVBQUMsR0FBM0Q7QUFBK0QsU0FBRyxFQUFHMVAsS0FBSyxDQUFDNFAsR0FBM0U7QUFBaUYsV0FBSyxFQUFHTCxRQUF6RjtBQUFvRyxjQUFRLEVBQUdNO0FBQS9HO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGSixlQUdJO0FBQVEsZUFBUyxFQUFDLDJCQUFsQjtBQUE4QyxhQUFPLEVBQUdGO0FBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQU9ILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7O0FBRUEsU0FBU00sTUFBVCxDQUFpQjtBQUFFckgsVUFBRjtBQUFZeUY7QUFBWixDQUFqQixFQUFnRDtBQUM1QyxRQUFNcE8sTUFBTSxHQUFHQyxzREFBUyxFQUF4QjtBQUVBZ1Esd0RBQWUsQ0FBRSxNQUFNO0FBQ25CN1AsWUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEtBQW9DRCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNHLFNBQWpDLENBQTJDRSxNQUEzQyxDQUFtRCxRQUFuRCxDQUFwQztBQUNILEdBRmMsRUFFWixDQUFFVixNQUFNLENBQUNPLFFBQVQsQ0FGWSxDQUFmO0FBSUFMLGtEQUFTLENBQUUsTUFBTTtBQUNib0csVUFBTSxDQUFDakQsZ0JBQVAsQ0FBeUIsUUFBekIsRUFBbUM2TSx5REFBbkMsRUFBeUQsSUFBekQ7QUFDQTVKLFVBQU0sQ0FBQ2pELGdCQUFQLENBQXlCLFFBQXpCLEVBQW1DOE0sd0RBQW5DLEVBQXdELElBQXhEO0FBQ0E3SixVQUFNLENBQUNqRCxnQkFBUCxDQUF5QixRQUF6QixFQUFtQzRELHdEQUFuQyxFQUF3RCxJQUF4RDtBQUNBWCxVQUFNLENBQUNqRCxnQkFBUCxDQUF5QixRQUF6QixFQUFtQzhNLHdEQUFuQztBQUNBN0osVUFBTSxDQUFDakQsZ0JBQVAsQ0FBeUIsUUFBekIsRUFBbUM0RCx3REFBbkM7QUFDQVgsVUFBTSxDQUFDakQsZ0JBQVAsQ0FBeUIsUUFBekIsRUFBbUMrTSxrREFBbkM7QUFFQSxXQUFPLE1BQU07QUFDVDlKLFlBQU0sQ0FBQy9DLG1CQUFQLENBQTRCLFFBQTVCLEVBQXNDMk0seURBQXRDLEVBQTRELElBQTVEO0FBQ0E1SixZQUFNLENBQUMvQyxtQkFBUCxDQUE0QixRQUE1QixFQUFzQzRNLHdEQUF0QyxFQUEyRCxJQUEzRDtBQUNBN0osWUFBTSxDQUFDL0MsbUJBQVAsQ0FBNEIsUUFBNUIsRUFBc0MwRCx3REFBdEMsRUFBMkQsSUFBM0Q7QUFDQVgsWUFBTSxDQUFDL0MsbUJBQVAsQ0FBNEIsUUFBNUIsRUFBc0M0TSx3REFBdEM7QUFDQTdKLFlBQU0sQ0FBQy9DLG1CQUFQLENBQTRCLFFBQTVCLEVBQXNDMEQsd0RBQXRDO0FBQ0FYLFlBQU0sQ0FBQy9DLG1CQUFQLENBQTRCLFFBQTVCLEVBQXNDNk0sa0RBQXRDO0FBQ0gsS0FQRDtBQVFILEdBaEJRLEVBZ0JOLEVBaEJNLENBQVQ7QUFrQkFsUSxrREFBUyxDQUFFLE1BQU07QUFDYmtPLGtCQUFjO0FBRWQsUUFBSWlDLFdBQVcsR0FBRyxDQUFFLEdBQUdqUSxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNHLFNBQXRDLENBQWxCOztBQUNBLFNBQU0sSUFBSThQLENBQUMsR0FBRyxDQUFkLEVBQWlCQSxDQUFDLEdBQUdELFdBQVcsQ0FBQ3pPLE1BQWpDLEVBQXlDME8sQ0FBQyxFQUExQyxFQUErQztBQUMzQ2xRLGNBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ0csU0FBakMsQ0FBMkNFLE1BQTNDLENBQW1EMlAsV0FBVyxDQUFFQyxDQUFGLENBQTlEO0FBQ0g7O0FBRUQzTSxjQUFVLENBQUUsTUFBTTtBQUNkdkQsY0FBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDRyxTQUFqQyxDQUEyQ0csR0FBM0MsQ0FBZ0QsUUFBaEQ7QUFDSCxLQUZTLEVBRVAsRUFGTyxDQUFWO0FBR0gsR0FYUSxFQVdOLENBQUVYLE1BQU0sQ0FBQ08sUUFBVCxDQVhNLENBQVQ7QUFhQSxzQkFDSTtBQUFBLDRCQUNJO0FBQUssZUFBUyxFQUFDLGNBQWY7QUFBQSw4QkFDSSw4REFBQyw4REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosRUFHTW9JLFFBSE4sZUFLSSw4REFBQywrREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBTEosZUFPSSw4REFBQyxzRUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBUEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFXSSw4REFBQyxxRUFBRDtBQUFPLFFBQUUsRUFBQyxZQUFWO0FBQXVCLFVBQUksRUFBQyxHQUE1QjtBQUFnQyxXQUFLLEVBQUMsS0FBdEM7QUFBNEMsVUFBSSxFQUFDLFFBQWpEO0FBQTBELGVBQVMsRUFBQyxZQUFwRTtBQUFpRixhQUFPLEVBQUcsTUFBTTRILHlEQUFnQixDQUFFLEtBQUYsQ0FBakg7QUFBQSw2QkFBNkg7QUFBRyxpQkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUE3SDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBWEosZUFhSSw4REFBQyw2RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBYkosZUFlSSw4REFBQywwREFBRDtBQUNJLGVBQVMsRUFBRyxJQURoQjtBQUVJLGNBQVEsRUFBRyxHQUZmO0FBR0ksZ0JBQVUsRUFBRyxJQUhqQjtBQUlJLGVBQVMsRUFBQyxpQkFKZDtBQUtJLGNBQVEsRUFBQyxhQUxiO0FBTUksaUJBQVcsRUFBRyxLQU5sQjtBQU9JLHFCQUFlLEVBQUcsSUFQdEI7QUFRSSxpQkFBVyxFQUFHO0FBUmxCO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFmSixlQTBCSSw4REFBQyx5RkFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBMUJKLGVBNEJJLDhEQUFDLDZFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUE1Qko7QUFBQSxrQkFESjtBQWdDSDs7QUFFRCwrREFBZTdOLG9EQUFPLENBQUUsSUFBRixFQUFRO0FBQUUwTCxnQkFBYyxFQUFFSCxzRUFBMkJHO0FBQTdDLENBQVIsQ0FBUCxDQUFrRTRCLE1BQWxFLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0ZBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7O0FBRUEsU0FBU1EsU0FBVCxDQUFvQnpRLEtBQXBCLEVBQTRCO0FBQ3hCLE1BQUlDLE1BQU0sR0FBR0Msc0RBQVMsRUFBdEI7QUFDQSxRQUFNO0FBQUV3QyxRQUFGO0FBQVFnTyxnQkFBWSxHQUFHLEtBQXZCO0FBQThCckksV0FBTyxHQUFHLEVBQXhDO0FBQTRDc0ksU0FBSyxHQUFHO0FBQXBELE1BQTZEM1EsS0FBbkU7QUFDQSxRQUFNO0FBQUU0USxrQkFBRjtBQUFrQkMsYUFBbEI7QUFBNkJDO0FBQTdCLE1BQTBDOVEsS0FBaEQ7QUFDQSxRQUFNO0FBQUEsT0FBRStRLFFBQUY7QUFBQSxPQUFZQztBQUFaLE1BQTRCaE8sK0NBQVEsQ0FBRSxNQUFGLENBQTFDO0FBQ0EsUUFBTTtBQUFBLE9BQUVpTyxPQUFGO0FBQUEsT0FBV0M7QUFBWCxNQUEwQmxPLCtDQUFRLENBQUUsTUFBRixDQUF4QztBQUNBLFFBQU07QUFBQSxPQUFFbU8sUUFBRjtBQUFBLE9BQVlDO0FBQVosTUFBNEJwTywrQ0FBUSxDQUFFLENBQUMsQ0FBSCxDQUExQztBQUNBLFFBQU07QUFBQSxPQUFFcU8sVUFBRjtBQUFBLE9BQWNDO0FBQWQsTUFBZ0N0TywrQ0FBUSxDQUFFLEtBQUYsQ0FBOUM7QUFDQSxRQUFNO0FBQUEsT0FBRXVNLFFBQUY7QUFBQSxPQUFZZ0M7QUFBWixNQUE0QnZPLCtDQUFRLENBQUUsQ0FBRixDQUExQztBQUNBLE1BQUlrQyxPQUFPLEdBQUd4QyxJQUFJLElBQUlBLElBQUksQ0FBQ3dDLE9BQTNCLENBVHdCLENBV3hCOztBQUNBLE1BQUlzTSxZQUFKO0FBQUEsTUFBa0JDLE1BQU0sR0FBRyxFQUEzQjtBQUFBLE1BQStCQyxLQUFLLEdBQUcsRUFBdkM7QUFDQUYsY0FBWSxHQUFHVixRQUFRLENBQUNhLFNBQVQsQ0FBb0JsUSxJQUFJLElBQUlBLElBQUksQ0FBQ08sSUFBTCxLQUFja0QsT0FBTyxDQUFDeEMsSUFBUixDQUFhVixJQUF2RCxJQUFnRSxDQUFDLENBQWpFLEdBQXFFLElBQXJFLEdBQTRFLEtBQTNGOztBQUVBLE1BQUtrRCxPQUFPLENBQUN4QyxJQUFSLElBQWdCd0MsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUFzQnRELE1BQXRCLEdBQStCLENBQXBELEVBQXdEO0FBQ3BELFFBQUtxRCxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCLENBQXZCLEVBQTJCeU0sSUFBaEMsRUFDSTFNLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBc0IwTSxPQUF0QixDQUErQnBRLElBQUksSUFBSTtBQUNuQyxVQUFLaVEsS0FBSyxDQUFDQyxTQUFOLENBQWlCQyxJQUFJLElBQUlBLElBQUksQ0FBQ3hQLElBQUwsS0FBY1gsSUFBSSxDQUFDbVEsSUFBTCxDQUFVeFAsSUFBakQsTUFBNEQsQ0FBQyxDQUFsRSxFQUFzRTtBQUNsRXNQLGFBQUssQ0FBQzFNLElBQU4sQ0FBWTtBQUFFNUMsY0FBSSxFQUFFWCxJQUFJLENBQUNtUSxJQUFMLENBQVV4UCxJQUFsQjtBQUF3QjBDLGVBQUssRUFBRXJELElBQUksQ0FBQ21RLElBQUwsQ0FBVUE7QUFBekMsU0FBWjtBQUNIO0FBQ0osS0FKRDs7QUFNSixRQUFLMU0sT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixFQUEyQjJNLEtBQWhDLEVBQXdDO0FBQ3BDNU0sYUFBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUFzQjBNLE9BQXRCLENBQStCcFEsSUFBSSxJQUFJO0FBQ25DLFlBQUtnUSxNQUFNLENBQUNFLFNBQVAsQ0FBa0JHLEtBQUssSUFBSUEsS0FBSyxDQUFDMVAsSUFBTixLQUFlWCxJQUFJLENBQUNxUSxLQUFMLENBQVcxUCxJQUFyRCxNQUFnRSxDQUFDLENBQXRFLEVBQ0lxUCxNQUFNLENBQUN6TSxJQUFQLENBQWE7QUFBRTVDLGNBQUksRUFBRVgsSUFBSSxDQUFDcVEsS0FBTCxDQUFXMVAsSUFBbkI7QUFBeUIwQyxlQUFLLEVBQUVyRCxJQUFJLENBQUNxUSxLQUFMLENBQVdBO0FBQTNDLFNBQWI7QUFDUCxPQUhEO0FBSUg7QUFDSjs7QUFFRDNSLGtEQUFTLENBQUUsTUFBTTtBQUNiLFdBQU8sTUFBTTtBQUNUaVIsaUJBQVcsQ0FBRSxDQUFDLENBQUgsQ0FBWDtBQUNBVyx1QkFBaUI7QUFDcEIsS0FIRDtBQUlILEdBTFEsRUFLTixDQUFFN00sT0FBRixDQUxNLENBQVQ7QUFPQS9FLGtEQUFTLENBQUUsTUFBTTtBQUNiLFFBQUsrRSxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXNCdEQsTUFBdEIsR0FBK0IsQ0FBcEMsRUFBd0M7QUFDcEMsVUFBT29QLE9BQU8sS0FBSyxNQUFaLElBQXNCRixRQUFRLEtBQUssTUFBckMsSUFBbURFLE9BQU8sS0FBSyxNQUFaLElBQXNCL0wsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixFQUEyQnlNLElBQTNCLEtBQW9DLElBQTFELElBQWtFYixRQUFRLEtBQUssTUFBbEksSUFBZ0pBLFFBQVEsS0FBSyxNQUFiLElBQXVCN0wsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixFQUEyQjJNLEtBQTNCLEtBQXFDLElBQTVELElBQW9FYixPQUFPLEtBQUssTUFBck8sRUFBZ1A7QUFDNU9LLHFCQUFhLENBQUUsSUFBRixDQUFiO0FBQ0FGLG1CQUFXLENBQUVsTSxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXNCd00sU0FBdEIsQ0FBaUNsUSxJQUFJLElBQU1BLElBQUksQ0FBQ21RLElBQUwsS0FBYyxJQUFkLElBQXNCblEsSUFBSSxDQUFDcVEsS0FBTCxLQUFlLElBQXJDLElBQTZDclEsSUFBSSxDQUFDcVEsS0FBTCxDQUFXMVAsSUFBWCxLQUFvQjJPLFFBQWpFLElBQTZFdFAsSUFBSSxDQUFDbVEsSUFBTCxDQUFVeFAsSUFBVixLQUFtQjZPLE9BQWxHLElBQWlIeFAsSUFBSSxDQUFDbVEsSUFBTCxLQUFjLElBQWQsSUFBc0JuUSxJQUFJLENBQUNxUSxLQUFMLENBQVcxUCxJQUFYLEtBQW9CMk8sUUFBM0osSUFBMkt0UCxJQUFJLENBQUNxUSxLQUFMLEtBQWUsSUFBZixJQUF1QnJRLElBQUksQ0FBQ21RLElBQUwsQ0FBVXhQLElBQVYsS0FBbUI2TyxPQUE5UCxDQUFGLENBQVg7QUFDSCxPQUhELE1BR087QUFDSEsscUJBQWEsQ0FBRSxLQUFGLENBQWI7QUFDSDtBQUNKLEtBUEQsTUFPTztBQUNIQSxtQkFBYSxDQUFFLElBQUYsQ0FBYjtBQUNIOztBQUVELFFBQUtwTSxPQUFPLENBQUM4TSxLQUFSLEtBQWtCLENBQXZCLEVBQTJCO0FBQ3ZCVixtQkFBYSxDQUFFLEtBQUYsQ0FBYjtBQUNIO0FBQ0osR0FmUSxFQWVOLENBQUVQLFFBQUYsRUFBWUUsT0FBWixFQUFxQi9MLE9BQXJCLENBZk0sQ0FBVDs7QUFpQkEsUUFBTStNLGVBQWUsR0FBSzdRLENBQUYsSUFBUztBQUM3QkEsS0FBQyxDQUFDQyxjQUFGOztBQUVBLFFBQUt1UCxjQUFjLElBQUksQ0FBQ1ksWUFBeEIsRUFBdUM7QUFDbkMsVUFBSWxRLGFBQWEsR0FBR0YsQ0FBQyxDQUFDRSxhQUF0QjtBQUNBQSxtQkFBYSxDQUFDYixTQUFkLENBQXdCRyxHQUF4QixDQUE2QixtQkFBN0IsRUFBa0QsU0FBbEQ7QUFDQWdRLG9CQUFjLENBQUUxTCxPQUFPLENBQUN4QyxJQUFWLENBQWQ7QUFFQWtCLGdCQUFVLENBQUUsTUFBTTtBQUNkdEMscUJBQWEsQ0FBQ2IsU0FBZCxDQUF3QkUsTUFBeEIsQ0FBZ0MsbUJBQWhDLEVBQXFELFNBQXJEO0FBQ0gsT0FGUyxFQUVQLElBRk8sQ0FBVjtBQUdILEtBUkQsTUFRTztBQUNIVixZQUFNLENBQUMrRSxJQUFQLENBQWEsaUJBQWI7QUFDSDtBQUNKLEdBZEQ7O0FBZ0JBLFFBQU1rTixlQUFlLEdBQUs5USxDQUFGLElBQVM7QUFDN0I0UCxlQUFXLENBQUU1UCxDQUFDLENBQUN3RCxNQUFGLENBQVNFLEtBQVgsQ0FBWDtBQUNILEdBRkQ7O0FBSUEsUUFBTXFOLGNBQWMsR0FBSy9RLENBQUYsSUFBUztBQUM1QjhQLGNBQVUsQ0FBRTlQLENBQUMsQ0FBQ3dELE1BQUYsQ0FBU0UsS0FBWCxDQUFWO0FBQ0gsR0FGRDs7QUFJQSxRQUFNc04sZ0JBQWdCLEdBQUcsTUFBTTtBQUMzQixRQUFLbE4sT0FBTyxDQUFDeEMsSUFBUixDQUFhc1AsS0FBYixHQUFxQixDQUFyQixJQUEwQlgsVUFBL0IsRUFBNEM7QUFDeEMsVUFBS25NLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBc0J0RCxNQUF0QixHQUErQixDQUFwQyxFQUF3QztBQUNwQyxZQUFJd1EsT0FBTyxHQUFHbk4sT0FBTyxDQUFDeEMsSUFBUixDQUFhTixJQUEzQjtBQUFBLFlBQWlDa1EsUUFBakM7QUFDQUQsZUFBTyxJQUFJdEIsUUFBUSxLQUFLLE1BQWIsR0FBc0IsTUFBTUEsUUFBNUIsR0FBdUMsRUFBbEQ7QUFDQXNCLGVBQU8sSUFBSXBCLE9BQU8sS0FBSyxNQUFaLEdBQXFCLE1BQU1BLE9BQTNCLEdBQXFDLEVBQWhEOztBQUVBLFlBQUsvTCxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsTUFBNEI0QyxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBakMsRUFBMkQ7QUFDdkRnUSxrQkFBUSxHQUFHcE4sT0FBTyxDQUFDeEMsSUFBUixDQUFhSixLQUFiLENBQW9CLENBQXBCLENBQVg7QUFDSCxTQUZELE1BRU8sSUFBSyxDQUFDNEMsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixFQUEyQjdDLEtBQTVCLElBQXFDNEMsT0FBTyxDQUFDeEMsSUFBUixDQUFhd00sUUFBYixHQUF3QixDQUFsRSxFQUFzRTtBQUN6RW9ELGtCQUFRLEdBQUdwTixPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBWDtBQUNILFNBRk0sTUFFQTtBQUNIZ1Esa0JBQVEsR0FBR3BOLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBdUJnTSxRQUF2QixFQUFrQ29CLFVBQWxDLEdBQStDck4sT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QmdNLFFBQXZCLEVBQWtDb0IsVUFBakYsR0FBOEZyTixPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCZ00sUUFBdkIsRUFBa0M3TyxLQUEzSTtBQUNIOztBQUVEdU8saUJBQVMsaUNBQU8zTCxPQUFPLENBQUN4QyxJQUFmO0FBQXFCTixjQUFJLEVBQUVpUSxPQUEzQjtBQUFvQ2hRLGFBQUcsRUFBRWtOLFFBQXpDO0FBQW1Eak4sZUFBSyxFQUFFZ1E7QUFBMUQsV0FBVDtBQUNILE9BZEQsTUFjTztBQUNIekIsaUJBQVMsaUNBQU8zTCxPQUFPLENBQUN4QyxJQUFmO0FBQXFCTCxhQUFHLEVBQUVrTixRQUExQjtBQUFvQ2pOLGVBQUssRUFBRTRDLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQjtBQUEzQyxXQUFUO0FBQ0g7QUFDSjtBQUNKLEdBcEJEOztBQXNCQSxRQUFNeVAsaUJBQWlCLEdBQUszUSxDQUFGLElBQVM7QUFDL0I0UCxlQUFXLENBQUUsTUFBRixDQUFYO0FBQ0FFLGNBQVUsQ0FBRSxNQUFGLENBQVY7QUFDSCxHQUhEOztBQUtBLFdBQVNzQixVQUFULENBQXFCVixLQUFyQixFQUE0QkYsSUFBNUIsRUFBbUM7QUFDL0IsUUFBS0UsS0FBSyxLQUFLLE1BQVYsSUFBb0JGLElBQUksS0FBSyxNQUFsQyxFQUEyQyxPQUFPLEtBQVA7O0FBRTNDLFFBQUtGLEtBQUssQ0FBQzdQLE1BQU4sS0FBaUIsQ0FBdEIsRUFBMEI7QUFDdEIsYUFBT3FELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBc0J3TSxTQUF0QixDQUFpQ2xRLElBQUksSUFBSUEsSUFBSSxDQUFDcVEsS0FBTCxDQUFXMVAsSUFBWCxLQUFvQjJPLFFBQTdELE1BQTRFLENBQUMsQ0FBcEY7QUFDSDs7QUFFRCxRQUFLVSxNQUFNLENBQUM1UCxNQUFQLEtBQWtCLENBQXZCLEVBQTJCO0FBQ3ZCLGFBQU9xRCxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXNCd00sU0FBdEIsQ0FBaUNsUSxJQUFJLElBQUlBLElBQUksQ0FBQ21RLElBQUwsQ0FBVXhQLElBQVYsS0FBbUI2TyxPQUE1RCxNQUEwRSxDQUFDLENBQWxGO0FBQ0g7O0FBRUQsV0FBTy9MLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBc0J3TSxTQUF0QixDQUFpQ2xRLElBQUksSUFBSUEsSUFBSSxDQUFDcVEsS0FBTCxDQUFXMVAsSUFBWCxLQUFvQjBQLEtBQXBCLElBQTZCclEsSUFBSSxDQUFDbVEsSUFBTCxDQUFVeFAsSUFBVixLQUFtQndQLElBQXpGLE1BQW9HLENBQUMsQ0FBNUc7QUFDSDs7QUFFRCxXQUFTL0IsU0FBVCxDQUFvQnhOLEdBQXBCLEVBQTBCO0FBQ3RCa1AsZUFBVyxDQUFFbFAsR0FBRixDQUFYO0FBQ0g7O0FBRUQsc0JBQ0k7QUFBSyxhQUFTLEVBQUcscUJBQXFCZ0csT0FBdEM7QUFBQSxlQUVRc0ksS0FBSyxnQkFDRDtBQUFLLGVBQVMsRUFBQyxvQkFBZjtBQUFBLDhCQUNJO0FBQUksaUJBQVMsRUFBQywwQkFBZDtBQUFBLGdDQUNJO0FBQUEsaUNBQUksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFDLEdBQVo7QUFBQSxtQ0FBZ0I7QUFBRyx1QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQUVJO0FBQUEsaUNBQUksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFDLEdBQVo7QUFBZ0IscUJBQVMsRUFBQyxRQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkosZUFHSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQU9JLDhEQUFDLDZFQUFEO0FBQVksZUFBTyxFQUFHekw7QUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURDLEdBU1EsRUFYckIsZUFjSTtBQUFJLGVBQVMsRUFBQyxjQUFkO0FBQUEsZ0JBQStCQSxPQUFPLENBQUN4QyxJQUFSLENBQWFOO0FBQTVDO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFkSixlQWdCSTtBQUFLLGVBQVMsRUFBQyxjQUFmO0FBQUEsdUNBQ1M7QUFBTSxpQkFBUyxFQUFDLGFBQWhCO0FBQUEsa0JBQWdDOEMsT0FBTyxDQUFDeEMsSUFBUixDQUFhK1A7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURULCtCQUVnQjtBQUFNLGlCQUFTLEVBQUMsZUFBaEI7QUFBQSxrQkFFSnZOLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYWdRLFVBQWIsQ0FBd0I1USxHQUF4QixDQUE2QixDQUFFTCxJQUFGLEVBQVFNLEtBQVIsa0JBQ3pCLDhEQUFDLHVEQUFEO0FBQUEsa0NBQ0ksOERBQUMscUVBQUQ7QUFBTyxnQkFBSSxFQUFHO0FBQUV2QixzQkFBUSxFQUFFLE9BQVo7QUFBcUJpRCxtQkFBSyxFQUFFO0FBQUVrUCx3QkFBUSxFQUFFbFIsSUFBSSxDQUFDTztBQUFqQjtBQUE1QixhQUFkO0FBQUEsc0JBQ01QLElBQUksQ0FBQ1c7QUFEWDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLEVBSU1MLEtBQUssR0FBR21ELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYWdRLFVBQWIsQ0FBd0I3USxNQUF4QixHQUFpQyxDQUF6QyxHQUE2QyxJQUE3QyxHQUFvRCxFQUoxRDtBQUFBLFdBQXNCSixJQUFJLENBQUNXLElBQUwsR0FBWSxHQUFaLEdBQWtCTCxLQUF4QztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKO0FBRkk7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUZoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFoQkosZUErQkk7QUFBSyxlQUFTLEVBQUMsb0JBQWY7QUFBQSxnQkFFUW1ELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQixNQUE0QjRDLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQixDQUE1QixHQUNJNEMsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUFzQnRELE1BQXRCLEtBQWlDLENBQWpDLElBQXdDcUQsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUFzQnRELE1BQXRCLEdBQStCLENBQS9CLElBQW9DLENBQUNxRCxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCLENBQXZCLEVBQTJCN0MsS0FBeEcsZ0JBQ0k7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsV0FBZjtBQUFBLDBCQUE4Qlosa0RBQVMsQ0FBRXdELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQixDQUFGLENBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQUVJO0FBQUssbUJBQVMsRUFBQyxXQUFmO0FBQUEsMEJBQThCWixrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFhSixLQUFiLENBQW9CLENBQXBCLENBQUYsQ0FBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZKO0FBQUEsc0JBREosZ0JBTUk7QUFBTSxpQkFBUyxFQUFDLFdBQWhCO0FBQUEsd0JBQStCWixrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFhSixLQUFiLENBQW9CLENBQXBCLENBQUYsQ0FBeEMsZUFBMkVaLGtEQUFTLENBQUV3RCxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBRixDQUFwRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FQUixnQkFRTTtBQUFLLGlCQUFTLEVBQUMsV0FBZjtBQUFBLHdCQUE4Qlosa0RBQVMsQ0FBRXdELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQixDQUFGLENBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVZkO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUEvQkosRUE4Q1E0QyxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsTUFBNEI0QyxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBNUIsSUFBdUQ0QyxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXNCdEQsTUFBdEIsS0FBaUMsQ0FBeEYsZ0JBQ0ksOERBQUMsbUVBQUQ7QUFBVyxVQUFJLEVBQUc7QUFBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLEdBQytCLEVBL0N2QyxlQWtESTtBQUFLLGVBQVMsRUFBQyxtQkFBZjtBQUFBLDhCQUNJO0FBQUssaUJBQVMsRUFBQyxjQUFmO0FBQUEsZ0NBQ0k7QUFBTSxtQkFBUyxFQUFDLFNBQWhCO0FBQTBCLGVBQUssRUFBRztBQUFFK1EsaUJBQUssRUFBRSxLQUFLMU4sT0FBTyxDQUFDeEMsSUFBUixDQUFhbVEsT0FBbEIsR0FBNEI7QUFBckM7QUFBbEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQUVJO0FBQU0sbUJBQVMsRUFBQyx5QkFBaEI7QUFBQSxvQkFBNENuUixrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFhbVEsT0FBZjtBQUFyRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBTUksOERBQUMscUVBQUQ7QUFBTyxZQUFJLEVBQUMsR0FBWjtBQUFnQixpQkFBUyxFQUFDLGdCQUExQjtBQUFBLHlCQUErQzNOLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYW9RLE9BQTVEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQWxESixlQTJESTtBQUFHLGVBQVMsRUFBQyxvQkFBYjtBQUFBLGdCQUFvQzVOLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXFRO0FBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUEzREosRUE4RFE3TixPQUFPLElBQUlBLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBc0J0RCxNQUF0QixHQUErQixDQUExQyxnQkFDSTtBQUFBLGlCQUVRcUQsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixFQUEyQjJNLEtBQTNCLGdCQUNJO0FBQUssaUJBQVMsRUFBQywrQ0FBZjtBQUFBLGdDQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBRUk7QUFBSyxtQkFBUyxFQUFDLFlBQWY7QUFBQSxpQ0FDSTtBQUFRLGdCQUFJLEVBQUMsT0FBYjtBQUFxQixxQkFBUyxFQUFDLDJCQUEvQjtBQUEyRCxvQkFBUSxFQUFHSSxlQUF0RTtBQUF3RixpQkFBSyxFQUFHbkIsUUFBaEc7QUFBQSxvQ0FDSTtBQUFRLG1CQUFLLEVBQUMsTUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESixFQUdRVSxNQUFNLENBQUMzUCxHQUFQLENBQVlMLElBQUksSUFDWixDQUFDK1EsVUFBVSxDQUFFL1EsSUFBSSxDQUFDVyxJQUFQLEVBQWE2TyxPQUFiLENBQVgsZ0JBQ0k7QUFBUSxtQkFBSyxFQUFHeFAsSUFBSSxDQUFDVyxJQUFyQjtBQUFBLHdCQUEyRFgsSUFBSSxDQUFDVztBQUFoRSxlQUFrQyxXQUFXWCxJQUFJLENBQUNXLElBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosR0FDc0YsRUFGMUYsQ0FIUjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLEdBY2EsRUFoQnJCLEVBb0JROEMsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixFQUEyQnlNLElBQTNCLGdCQUNJO0FBQUssaUJBQVMsRUFBQyx3REFBZjtBQUFBLGdDQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBRUk7QUFBSyxtQkFBUyxFQUFDLG9CQUFmO0FBQUEsa0NBQ0k7QUFBSyxxQkFBUyxFQUFDLFlBQWY7QUFBQSxtQ0FDSTtBQUFRLGtCQUFJLEVBQUMsTUFBYjtBQUFvQix1QkFBUyxFQUFDLDBCQUE5QjtBQUF5RCxzQkFBUSxFQUFHTyxjQUFwRTtBQUFxRixtQkFBSyxFQUFHbEIsT0FBN0Y7QUFBQSxzQ0FDSTtBQUFRLHFCQUFLLEVBQUMsTUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixFQUdRUyxLQUFLLENBQUM1UCxHQUFOLENBQVdMLElBQUksSUFDWCxDQUFDK1EsVUFBVSxDQUFFekIsUUFBRixFQUFZdFAsSUFBSSxDQUFDVyxJQUFqQixDQUFYLGdCQUNJO0FBQVEscUJBQUssRUFBR1gsSUFBSSxDQUFDVyxJQUFyQjtBQUFBLDBCQUEwRFgsSUFBSSxDQUFDVztBQUEvRCxpQkFBa0MsVUFBVVgsSUFBSSxDQUFDVyxJQUFqRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLEdBQ3FGLEVBRnpGLENBSFI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQWFJLDhEQUFDLGlFQUFEO0FBQVUsY0FBRSxFQUFHLFdBQVcyTyxRQUFYLElBQXVCLFdBQVdFLE9BQWpEO0FBQUEsbUNBQ0k7QUFBSyx1QkFBUyxFQUFDLDREQUFmO0FBQUEscUNBQ0ksOERBQUMscUVBQUQ7QUFBTyxvQkFBSSxFQUFDLEdBQVo7QUFBZ0IseUJBQVMsRUFBQyx5QkFBMUI7QUFBb0QsdUJBQU8sRUFBR2MsaUJBQTlEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBYko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLEdBc0JhLEVBMUNyQixlQTZDSTtBQUFLLGlCQUFTLEVBQUMseUJBQWY7QUFBQSwrQkFDSSw4REFBQyxpRUFBRDtBQUFVLFlBQUUsRUFBR1YsVUFBVSxJQUFJRixRQUFRLEdBQUcsQ0FBQyxDQUF6QztBQUFBLGlDQUNJO0FBQUsscUJBQVMsRUFBQyxjQUFmO0FBQUEsc0JBRVFBLFFBQVEsR0FBRyxDQUFDLENBQVosZ0JBQ0k7QUFBSyx1QkFBUyxFQUFDLHNCQUFmO0FBQUEsd0JBRVFqTSxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCZ00sUUFBdkIsRUFBa0M3TyxLQUFsQyxHQUNJNEMsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QmdNLFFBQXZCLEVBQWtDb0IsVUFBbEMsZ0JBQ0k7QUFBSyx5QkFBUyxFQUFDLG9CQUFmO0FBQUEsd0NBQ0k7QUFBSywyQkFBUyxFQUFDLFdBQWY7QUFBQSxrQ0FBOEI3USxrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QmdNLFFBQXZCLEVBQWtDb0IsVUFBcEMsQ0FBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURKLGVBRUk7QUFBSywyQkFBUyxFQUFDLFdBQWY7QUFBQSxrQ0FBOEI3USxrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QmdNLFFBQXZCLEVBQWtDN08sS0FBcEMsQ0FBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixnQkFLTTtBQUFLLHlCQUFTLEVBQUMsb0JBQWY7QUFBQSx1Q0FDRTtBQUFLLDJCQUFTLEVBQUMsV0FBZjtBQUFBLGtDQUE4Qlosa0RBQVMsQ0FBRXdELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBdUJnTSxRQUF2QixFQUFrQzdPLEtBQXBDLENBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBTlYsR0FTTTtBQVhkO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosR0FjYTtBQWhCckI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBN0NKO0FBQUEsb0JBREosR0FxRVUsRUFuSWxCLGVBc0lJO0FBQUksZUFBUyxFQUFDO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXRJSixFQXlJUW9PLFlBQVksZ0JBQ1I7QUFBSyxlQUFTLEVBQUMsK0NBQWY7QUFBQSw2QkFDSTtBQUFLLGlCQUFTLEVBQUMsV0FBZjtBQUFBLGdDQUNJO0FBQUssbUJBQVMsRUFBQyx3QkFBZjtBQUFBLGtDQUNJO0FBQVEscUJBQVMsRUFBQyxlQUFsQjtBQUFBLG1DQUNJLDhEQUFDLHFFQUFEO0FBQU8sa0JBQUksRUFBRyxzQkFBc0J4TCxPQUFPLENBQUN4QyxJQUFSLENBQWFWLElBQWpEO0FBQUEscUNBQ0k7QUFBSyxtQkFBRyxFQUFHQyx1QkFBQSxHQUFvQ2lELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYVIsUUFBYixDQUF1QixDQUF2QixFQUEyQkMsR0FBMUU7QUFBZ0YscUJBQUssRUFBQyxJQUF0RjtBQUEyRixzQkFBTSxFQUFDLElBQWxHO0FBQ0ksbUJBQUcsRUFBQztBQURSO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixlQU9JO0FBQUEsb0NBQ0k7QUFBSSx1QkFBUyxFQUFDLGVBQWQ7QUFBQSxxQ0FBOEIsOERBQUMscUVBQUQ7QUFBTyxvQkFBSSxFQUFHLHNCQUFzQitDLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYVYsSUFBakQ7QUFBQSwwQkFBMERrRCxPQUFPLENBQUN4QyxJQUFSLENBQWFOO0FBQXZFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESixlQUVJO0FBQUssdUJBQVMsRUFBQyxjQUFmO0FBQUEsc0NBQ0k7QUFBSyx5QkFBUyxFQUFDLG9CQUFmO0FBQUEsMEJBRVErTyxRQUFRLEdBQUcsQ0FBQyxDQUFaLElBQWlCak0sT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QixDQUF2QixDQUFqQixHQUNJRCxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCZ00sUUFBdkIsRUFBa0M3TyxLQUFsQyxHQUNJNEMsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QmdNLFFBQXZCLEVBQWtDb0IsVUFBbEMsZ0JBQ0k7QUFBQSwwQ0FDSTtBQUFLLDZCQUFTLEVBQUMsV0FBZjtBQUFBLG9DQUE4QjdRLGtEQUFTLENBQUV3RCxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCZ00sUUFBdkIsRUFBa0NvQixVQUFwQyxDQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBREosZUFFSTtBQUFLLDZCQUFTLEVBQUMsV0FBZjtBQUFBLG9DQUE4QjdRLGtEQUFTLENBQUV3RCxPQUFPLENBQUN4QyxJQUFSLENBQWF5QyxRQUFiLENBQXVCZ00sUUFBdkIsRUFBa0M3TyxLQUFwQyxDQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBRko7QUFBQSxnQ0FESixnQkFNSTtBQUFBLHlDQUNJO0FBQUssNkJBQVMsRUFBQyxXQUFmO0FBQUEsb0NBQThCWixrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFheUMsUUFBYixDQUF1QmdNLFFBQXZCLEVBQWtDN08sS0FBcEMsQ0FBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREosaUNBUFIsR0FVTSxFQVhWLEdBYUk0QyxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsTUFBNEI0QyxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBNUIsR0FDSTRDLE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYXlDLFFBQWIsQ0FBc0J0RCxNQUF0QixLQUFpQyxDQUFqQyxnQkFDSTtBQUFBLDBDQUNJO0FBQUssNkJBQVMsRUFBQyxXQUFmO0FBQUEsb0NBQThCSCxrREFBUyxDQUFFd0QsT0FBTyxDQUFDeEMsSUFBUixDQUFhSixLQUFiLENBQW9CLENBQXBCLENBQUYsQ0FBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURKLGVBRUk7QUFBSyw2QkFBUyxFQUFDLFdBQWY7QUFBQSxvQ0FBOEJaLGtEQUFTLENBQUV3RCxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBRixDQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBRko7QUFBQSxnQ0FESixnQkFNSTtBQUFNLDJCQUFTLEVBQUMsV0FBaEI7QUFBQSxrQ0FBK0JaLGtEQUFTLENBQUV3RCxPQUFPLENBQUN4QyxJQUFSLENBQWFKLEtBQWIsQ0FBb0IsQ0FBcEIsQ0FBRixDQUF4QyxlQUEyRVosa0RBQVMsQ0FBRXdELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQixDQUFGLENBQXBGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFQUixnQkFRTTtBQUFLLDJCQUFTLEVBQUMsV0FBZjtBQUFBLGtDQUE4Qlosa0RBQVMsQ0FBRXdELE9BQU8sQ0FBQ3hDLElBQVIsQ0FBYUosS0FBYixDQUFvQixDQUFwQixDQUFGLENBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXZCbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFESixlQTRCSTtBQUFLLHlCQUFTLEVBQUMsd0JBQWY7QUFBQSx3Q0FDSTtBQUFLLDJCQUFTLEVBQUMsY0FBZjtBQUFBLDBDQUNJO0FBQU0sNkJBQVMsRUFBQyxTQUFoQjtBQUEwQix5QkFBSyxFQUFHO0FBQUVzUSwyQkFBSyxFQUFFLEtBQUsxTixPQUFPLENBQUN4QyxJQUFSLENBQWFtUSxPQUFsQixHQUE0QjtBQUFyQztBQUFsQztBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURKLGVBRUk7QUFBTSw2QkFBUyxFQUFDLHlCQUFoQjtBQUFBLDhCQUE0Q25SLGtEQUFTLENBQUV3RCxPQUFPLENBQUN4QyxJQUFSLENBQWFtUSxPQUFmO0FBQXJEO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURKLGVBTUksOERBQUMscUVBQUQ7QUFBTyxzQkFBSSxFQUFDLEdBQVo7QUFBZ0IsMkJBQVMsRUFBQyxnQkFBMUI7QUFBQSxtQ0FBK0MzTixPQUFPLENBQUN4QyxJQUFSLENBQWFvUSxPQUE1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQTVCSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQWlESTtBQUFLLG1CQUFTLEVBQUMsK0JBQWY7QUFBQSxrQ0FDSTtBQUFPLHFCQUFTLEVBQUMsUUFBakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREosZUFFSTtBQUFLLHFCQUFTLEVBQUMsb0JBQWY7QUFBQSxvQ0FDSSw4REFBQyxrRUFBRDtBQUFVLGlCQUFHLEVBQUc1TixPQUFPLENBQUN4QyxJQUFSLENBQWFzUCxLQUE3QjtBQUFxQyxxQkFBTyxFQUFHOU0sT0FBL0M7QUFBeUQseUJBQVcsRUFBRzJLO0FBQXZFO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosZUFFSTtBQUFRLHVCQUFTLEVBQUksb0VBQW9Fd0IsVUFBVSxHQUFHLEVBQUgsR0FBUSxVQUFZLEVBQXZIO0FBQTJILHFCQUFPLEVBQUdlLGdCQUFySTtBQUFBLHNDQUF3SjtBQUFHLHlCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUF4SjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFqREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURRLGdCQTZEUjtBQUFLLGVBQVMsRUFBQywrQkFBZjtBQUFBLDhCQUNJO0FBQU8saUJBQVMsRUFBQyxRQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBRUk7QUFBSyxpQkFBUyxFQUFDLG9CQUFmO0FBQUEsZ0NBQ0ksOERBQUMsa0VBQUQ7QUFBVSxhQUFHLEVBQUdsTixPQUFPLENBQUN4QyxJQUFSLENBQWFzUCxLQUE3QjtBQUFxQyxpQkFBTyxFQUFHOU0sT0FBL0M7QUFBeUQscUJBQVcsRUFBRzJLO0FBQXZFO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFFSTtBQUFRLG1CQUFTLEVBQUksb0VBQW9Fd0IsVUFBVSxHQUFHLEVBQUgsR0FBUSxVQUFZLEVBQXZIO0FBQTJILGlCQUFPLEVBQUdlLGdCQUFySTtBQUFBLGtDQUF3SjtBQUFHLHFCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUF4SjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBdE1aLGVBK01JO0FBQUksZUFBUyxFQUFDO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQS9NSixlQWlOSTtBQUFLLGVBQVMsRUFBQyxnQkFBZjtBQUFBLDhCQUNJO0FBQUssaUJBQVMsRUFBQyxtQkFBZjtBQUFBLGdDQUNJLDhEQUFDLHFFQUFEO0FBQU8sY0FBSSxFQUFDLEdBQVo7QUFBZ0IsbUJBQVMsRUFBQztBQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLGVBRUksOERBQUMscUVBQUQ7QUFBTyxjQUFJLEVBQUMsR0FBWjtBQUFnQixtQkFBUyxFQUFDO0FBQTFCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkosZUFHSSw4REFBQyxxRUFBRDtBQUFPLGNBQUksRUFBQyxHQUFaO0FBQWdCLG1CQUFTLEVBQUM7QUFBMUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixvQkFLVztBQUFNLGlCQUFTLEVBQUM7QUFBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUxYLG9CQUt1RDtBQUFHLFlBQUksRUFBQyxHQUFSO0FBQVksaUJBQVMsRUFBSSwwQkFBekI7QUFBcUQsYUFBSyxFQUFHWixZQUFZLEdBQUcsaUJBQUgsR0FBdUIsaUJBQWhHO0FBQW9ILGVBQU8sRUFBR1MsZUFBOUg7QUFBQSxnQ0FDL0M7QUFBRyxtQkFBUyxFQUFHVCxZQUFZLEdBQUcsbUJBQUgsR0FBeUI7QUFBcEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFEK0MsT0FFM0NBLFlBQVksR0FBRyxpQkFBSCxHQUF1QixpQkFGUTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FMdkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBak5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBK05IOztBQUVELFNBQVNqUCxlQUFULENBQTBCQyxLQUExQixFQUFrQztBQUM5QixTQUFPO0FBQ0hzTyxZQUFRLEVBQUV0TyxLQUFLLENBQUNzTyxRQUFOLENBQWVwTyxJQUFmLEdBQXNCRixLQUFLLENBQUNzTyxRQUFOLENBQWVwTyxJQUFyQyxHQUE0QztBQURuRCxHQUFQO0FBR0g7O0FBRUQsK0RBQWVDLG9EQUFPLENBQUVKLGVBQUYsRUFBbUI7QUFBRXFPLGdCQUFjLEVBQUVvQywyRUFBbEI7QUFBa0RuQyxXQUFTLEVBQUVqTywrREFBcUJpTztBQUFsRixDQUFuQixDQUFQLENBQWtISixTQUFsSCxDQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuWEE7QUFDQTtBQUVlLFNBQVN3QyxVQUFULENBQXNCalQsS0FBdEIsRUFBOEI7QUFDekMsUUFBTTtBQUFFa0Y7QUFBRixNQUFjbEYsS0FBcEI7QUFFQSxRQUFNQyxNQUFNLEdBQUdDLHNEQUFTLEVBQXhCO0FBRUEsc0JBQ0k7QUFBSSxhQUFTLEVBQUMsYUFBZDtBQUFBLGVBRVFnRixPQUFPLENBQUNnTyxJQUFSLGdCQUNJO0FBQUksZUFBUyxFQUFJLGVBQWVoTyxPQUFPLENBQUNpTyxJQUFSLEdBQWUsTUFBZixHQUF3QixjQUFnQixFQUF4RTtBQUFBLDZCQUNJLDhEQUFDLHFFQUFEO0FBQU8sWUFBSSxFQUFHO0FBQUUzUyxrQkFBUSxFQUFFUCxNQUFNLENBQUNPLFFBQW5CO0FBQTZCaUQsZUFBSyxFQUFFO0FBQUV6QixnQkFBSSxFQUFFa0QsT0FBTyxDQUFDZ08sSUFBUixDQUFhbFI7QUFBckI7QUFBcEMsU0FBZDtBQUFrRixjQUFNLEVBQUcsS0FBM0Y7QUFBQSxnQ0FDSTtBQUFHLG1CQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURKLHdCQUVZO0FBQU0sbUJBQVMsRUFBQyxtQkFBaEI7QUFBQSxrQ0FDSjtBQUFLLGVBQUcsRUFBR0MsdUJBQUEsR0FBb0NpRCxPQUFPLENBQUNnTyxJQUFSLENBQWFoUixRQUFiLENBQXVCLENBQXZCLEVBQTJCQyxHQUExRTtBQUNJLGVBQUcsRUFBQyxtQkFEUjtBQUM0QixpQkFBSyxFQUFDLEtBRGxDO0FBQ3dDLGtCQUFNLEVBQUM7QUFEL0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESSxlQUdKO0FBQU0scUJBQVMsRUFBQyxjQUFoQjtBQUFBLHNCQUFpQytDLE9BQU8sQ0FBQ2dPLElBQVIsQ0FBYTlRO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFESixHQVVZLEVBWnBCLEVBZ0JROEMsT0FBTyxDQUFDaU8sSUFBUixnQkFDSTtBQUFJLGVBQVMsRUFBQyxrQkFBZDtBQUFBLDZCQUNJLDhEQUFDLHFFQUFEO0FBQU8sWUFBSSxFQUFHO0FBQUUzUyxrQkFBUSxFQUFFUCxNQUFNLENBQUNPLFFBQW5CO0FBQTZCaUQsZUFBSyxFQUFFO0FBQUV6QixnQkFBSSxFQUFFa0QsT0FBTyxDQUFDaU8sSUFBUixDQUFhblI7QUFBckI7QUFBcEMsU0FBZDtBQUFrRixjQUFNLEVBQUcsS0FBM0Y7QUFBQSx5Q0FDUztBQUFHLG1CQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURULGVBRUk7QUFBTSxtQkFBUyxFQUFDLG1CQUFoQjtBQUFBLGtDQUNJO0FBQUssZUFBRyxFQUFHQyx1QkFBQSxHQUFvQ2lELE9BQU8sQ0FBQ2lPLElBQVIsQ0FBYWpSLFFBQWIsQ0FBdUIsQ0FBdkIsRUFBMkJDLEdBQTFFO0FBQ0ksZUFBRyxFQUFDLG1CQURSO0FBQzRCLGlCQUFLLEVBQUMsS0FEbEM7QUFDd0Msa0JBQU0sRUFBQztBQUQvQztBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURKLGVBR0k7QUFBTSxxQkFBUyxFQUFDLGNBQWhCO0FBQUEsc0JBQWlDK0MsT0FBTyxDQUFDaU8sSUFBUixDQUFhL1E7QUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLEdBVVksRUExQnBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBK0JILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFFQTs7QUFFQSxNQUFNZ1IsR0FBRyxHQUFHLENBQUM7QUFBRUMsV0FBRjtBQUFhQyxXQUFiO0FBQXdCQztBQUF4QixDQUFELEtBQXFDO0FBQzdDcFQsa0RBQVMsQ0FBQyxNQUFNO0FBQ1osUUFBSW9ULEtBQUssQ0FBQ0MsUUFBTixHQUFpQkMsSUFBakIsQ0FBc0JDLE9BQXRCLEtBQWtDQyx3REFBdEMsRUFBbUQ7QUFDL0NKLFdBQUssQ0FBQ0ssUUFBTixDQUFlQyxpRUFBQSxDQUF5QkYsd0RBQXpCLENBQWY7QUFDSDtBQUNKLEdBSlEsRUFJTixFQUpNLENBQVQ7QUFNQSxzQkFDSSw4REFBQyxpREFBRDtBQUFVLFNBQUssRUFBRUosS0FBakI7QUFBQSwyQkFDSSw4REFBQyx3RUFBRDtBQUNJLGVBQVMsRUFBRUEsS0FBSyxDQUFDTyxXQURyQjtBQUVJLGFBQU8sZUFBRTtBQUFLLGlCQUFTLEVBQUMsaUJBQWY7QUFBQSwrQkFDTDtBQUFLLG1CQUFTLEVBQUMsZUFBZjtBQUFBLGtDQUNJO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREosZUFFSTtBQUFLLHFCQUFTLEVBQUM7QUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUZKLGVBR0k7QUFBSyxxQkFBUyxFQUFDO0FBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFISixlQUlJO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREs7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGYjtBQUFBLDhCQVVJLDhEQUFDLHFEQUFEO0FBQUEsZ0NBQ0k7QUFBTSxpQkFBTyxFQUFDO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFESixlQUVJO0FBQU0sd0JBQVcsaUJBQWpCO0FBQW1DLGlCQUFPLEVBQUM7QUFBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGSixlQUdJO0FBQU0sY0FBSSxFQUFDLFVBQVg7QUFBc0IsaUJBQU8sRUFBQztBQUE5QjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUhKLGVBS0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBTEosZUFPSTtBQUFNLGNBQUksRUFBQyxVQUFYO0FBQXNCLGlCQUFPLEVBQUM7QUFBOUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFQSixlQVFJO0FBQU0sY0FBSSxFQUFDLGFBQVg7QUFBeUIsaUJBQU8sRUFBQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQVJKLGVBU0k7QUFBTSxjQUFJLEVBQUMsUUFBWDtBQUFvQixpQkFBTyxFQUFDO0FBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBVEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVZKLGVBc0JJLDhEQUFDLHVEQUFEO0FBQUEsK0JBQ0ksOERBQUMsU0FBRCxvQkFBZVIsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkF0Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKO0FBOEJILENBckNEOztBQXVDQUYsR0FBRyxDQUFDVyxlQUFKLEdBQXNCLE9BQU87QUFBRVYsV0FBRjtBQUFhVztBQUFiLENBQVAsS0FBOEI7QUFDaEQsTUFBSVYsU0FBUyxHQUFHLEVBQWhCOztBQUNBLE1BQUlELFNBQVMsQ0FBQ1UsZUFBZCxFQUErQjtBQUMzQlQsYUFBUyxHQUFHLE1BQU1ELFNBQVMsQ0FBQ1UsZUFBVixDQUEwQkMsR0FBMUIsQ0FBbEI7QUFDSDs7QUFDRCxTQUFPO0FBQUVWO0FBQUYsR0FBUDtBQUNILENBTkQ7O0FBUUEsK0RBQWVXLHlEQUFTLENBQUNDLDJDQUFELENBQVQsQ0FBcUJkLEdBQXJCLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5REE7QUFDQTtBQUVPLE1BQU1lLFdBQVcsR0FBRztBQUN2QkMsY0FBWSxFQUFFO0FBRFMsQ0FBcEI7QUFJUCxJQUFJQyxZQUFZLEdBQUc7QUFDZlgsU0FBTyxFQUFFO0FBRE0sQ0FBbkI7O0FBSUEsTUFBTVksV0FBVyxHQUFHLENBQUU5UixLQUFLLEdBQUc2UixZQUFWLEVBQXdCRSxNQUF4QixLQUFvQztBQUNwRCxVQUFTQSxNQUFNLENBQUNoTSxJQUFoQjtBQUNJLFNBQUs0TCxXQUFXLENBQUNDLFlBQWpCO0FBQ0ksNkNBQ081UixLQURQO0FBRUlrUixlQUFPLEVBQUVhLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlZDtBQUY1Qjs7QUFLSjtBQUNJLGFBQU9sUixLQUFQO0FBUlI7QUFVSCxDQVhEOztBQWFPLE1BQU1xUixXQUFXLEdBQUc7QUFDdkJZLGNBQVksRUFBSWYsT0FBRixLQUFpQjtBQUFFbkwsUUFBSSxFQUFFNEwsV0FBVyxDQUFDQyxZQUFwQjtBQUFrQ0ksV0FBTyxFQUFFO0FBQUVkO0FBQUY7QUFBM0MsR0FBakI7QUFEUyxDQUFwQjtBQUlQLE1BQU1nQixhQUFhLEdBQUc7QUFDbEJDLFdBQVMsRUFBRSxRQURPO0FBRWxCQyxLQUFHLEVBQUUsTUFGYTtBQUdsQkMsU0FBT0E7QUFIVyxDQUF0QjtBQU1BLCtEQUFlQyw2REFBYyxDQUFFSixhQUFGLEVBQWlCSixXQUFqQixDQUE3QixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTVMsY0FBYyxHQUFHQyxpREFBb0IsRUFBM0M7QUFFQSxNQUFNQyxZQUFZLEdBQUdDLHNEQUFlLENBQUU7QUFDbEN6UyxNQUFJLEVBQUUwUyxnREFENEI7QUFFbENsSCxPQUFLLEVBQUVtSCxpREFGMkI7QUFHbEN0RSxVQUFRLEVBQUV1RSxvREFId0I7QUFJbEM1QixNQUFJLEVBQUVhLGdEQUFXQTtBQUppQixDQUFGLENBQXBDOztBQU9BLE1BQU1KLFNBQVMsR0FBRyxDQUFFRyxZQUFGLEVBQWdCaUIsT0FBaEIsS0FBNkI7QUFDM0MsUUFBTS9CLEtBQUssR0FBR2dDLGtEQUFXLENBQUVOLFlBQUYsRUFBZ0JPLHNEQUFlLENBQUVULGNBQUYsQ0FBL0IsQ0FBekI7QUFFQXhCLE9BQUssQ0FBQ2tDLFFBQU4sR0FBaUJWLGNBQWMsQ0FBQ1csR0FBZixDQUFvQkMscURBQXBCLENBQWpCLENBSDJDLENBSzNDOztBQUNBcEMsT0FBSyxDQUFDTyxXQUFOLEdBQW9COEIsMkRBQVksQ0FBRXJDLEtBQUYsQ0FBaEM7QUFDQSxTQUFPQSxLQUFQO0FBQ0gsQ0FSRDs7QUFVQSwrREFBZVcsU0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QkE7QUFFQTtBQUVlLFVBQVV5QixRQUFWLEdBQXFCO0FBQ2hDLFFBQU1FLHVEQUFHLENBQUUsQ0FDUEMscURBQVEsRUFERCxDQUFGLENBQVQ7QUFHSCxDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ1JNLE1BQU1uUCxRQUFRLEdBQUc7QUFDcEIsVUFBUTtBQUNKLGtCQUFjLENBQ1Y7QUFDSSxlQUFTLHFCQURiO0FBRUksYUFBTztBQUZYLEtBRFU7QUFEVixHQURZO0FBU3BCLGFBQVc7QUFDUCxhQUFTLENBQ0w7QUFDSSxlQUFTLGdCQURiO0FBRUksYUFBTztBQUZYLEtBREssQ0FERjtBQU9QLGNBQVUsQ0FDTjtBQUNJLGVBQVMsYUFEYjtBQUVJLGFBQU8sMENBRlg7QUFHSSxhQUFPO0FBSFgsS0FETTtBQVBILEdBVFM7QUF3QnBCLFdBQVMsQ0FDTDtBQUNJLGFBQVMsT0FEYjtBQUVJLFdBQU87QUFGWCxHQURLLEVBS0w7QUFDSSxhQUFTLFlBRGI7QUFFSSxXQUFPO0FBRlgsR0FMSyxFQVNMO0FBQ0ksYUFBUyxZQURiO0FBRUksV0FBTztBQUZYLEdBVEssRUFhTDtBQUNJLGFBQVMsTUFEYjtBQUVJLFdBQU87QUFGWCxHQWJLLEVBaUJMO0FBQ0ksYUFBUyxXQURiO0FBRUksV0FBTztBQUZYLEdBakJLLEVBcUJMO0FBQ0ksYUFBUyxhQURiO0FBRUksV0FBTztBQUZYLEdBckJLLENBeEJXO0FBa0RwQixVQUFRLENBQ0o7QUFDSSxhQUFTLFNBRGI7QUFFSSxXQUFPO0FBRlgsR0FESSxFQUtKO0FBQ0ksYUFBUyxTQURiO0FBRUksV0FBTztBQUZYLEdBTEksRUFTSjtBQUNJLGFBQVMsTUFEYjtBQUVJLFdBQU8saUJBRlg7QUFHSSxnQkFBWSxDQUNSO0FBQ0ksZUFBUyxnQkFEYjtBQUVJLGFBQU87QUFGWCxLQURRLEVBS1I7QUFDSSxlQUFTLGdCQURiO0FBRUksYUFBTztBQUZYLEtBTFEsRUFTUjtBQUNJLGVBQVMsZ0JBRGI7QUFFSSxhQUFPO0FBRlgsS0FUUSxFQWFSO0FBQ0ksZUFBUyxjQURiO0FBRUksYUFBTztBQUZYLEtBYlE7QUFIaEIsR0FUSSxFQStCSjtBQUNJLGFBQVMsU0FEYjtBQUVJLFdBQU8sb0JBRlg7QUFHSSxnQkFBWSxDQUNSO0FBQ0ksZUFBUyxtQkFEYjtBQUVJLGFBQU87QUFGWCxLQURRLEVBS1I7QUFDSSxlQUFTLG1CQURiO0FBRUksYUFBTztBQUZYLEtBTFEsRUFTUjtBQUNJLGVBQVMsbUJBRGI7QUFFSSxhQUFPO0FBRlgsS0FUUSxFQWFSO0FBQ0ksZUFBUyxpQkFEYjtBQUVJLGFBQU87QUFGWCxLQWJRO0FBSGhCLEdBL0JJLEVBcURKO0FBQ0ksYUFBUyxNQURiO0FBRUksV0FBTyxnQkFGWDtBQUdJLGdCQUFZLENBQ1I7QUFDSSxlQUFTLGdCQURiO0FBRUksYUFBTztBQUZYLEtBRFEsRUFLUjtBQUNJLGVBQVMsbUJBRGI7QUFFSSxhQUFPO0FBRlgsS0FMUTtBQUhoQixHQXJESSxFQW1FSjtBQUNJLGFBQVMsYUFEYjtBQUVJLFdBQU87QUFGWCxHQW5FSSxDQWxEWTtBQTBIcEIsYUFBVyxDQUNQO0FBQ0ksYUFBUyxVQURiO0FBRUksV0FBTztBQUZYLEdBRE8sRUFLUDtBQUNJLGFBQVMsWUFEYjtBQUVJLFdBQU87QUFGWCxHQUxPLEVBU1A7QUFDSSxhQUFTLFFBRGI7QUFFSSxXQUFPO0FBRlgsR0FUTyxFQWFQO0FBQ0ksYUFBUyxrQkFEYjtBQUVJLFdBQU87QUFGWCxHQWJPLEVBaUJQO0FBQ0ksYUFBUyxTQURiO0FBRUksV0FBTztBQUZYLEdBakJPLEVBcUJQO0FBQ0ksYUFBUyxZQURiO0FBRUksV0FBTztBQUZYLEdBckJPLEVBeUJQO0FBQ0ksYUFBUyxzQkFEYjtBQUVJLFdBQU87QUFGWCxHQXpCTyxFQTZCUDtBQUNJLGFBQVMsTUFEYjtBQUVJLFdBQU87QUFGWCxHQTdCTyxFQWlDUDtBQUNJLGFBQVMsY0FEYjtBQUVJLFdBQU87QUFGWCxHQWpDTyxFQXFDUDtBQUNJLGFBQVMsWUFEYjtBQUVJLFdBQU87QUFGWCxHQXJDTyxFQXlDUDtBQUNJLGFBQVMsWUFEYjtBQUVJLFdBQU87QUFGWCxHQXpDTyxFQTZDUDtBQUNJLGFBQVMsZ0JBRGI7QUFFSSxXQUFPO0FBRlgsR0E3Q08sRUFpRFA7QUFDSSxhQUFTLFlBRGI7QUFFSSxXQUFPO0FBRlgsR0FqRE8sRUFxRFA7QUFDSSxhQUFTLE9BRGI7QUFFSSxXQUFPO0FBRlgsR0FyRE87QUExSFMsQ0FBakI7QUF1TEEsTUFBTW9QLFlBQVksR0FBRyxDQUN4QjtBQUNJLFNBQU8sWUFEWDtBQUVJLFdBQVMsbUJBRmI7QUFHSSxXQUFTO0FBSGIsQ0FEd0IsRUFNeEI7QUFDSSxTQUFPLFlBRFg7QUFFSSxXQUFTLGNBRmI7QUFHSSxXQUFTO0FBSGIsQ0FOd0IsRUFXeEI7QUFDSSxTQUFPLFNBRFg7QUFFSSxXQUFTLGdCQUZiO0FBR0ksV0FBUztBQUhiLENBWHdCLEVBZ0J4QjtBQUNJLFNBQU8sS0FEWDtBQUVJLFdBQVMsYUFGYjtBQUdJLFdBQVM7QUFIYixDQWhCd0IsRUFxQnhCO0FBQ0ksU0FBTyxZQURYO0FBRUksV0FBUyxrQkFGYjtBQUdJLFdBQVM7QUFIYixDQXJCd0IsRUEwQnhCO0FBQ0ksU0FBTyxPQURYO0FBRUksV0FBUyxjQUZiO0FBR0ksV0FBUztBQUhiLENBMUJ3QixFQStCeEI7QUFDSSxTQUFPLFlBRFg7QUFFSSxXQUFTLG1CQUZiO0FBR0ksV0FBUztBQUhiLENBL0J3QixFQW9DeEI7QUFDSSxTQUFPLFlBRFg7QUFFSSxXQUFTLGtCQUZiO0FBR0ksV0FBUztBQUhiLENBcEN3QixFQXlDeEI7QUFDSSxTQUFPLFVBRFg7QUFFSSxXQUFTLGlCQUZiO0FBR0ksV0FBUztBQUhiLENBekN3QixFQThDeEI7QUFDSSxTQUFPLE1BRFg7QUFFSSxXQUFTLGFBRmI7QUFHSSxXQUFTO0FBSGIsQ0E5Q3dCLEVBbUR4QjtBQUNJLFNBQU8sY0FEWDtBQUVJLFdBQVMscUJBRmI7QUFHSSxXQUFTO0FBSGIsQ0FuRHdCLEVBd0R4QjtBQUNJLFNBQU8sUUFEWDtBQUVJLFdBQVMsZUFGYjtBQUdJLFdBQVM7QUFIYixDQXhEd0IsRUE2RHhCO0FBQ0ksU0FBTyxZQURYO0FBRUksV0FBUyxvQkFGYjtBQUdJLFdBQVM7QUFIYixDQTdEd0IsRUFrRXhCO0FBQ0ksU0FBTyxRQURYO0FBRUksV0FBUyxlQUZiO0FBR0ksV0FBUztBQUhiLENBbEV3QixDQUFyQjtBQXlFQSxNQUFNeFYsc0JBQXNCLEdBQUcsQ0FDbEMsR0FEa0MsRUFFbEMsT0FGa0MsRUFHbEMsdUJBSGtDLEVBSWxDLHlCQUprQyxFQUtsQyx5QkFMa0MsRUFNbEMseUJBTmtDLEVBT2xDLHFCQVBrQyxFQVFsQyxtQkFSa0MsRUFTbEMsWUFUa0MsRUFVbEMsV0FWa0MsRUFXbEMsb0JBWGtDLEVBWWxDLHNCQVprQyxFQWFsQyxrQkFia0MsRUFjbEMsNEJBZGtDLEVBZWxDLG1CQWZrQyxFQWdCbEMsc0JBaEJrQyxFQWlCbEMsa0JBakJrQyxFQWtCbEMsZ0JBbEJrQyxFQW1CbEMsd0JBbkJrQyxFQW9CbEMsc0JBcEJrQyxFQXFCbEMsc0JBckJrQyxFQXNCbEMsZUF0QmtDLEVBdUJsQyxzQkF2QmtDLEVBd0JsQyxpQkF4QmtDLENBQS9CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hRUCxpRDs7Ozs7Ozs7Ozs7QUNBQSwwQzs7Ozs7Ozs7Ozs7QUNBQSxtQzs7Ozs7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7QUNBQSwwQzs7Ozs7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSx5RTs7Ozs7Ozs7Ozs7QUNBQSxpRzs7Ozs7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7QUNBQSxtQzs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSwwQzs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxzQzs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7QUNBQSxpRDs7Ozs7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSx3Qzs7Ozs7Ozs7Ozs7QUNBQSw0Qzs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxtQzs7Ozs7Ozs7Ozs7QUNBQSwyQzs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSx3Qzs7Ozs7Ozs7Ozs7QUNBQSxnRCIsImZpbGUiOiJwYWdlcy9fYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBGb290ZXIgKCkge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8Zm9vdGVyIGNsYXNzTmFtZT1cImZvb3RlclwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb290ZXItdG9wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgYWxpZ24taXRlbXMtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiL1wiIGNsYXNzTmFtZT1cImxvZ28tZm9vdGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2xvZ28ucG5nXCIgYWx0PVwibG9nby1mb290ZXJcIiB3aWR0aD1cIjE1NFwiIGhlaWdodD1cIjQzXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LyogPGgyICAgc3R5bGU9eyB7IGNvbG9yOiBcIndoaXRlXCIgfSB9PkRTUVVBUkU8L2gyPiAqL31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbGctOVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3aWRnZXQgd2lkZ2V0LW5ld3NsZXR0ZXIgZm9ybS13cmFwcGVyIGZvcm0td3JhcHBlci1pbmxpbmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5ld3NsZXR0ZXItaW5mbyBteC1hdXRvIG1yLWxnLTIgbWwtbGctNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwid2lkZ2V0LXRpdGxlXCI+U3Vic2NyaWJlIHRvIG91ciBOZXdzbGV0dGVyPC9oND5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+R2V0IGFsbCB0aGUgbGF0ZXN0IGluZm9ybWF0aW9uLCBTYWxlcyBhbmQgT2ZmZXJzLjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Zm9ybSBhY3Rpb249XCIjXCIgY2xhc3NOYW1lPVwiaW5wdXQtd3JhcHBlciBpbnB1dC13cmFwcGVyLWlubGluZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImVtYWlsXCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgbmFtZT1cImVtYWlsXCIgaWQ9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVtYWlsIGFkZHJlc3MgaGVyZS4uLlwiIHJlcXVpcmVkIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1yb3VuZGVkIGJ0bi1tZCBtbC0yXCIgdHlwZT1cInN1Ym1pdFwiPnN1YnNjcmliZTxpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWljb24tYXJyb3ctcmlnaHRcIj48L2k+PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9vdGVyLW1pZGRsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTMgY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwid2lkZ2V0IHdpZGdldC1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cIndpZGdldC10aXRsZVwiPkNvbnRhY3QgSW5mbzwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cIndpZGdldC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbD5QaG9uZTogPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwidGVsOiNcIj5Ub2xsIEZyZWUgKDEyMykgNDU2LTc4OTA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+RW1haWw6IDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj5tYWlsQGRzcWF1cmUuY29tPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPkFkZHJlc3M6IDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj4xMjMgU3RyZWV0IE5hbWUsIENpdHksIEluZGlhPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPldPUktJTkcgREFZUyAvIEhPVVJTOiA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj5Nb24gLSBTdW4gLyA5OjAwIEFNIC0gODowMCBQTTwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTMgY29sLW1kLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwid2lkZ2V0IG1sLWxnLTRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwid2lkZ2V0LXRpdGxlXCI+TXkgQWNjb3VudDwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cIndpZGdldC1ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiL3BhZ2VzL2Fib3V0LXVzXCI+QWJvdXQgVXM8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj5PcmRlciBIaXN0b3J5PC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCI+UmV0dXJuczwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPkN1c3RvbSBTZXJ2aWNlPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCI+VGVybXMgJmFtcDsgQ29uZGl0aW9uPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbGctMyBjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3aWRnZXQgbWwtbGctNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJ3aWRnZXQtdGl0bGVcIj5Db250YWN0IEluZm88L2g0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJ3aWRnZXQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9wYWdlcy9sb2dpblwiPlNpZ24gaW48L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9wYWdlcy9jYXJ0XCI+VmlldyBDYXJ0PC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCI+TXkgV2lzaGxpc3Q8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj5UcmFjayBNeSBPcmRlcjwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPkhlbHA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1sZy0zIGNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIndpZGdldCB3aWRnZXQtaW5zdGFncmFtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cIndpZGdldC10aXRsZVwiPkluc3RhZ3JhbTwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGZpZ3VyZSBjbGFzc05hbWU9XCJ3aWRnZXQtYm9keSByb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2luc3RhZ3JhbS8wMS5qcGdcIiBhbHQ9XCJpbnN0YWdyYW0gMVwiIHdpZHRoPVwiNjRcIiBoZWlnaHQ9XCI2NFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaW5zdGFncmFtLzAyLmpwZ1wiIGFsdD1cImluc3RhZ3JhbSAyXCIgd2lkdGg9XCI2NFwiIGhlaWdodD1cIjY0XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9pbnN0YWdyYW0vMDMuanBnXCIgYWx0PVwiaW5zdGFncmFtIDNcIiB3aWR0aD1cIjY0XCIgaGVpZ2h0PVwiNjRcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2luc3RhZ3JhbS8wNC5qcGdcIiBhbHQ9XCJpbnN0YWdyYW0gNFwiIHdpZHRoPVwiNjRcIiBoZWlnaHQ9XCI2NFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaW5zdGFncmFtLzA1LmpwZ1wiIGFsdD1cImluc3RhZ3JhbSA1XCIgd2lkdGg9XCI2NFwiIGhlaWdodD1cIjY0XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiL2ltYWdlcy9pbnN0YWdyYW0vMDYuanBnXCIgYWx0PVwiaW5zdGFncmFtIDZcIiB3aWR0aD1cIjY0XCIgaGVpZ2h0PVwiNjRcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL2luc3RhZ3JhbS8wNy5qcGdcIiBhbHQ9XCJpbnN0YWdyYW0gN1wiIHdpZHRoPVwiNjRcIiBoZWlnaHQ9XCI2NFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi9pbWFnZXMvaW5zdGFncmFtLzA4LmpwZ1wiIGFsdD1cImluc3RhZ3JhbSA4XCIgd2lkdGg9XCI2NFwiIGhlaWdodD1cIjY0XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9maWd1cmU+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvb3Rlci1ib3R0b21cIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvb3Rlci1sZWZ0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxmaWd1cmUgY2xhc3NOYW1lPVwicGF5bWVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1hZ2VzL3BheW1lbnQucG5nXCIgYWx0PVwicGF5bWVudFwiIHdpZHRoPVwiMTU5XCIgaGVpZ2h0PVwiMjlcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2ZpZ3VyZT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvb3Rlci1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiY29weXJpZ2h0IGxzLW5vcm1hbFwiPkRzcWF1cmUgZUNvbW1lcmNlICZjb3B5OyAyMDIxLiBBbGwgUmlnaHRzIFJlc2VydmVkPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9vdGVyLXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic29jaWFsLWxpbmtzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIiBjbGFzc05hbWU9XCJzb2NpYWwtbGluayBzb2NpYWwtZmFjZWJvb2sgZmFiIGZhLWZhY2Vib29rLWZcIj48L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwic29jaWFsLWxpbmsgc29jaWFsLXR3aXR0ZXIgZmFiIGZhLXR3aXR0ZXJcIj48L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwic29jaWFsLWxpbmsgc29jaWFsLWxpbmtlZGluIGZhYiBmYS1saW5rZWRpbi1pblwiPjwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZm9vdGVyPlxyXG4gICAgKVxyXG59IiwiaW1wb3J0IHsgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XHJcblxyXG5pbXBvcnQgQUxpbmsgZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL2N1c3RvbS1saW5rJztcclxuXHJcbmltcG9ydCBDYXJ0TWVudSBmcm9tICd+L2NvbXBvbmVudHMvY29tbW9uL3BhcnRpYWxzL2NhcnQtbWVudSc7XHJcbmltcG9ydCBNYWluTWVudSBmcm9tICd+L2NvbXBvbmVudHMvY29tbW9uL3BhcnRpYWxzL21haW4tbWVudSc7XHJcbmltcG9ydCBTZWFyY2hCb3ggZnJvbSAnfi9jb21wb25lbnRzL2NvbW1vbi9wYXJ0aWFscy9zZWFyY2gtYm94JztcclxuaW1wb3J0IExvZ2luTW9kYWwgZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL21vZGFscy9sb2dpbi1tb2RhbCc7XHJcblxyXG5pbXBvcnQgeyBoZWFkZXJCb3JkZXJSZW1vdmVMaXN0IH0gZnJvbSAnfi91dGlscy9kYXRhL21lbnUnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIZWFkZXIoIHByb3BzICkge1xyXG4gICAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgbGV0IGhlYWRlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdoZWFkZXInICk7XHJcbiAgICAgICAgaWYgKCBoZWFkZXIgKSB7XHJcbiAgICAgICAgICAgIGlmICggaGVhZGVyQm9yZGVyUmVtb3ZlTGlzdC5pbmNsdWRlcyggcm91dGVyLnBhdGhuYW1lICkgJiYgaGVhZGVyLmNsYXNzTGlzdC5jb250YWlucyggJ2hlYWRlci1ib3JkZXInICkgKSBoZWFkZXIuY2xhc3NMaXN0LnJlbW92ZSggJ2hlYWRlci1ib3JkZXInIClcclxuICAgICAgICAgICAgZWxzZSBpZiAoICFoZWFkZXJCb3JkZXJSZW1vdmVMaXN0LmluY2x1ZGVzKCByb3V0ZXIucGF0aG5hbWUgKSApIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdoZWFkZXInICkuY2xhc3NMaXN0LmFkZCggJ2hlYWRlci1ib3JkZXInICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgWyByb3V0ZXIucGF0aG5hbWUgXSApXHJcblxyXG4gICAgY29uc3Qgc2hvd01vYmlsZU1lbnUgPSAoKSA9PiB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ2JvZHknICkuY2xhc3NMaXN0LmFkZCggJ21tZW51LWFjdGl2ZScgKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPVwiaGVhZGVyIGhlYWRlci1ib3JkZXJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXItdG9wXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLWxlZnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwid2VsY29tZS1tc2dcIj5XZWxjb21lIHRvIERzcWF1cmUgc3RvcmUhPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZHJvcGRvd25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPlVTRDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwiZHJvcGRvd24tYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxBTGluayBocmVmPVwiI1wiPlVTRDwvQUxpbms+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PEFMaW5rIGhyZWY9XCIjXCI+RVVSPC9BTGluaz48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImRyb3Bkb3duIG1sLTVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPkVORzwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwiZHJvcGRvd24tYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj5FTkc8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIj5GUkg8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImRpdmlkZXJcIj48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiL3BhZ2VzL2NvbnRhY3QtdXNcIiBjbGFzc05hbWU9XCJjb250YWN0IGQtbGctc2hvd1wiPjxpIGNsYXNzTmFtZT1cImQtaWNvbi1tYXBcIj48L2k+Q29udGFjdDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiIGNsYXNzTmFtZT1cImhlbHAgZC1sZy1zaG93XCI+PGkgY2xhc3NOYW1lPVwiZC1pY29uLWluZm9cIj48L2k+IE5lZWQgSGVscDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMb2dpbk1vZGFsIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlci1taWRkbGUgc3RpY2t5LWhlYWRlciBmaXgtdG9wIHN0aWNreS1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLWxlZnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwibW9iaWxlLW1lbnUtdG9nZ2xlXCIgb25DbGljaz17IHNob3dNb2JpbGVNZW51IH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJkLWljb24tYmFyczJcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9cIiBjbGFzc05hbWU9XCJsb2dvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz0nL2ltYWdlcy9sb2dvLnBuZycgYWx0PVwibG9nb1wiIHdpZHRoPVwiMTUzXCIgaGVpZ2h0PVwiNDRcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8U2VhcmNoQm94IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwidGVsOiNcIiBjbGFzc05hbWU9XCJpY29uLWJveCBpY29uLWJveC1zaWRlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImljb24tYm94LWljb24gbXItMCBtci1sZy0yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZC1pY29uLXBob25lXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImljb24tYm94LWNvbnRlbnQgZC1sZy1zaG93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cImljb24tYm94LXRpdGxlXCI+Q2FsbCBVcyBOb3c6PC9oND5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cD4wKDgwMCkgMTIzLTQ1NjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJkaXZpZGVyXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Q2FydE1lbnUgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLWJvdHRvbSBkLWxnLXNob3dcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXItbGVmdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8TWFpbk1lbnUgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXItcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwiYnRuIGJ0bi1zbSBidG4tcHJpbWFyeSBidG4tcm91bmRlZCBidG4taWNvbi1yaWdodFwiPjxpIGNsYXNzTmFtZT1cImZhcyBmYS1jbG91ZC11cGxvYWQtYWx0XCI+PC9pPlVwbG9hZDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9oZWFkZXIgPlxyXG4gICAgKTtcclxufSIsImltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5cclxuaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcblxyXG5pbXBvcnQgeyBjYXJ0QWN0aW9ucyB9IGZyb20gJ34vc3RvcmUvY2FydCc7XHJcblxyXG5pbXBvcnQgeyBnZXRUb3RhbFByaWNlLCBnZXRDYXJ0Q291bnQsIHRvRGVjaW1hbCB9IGZyb20gJ34vdXRpbHMnO1xyXG5cclxuZnVuY3Rpb24gQ2FydE1lbnUoIHByb3BzICkge1xyXG4gICAgY29uc3QgeyBjYXJ0TGlzdCwgcmVtb3ZlRnJvbUNhcnQgfSA9IHByb3BzO1xyXG4gICAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgaGlkZUNhcnRNZW51KCk7XHJcbiAgICB9LCBbIHJvdXRlci5hc1BhdGggXSApXHJcblxyXG4gICAgY29uc3Qgc2hvd0NhcnRNZW51ID0gKCBlICkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLmN1cnJlbnRUYXJnZXQuY2xvc2VzdCggJy5jYXJ0LWRyb3Bkb3duJyApLmNsYXNzTGlzdC5hZGQoICdvcGVuZWQnICk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgaGlkZUNhcnRNZW51ID0gKCkgPT4ge1xyXG4gICAgICAgIGlmICggZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5jYXJ0LWRyb3Bkb3duJyApLmNsYXNzTGlzdC5jb250YWlucyggJ29wZW5lZCcgKSApXHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuY2FydC1kcm9wZG93bicgKS5jbGFzc0xpc3QucmVtb3ZlKCAnb3BlbmVkJyApO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHJlbW92ZUNhcnQgPSAoIGl0ZW0gKSA9PiB7XHJcbiAgICAgICAgcmVtb3ZlRnJvbUNhcnQoIGl0ZW0gKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZHJvcGRvd24gY2FydC1kcm9wZG93biB0eXBlMiBjYXJ0LW9mZmNhbnZhcyBtci0wIG1yLWxnLTJcIj5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBjbGFzc05hbWU9XCJjYXJ0LXRvZ2dsZSBsYWJlbC1ibG9jayBsaW5rXCIgb25DbGljaz17IHNob3dDYXJ0TWVudSB9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJ0LWxhYmVsIGQtbGctc2hvd1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNhcnQtbmFtZVwiPlNob3BwaW5nIENhcnQ6PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNhcnQtcHJpY2VcIj4keyB0b0RlY2ltYWwoIGdldFRvdGFsUHJpY2UoIGNhcnRMaXN0ICkgKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJkLWljb24tYmFnXCI+PHNwYW4gY2xhc3NOYW1lPVwiY2FydC1jb3VudFwiPnsgZ2V0Q2FydENvdW50KCBjYXJ0TGlzdCApIH08L3NwYW4+PC9pPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FydC1vdmVybGF5XCIgb25DbGljaz17IGhpZGVDYXJ0TWVudSB9PjwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImRyb3Bkb3duLWJveFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJ0LWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJjYXJ0LXRpdGxlXCI+U2hvcHBpbmcgQ2FydDwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwiYnRuIGJ0bi1kYXJrIGJ0bi1saW5rIGJ0bi1pY29uLXJpZ2h0IGJ0bi1jbG9zZVwiIG9uQ2xpY2s9eyBoaWRlQ2FydE1lbnUgfT5jbG9zZTxpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtaWNvbi1hcnJvdy1yaWdodFwiPjwvaT48c3BhbiBjbGFzc05hbWU9XCJzci1vbmx5XCI+Q2FydDwvc3Bhbj48L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FydExpc3QubGVuZ3RoID4gMCA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3RzIHNjcm9sbGFibGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhcnRMaXN0Lm1hcCggKCBpdGVtLCBpbmRleCApID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QgcHJvZHVjdC1jYXJ0XCIga2V5PXsgJ2NhcnQtbWVudS1wcm9kdWN0LScgKyBpbmRleCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmaWd1cmUgY2xhc3NOYW1lPVwicHJvZHVjdC1tZWRpYSBwdXJlLW1lZGlhXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgJy9wcm9kdWN0L2RlZmF1bHQvJyArIGl0ZW0uc2x1ZyB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9eyBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BU1NFVF9VUkkgKyBpdGVtLnBpY3R1cmVzWyAwIF0udXJsIH0gYWx0PVwicHJvZHVjdFwiIHdpZHRoPVwiODBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjg4XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidG4gYnRuLWxpbmsgYnRuLWNsb3NlXCIgb25DbGljaz17ICgpID0+IHsgcmVtb3ZlQ2FydCggaXRlbSApIH0gfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS10aW1lc1wiPjwvaT48c3BhbiBjbGFzc05hbWU9XCJzci1vbmx5XCI+Q2xvc2U8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZmlndXJlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1kZXRhaWxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyAnL3Byb2R1Y3QvZGVmYXVsdC8nICsgaXRlbS5zbHVnIH0gY2xhc3NOYW1lPVwicHJvZHVjdC1uYW1lXCI+eyBpdGVtLm5hbWUgfTwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJpY2UtYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwcm9kdWN0LXF1YW50aXR5XCI+eyBpdGVtLnF0eSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJvZHVjdC1wcmljZVwiPiR7IHRvRGVjaW1hbCggaXRlbS5wcmljZSApIH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJ0LXRvdGFsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlN1YnRvdGFsOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJpY2VcIj4keyB0b0RlY2ltYWwoIGdldFRvdGFsUHJpY2UoIGNhcnRMaXN0ICkgKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJ0LWFjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiL3BhZ2VzL2NhcnRcIiBjbGFzc05hbWU9XCJidG4gYnRuLWRhcmsgYnRuLWxpbmtcIiBvbkNsaWNrPXsgaGlkZUNhcnRNZW51IH0+VmlldyBDYXJ0PC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9wYWdlcy9jaGVja291dFwiIGNsYXNzTmFtZT1cImJ0biBidG4tZGFya1wiIG9uQ2xpY2s9eyBoaWRlQ2FydE1lbnUgfT48c3Bhbj5HbyBUbyBDaGVja291dDwvc3Bhbj48L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvPiA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIm10LTQgdGV4dC1jZW50ZXIgZm9udC13ZWlnaHQtc2VtaS1ib2xkIGxzLW5vcm1hbCB0ZXh0LWJvZHlcIj5ObyBwcm9kdWN0cyBpbiB0aGUgY2FydC48L3A+XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG59XHJcblxyXG5mdW5jdGlvbiBtYXBTdGF0ZVRvUHJvcHMoIHN0YXRlICkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBjYXJ0TGlzdDogc3RhdGUuY2FydC5kYXRhXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QoIG1hcFN0YXRlVG9Qcm9wcywgeyByZW1vdmVGcm9tQ2FydDogY2FydEFjdGlvbnMucmVtb3ZlRnJvbUNhcnQgfSApKCBDYXJ0TWVudSApOyIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XHJcbmltcG9ydCB7IHVzZUxhenlRdWVyeSB9IGZyb20gJ0BhcG9sbG8vcmVhY3QtaG9va3MnO1xyXG5pbXBvcnQgeyBMYXp5TG9hZEltYWdlIH0gZnJvbSAncmVhY3QtbGF6eS1sb2FkLWltYWdlLWNvbXBvbmVudCc7XHJcblxyXG5pbXBvcnQgQUxpbmsgZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL2N1c3RvbS1saW5rJztcclxuXHJcbmltcG9ydCB7IEdFVF9QUk9EVUNUUyB9IGZyb20gJ34vc2VydmVyL3F1ZXJpZXMnO1xyXG5pbXBvcnQgd2l0aEFwb2xsbyBmcm9tICd+L3NlcnZlci9hcG9sbG8nO1xyXG5cclxuaW1wb3J0IHsgdG9EZWNpbWFsIH0gZnJvbSAnfi91dGlscyc7XHJcblxyXG5mdW5jdGlvbiBTZWFyY2hGb3JtKCkge1xyXG4gICAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgICBjb25zdCBbIHNlYXJjaCwgc2V0U2VhcmNoIF0gPSB1c2VTdGF0ZSggXCJcIiApO1xyXG4gICAgY29uc3QgWyBzZWFyY2hQcm9kdWN0cywgeyBkYXRhIH0gXSA9IHVzZUxhenlRdWVyeSggR0VUX1BST0RVQ1RTICk7XHJcbiAgICBjb25zdCBbIHRpbWVyLCBzZXRUaW1lciBdID0gdXNlU3RhdGUoIG51bGwgKTtcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcImJvZHlcIiApLmFkZEV2ZW50TGlzdGVuZXIoIFwiY2xpY2tcIiwgb25Cb2R5Q2xpY2sgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuICggKCkgPT4ge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcImJvZHlcIiApLnJlbW92ZUV2ZW50TGlzdGVuZXIoIFwiY2xpY2tcIiwgb25Cb2R5Q2xpY2sgKTtcclxuICAgICAgICB9IClcclxuICAgIH0sIFtdIClcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBzZXRTZWFyY2goIFwiXCIgKTtcclxuICAgIH0sIFsgcm91dGVyLnF1ZXJ5LnNsdWcgXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgaWYgKCBzZWFyY2gubGVuZ3RoID4gMiApIHtcclxuICAgICAgICAgICAgaWYgKCB0aW1lciApIGNsZWFyVGltZW91dCggdGltZXIgKTtcclxuICAgICAgICAgICAgbGV0IHRpbWVySWQgPSBzZXRUaW1lb3V0KCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hQcm9kdWN0cyggeyB2YXJpYWJsZXM6IHsgc2VhcmNoOiBzZWFyY2ggfSB9ICk7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lciggbnVsbCApOztcclxuICAgICAgICAgICAgfSwgNTAwICk7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lciggdGltZXJJZCApO1xyXG4gICAgICAgIH1cclxuICAgIH0sIFsgc2VhcmNoIF0gKVxyXG5cclxuICAgIHVzZUVmZmVjdCggKCkgPT4ge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93LXJlc3VsdHMnICkgJiYgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5oZWFkZXItc2VhcmNoLnNob3ctcmVzdWx0cycgKS5jbGFzc0xpc3QucmVtb3ZlKCAnc2hvdy1yZXN1bHRzJyApO1xyXG4gICAgfSwgWyByb3V0ZXIucGF0aG5hbWUgXSApXHJcblxyXG4gICAgZnVuY3Rpb24gcmVtb3ZlWFNTQXR0YWNrcyggaHRtbCApIHtcclxuICAgICAgICBjb25zdCBTQ1JJUFRfUkVHRVggPSAvPHNjcmlwdFxcYltePF0qKD86KD8hPFxcL3NjcmlwdD4pPFtePF0qKSo8XFwvc2NyaXB0Pi9naTtcclxuXHJcbiAgICAgICAgLy8gUmVtb3ZpbmcgdGhlIDxzY3JpcHQ+IHRhZ3NcclxuICAgICAgICB3aGlsZSAoIFNDUklQVF9SRUdFWC50ZXN0KCBodG1sICkgKSB7XHJcbiAgICAgICAgICAgIGh0bWwgPSBodG1sLnJlcGxhY2UoIFNDUklQVF9SRUdFWCwgXCJcIiApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gUmVtb3ZpbmcgYWxsIGV2ZW50cyBmcm9tIHRhZ3MuLi5cclxuICAgICAgICBodG1sID0gaHRtbC5yZXBsYWNlKCAvIG9uXFx3Kz1cIlteXCJdKlwiL2csIFwiXCIgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgX19odG1sOiBodG1sXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIG1hdGNoRW1waGFzaXplKCBuYW1lICkge1xyXG4gICAgICAgIGxldCByZWdFeHAgPSBuZXcgUmVnRXhwKCBzZWFyY2gsIFwiaVwiICk7XHJcbiAgICAgICAgcmV0dXJuIG5hbWUucmVwbGFjZShcclxuICAgICAgICAgICAgcmVnRXhwLFxyXG4gICAgICAgICAgICAoIG1hdGNoICkgPT4gXCI8c3Ryb25nPlwiICsgbWF0Y2ggKyBcIjwvc3Ryb25nPlwiXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBvblNlYXJjaENsaWNrKCBlICkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGUuY3VycmVudFRhcmdldC5wYXJlbnROb2RlLmNsYXNzTGlzdC50b2dnbGUoICdzaG93JyApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIG9uQm9keUNsaWNrKCBlICkge1xyXG4gICAgICAgIGlmICggZS50YXJnZXQuY2xvc2VzdCggJy5oZWFkZXItc2VhcmNoJyApICkgcmV0dXJuIGUudGFyZ2V0LmNsb3Nlc3QoICcuaGVhZGVyLXNlYXJjaCcgKS5jbGFzc0xpc3QuY29udGFpbnMoICdzaG93LXJlc3VsdHMnICkgfHwgZS50YXJnZXQuY2xvc2VzdCggJy5oZWFkZXItc2VhcmNoJyApLmNsYXNzTGlzdC5hZGQoICdzaG93LXJlc3VsdHMnICk7XHJcblxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93JyApICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93JyApLmNsYXNzTGlzdC5yZW1vdmUoICdzaG93JyApO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93LXJlc3VsdHMnICkgJiYgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5oZWFkZXItc2VhcmNoLnNob3ctcmVzdWx0cycgKS5jbGFzc0xpc3QucmVtb3ZlKCAnc2hvdy1yZXN1bHRzJyApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIG9uU2VhcmNoQ2hhbmdlKCBlICkge1xyXG4gICAgICAgIHNldFNlYXJjaCggZS50YXJnZXQudmFsdWUgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBvblN1Ym1pdFNlYXJjaEZvcm0oIGUgKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHJvdXRlci5wdXNoKCB7XHJcbiAgICAgICAgICAgIHBhdGhuYW1lOiAnL3Nob3AnLFxyXG4gICAgICAgICAgICBxdWVyeToge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoOiBzZWFyY2hcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLXNlYXJjaCBocy10b2dnbGUgZGlyLXVwXCI+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwic2VhcmNoLXRvZ2dsZSBzdGlja3ktbGlua1wiIHJvbGU9XCJidXR0b25cIiBvbkNsaWNrPXsgb25TZWFyY2hDbGljayB9PlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZC1pY29uLXNlYXJjaFwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxzcGFuPlNlYXJjaDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8Zm9ybSBhY3Rpb249XCIjXCIgbWV0aG9kPVwiZ2V0XCIgb25TdWJtaXQ9eyBvblN1Ym1pdFNlYXJjaEZvcm0gfSBjbGFzc05hbWU9XCJpbnB1dC13cmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwic2VhcmNoXCIgYXV0b0NvbXBsZXRlPVwib2ZmXCIgdmFsdWU9eyBzZWFyY2ggfSBvbkNoYW5nZT17IG9uU2VhcmNoQ2hhbmdlIH1cclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlNlYXJjaC4uLlwiIHJlcXVpcmVkIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidG4gYnRuLXNlYXJjaFwiIHR5cGU9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJkLWljb24tc2VhcmNoXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcblxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsaXZlLXNlYXJjaC1saXN0IGJnLXdoaXRlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgeyBzZWFyY2gubGVuZ3RoID4gMiAmJiBkYXRhICYmIGRhdGEucHJvZHVjdHMuZGF0YS5tYXAoICggcHJvZHVjdCwgaW5kZXggKSA9PiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgYC9wcm9kdWN0L2RlZmF1bHQvJHsgcHJvZHVjdC5zbHVnIH1gIH0gY2xhc3NOYW1lPVwiYXV0b2NvbXBsZXRlLXN1Z2dlc3Rpb25cIiBrZXk9eyBgc2VhcmNoLXJlc3VsdC0keyBpbmRleCB9YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhenlMb2FkSW1hZ2UgZWZmZWN0PVwib3BhY2l0eVwiIHNyYz17IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FTU0VUX1VSSSArIHByb2R1Y3QucGljdHVyZXNbIDAgXS51cmwgfSB3aWR0aD17IDQwIH0gaGVpZ2h0PXsgNDAgfSBhbHQ9XCJwcm9kdWN0XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2VhcmNoLW5hbWVcIiBkYW5nZXJvdXNseVNldElubmVySFRNTD17IHJlbW92ZVhTU0F0dGFja3MoIG1hdGNoRW1waGFzaXplKCBwcm9kdWN0Lm5hbWUgKSApIH0+PC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwic2VhcmNoLXByaWNlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LnByaWNlWyAwIF0gIT09IHByb2R1Y3QucHJpY2VbIDEgXSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LnZhcmlhbnRzLmxlbmd0aCA9PT0gMCA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibmV3LXByaWNlIG1yLTFcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QucHJpY2VbIDAgXSApIH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm9sZC1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5wcmljZVsgMSBdICkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPCBzcGFuIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5wcmljZVsgMCBdICkgfSDigJMgJHsgdG9EZWNpbWFsKCBwcm9kdWN0LnByaWNlWyAxIF0gKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiA8c3BhbiBjbGFzc05hbWU9XCJuZXctcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QucHJpY2VbIDAgXSApIH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICkgKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCB3aXRoQXBvbGxvKCB7IHNzcjogdHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcgfSApKCBTZWFyY2hGb3JtICk7IiwiaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5pbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcbmltcG9ydCB7IG1haW5NZW51IH0gZnJvbSAnfi91dGlscy9kYXRhL21lbnUnO1xyXG5pbXBvcnQgeyBQUk9EVUNUX0NBVEVHT1JZX0FQSSB9IGZyb20gJy4uL0FwaSc7XHJcblxyXG5mdW5jdGlvbiBNYWluTWVudSgpIHtcclxuICAgIGNvbnN0IHBhdGhuYW1lID0gdXNlUm91dGVyKCkucGF0aG5hbWU7XHJcbiAgICBjb25zdCBbYWxsQ2F0ZWdvcnksIHNldENhdGVnb3J5XSA9IHVzZVN0YXRlKFtdKVxyXG4gICAgXHJcblxyXG4gICAgIGNvbnN0IEdldEFsbENhdGVnb3J5ID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgXHJcbiAgICAgICAgY29uc3QgY29uZmlnID0geyAgXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHsgYWNjZXNzVG9rZW46ICdhJyB9XHJcbiAgICAgICAgICB9O1xyXG5cclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgY29uc3QgeyBkYXRhOiB7IGJvZHksIGVycm9yIH0gfSA9IGF3YWl0IGF4aW9zLmdldChQUk9EVUNUX0NBVEVHT1JZX0FQSSxjb25maWcpXHJcbiAgICAgICAgICAgIHNldENhdGVnb3J5KGJvZHkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICBcclxuICAgIC8vICAgY29uc29sZS5sb2coYWxsQ2F0ZWdvcnksXCJycnJycnJycnJycnJcIik7XHJcbiAgICAgdXNlRWZmZWN0KCgpID0+e1xyXG4gICAgICAgIEdldEFsbENhdGVnb3J5KCk7XHJcbiAgICAgfSxbXSlcclxuICAgIFxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8bmF2IGNsYXNzTmFtZT1cIm1haW4tbmF2XCI+XHJcbiAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJtZW51XCI+XHJcbiAgICAgICAgICAgICAgICA8bGkgaWQ9XCJtZW51LWhvbWVcIiBjbGFzc05hbWU9eyBwYXRobmFtZSA9PT0gJy8nID8gJ2FjdGl2ZScgOiAnJyB9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPScvJz5Ib21lPC9BTGluaz5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcblxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT17IGBzdWJtZW51ICAkeyBwYXRobmFtZS5pbmNsdWRlcyggJy9zaG9wJyApID8gJ2FjdGl2ZScgOiAnJyB9YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPkNhdGVnb3JpZXM8L0FMaW5rPlxyXG5cclxuICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGxDYXRlZ29yeS5tYXAoICggaXRlbSwgaW5kZXggKSA9PiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGtleT17IGBzaG9wLSR7IGl0ZW0uaWQgfWAgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyAnLycgKyBpdGVtLmNhdGVnb3J5TmFtZSB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpdGVtLmNhdGVnb3J5TmFtZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGl0ZW0uaG90ID8gPHNwYW4gY2xhc3NOYW1lPVwidGlwIHRpcC1ob3RcIj5Ib3Q8L3NwYW4+IDogXCJcIiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcblxyXG57LyogXHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPXsgYHN1Ym1lbnUgICR7IHBhdGhuYW1lLmluY2x1ZGVzKCAnL3BhZ2VzJyApID8gJ2FjdGl2ZScgOiAnJyB9YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPlBhZ2VzPC9BTGluaz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYWluTWVudS5vdGhlci5tYXAoICggaXRlbSwgaW5kZXggKSA9PiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGtleT17IGBvdGhlci0keyBpdGVtLnRpdGxlIH1gIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgJy8nICsgaXRlbS51cmwgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgaXRlbS50aXRsZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGl0ZW0ubmV3ID8gPHNwYW4gY2xhc3NOYW1lPVwidGlwIHRpcC1uZXdcIj5OZXc8L3NwYW4+IDogXCJcIiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICkgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgIDwvbGk+ICovfVxyXG5cclxuICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiPjxpIGNsYXNzTmFtZT1cImQtaWNvbi1jYXJkXCI+PC9pPlNwZWNpYWwgT2ZmZXJzPC9BTGluaz4gIFxyXG4gICAgICAgICAgICAgICAgPC9saT4gICAgXHJcbiAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIvcGFnZXMvY29udGFjdC11c1wiPkNvbnRhY3QgVXM8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9wYWdlcy9hYm91dC11c1wiPkFib3V0IFVzPC9BTGluaz5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgPC9uYXY+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1haW5NZW51OyIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XHJcblxyXG5pbXBvcnQgQUxpbmsgZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL2N1c3RvbS1saW5rJztcclxuaW1wb3J0IENhcmQgZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL2FjY29yZGlvbi9jYXJkJztcclxuXHJcbmltcG9ydCB7IG1haW5NZW51IH0gZnJvbSAnfi91dGlscy9kYXRhL21lbnUnO1xyXG5cclxuZnVuY3Rpb24gTW9iaWxlTWVudSggcHJvcHMgKSB7XHJcbiAgICBjb25zdCBbIHNlYXJjaCwgc2V0U2VhcmNoIF0gPSB1c2VTdGF0ZSggXCJcIiApO1xyXG4gICAgY29uc3QgWyB0aW1lciwgc2V0VGltZXIgXSA9IHVzZVN0YXRlKCBudWxsICk7XHJcbiAgICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ3Jlc2l6ZScsIGhpZGVNb2JpbGVNZW51SGFuZGxlciApO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiYm9keVwiICkuYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCBvbkJvZHlDbGljayApO1xyXG5cclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ3Jlc2l6ZScsIGhpZGVNb2JpbGVNZW51SGFuZGxlciApO1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcImJvZHlcIiApLnJlbW92ZUV2ZW50TGlzdGVuZXIoIFwiY2xpY2tcIiwgb25Cb2R5Q2xpY2sgKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgc2V0U2VhcmNoKCBcIlwiICk7XHJcbiAgICB9LCBbIHJvdXRlci5xdWVyeS5zbHVnIF0gKVxyXG5cclxuICAgIGNvbnN0IGhpZGVNb2JpbGVNZW51SGFuZGxlciA9ICgpID0+IHtcclxuICAgICAgICBpZiAoIHdpbmRvdy5pbm5lcldpZHRoID4gOTkxICkge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnYm9keScgKS5jbGFzc0xpc3QucmVtb3ZlKCAnbW1lbnUtYWN0aXZlJyApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBoaWRlTW9iaWxlTWVudSA9ICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnYm9keScgKS5jbGFzc0xpc3QucmVtb3ZlKCAnbW1lbnUtYWN0aXZlJyApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIG9uU2VhcmNoQ2hhbmdlKCBlICkge1xyXG4gICAgICAgIHNldFNlYXJjaCggZS50YXJnZXQudmFsdWUgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBvbkJvZHlDbGljayggZSApIHtcclxuICAgICAgICBpZiAoIGUudGFyZ2V0LmNsb3Nlc3QoICcuaGVhZGVyLXNlYXJjaCcgKSApIHJldHVybiBlLnRhcmdldC5jbG9zZXN0KCAnLmhlYWRlci1zZWFyY2gnICkuY2xhc3NMaXN0LmNvbnRhaW5zKCAnc2hvdy1yZXN1bHRzJyApIHx8IGUudGFyZ2V0LmNsb3Nlc3QoICcuaGVhZGVyLXNlYXJjaCcgKS5jbGFzc0xpc3QuYWRkKCAnc2hvdy1yZXN1bHRzJyApO1xyXG5cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdycgKSAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdycgKS5jbGFzc0xpc3QucmVtb3ZlKCAnc2hvdycgKTtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdy1yZXN1bHRzJyApICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93LXJlc3VsdHMnICkuY2xhc3NMaXN0LnJlbW92ZSggJ3Nob3ctcmVzdWx0cycgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBvblN1Ym1pdFNlYXJjaEZvcm0oIGUgKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHJvdXRlci5wdXNoKCB7XHJcbiAgICAgICAgICAgIHBhdGhuYW1lOiAnL3Nob3AnLFxyXG4gICAgICAgICAgICBxdWVyeToge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoOiBzZWFyY2hcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibW9iaWxlLW1lbnUtd3JhcHBlclwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1vYmlsZS1tZW51LW92ZXJsYXlcIiBvbkNsaWNrPXsgaGlkZU1vYmlsZU1lbnUgfT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8QUxpbmsgY2xhc3NOYW1lPVwibW9iaWxlLW1lbnUtY2xvc2VcIiBocmVmPVwiI1wiIG9uQ2xpY2s9eyBoaWRlTW9iaWxlTWVudSB9PjxpIGNsYXNzTmFtZT1cImQtaWNvbi10aW1lc1wiPjwvaT48L0FMaW5rPlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2JpbGUtbWVudS1jb250YWluZXIgc2Nyb2xsYWJsZVwiPlxyXG4gICAgICAgICAgICAgICAgPGZvcm0gYWN0aW9uPVwiI1wiIGNsYXNzTmFtZT1cImlucHV0LXdyYXBwZXJcIiBvblN1Ym1pdD17IG9uU3VibWl0U2VhcmNoRm9ybSB9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJzZWFyY2hcIiBhdXRvQ29tcGxldGU9XCJvZmZcIiB2YWx1ZT17IHNlYXJjaCB9IG9uQ2hhbmdlPXsgb25TZWFyY2hDaGFuZ2UgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlNlYXJjaCB5b3VyIGtleXdvcmQuLi5cIiByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYnRuIGJ0bi1zZWFyY2hcIiB0eXBlPVwic3VibWl0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImQtaWNvbi1zZWFyY2hcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcblxyXG4gICAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cIm1vYmlsZS1tZW51IG1tZW51LWFuaW1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiL1wiPkhvbWU8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPENhcmQgdGl0bGU9XCJjYXRlZ29yaWVzXCIgdHlwZT1cIm1vYmlsZVwiIHVybD1cIi9zaG9wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2FyZCB0aXRsZT1cIlZhcmlhdGlvbnMgMVwiIHR5cGU9XCJtb2JpbGVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1haW5NZW51LnNob3AudmFyaWF0aW9uMS5tYXAoICggaXRlbSwgaW5kZXggKSA9PiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkga2V5PXsgYHNob3AtJHsgaXRlbS50aXRsZSB9YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgJy8nICsgaXRlbS51cmwgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpdGVtLnRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpdGVtLmhvdCA/IDxzcGFuIGNsYXNzTmFtZT1cInRpcCB0aXAtaG90XCI+SG90PC9zcGFuPiA6IFwiXCIgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG5cclxuICAgICAgICAgICAgICAgICBcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Q2FyZCB0aXRsZT1cIlBhZ2VzXCIgdHlwZT1cIm1vYmlsZVwiIHVybD1cIi9wYWdlcy9hYm91dC11c1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFpbk1lbnUub3RoZXIubWFwKCAoIGl0ZW0sIGluZGV4ICkgPT4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGtleT17IGBvdGhlci0keyBpdGVtLnRpdGxlIH1gIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyAnLycgKyBpdGVtLnVybCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGl0ZW0udGl0bGUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGl0ZW0ubmV3ID8gPHNwYW4gY2xhc3NOYW1lPVwidGlwIHRpcC1uZXdcIj5OZXc8L3NwYW4+IDogXCJcIiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxDYXJkIHRpdGxlPVwiQmxvZ1wiIHR5cGU9XCJtb2JpbGVcIiB1cmw9XCIvYmxvZy9jbGFzc2ljXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYWluTWVudS5ibG9nLm1hcCggKCBpdGVtLCBpbmRleCApID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0uc3ViUGFnZXMgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBrZXk9eyBcImJsb2dcIiArIGl0ZW0udGl0bGUgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPENhcmQgdGl0bGU9eyBpdGVtLnRpdGxlIH0gdXJsPXsgJy8nICsgaXRlbS51cmwgfSB0eXBlPVwibW9iaWxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLnN1YlBhZ2VzLm1hcCggKCBpdGVtLCBpbmRleCApID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBrZXk9eyBgYmxvZy0keyBpdGVtLnRpdGxlIH1gIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyAnLycgKyBpdGVtLnVybCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGl0ZW0udGl0bGUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT4gOlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkga2V5PXsgXCJibG9nXCIgKyBpdGVtLnRpdGxlIH0gY2xhc3NOYW1lPXsgaXRlbS5zdWJQYWdlcyA/IFwic3VibWVudVwiIDogXCJcIiB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj17ICcvJyArIGl0ZW0udXJsIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGl0ZW0udGl0bGUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxDYXJkIHRpdGxlPVwiZWxlbWVudHNcIiB0eXBlPVwibW9iaWxlXCIgdXJsPVwiL2VsZW1lbnRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYWluTWVudS5lbGVtZW50Lm1hcCggKCBpdGVtLCBpbmRleCApID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBrZXk9eyBgZWxlbWVudHMtJHsgaXRlbS50aXRsZSB9YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgJy8nICsgaXRlbS51cmwgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpdGVtLnRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT48QUxpbmsgaHJlZj17ICcvcGFnZXMvYWNjb3VudCcgfT5Mb2dpbjwvQUxpbms+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+PEFMaW5rIGhyZWY9eyAnL3BhZ2VzL2NhcnQnIH0+TXkgQ2FydDwvQUxpbms+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+PEFMaW5rIGhyZWY9eyAnL3BhZ2VzL3dpc2hsaXN0JyB9Pldpc2hsaXN0PC9BTGluaz48L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFJlYWN0Lm1lbW8oIE1vYmlsZU1lbnUgKTsiLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInO1xyXG5pbXBvcnQgeyB1c2VMYXp5UXVlcnkgfSBmcm9tICdAYXBvbGxvL3JlYWN0LWhvb2tzJztcclxuaW1wb3J0IHsgTGF6eUxvYWRJbWFnZSB9IGZyb20gJ3JlYWN0LWxhenktbG9hZC1pbWFnZS1jb21wb25lbnQnO1xyXG5cclxuaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcblxyXG5pbXBvcnQgeyBHRVRfUFJPRFVDVFMgfSBmcm9tICd+L3NlcnZlci9xdWVyaWVzJztcclxuaW1wb3J0IHdpdGhBcG9sbG8gZnJvbSAnfi9zZXJ2ZXIvYXBvbGxvJztcclxuXHJcbmltcG9ydCB7IHRvRGVjaW1hbCB9IGZyb20gJ34vdXRpbHMnO1xyXG5cclxuZnVuY3Rpb24gU2VhcmNoRm9ybSgpIHtcclxuICAgIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gICAgY29uc3QgWyBzZWFyY2gsIHNldFNlYXJjaCBdID0gdXNlU3RhdGUoIFwiXCIgKTtcclxuICAgIGNvbnN0IFsgc2VhcmNoUHJvZHVjdHMsIHsgZGF0YSB9IF0gPSB1c2VMYXp5UXVlcnkoIEdFVF9QUk9EVUNUUyApO1xyXG4gICAgY29uc3QgWyB0aW1lciwgc2V0VGltZXIgXSA9IHVzZVN0YXRlKCBudWxsICk7XHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCJib2R5XCIgKS5hZGRFdmVudExpc3RlbmVyKCBcImNsaWNrXCIsIG9uQm9keUNsaWNrICk7XHJcblxyXG4gICAgICAgIHJldHVybiAoICgpID0+IHtcclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCJib2R5XCIgKS5yZW1vdmVFdmVudExpc3RlbmVyKCBcImNsaWNrXCIsIG9uQm9keUNsaWNrICk7XHJcbiAgICAgICAgfSApXHJcbiAgICB9LCBbXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgc2V0U2VhcmNoKCBcIlwiICk7XHJcbiAgICB9LCBbIHJvdXRlci5xdWVyeS5zbHVnIF0gKVxyXG5cclxuICAgIHVzZUVmZmVjdCggKCkgPT4ge1xyXG4gICAgICAgIGlmICggc2VhcmNoLmxlbmd0aCA+IDIgKSB7XHJcbiAgICAgICAgICAgIGlmICggdGltZXIgKSBjbGVhclRpbWVvdXQoIHRpbWVyICk7XHJcbiAgICAgICAgICAgIGxldCB0aW1lcklkID0gc2V0VGltZW91dCggKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoUHJvZHVjdHMoIHsgdmFyaWFibGVzOiB7IHNlYXJjaDogc2VhcmNoIH0gfSApO1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZXIoIG51bGwgKTs7XHJcbiAgICAgICAgICAgIH0sIDUwMCApO1xyXG5cclxuICAgICAgICAgICAgc2V0VGltZXIoIHRpbWVySWQgKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbIHNlYXJjaCBdIClcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdy1yZXN1bHRzJyApICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93LXJlc3VsdHMnICkuY2xhc3NMaXN0LnJlbW92ZSggJ3Nob3ctcmVzdWx0cycgKTtcclxuICAgIH0sIFsgcm91dGVyLnBhdGhuYW1lIF0gKVxyXG5cclxuICAgIGZ1bmN0aW9uIHJlbW92ZVhTU0F0dGFja3MoIGh0bWwgKSB7XHJcbiAgICAgICAgY29uc3QgU0NSSVBUX1JFR0VYID0gLzxzY3JpcHRcXGJbXjxdKig/Oig/ITxcXC9zY3JpcHQ+KTxbXjxdKikqPFxcL3NjcmlwdD4vZ2k7XHJcblxyXG4gICAgICAgIC8vIFJlbW92aW5nIHRoZSA8c2NyaXB0PiB0YWdzXHJcbiAgICAgICAgd2hpbGUgKCBTQ1JJUFRfUkVHRVgudGVzdCggaHRtbCApICkge1xyXG4gICAgICAgICAgICBodG1sID0gaHRtbC5yZXBsYWNlKCBTQ1JJUFRfUkVHRVgsIFwiXCIgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFJlbW92aW5nIGFsbCBldmVudHMgZnJvbSB0YWdzLi4uXHJcbiAgICAgICAgaHRtbCA9IGh0bWwucmVwbGFjZSggLyBvblxcdys9XCJbXlwiXSpcIi9nLCBcIlwiICk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIF9faHRtbDogaHRtbFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBtYXRjaEVtcGhhc2l6ZSggbmFtZSApIHtcclxuICAgICAgICBsZXQgcmVnRXhwID0gbmV3IFJlZ0V4cCggc2VhcmNoLCBcImlcIiApO1xyXG4gICAgICAgIHJldHVybiBuYW1lLnJlcGxhY2UoXHJcbiAgICAgICAgICAgIHJlZ0V4cCxcclxuICAgICAgICAgICAgKCBtYXRjaCApID0+IFwiPHN0cm9uZz5cIiArIG1hdGNoICsgXCI8L3N0cm9uZz5cIlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gb25TZWFyY2hDbGljayggZSApIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBlLmN1cnJlbnRUYXJnZXQucGFyZW50Tm9kZS5jbGFzc0xpc3QudG9nZ2xlKCAnc2hvdycgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBvbkJvZHlDbGljayggZSApIHtcclxuICAgICAgICBpZiAoIGUudGFyZ2V0LmNsb3Nlc3QoICcuaGVhZGVyLXNlYXJjaCcgKSApIHJldHVybiBlLnRhcmdldC5jbG9zZXN0KCAnLmhlYWRlci1zZWFyY2gnICkuY2xhc3NMaXN0LmNvbnRhaW5zKCAnc2hvdy1yZXN1bHRzJyApIHx8IGUudGFyZ2V0LmNsb3Nlc3QoICcuaGVhZGVyLXNlYXJjaCcgKS5jbGFzc0xpc3QuYWRkKCAnc2hvdy1yZXN1bHRzJyApO1xyXG5cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdycgKSAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdycgKS5jbGFzc0xpc3QucmVtb3ZlKCAnc2hvdycgKTtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLmhlYWRlci1zZWFyY2guc2hvdy1yZXN1bHRzJyApICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuaGVhZGVyLXNlYXJjaC5zaG93LXJlc3VsdHMnICkuY2xhc3NMaXN0LnJlbW92ZSggJ3Nob3ctcmVzdWx0cycgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBvblNlYXJjaENoYW5nZSggZSApIHtcclxuICAgICAgICBzZXRTZWFyY2goIGUudGFyZ2V0LnZhbHVlICk7XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gb25TdWJtaXRTZWFyY2hGb3JtKCBlICkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICByb3V0ZXIucHVzaCgge1xyXG4gICAgICAgICAgICBwYXRobmFtZTogJy9zaG9wJyxcclxuICAgICAgICAgICAgcXVlcnk6IHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaDogc2VhcmNoXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9ICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlci1zZWFyY2ggaHMtc2ltcGxlXCI+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwic2VhcmNoLXRvZ2dsZVwiIHJvbGU9XCJidXR0b25cIiBvbkNsaWNrPXsgb25TZWFyY2hDbGljayB9PjxpIGNsYXNzTmFtZT1cImljb24tc2VhcmNoLTNcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICA8Zm9ybSBhY3Rpb249XCIjXCIgbWV0aG9kPVwiZ2V0XCIgb25TdWJtaXQ9eyBvblN1Ym1pdFNlYXJjaEZvcm0gfSBjbGFzc05hbWU9XCJpbnB1dC13cmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwic2VhcmNoXCIgYXV0b0NvbXBsZXRlPVwib2ZmXCIgdmFsdWU9eyBzZWFyY2ggfSBvbkNoYW5nZT17IG9uU2VhcmNoQ2hhbmdlIH1cclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlNlYXJjaC4uLlwiIHJlcXVpcmVkIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidG4gYnRuLXNlYXJjaFwiIHR5cGU9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJkLWljb24tc2VhcmNoXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPC9idXR0b24+XHJcblxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJsaXZlLXNlYXJjaC1saXN0IGJnLXdoaXRlIHNjcm9sbGFibGVcIj5cclxuICAgICAgICAgICAgICAgICAgICB7IHNlYXJjaC5sZW5ndGggPiAyICYmIGRhdGEgJiYgZGF0YS5wcm9kdWN0cy5kYXRhLm1hcCggKCBwcm9kdWN0LCBpbmRleCApID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9eyBgL3Byb2R1Y3QvZGVmYXVsdC8keyBwcm9kdWN0LnNsdWcgfWAgfSBjbGFzc05hbWU9XCJhdXRvY29tcGxldGUtc3VnZ2VzdGlvblwiIGtleT17IGBzZWFyY2gtcmVzdWx0LSR7IGluZGV4IH1gIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGF6eUxvYWRJbWFnZSBlZmZlY3Q9XCJvcGFjaXR5XCIgc3JjPXsgcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQVNTRVRfVVJJICsgcHJvZHVjdC5waWN0dXJlc1sgMCBdLnVybCB9IHdpZHRoPXsgNDAgfSBoZWlnaHQ9eyA0MCB9IGFsdD1cInByb2R1Y3RcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzZWFyY2gtbmFtZVwiIGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXsgcmVtb3ZlWFNTQXR0YWNrcyggbWF0Y2hFbXBoYXNpemUoIHByb2R1Y3QubmFtZSApICkgfT48L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInNlYXJjaC1wcmljZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC5wcmljZVsgMCBdICE9PSBwcm9kdWN0LnByaWNlWyAxIF0gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC52YXJpYW50cy5sZW5ndGggPT09IDAgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm5ldy1wcmljZSBtci0xXCI+JHsgdG9EZWNpbWFsKCBwcm9kdWN0LnByaWNlWyAwIF0gKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJvbGQtcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QucHJpY2VbIDEgXSApIH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwgc3BhbiBjbGFzc05hbWU9XCJuZXctcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QucHJpY2VbIDAgXSApIH0g4oCTICR7IHRvRGVjaW1hbCggcHJvZHVjdC5wcmljZVsgMSBdICkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogPHNwYW4gY2xhc3NOYW1lPVwibmV3LXByaWNlXCI+JHsgdG9EZWNpbWFsKCBwcm9kdWN0LnByaWNlWyAwIF0gKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICApIClcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgd2l0aEFwb2xsbyggeyBzc3I6IHR5cGVvZiB3aW5kb3cgPT09ICd1bmRlZmluZWQnIH0gKSggU2VhcmNoRm9ybSApOyIsImltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuXHJcbmltcG9ydCBBTGluayBmcm9tICd+L2NvbXBvbmVudHMvZmVhdHVyZXMvY3VzdG9tLWxpbmsnO1xyXG5cclxuaW1wb3J0IEZvb3RlclNlYXJjaEJveCBmcm9tICd+L2NvbXBvbmVudHMvY29tbW9uL3BhcnRpYWxzL2Zvb3Rlci1zZWFyY2gtYm94JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFN0aWNreUZvb3RlcigpIHtcclxuICAgIGxldCB0bXAgPSAwO1xyXG5cclxuICAgIHVzZUVmZmVjdCggKCkgPT4ge1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnc2Nyb2xsJywgc3RpY2t5Rm9vdGVySGFuZGxlciApO1xyXG5cclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ3Njcm9sbCcsIHN0aWNreUZvb3RlckhhbmRsZXIgKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbXSApXHJcblxyXG4gICAgY29uc3Qgc3RpY2t5Rm9vdGVySGFuZGxlciA9ICggZSApID0+IHtcclxuICAgICAgICBsZXQgdG9wID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5wYWdlLWNvbnRlbnQnICkgPyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnBhZ2UtY29udGVudCcgKS5vZmZzZXRUb3AgKyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnaGVhZGVyJyApLm9mZnNldEhlaWdodCArIDEwMCA6IDYwMDtcclxuICAgICAgICBsZXQgc3RpY2t5Rm9vdGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktZm9vdGVyLnN0aWNreS1jb250ZW50JyApO1xyXG4gICAgICAgIGxldCBoZWlnaHQgPSAwO1xyXG5cclxuICAgICAgICBpZiAoIHN0aWNreUZvb3RlciApIHtcclxuICAgICAgICAgICAgaGVpZ2h0ID0gc3RpY2t5Rm9vdGVyLm9mZnNldEhlaWdodDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggd2luZG93LnBhZ2VZT2Zmc2V0ID49IHRvcCAmJiB3aW5kb3cuaW5uZXJXaWR0aCA8IDc2OCAmJiBlLmN1cnJlbnRUYXJnZXQuc2Nyb2xsWSA+PSB0bXAgKSB7XHJcbiAgICAgICAgICAgIGlmICggc3RpY2t5Rm9vdGVyICkge1xyXG4gICAgICAgICAgICAgICAgc3RpY2t5Rm9vdGVyLmNsYXNzTGlzdC5hZGQoICdmaXhlZCcgKTtcclxuICAgICAgICAgICAgICAgIHN0aWNreUZvb3Rlci5zZXRBdHRyaWJ1dGUoICdzdHlsZScsIFwibWFyZ2luLWJvdHRvbTogMFwiIClcclxuICAgICAgICAgICAgICAgIGlmICggIWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKSApIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbmV3Tm9kZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoIFwiZGl2XCIgKTtcclxuICAgICAgICAgICAgICAgICAgICBuZXdOb2RlLmNsYXNzTmFtZSA9IFwic3RpY2t5LWNvbnRlbnQtd3JhcHBlclwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHN0aWNreUZvb3Rlci5wYXJlbnROb2RlLmluc2VydEJlZm9yZSggbmV3Tm9kZSwgc3RpY2t5Rm9vdGVyICk7XHJcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLmluc2VydEFkamFjZW50RWxlbWVudCggJ2JlZm9yZWVuZCcsIHN0aWNreUZvb3RlciApO1xyXG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKS5zZXRBdHRyaWJ1dGUoIFwic3R5bGVcIiwgXCJoZWlnaHQ6IFwiICsgaGVpZ2h0ICsgXCJweFwiICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCAhZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLmdldEF0dHJpYnV0ZSggXCJzdHlsZVwiICkgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLnNldEF0dHJpYnV0ZSggXCJzdHlsZVwiLCBcImhlaWdodDogXCIgKyBoZWlnaHQgKyBcInB4XCIgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICggc3RpY2t5Rm9vdGVyICkge1xyXG4gICAgICAgICAgICAgICAgc3RpY2t5Rm9vdGVyLmNsYXNzTGlzdC5yZW1vdmUoICdmaXhlZCcgKTtcclxuICAgICAgICAgICAgICAgIHN0aWNreUZvb3Rlci5zZXRBdHRyaWJ1dGUoICdzdHlsZScsIGBtYXJnaW4tYm90dG9tOiAtJHsgaGVpZ2h0IH1weGAgKVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKSApIHtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKS5yZW1vdmVBdHRyaWJ1dGUoIFwic3R5bGVcIiApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIHdpbmRvdy5pbm5lcldpZHRoID4gNzY3ICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKSApIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLnN0eWxlLmhlaWdodCA9ICdhdXRvJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRtcCA9IGUuY3VycmVudFRhcmdldC5zY3JvbGxZO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzdGlja3ktZm9vdGVyIHN0aWNreS1jb250ZW50IGZpeC1ib3R0b21cIj5cclxuICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIvXCIgY2xhc3NOYW1lPVwic3RpY2t5LWxpbmsgYWN0aXZlXCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJkLWljb24taG9tZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxzcGFuPkhvbWU8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgIDxBTGluayBocmVmPVwiL3Nob3BcIiBjbGFzc05hbWU9XCJzdGlja3ktbGlua1wiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZC1pY29uLXZvbHVtZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxzcGFuPkNhdGVnb3JpZXM8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgIDxBTGluayBocmVmPVwiL3BhZ2VzL3dpc2hsaXN0XCIgY2xhc3NOYW1lPVwic3RpY2t5LWxpbmtcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImQtaWNvbi1oZWFydFwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxzcGFuPldpc2hsaXN0PC9zcGFuPlxyXG4gICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICA8QUxpbmsgaHJlZj1cIi9wYWdlcy9hY2NvdW50XCIgY2xhc3NOYW1lPVwic3RpY2t5LWxpbmtcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImQtaWNvbi11c2VyXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4+QWNjb3VudDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9BTGluaz5cclxuXHJcbiAgICAgICAgICAgIDxGb290ZXJTZWFyY2hCb3ggLz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIClcclxufSIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcblxyXG5cclxuaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcblxyXG5pbXBvcnQgU2xpZGVUb2dnbGUgZnJvbSAncmVhY3Qtc2xpZGUtdG9nZ2xlJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIENhcmQoIHByb3BzICkge1xyXG4gICAgY29uc3QgeyB0aXRsZSwgZXhwYW5kZWQgPSBmYWxzZSwgYWRDbGFzcywgaWNvbkNsYXNzLCB0eXBlID0gXCJub3JtYWxcIiwgdXJsIH0gPSBwcm9wcztcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIFwibm9ybWFsXCIgPT09IHR5cGUgP1xyXG4gICAgICAgICAgICA8IFNsaWRlVG9nZ2xlIGNvbGxhcHNlZD17IGV4cGFuZGVkID8gZmFsc2UgOiB0cnVlIH0gPlxyXG4gICAgICAgICAgICAgICAgeyAoIHsgb25Ub2dnbGUsIHNldENvbGxhcHNpYmxlRWxlbWVudCwgdG9nZ2xlU3RhdGUgfSApID0+IChcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17IGBjYXJkICR7IGFkQ2xhc3MgfWAgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9eyBgY2FyZC1oZWFkZXJgIH0gb25DbGljaz17IG9uVG9nZ2xlIH0gPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPXsgYHRvZ2dsZS1idXR0b24gJHsgdG9nZ2xlU3RhdGUudG9Mb3dlckNhc2UoKSB9YCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbkNsYXNzID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT17IGljb25DbGFzcyB9PjwvaT4gOiBcIlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgdGl0bGUgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZSA6IFwiXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgcmVmPXsgc2V0Q29sbGFwc2libGVFbGVtZW50IH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtYm9keSBvdmVyZmxvdy1oaWRkZW5cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHByb3BzLmNoaWxkcmVuIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgPC9TbGlkZVRvZ2dsZSA+IDpcclxuICAgICAgICAgICAgXCJwYXJzZVwiID09PSB0eXBlID9cclxuICAgICAgICAgICAgICAgIDxTbGlkZVRvZ2dsZSBjb2xsYXBzZWQ9eyBleHBhbmRlZCA/IGZhbHNlIDogdHJ1ZSB9ID5cclxuICAgICAgICAgICAgICAgICAgICB7ICggeyBvblRvZ2dsZSwgc2V0Q29sbGFwc2libGVFbGVtZW50LCB0b2dnbGVTdGF0ZSB9ICkgPT4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEFMaW5rXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZj17IHVybCA/IHVybCA6ICcjJyB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudD17IHRpdGxlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9eyBgcGFyc2UtY29udGVudCAkeyB0b2dnbGVTdGF0ZS50b0xvd2VyQ2FzZSgpIH1gIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsgKCBlICkgPT4geyBvblRvZ2dsZSgpOyB9IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiByZWY9eyBzZXRDb2xsYXBzaWJsZUVsZW1lbnQgfSBjbGFzc05hbWU9XCJvdmVyZmxvdy1oaWRkZW5cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IHByb3BzLmNoaWxkcmVuIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICApIH1cclxuICAgICAgICAgICAgICAgIDwvU2xpZGVUb2dnbGUgPiA6XHJcbiAgICAgICAgICAgICAgICA8U2xpZGVUb2dnbGUgY29sbGFwc2VkPXsgZXhwYW5kZWQgPyBmYWxzZSA6IHRydWUgfSA+XHJcbiAgICAgICAgICAgICAgICAgICAgeyAoIHsgb25Ub2dnbGUsIHNldENvbGxhcHNpYmxlRWxlbWVudCwgdG9nZ2xlU3RhdGUgfSApID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgdXJsID8gdXJsIDogXCIjXCIgfSA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9eyBgdG9nZ2xlLWJ0biAkeyB0b2dnbGVTdGF0ZS50b0xvd2VyQ2FzZSgpIH1gIH0gb25DbGljaz17ICggZSApID0+IHsgb25Ub2dnbGUoKTsgZS5wcmV2ZW50RGVmYXVsdCgpOyB9IH0+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9BTGluaz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHJlZj17IHNldENvbGxhcHNpYmxlRWxlbWVudCB9IGNsYXNzTmFtZT1cIm92ZXJmbG93LWhpZGRlblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgcHJvcHMuY2hpbGRyZW4gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICkgfVxyXG4gICAgICAgICAgICAgICAgPC9TbGlkZVRvZ2dsZT5cclxuICAgIClcclxuICAgIHJldHVybiAnJztcclxufSIsImltcG9ydCBDb3VudGRvd24sIHsgemVyb1BhZCB9IGZyb20gJ3JlYWN0LWNvdW50ZG93bic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQcm9kdWN0Q291bnREb3duICggcHJvcHMgKSB7XHJcbiAgICBjb25zdCB7IGRhdGUgPSBcIjIwMjEtMDgtMjBcIiwgdHlwZSA9IDEsIGFkQ2xhc3MgPSAnJyB9ID0gcHJvcHM7XHJcblxyXG4gICAgY29uc3QgcmVuZGVyZXIgPSAoIHsgZGF5cywgaG91cnMsIG1pbnV0ZXMsIHNlY29uZHMsIGNvbXBsZXRlZCB9ICkgPT4ge1xyXG4gICAgICAgIGlmICggY29tcGxldGVkICkge1xyXG4gICAgICAgICAgICByZXR1cm4gPHNwYW4+UHJvZHVjdCBTZWxsaW5nIEZpbmlzaGVkITwvc3Bhbj5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgdHlwZSA9PT0gMSA/XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9eyBgY291bnRkb3duICR7IGFkQ2xhc3MgfWAgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb3VudGRvd24tcm93IGNvdW50ZG93bi1zaG93NFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLXNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tYW1vdW50XCI+eyB6ZXJvUGFkKCBkYXlzICkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tcGVyaW9kXCI+REFZUzwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tc2VjdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNvdW50ZG93bi1hbW91bnRcIj57IHplcm9QYWQoIGhvdXJzICkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tcGVyaW9kXCI+SE9VUlM8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLXNlY3Rpb25cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tYW1vdW50XCI+eyB6ZXJvUGFkKCBtaW51dGVzICkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tcGVyaW9kXCI+TUlOVVRFUzwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNvdW50ZG93bi1zZWN0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLWFtb3VudFwiPnsgemVyb1BhZCggc2Vjb25kcyApIH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLXBlcmlvZFwiPlNFQ09ORFM8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtY291bnRkb3duLWNvbnRhaW5lciBmb250LXdlaWdodC1zZW1pLWJvbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJvZHVjdC1jb3VudGRvd24tdGl0bGVcIj5PZmZlciBFbmRzIEluOiZuYnNwOzwvc3Bhbj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1jb3VudGRvd24gY291bnRkb3duLWNvbXBhY3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNvdW50ZG93bi1zZWN0aW9uIGRheXNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJjb3VudGRvd24tYW1vdW50XCI+eyB6ZXJvUGFkKCBkYXlzICkgfSA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLXBlcmlvZFwiPmRheXMsJm5ic3A7PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNvdW50ZG93bi1zZWN0aW9uIGhvdXJzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLWFtb3VudFwiPnsgemVyb1BhZCggaG91cnMgKSB9PHNwYW4gY2xhc3NOYW1lPVwibXItMSBtbC0xXCI+Ojwvc3Bhbj48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLXNlY3Rpb24gbWludXRlc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNvdW50ZG93bi1hbW91bnRcIj57IHplcm9QYWQoIG1pbnV0ZXMgKSB9PHNwYW4gY2xhc3NOYW1lPVwibXItMSBtbC0xXCI+Ojwvc3Bhbj48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiY291bnRkb3duLXNlY3Rpb24gc2Vjb25kc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNvdW50ZG93bi1hbW91bnRcIj57IHplcm9QYWQoIHNlY29uZHMgKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPENvdW50ZG93blxyXG4gICAgICAgICAgICBkYXRlPXsgbmV3IERhdGUoIGRhdGUgKSB9XHJcbiAgICAgICAgICAgIHJlbmRlcmVyPXsgcmVuZGVyZXIgfSA+XHJcbiAgICAgICAgPC9Db3VudGRvd24+XHJcbiAgICApO1xyXG59IiwiaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBheGlvcyBmcm9tIFwiYXhpb3NcIjtcclxuLy8gaW1wb3J0IHsgdXNlSGlzdG9yeSB9IGZyb20gJ3JlYWN0LXJvdXRlcidcclxuaW1wb3J0IHsgVGFicywgVGFiTGlzdCwgVGFiLCBUYWJQYW5lbCB9IGZyb20gJ3JlYWN0LXRhYnMnO1xyXG5pbXBvcnQgTW9kYWwgZnJvbSAncmVhY3QtbW9kYWwnO1xyXG5cclxuaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcbmltcG9ydCB7IEFERF9BRERSRVNTX0FQSSwgU0VORE9UUF9BUEksIFNJR05VUF9BUEksIFZFUklGWV9BUEkgfSBmcm9tICd+L2NvbXBvbmVudHMvY29tbW9uL0FwaSc7XHJcblxyXG5jb25zdCBjdXN0b21TdHlsZXMgPSB7XHJcbiAgICBvdmVybGF5OiB7XHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSgwLDAsMCwwLjQpJyxcclxuICAgICAgICBkaXNwbGF5OiBcImZsZXhcIlxyXG4gICAgfVxyXG59O1xyXG5cclxubGV0IGluZGV4ID0gMDtcclxuXHJcbk1vZGFsLnNldEFwcEVsZW1lbnQoIFwiI19fbmV4dFwiICk7XHJcblxyXG5mdW5jdGlvbiBMb2dpbk1vZGFsKCkge1xyXG4gICAgY29uc3QgWyBvcGVuLCBzZXRPcGVuIF0gPSB1c2VTdGF0ZSggZmFsc2UgKTtcclxuICAgIGNvbnN0IFtwaG9uZU51bWJlciwgc2V0UGhvbmVdID0gdXNlU3RhdGUoJycpO1xyXG4gICAgY29uc3QgW3N0ZXAsIHNldFN0ZXBdID0gdXNlU3RhdGUoMSk7XHJcbiAgICBjb25zdCBbT1RQLCBzZXRPdHBdID0gdXNlU3RhdGUoJycpO1xyXG4gICAgY29uc3QgW3ZlcnRpZnlPdHAgLCBzZXR2ZXJ0aWZ5RGF0YV0gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgICBcclxuICAgIC8vIGNvbnN0IGhpc3RvcnkgPSB1c2VIaXN0b3J5KCk7XHJcbiAgICAvLyBHZXR0aW5nIGZvcm0gdmFsdWUgaGVyZVxyXG4gIGNvbnN0IFtmb3JtLCBzZXRGb3JtXSA9IHVzZVN0YXRlKHtcclxuXHJcbiAgICB1c2VyTmFtZTogXCJcIixcclxuICAgIGZpcnN0TmFtZTogXCJcIixcclxuICAgIGxhc3ROYW1lOiBcIlwiLFxyXG4gICAgZW1haWw6IFwiXCIsXHJcbiAgICBwYXNzd29yZDogXCJcIixcclxuICAgIGFnZTogXCJcIlxyXG4gIH0pO1xyXG5cclxuICBjb25zdCBbZm9ybUFkZHJlc3MsIHNldEFkZHJlc3NGb3JtXSA9IHVzZVN0YXRlKHtcclxuXHJcbiAgICBhZGRyZXNzSWQ6IFwiXCIsXHJcbiAgICBuYW1lOiBcIlwiLFxyXG4gICAgbG9jYWxpdHk6IFwiXCIsXHJcbiAgICBhcmVhOiBcIlwiLFxyXG4gICAgc3RyZWV0OiBcIlwiLFxyXG4gICAgcGluY29kZUlkOiBcIlwiLFxyXG4gICAgY2l0eUlkOiBcIlwiLFxyXG4gICAgc3RhdGVJZDogXCJcIixcclxuICAgIGFkZHJlc3NUeXBlOiBcIlwiXHJcbiAgfSk7XHJcblxyXG4gXHJcblxyXG5cclxuICAgIGZ1bmN0aW9uIGNsb3NlTW9kYWwoKSB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIuUmVhY3RNb2RhbF9fT3ZlcmxheVwiICkuY2xhc3NMaXN0LmFkZCggJ3JlbW92ZWQnICk7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIubG9naW4tcG9wdXAuUmVhY3RNb2RhbF9fQ29udGVudFwiICkuY2xhc3NMaXN0LnJlbW92ZSggXCJSZWFjdE1vZGFsX19Db250ZW50LS1hZnRlci1vcGVuXCIgKTtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcIi5sb2dpbi1wb3B1cC1vdmVybGF5LlJlYWN0TW9kYWxfX092ZXJsYXlcIiApLmNsYXNzTGlzdC5yZW1vdmUoIFwiUmVhY3RNb2RhbF9fT3ZlcmxheS0tYWZ0ZXItb3BlblwiICk7XHJcbiAgICAgICAgc2V0VGltZW91dCggKCkgPT4ge1xyXG4gICAgICAgICAgICBzZXRPcGVuKCBmYWxzZSApO1xyXG4gICAgICAgIH0sIDMzMCApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIG9wZW5Nb2RhbCggZSwgbG9naW5JbmRleCA9IDAgKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGluZGV4ID0gbG9naW5JbmRleDtcclxuICAgICAgICBzZXRPcGVuKCB0cnVlICk7XHJcbiAgICB9XHJcblxyXG4gICAgICBjb25zdCBjb25maWcgPSB7ICBcclxuICAgICAgaGVhZGVyczogeyBhY2Nlc3NUb2tlbjogJ2EnIH1cclxuICAgIH07XHJcblxyXG4gXHJcblxyXG5cclxuICAvLyBPbmx5IG51bWJlcnMgYWxsb3dlZFxyXG4gIGNvbnN0IGhhbmRsZVBob25lQ2hhbmdlID0gKGUpID0+IHtcclxuICAgIGNvbnN0IHZhbHVlID0gZS50YXJnZXQudmFsdWUucmVwbGFjZSgvXFxEL2csIFwiXCIpO1xyXG4gICAgc2V0UGhvbmUodmFsdWUpO1xyXG4gIH07XHJcblxyXG4gLy8gU2VuZCBvdHAgXHJcbiAgY29uc3Qgc2VuZE90cCA9IChlKSA9PntcclxuXHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBjb25zdCBib2R5UGFyYW1ldGVycyA9IHtcclxuICAgICAgICBwaG9uZU51bWJlcjogcGhvbmVOdW1iZXIsXHJcbiAgICAgICAgZGV2aWNlSWQ6IFwiMzE0MTQ1MTY1MTg1XCJcclxuICAgICAgfTtcclxuXHJcbiAgICAgIGF4aW9zLnBvc3QoU0VORE9UUF9BUEksIGJvZHlQYXJhbWV0ZXJzLGNvbmZpZylcclxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgIHNldFN0ZXAoc3RlcCArIDEpXHJcblxyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG5cclxuICAgICAgICB9KTtcclxuICB9XHJcblxyXG4gLy8gVmVyaWZ5IE9UcFxyXG4gY29uc3QgdmVyaWZ5T3RwID0gKGUpID0+e1xyXG5cclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGNvbnN0IGJvZHlQYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgIHBob25lTnVtYmVyOiBwaG9uZU51bWJlcixcclxuICAgICAgICBvdHA6IHBhcnNlSW50KE9UUCksXHJcbiAgICAgICAgZGV2aWNlSWQ6IFwiMzE0MTQ1MTY1MTg1XCJcclxuICAgICAgfTtcclxuXHJcblxyXG4gICAgICBheGlvcy5wb3N0KFZFUklGWV9BUEksIGJvZHlQYXJhbWV0ZXJzLGNvbmZpZylcclxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICBzZXR2ZXJ0aWZ5RGF0YShyZXNwb25zZS5kYXRhKTtcclxuICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnc2Vzc2lvbl9pZCcsIHJlc3BvbnNlLmRhdGEudG9rZW4pO1xyXG4gICAgICAgICAgIHNldFN0ZXAoc3RlcCArIDEpXHJcblxyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG5cclxuICAgICAgICBcclxuICAgICAgICB9KTtcclxuICB9XHJcblxyXG4gXHJcblxyXG4vLyAgIEdldCBhbGwgZm9ybSBmaWVsZHMgYXQgb25jZVxyXG5cclxuY29uc3QgaGFuZGxlQ2hhbmdlID0gZSA9PiB7XHJcbiAgICBzZXRGb3JtKHtcclxuICAgICAgLi4uZm9ybSxcclxuICAgICAgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSxcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuXHJcbiBjb25zdCBoYW5kbGVBZGRyZXNzID0gZSA9PntcclxuICAgICBzZXRBZGRyZXNzRm9ybSh7XHJcbiAgICAgICAgIC4uLmZvcm1BZGRyZXNzLFxyXG4gICAgICAgICBbZS50YXJnZXQubmFtZV06IGUudGFyZ2V0LnZhbHVlLFxyXG4gICAgIH0pXHJcbiB9XHJcbiAvLyBDcmVhdGUgb3IgcmVnaXN0ZXIgYWNjb3VudCBcclxuIGNvbnN0IGNyZWF0ZUFjY291bnQgPSAoZSkgPT57XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgY29uc3QgY29uZmlnID0geyAgXHJcbiAgICAgICAgaGVhZGVyczogeyBBdXRob3JpemF0aW9uOiBgJHt2ZXJ0aWZ5T3RwLnRva2VufWAgfVxyXG4gICAgICB9O1xyXG4gIFxyXG4gICAgY29uc3QgYm9keVBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgdXNlck5hbWU6IGZvcm0udXNlck5hbWUsXHJcbiAgICAgICAgZmlyc3ROYW1lOiBmb3JtLmZpcnN0TmFtZSxcclxuICAgICAgICBsYXN0TmFtZTogZm9ybS5sYXN0TmFtZSxcclxuICAgICAgICBlbWFpbDogZm9ybS5lbWFpbCxcclxuICAgICAgICBwYXNzd29yZDogZm9ybS5wYXNzd29yZCxcclxuICAgICAgICBhZ2U6IHBhcnNlSW50KGZvcm0uYWdlKSBcclxuICAgICAgfTtcclxuXHJcbiAgICAgIGF4aW9zLnBvc3QoU0lHTlVQX0FQSSxib2R5UGFyYW1ldGVycyxjb25maWcpXHJcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT57XHJcbiAgICAgICAgc2V0U3RlcChzdGVwICsgMSlcclxuICAgICAgfSwgKGVycm9yKSA9PntcclxuXHJcbiAgICAgIH0pO1xyXG4gfVxyXG5cclxuLy8gIEFkZHJlc3MgRmllbGQgZW50ZXIgXHJcbmNvbnN0IGFkZEFkZHJlc3MgPSAoZSkgPT57XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgY29uc3QgY29uZmlnID0geyAgXHJcbiAgICAgICAgaGVhZGVyczogeyBBdXRob3JpemF0aW9uOiBgJHt2ZXJ0aWZ5T3RwLnRva2VufWAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICBcclxuICAgIGNvbnN0IGJvZHlQYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgIGFkZHJlc3NJZDogZm9ybUFkZHJlc3MuYWRkcmVzc0lkLFxyXG4gICAgICAgIG5hbWU6IGZvcm1BZGRyZXNzLm5hbWUsXHJcbiAgICAgICAgbG9jYWxpdHk6IGZvcm1BZGRyZXNzLmxvY2FsaXR5LFxyXG4gICAgICAgIGFyZWE6IGZvcm1BZGRyZXNzLmFyZWEsXHJcbiAgICAgICAgc3RyZWV0OiBmb3JtQWRkcmVzcy5zdHJlZXQsXHJcbiAgICAgICAgcGluY29kZUlkOiBwYXJzZUludChmb3JtQWRkcmVzcy5waW5jb2RlSWQpLFxyXG4gICAgICAgIGNpdHlJZDogcGFyc2VJbnQoZm9ybUFkZHJlc3MuY2l0eUlkKSxcclxuICAgICAgICBzdGF0ZUlkOiBwYXJzZUludChmb3JtQWRkcmVzcy5zdGF0ZUlkKSxcclxuICAgICAgICBhZGRyZXNzVHlwZTogZm9ybUFkZHJlc3MuYWRkcmVzc1R5cGVcclxuXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBheGlvcy5wb3N0KEFERF9BRERSRVNTX0FQSSxib2R5UGFyYW1ldGVycyxjb25maWcpXHJcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT57XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLFwiYWFhYWFhYWFhYWFcIik7XHJcbiAgICAgICAgc2V0U3RlcChzdGVwICsgMSlcclxuICAgICAgfSwgKGVycm9yKSA9PntcclxuXHJcbiAgICAgIH0pO1xyXG4gfVxyXG5cclxuICBjb25zdCB0YWJTY3JlZW4gPSAoKSA9PiB7XHJcblxyXG4gICAgc3dpdGNoIChzdGVwKSB7XHJcbiAgICAgIGNhc2UgMTpcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICA8VGFiTGlzdCBjbGFzc05hbWU9XCJuYXYgbmF2LXRhYnMgbmF2LWZpbGwgYWxpZ24taXRlbXMtY2VudGVyIGJvcmRlci1ubyBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyIG1iLTVcIj5cclxuICAgICAgICAgICAgPFRhYiBjbGFzc05hbWU9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibmF2LWxpbmsgYm9yZGVyLW5vIGxoLTEgbHMtbm9ybWFsXCI+RHNxdWFyZTwvc3Bhbj5cclxuICAgICAgICAgICAgPC9UYWI+XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDwvVGFiTGlzdD5cclxuICAgICAgICAgICAgPGZvcm0gYWN0aW9uPVwiI1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXAgbWItM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwicGhvbmVfbnVtYmVyXCIgaWQ9XCJwaG9uZV9udW1iZXJcIiBtYXhsZW5ndGg9XCIxOVwiIHR5cGU9XCJ0ZXh0XCIgcGxhY2Vob2xkZXI9XCJFbnRlciBQaG9uZSBOdW1iZXIgKlwiIHZhbHVlPXtwaG9uZU51bWJlcn0gb25DaGFuZ2U9e2hhbmRsZVBob25lQ2hhbmdlfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ0biBidG4tZGFyayBidG4tYmxvY2sgYnRuLXJvdW5kZWRcIiB0eXBlPVwic3VibWl0XCIgb25DbGljaz17c2VuZE90cH0+U2VuZCBPVFA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICA8Lz5cclxuICAgICAgICApO1xyXG4gICAgICBjYXNlIDI6XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgPFRhYkxpc3QgY2xhc3NOYW1lPVwibmF2IG5hdi10YWJzIG5hdi1maWxsIGFsaWduLWl0ZW1zLWNlbnRlciBib3JkZXItbm8ganVzdGlmeS1jb250ZW50LWNlbnRlciBtYi01XCI+XHJcbiAgICAgICAgICAgIDxUYWIgY2xhc3NOYW1lPVwibmF2LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm5hdi1saW5rIGJvcmRlci1ubyBsaC0xIGxzLW5vcm1hbFwiPkVudGVyIENvZGU8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvVGFiPlxyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICA8L1RhYkxpc3Q+XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8Zm9ybSBhY3Rpb249XCIjXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cCBtYi0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIG5hbWU9XCJvdHBfbnVtYmVyXCIgaWQ9XCJvdHBfbnVtYmVyXCIgbWF4bGVuZ3RoPVwiNlwiIHR5cGU9XCJ0ZXh0XCIgcGxhY2Vob2xkZXI9XCJFbnRlciBPVFAgTnVtYmVyICpcIiB2YWx1ZT17T1RQfSBvbkNoYW5nZT17ZSA9PiBzZXRPdHAoZS50YXJnZXQudmFsdWUpfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ0biBidG4tZGFyayBidG4tYmxvY2sgYnRuLXJvdW5kZWRcIiB0eXBlPVwic3VibWl0XCIgb25DbGljaz17dmVyaWZ5T3RwfT5WZXJpZnkgT1RQPC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgKTtcclxuICAgICAgY2FzZSAzOlxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgIDxUYWJMaXN0IGNsYXNzTmFtZT1cIm5hdiBuYXYtdGFicyBuYXYtZmlsbCBhbGlnbi1pdGVtcy1jZW50ZXIgYm9yZGVyLW5vIGp1c3RpZnktY29udGVudC1jZW50ZXIgbWItNVwiPlxyXG4gICAgICAgICAgICA8VGFiIGNsYXNzTmFtZT1cIm5hdi1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJuYXYtbGluayBib3JkZXItbm8gbGgtMSBscy1ub3JtYWxcIj5DcmVhdGUgeW91ciBwcm9maWxlPC9zcGFuPlxyXG4gICAgICAgICAgICA8L1RhYj5cclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgPC9UYWJMaXN0PlxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgPGZvcm0gYWN0aW9uPVwiI1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwic2luZ2luLWVtYWlsXCI+VXNlcm5hbWU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwidXNlck5hbWVcIiBuYW1lPVwidXNlck5hbWVcIiBwbGFjZWhvbGRlcj1cIkVudGVyIHVzZXJuYW1lICpcIiAgdmFsdWU9e2Zvcm0udXNlck5hbWV9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IHJlcXVpcmVkICAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cInNpbmdpbi1lbWFpbFwiPkZpcnN0IE5hbWU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwiZmlyc3ROYW1lXCIgbmFtZT1cImZpcnN0TmFtZVwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgZmlyc3QgbmFtZVwiIHZhbHVlPXtmb3JtLmZpcnN0TmFtZX0gb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX0gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJzaW5naW4tZW1haWxcIj5MYXN0IE5hbWU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwibGFzdE5hbWVcIiBuYW1lPVwibGFzdE5hbWVcIiBwbGFjZWhvbGRlcj1cIkVudGVyIGxhc3QgbmFtZVwiIHZhbHVlPXtmb3JtLmxhc3ROYW1lfSBvbkNoYW5nZT17aGFuZGxlQ2hhbmdlfSAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cInNpbmdpbi1lbWFpbFwiPllvdXIgZW1haWwgYWRkcmVzczo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJlbWFpbFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwiZW1haWxcIiBuYW1lPVwiZW1haWxcIiBwbGFjZWhvbGRlcj1cIllvdXIgRW1haWwgYWRkcmVzcyAqXCIgdmFsdWU9e2Zvcm0uZW1haWx9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IHJlcXVpcmVkIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwic2luZ2luLXBhc3N3b3JkXCI+UGFzc3dvcmQ6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInBhc3N3b3JkXCIgbmFtZT1cInBhc3N3b3JkXCIgcGxhY2Vob2xkZXI9XCJQYXNzd29yZCAqXCIgdmFsdWU9e2Zvcm0ucGFzc3dvcmR9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IHJlcXVpcmVkIC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwic2luZ2luLXBhc3N3b3JkXCI+UGFzc3dvcmQ6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwibnVtYmVyXCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJhZ2VcIiBuYW1lPVwiYWdlXCIgcGxhY2Vob2xkZXI9XCJhZ2VcIiB2YWx1ZT17Zm9ybS5hZ2V9IG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2V9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWZvb3RlclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWNoZWNrYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzTmFtZT1cImN1c3RvbS1jaGVja2JveFwiIGlkPVwicmVnaXN0ZXItYWdyZWVcIiBuYW1lPVwicmVnaXN0ZXItYWdyZWVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2wtbGFiZWxcIiBodG1sRm9yPVwicmVnaXN0ZXItYWdyZWVcIj5JIGFncmVlIHRvIHRoZSBwcml2YWN5IHBvbGljeTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYnRuIGJ0bi1kYXJrIGJ0bi1ibG9jayBidG4tcm91bmRlZFwiIHR5cGU9XCJzdWJtaXRcIiBvbkNsaWNrPXsgY3JlYXRlQWNjb3VudCB9PkNyZWF0ZSBhY2NvdW50PC9idXR0b24+XHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgIDwvPlxyXG4gICAgICAgICk7XHJcbiAgICAgIGNhc2UgNDpcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICA8VGFiTGlzdCBjbGFzc05hbWU9XCJuYXYgbmF2LXRhYnMgbmF2LWZpbGwgYWxpZ24taXRlbXMtY2VudGVyIGJvcmRlci1ubyBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyIG1iLTVcIj5cclxuICAgICAgICAgICAgPFRhYiBjbGFzc05hbWU9XCJuYXYtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibmF2LWxpbmsgYm9yZGVyLW5vIGxoLTEgbHMtbm9ybWFsXCI+QWRkIHlvdXIgYWRkcmVzczwvc3Bhbj5cclxuICAgICAgICAgICAgPC9UYWI+XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDwvVGFiTGlzdD5cclxuXHJcbiAgICAgICAgICAgIDxmb3JtIGFjdGlvbj1cIiNcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cInNpbmdpbi1lbWFpbFwiPkFkZHJlc3MgSUQ6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwiYWRkcmVzc0lkXCIgbmFtZT1cImFkZHJlc3NJZFwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgYWRkcmVzcyBpZCAqXCIgIHZhbHVlPXtmb3JtQWRkcmVzcy5hZGRyZXNzSWR9IG9uQ2hhbmdlPXtoYW5kbGVBZGRyZXNzfSByZXF1aXJlZCAgLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJzaW5naW4tZW1haWxcIj5OYW1lOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBpZD1cIm5hbWVcIiBuYW1lPVwibmFtZVwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgbmFtZVwiIHZhbHVlPXtmb3JtQWRkcmVzcy5uYW1lfSBvbkNoYW5nZT17aGFuZGxlQWRkcmVzc30gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJzaW5naW4tZW1haWxcIj5Mb2NhbGl0eTo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJsb2NhbGl0eVwiIG5hbWU9XCJsb2NhbGl0eVwiIHBsYWNlaG9sZGVyPVwiRW50ZXIgeW91ciBsb2NhbGl0eVwiIHZhbHVlPXtmb3JtQWRkcmVzcy5sb2NhbGl0eX0gb25DaGFuZ2U9e2hhbmRsZUFkZHJlc3N9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgIDxsYWJlbCBodG1sRm9yPVwic2luZ2luLWVtYWlsXCI+QXJlYTo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJhcmVhXCIgbmFtZT1cImFyZWFcIiBwbGFjZWhvbGRlcj1cIkVudGVyIHlvdXIgYXJlYSAqXCIgdmFsdWU9e2Zvcm1BZGRyZXNzLmFyZWF9IG9uQ2hhbmdlPXtoYW5kbGVBZGRyZXNzfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cInNpbmdpbi1wYXNzd29yZFwiPnN0cmVldDo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJzdHJlZXRcIiBuYW1lPVwic3RyZWV0XCIgcGxhY2Vob2xkZXI9XCJzdHJlZXQgXCIgdmFsdWU9e2Zvcm1BZGRyZXNzLnN0cmVldH0gb25DaGFuZ2U9e2hhbmRsZUFkZHJlc3N9ICAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cInNpbmdpbi1wYXNzd29yZFwiPlBpbmNvZGUgSUQ6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwibnVtYmVyXCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJwaW5jb2RlSWRcIiBuYW1lPVwicGluY29kZUlkXCIgcGxhY2Vob2xkZXI9XCJQaW5jb2RlIElkICpcIiB2YWx1ZT17Zm9ybUFkZHJlc3MucGluY29kZUlkfSBvbkNoYW5nZT17aGFuZGxlQWRkcmVzc30gcmVxdWlyZWQgLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJzaW5naW4tcGFzc3dvcmRcIj5DaXR5IElEOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cIm51bWJlclwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwiY2l0eUlkXCIgbmFtZT1cImNpdHlJZFwiIHBsYWNlaG9sZGVyPVwiQ2l0eSBJZFwiIHZhbHVlPXtmb3JtQWRkcmVzcy5jaXR5SWR9IG9uQ2hhbmdlPXtoYW5kbGVBZGRyZXNzfSAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cInNpbmdpbi1wYXNzd29yZFwiPlN0YXRlIElEOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cIm51bWJlclwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwic3RhdGVJZFwiIG5hbWU9XCJzdGF0ZUlkXCIgcGxhY2Vob2xkZXI9XCJTdGF0ZSBJZFwiIHZhbHVlPXtmb3JtQWRkcmVzcy5zdGF0ZUlkfSBvbkNoYW5nZT17aGFuZGxlQWRkcmVzc30gLz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJzaW5naW4tcGFzc3dvcmRcIj5BZGRyZXNzIFR5cGU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwiYWRkcmVzc1R5cGVcIiBuYW1lPVwiYWRkcmVzc1R5cGVcIiBwbGFjZWhvbGRlcj1cIkFkZHJlc3MgVHlwZVwiIHZhbHVlPXtmb3JtQWRkcmVzcy5hZGRyZXNzVHlwZX0gb25DaGFuZ2U9e2hhbmRsZUFkZHJlc3N9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWZvb3RlclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWNoZWNrYm94XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzTmFtZT1cImN1c3RvbS1jaGVja2JveFwiIGlkPVwicmVnaXN0ZXItYWdyZWVcIiBuYW1lPVwicmVnaXN0ZXItYWdyZWVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2wtbGFiZWxcIiBodG1sRm9yPVwicmVnaXN0ZXItYWdyZWVcIj5JIGFncmVlIHRvIHRoZSBwcml2YWN5IHBvbGljeTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYnRuIGJ0bi1kYXJrIGJ0bi1ibG9jayBidG4tcm91bmRlZFwiIHR5cGU9XCJzdWJtaXRcIiBvbkNsaWNrPXsgYWRkQWRkcmVzcyB9PkFkZCBBZGRyZXNzPC9idXR0b24+XHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgIDwvPlxyXG4gICAgICAgICk7XHJcbiAgICAgIFxyXG4gICAgICBkZWZhdWx0OlxyXG4gICAgICAgIHJldHVybiAnZm9vJztcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuXHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8PlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJsb2dpbi1saW5rIGQtbGctc2hvd1wiIGhyZWY9XCIjXCIgb25DbGljaz17IG9wZW5Nb2RhbCB9PlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZC1pY29uLXVzZXJcIj48L2k+U2lnbiBpbjwvYT5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiZGVsaW1pdGVyXCI+Lzwvc3Bhbj5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwicmVnaXN0ZXItbGluayBtbC0wXCIgb25DbGljaz17ICggZSApID0+IG9wZW5Nb2RhbCggZSwgMSApIH0gaHJlZj1cIiNcIj5SZWdpc3RlcjwvYT5cclxuXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIG9wZW4gP1xyXG4gICAgICAgICAgICAgICAgICAgIDxNb2RhbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc09wZW49eyBvcGVuIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25SZXF1ZXN0Q2xvc2U9eyBjbG9zZU1vZGFsIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9eyBjdXN0b21TdHlsZXMgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50TGFiZWw9XCJMb2dpbiBNb2RhbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImxvZ2luLXBvcHVwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcmxheUNsYXNzTmFtZT1cImxvZ2luLXBvcHVwLW92ZXJsYXlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaG91bGRSZXR1cm5Gb2N1c0FmdGVyQ2xvc2U9eyBmYWxzZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwibG9naW4tbW9kYWxcIlxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWJveFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YWIgdGFiLW5hdi1zaW1wbGUgdGFiLW5hdi1ib3hlZCBmb3JtLXRhYlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUYWJzIHNlbGVjdGVkVGFiQ2xhc3NOYW1lPVwiYWN0aXZlXCIgc2VsZWN0ZWRUYWJQYW5lbENsYXNzTmFtZT1cImFjdGl2ZVwiIGRlZmF1bHRJbmRleD17IGluZGV4IH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFiLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3RhYlNjcmVlbigpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvVGFicz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdGl0bGU9XCJDbG9zZSAoRXNjKVwiIHR5cGU9XCJidXR0b25cIiBjbGFzc05hbWU9XCJtZnAtY2xvc2VcIiBvbkNsaWNrPXsgY2xvc2VNb2RhbCB9PjxzcGFuPsOXPC9zcGFuPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvTW9kYWw+IDogJydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIDwvPlxyXG4gICAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCAoIExvZ2luTW9kYWwgKTsiLCJpbXBvcnQgeyB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJ3JlYWN0LW1vZGFsJztcclxuXHJcbmltcG9ydCB7IG1vZGFsQWN0aW9ucyB9IGZyb20gJ34vc3RvcmUvbW9kYWwnO1xyXG5cclxuY29uc3QgY3VzdG9tU3R5bGVzID0ge1xyXG4gICAgY29udGVudDoge1xyXG4gICAgICAgIHBvc2l0aW9uOiBcInJlbGF0aXZlXCJcclxuICAgIH0sXHJcbiAgICBvdmVybGF5OiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJ3JnYmEoMCwwLDAsLjQpJyxcclxuICAgICAgICBvdmVyZmxvd1g6ICdoaWRkZW4nLFxyXG4gICAgICAgIGRpc3BsYXk6ICdmbGV4JyxcclxuICAgICAgICBvdmVyZmxvd1k6ICdhdXRvJyxcclxuICAgICAgICBvcGFjaXR5OiAwXHJcbiAgICB9XHJcbn1cclxuXHJcbk1vZGFsLnNldEFwcEVsZW1lbnQoICcjX19uZXh0JyApO1xyXG5cclxuZnVuY3Rpb24gVmlkZW9Nb2RhbCAoIHByb3BzICkge1xyXG4gICAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgICBjb25zdCB7IGlzT3BlbiwgY2xvc2VNb2RhbCwgc2luZ2xlU2x1ZyB9ID0gcHJvcHM7XHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgY2xvc2VNb2RhbCgpO1xyXG5cclxuICAgICAgICByb3V0ZXIuZXZlbnRzLm9uKCAncm91dGVDaGFuZ2VTdGFydCcsIGNsb3NlTW9kYWwgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgICAgICAgcm91dGVyLmV2ZW50cy5vZmYoICdyb3V0ZUNoYW5nZVN0YXJ0JywgY2xvc2VNb2RhbCApO1xyXG4gICAgICAgIH1cclxuICAgIH0sIFtdIClcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBpZiAoIGlzT3BlbiApXHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiLlJlYWN0TW9kYWxfX092ZXJsYXlcIiApLmNsYXNzTGlzdC5hZGQoICdvcGVuZWQnICk7XHJcbiAgICAgICAgICAgIH0sIDEwMCApXHJcbiAgICB9LCBbIGlzT3BlbiBdIClcclxuXHJcbiAgICBjb25zdCBjbG9zZVZpZGVvID0gKCkgPT4ge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiLlJlYWN0TW9kYWxfX092ZXJsYXlcIiApLmNsYXNzTGlzdC5hZGQoICdyZW1vdmVkJyApO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiLlJlYWN0TW9kYWxfX092ZXJsYXlcIiApLmNsYXNzTGlzdC5yZW1vdmUoICdvcGVuZWQnICk7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIudmlkZW8tbW9kYWwuUmVhY3RNb2RhbF9fQ29udGVudFwiICkuY2xhc3NMaXN0LnJlbW92ZSggXCJSZWFjdE1vZGFsX19Db250ZW50LS1hZnRlci1vcGVuXCIgKTtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcIi52aWRlby1tb2RhbC1vdmVybGF5LlJlYWN0TW9kYWxfX092ZXJsYXlcIiApLmNsYXNzTGlzdC5yZW1vdmUoIFwiUmVhY3RNb2RhbF9fT3ZlcmxheS0tYWZ0ZXItb3BlblwiICk7XHJcblxyXG4gICAgICAgIHNldFRpbWVvdXQoICgpID0+IHtcclxuICAgICAgICAgICAgY2xvc2VNb2RhbCgpO1xyXG4gICAgICAgIH0sIDMzMCApO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPCBNb2RhbFxyXG4gICAgICAgICAgICBpc09wZW49eyBpc09wZW4gfVxyXG4gICAgICAgICAgICBjb250ZW50TGFiZWw9XCJWaWRlb01vZGFsXCJcclxuICAgICAgICAgICAgb25SZXF1ZXN0Q2xvc2U9eyBjbG9zZVZpZGVvIH1cclxuICAgICAgICAgICAgc2hvdWxkRm9jdXNBZnRlclJlbmRlcj17IGZhbHNlIH1cclxuICAgICAgICAgICAgc3R5bGU9eyBjdXN0b21TdHlsZXMgfVxyXG4gICAgICAgICAgICBvdmVybGF5Q2xhc3NOYW1lPVwidmlkZW8tbW9kYWwtb3ZlcmxheVwiXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cInJvdyB2aWRlby1tb2RhbFwiIGlkPVwidmlkZW8tbW9kYWxcIlxyXG4gICAgICAgID5cclxuICAgICAgICAgICAgPHZpZGVvIHNyYz17IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FTU0VUX1VSSSArIHNpbmdsZVNsdWcgfSBhdXRvUGxheT17IHRydWUgfSBsb29wPXsgdHJ1ZSB9IGNvbnRyb2xzPXsgdHJ1ZSB9IGNsYXNzTmFtZT1cInAtMFwiPjwvdmlkZW8+XHJcblxyXG4gICAgICAgICAgICA8YnV0dG9uIHRpdGxlPVwiQ2xvc2UgKEVzYylcIiB0eXBlPVwiYnV0dG9uXCIgY2xhc3NOYW1lPVwibWZwLWNsb3NlXCIgb25DbGljaz17IGNsb3NlTW9kYWwgfSA+PHNwYW4+w5c8L3NwYW4+PC9idXR0b24+XHJcbiAgICAgICAgPC9Nb2RhbCA+XHJcbiAgICApXHJcbn1cclxuXHJcbmZ1bmN0aW9uIG1hcFN0YXRlVG9Qcm9wcyAoIHN0YXRlICkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBpc09wZW46IHN0YXRlLm1vZGFsLm9wZW5Nb2RhbCxcclxuICAgICAgICBzaW5nbGVTbHVnOiBzdGF0ZS5tb2RhbC5zaW5nbGVTbHVnXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QoIG1hcFN0YXRlVG9Qcm9wcywgeyBjbG9zZU1vZGFsOiBtb2RhbEFjdGlvbnMuY2xvc2VNb2RhbCB9ICkoIFZpZGVvTW9kYWwgKTsiLCJpbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgeyB1c2VRdWVyeSB9IGZyb20gJ0BhcG9sbG8vcmVhY3QtaG9va3MnO1xyXG5pbXBvcnQgeyBNYWduaWZpZXIgfSBmcm9tICdyZWFjdC1pbWFnZS1tYWduaWZpZXJzJztcclxuaW1wb3J0IE1vZGFsIGZyb20gJ3JlYWN0LW1vZGFsJztcclxuaW1wb3J0IGltYWdlc0xvYWRlZCBmcm9tICdpbWFnZXNsb2FkZWQnO1xyXG5cclxuaW1wb3J0IHsgR0VUX1BST0RVQ1QgfSBmcm9tICd+L3NlcnZlci9xdWVyaWVzJztcclxuaW1wb3J0IHdpdGhBcG9sbG8gZnJvbSAnfi9zZXJ2ZXIvYXBvbGxvJztcclxuXHJcbmltcG9ydCBPd2xDYXJvdXNlbCBmcm9tICd+L2NvbXBvbmVudHMvZmVhdHVyZXMvb3dsLWNhcm91c2VsJztcclxuXHJcbmltcG9ydCBEZXRhaWxPbmUgZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL3Byb2R1Y3QvZGV0YWlsL2RldGFpbC1vbmUnO1xyXG5cclxuaW1wb3J0IHsgbW9kYWxBY3Rpb25zIH0gZnJvbSAnfi9zdG9yZS9tb2RhbCc7XHJcblxyXG5pbXBvcnQgeyBtYWluU2xpZGVyMyB9IGZyb20gJ34vdXRpbHMvZGF0YS9jYXJvdXNlbCc7XHJcblxyXG5jb25zdCBjdXN0b21TdHlsZXMgPSB7XHJcbiAgICBjb250ZW50OiB7XHJcbiAgICAgICAgcG9zaXRpb246IFwicmVsYXRpdmVcIlxyXG4gICAgfSxcclxuICAgIG92ZXJsYXk6IHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAncmdiYSgwLDAsMCwuNCknLFxyXG4gICAgICAgIHpJbmRleDogJzEwMDAwJyxcclxuICAgICAgICBvdmVyZmxvd1g6ICdoaWRkZW4nLFxyXG4gICAgICAgIG92ZXJmbG93WTogJ2F1dG8nXHJcbiAgICB9XHJcbn1cclxuXHJcbk1vZGFsLnNldEFwcEVsZW1lbnQoICcjX19uZXh0JyApO1xyXG5cclxuZnVuY3Rpb24gUXVpY2t2aWV3KCBwcm9wcyApIHtcclxuICAgIGNvbnN0IHsgc2x1ZywgY2xvc2VRdWlja3ZpZXcsIGlzT3BlbiB9ID0gcHJvcHM7XHJcblxyXG4gICAgaWYgKCAhaXNPcGVuICkgcmV0dXJuIDxkaXY+PC9kaXY+O1xyXG5cclxuICAgIGNvbnN0IFsgbG9hZGVkLCBzZXRMb2FkaW5nU3RhdGUgXSA9IHVzZVN0YXRlKCBmYWxzZSApO1xyXG5cclxuICAgIGNvbnN0IHsgZGF0YSwgbG9hZGluZyB9ID0gdXNlUXVlcnkoIEdFVF9QUk9EVUNULCB7IHZhcmlhYmxlczogeyBzbHVnLCBvbmx5RGF0YTogdHJ1ZSB9IH0gKTtcclxuICAgIGNvbnN0IHByb2R1Y3QgPSBkYXRhICYmIGRhdGEucHJvZHVjdDtcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBzZXRUaW1lb3V0KCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICggIWxvYWRpbmcgJiYgZGF0YSAmJiBpc09wZW4gJiYgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5xdWlja3ZpZXctbW9kYWwnICkgKVxyXG4gICAgICAgICAgICAgICAgaW1hZ2VzTG9hZGVkKCAnLnF1aWNrdmlldy1tb2RhbCcgKS5vbiggJ2RvbmUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0TG9hZGluZ1N0YXRlKCB0cnVlICk7XHJcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmpRdWVyeSggJy5xdWlja3ZpZXctbW9kYWwgLnByb2R1Y3Qtc2luZ2xlLWNhcm91c2VsJyApLnRyaWdnZXIoICdyZWZyZXNoLm93bC5jYXJvdXNlbCcgKTtcclxuICAgICAgICAgICAgICAgIH0gKS5vbiggJ3Byb2dyZXNzJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNldExvYWRpbmdTdGF0ZSggZmFsc2UgKTtcclxuICAgICAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9LCAyMDAgKTtcclxuICAgIH0sIFsgZGF0YSwgaXNPcGVuIF0gKTtcclxuXHJcbiAgICBpZiAoIHNsdWcgPT09ICcnIHx8ICFwcm9kdWN0IHx8ICFwcm9kdWN0LmRhdGEgKSByZXR1cm4gJyc7XHJcblxyXG4gICAgY29uc3QgY2xvc2VRdWljayA9ICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcIi5SZWFjdE1vZGFsX19PdmVybGF5XCIgKS5jbGFzc0xpc3QuYWRkKCAncmVtb3ZlZCcgKTtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnF1aWNrdmlldy1tb2RhbCcgKS5jbGFzc0xpc3QuYWRkKCAncmVtb3ZlZCcgKTtcclxuICAgICAgICBzZXRMb2FkaW5nU3RhdGUoIGZhbHNlIClcclxuICAgICAgICBzZXRUaW1lb3V0KCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNsb3NlUXVpY2t2aWV3KCk7XHJcbiAgICAgICAgfSwgMzMwICk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8TW9kYWxcclxuICAgICAgICAgICAgaXNPcGVuPXsgaXNPcGVuIH1cclxuICAgICAgICAgICAgY29udGVudExhYmVsPVwiUXVpY2tWaWV3XCJcclxuICAgICAgICAgICAgb25SZXF1ZXN0Q2xvc2U9eyBjbG9zZVF1aWNrIH1cclxuICAgICAgICAgICAgc2hvdWxkRm9jdXNBZnRlclJlbmRlcj17IGZhbHNlIH1cclxuICAgICAgICAgICAgc3R5bGU9eyBjdXN0b21TdHlsZXMgfVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0IHByb2R1Y3Qtc2luZ2xlIHJvdyBwcm9kdWN0LXBvcHVwIHF1aWNrdmlldy1tb2RhbFwiIGlkPVwicHJvZHVjdC1xdWlja3ZpZXdcIlxyXG4gICAgICAgID5cclxuICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgYHJvdyBwLTAgbS0wICR7IGxvYWRlZCA/ICcnIDogJ2Qtbm9uZScgfWAgfSA+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtZ2FsbGVyeSBtYi1tZC0wIHBiLTBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1sYWJlbC1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgcHJvZHVjdC5kYXRhLmlzX25ldyA/IDxsYWJlbCBjbGFzc05hbWU9XCJwcm9kdWN0LWxhYmVsIGxhYmVsLW5ld1wiPk5ldzwvbGFiZWw+IDogJycgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgcHJvZHVjdC5kYXRhLmlzX3RvcCA/IDxsYWJlbCBjbGFzc05hbWU9XCJwcm9kdWN0LWxhYmVsIGxhYmVsLXRvcFwiPlRvcDwvbGFiZWw+IDogJycgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC5kYXRhLmRpc2NvdW50ID4gMCA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LmRhdGEudmFyaWFudHMubGVuZ3RoID09PSAwID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwicHJvZHVjdC1sYWJlbCBsYWJlbC1zYWxlXCI+eyBwcm9kdWN0LmRhdGEuZGlzY291bnQgfSUgT0ZGPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IDxsYWJlbCBjbGFzc05hbWU9XCJwcm9kdWN0LWxhYmVsIGxhYmVsLXNhbGVcIj5TYWxlPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8T3dsQ2Fyb3VzZWwgYWRDbGFzcz1cInByb2R1Y3Qtc2luZ2xlLWNhcm91c2VsIG93bC10aGVtZSBvd2wtbmF2LWlubmVyXCIgb3B0aW9ucz17IG1haW5TbGlkZXIzIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0ICYmIHByb2R1Y3QuZGF0YSAmJiBwcm9kdWN0LmRhdGEubGFyZ2VfcGljdHVyZXMubWFwKCAoIGl0ZW0sIGluZGV4ICkgPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxNYWduaWZpZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9eyAncXVpY2t2aWV3LWltYWdlLScgKyBpbmRleCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2VTcmM9eyBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BU1NFVF9VUkkgKyBpdGVtLnVybCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2VBbHQ9XCJtYWduaWZpZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhcmdlSW1hZ2VTcmM9eyBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BU1NFVF9VUkkgKyBpdGVtLnVybCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZHJhZ1RvTW92ZT17IGZhbHNlIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb3VzZUFjdGl2YXRpb249XCJob3ZlclwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yU3R5bGVBY3RpdmU9XCJjcm9zc2hhaXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtaW1hZ2UgbGFyZ2UtaW1hZ2VcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvT3dsQ2Fyb3VzZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxEZXRhaWxPbmUgZGF0YT17IGRhdGEgfSBhZENsYXNzPVwic2Nyb2xsYWJsZSBwci0zXCIgaXNOYXY9eyBmYWxzZSB9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8YnV0dG9uIHRpdGxlPVwiQ2xvc2UgKEVzYylcIiB0eXBlPVwiYnV0dG9uXCIgY2xhc3NOYW1lPVwibWZwLWNsb3NlIHAtMFwiIG9uQ2xpY2s9eyBjbG9zZVF1aWNrIH0gPjxzcGFuPsOXPC9zcGFuPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbG9hZGVkID8gJycgOiA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3Qgcm93IHAtMCBtLTAgc2tlbGV0b24tYm9keSBtZnAtcHJvZHVjdFwiID5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2tlbC1wcm8tZ2FsbGVyeVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbWQtNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNrZWwtcHJvLXN1bW1hcnlcIj48L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgPC9Nb2RhbD5cclxuICAgIClcclxufVxyXG5cclxuZnVuY3Rpb24gbWFwU3RhdGVUb1Byb3BzKCBzdGF0ZSApIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgc2x1Zzogc3RhdGUubW9kYWwuc2luZ2xlU2x1ZyxcclxuICAgICAgICBpc09wZW46IHN0YXRlLm1vZGFsLnF1aWNrdmlld1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCB3aXRoQXBvbGxvKCB7IHNzcjogdHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcgfSApKCBjb25uZWN0KCBtYXBTdGF0ZVRvUHJvcHMsIHsgY2xvc2VRdWlja3ZpZXc6IG1vZGFsQWN0aW9ucy5jbG9zZVF1aWNrdmlldyB9ICkoIFF1aWNrdmlldyApICk7IiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFF1YW50aXR5KCB7IHF0eSA9IDEsIC4uLnByb3BzIH0gKSB7XHJcbiAgICBjb25zdCB7IGFkQ2xhc3MgPSAnbXItMiBpbnB1dC1ncm91cCcgfSA9IHByb3BzO1xyXG4gICAgY29uc3QgWyBxdWFudGl0eSwgc2V0UXVhbnRpdHkgXSA9IHVzZVN0YXRlKCBwYXJzZUludCggcXR5ICkgKTtcclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBzZXRRdWFudGl0eSggcXR5ICk7XHJcbiAgICB9LCBbIHByb3BzLnByb2R1Y3QgXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgcHJvcHMub25DaGFuZ2VRdHkgJiYgcHJvcHMub25DaGFuZ2VRdHkoIHF1YW50aXR5ICk7XHJcbiAgICB9LCBbIHF1YW50aXR5IF0gKVxyXG5cclxuICAgIGZ1bmN0aW9uIG1pbnVzUXVhbnRpdHkoKSB7XHJcbiAgICAgICAgaWYgKCBxdWFudGl0eSA+IDEgKSB7XHJcbiAgICAgICAgICAgIHNldFF1YW50aXR5KCBwYXJzZUludCggcXVhbnRpdHkgKSAtIDEgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gcGx1c1F1YW50aXR5KCkge1xyXG4gICAgICAgIGlmICggcXVhbnRpdHkgPCBwcm9wcy5tYXggKSB7XHJcbiAgICAgICAgICAgIHNldFF1YW50aXR5KCBwYXJzZUludCggcXVhbnRpdHkgKSArIDEgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gY2hhbmdlUXR5KCBlICkge1xyXG4gICAgICAgIGxldCBuZXdRdHk7XHJcblxyXG4gICAgICAgIGlmICggZS5jdXJyZW50VGFyZ2V0LnZhbHVlICE9PSAnJyApIHtcclxuICAgICAgICAgICAgbmV3UXR5ID0gTWF0aC5taW4oIHBhcnNlSW50KCBlLmN1cnJlbnRUYXJnZXQudmFsdWUgKSwgcHJvcHMubWF4ICk7XHJcbiAgICAgICAgICAgIG5ld1F0eSA9IE1hdGgubWF4KCBuZXdRdHksIDEgKTtcclxuICAgICAgICAgICAgc2V0UXVhbnRpdHkoIG5ld1F0eSApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgYWRDbGFzcyB9PlxyXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT0ncXVhbnRpdHktbWludXMgZC1pY29uLW1pbnVzJyBvbkNsaWNrPXsgbWludXNRdWFudGl0eSB9PjwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8aW5wdXQgY2xhc3NOYW1lPSdxdWFudGl0eSBmb3JtLWNvbnRyb2wnIHR5cGU9J251bWJlcicgbWluPVwiMVwiIG1heD17IHByb3BzLm1heCB9IHZhbHVlPXsgcXVhbnRpdHkgfSBvbkNoYW5nZT17IGNoYW5nZVF0eSB9IC8+XHJcbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPSdxdWFudGl0eS1wbHVzIGQtaWNvbi1wbHVzJyBvbkNsaWNrPXsgcGx1c1F1YW50aXR5IH0+PC9idXR0b24+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApXHJcbn0iLCJpbXBvcnQgeyB1c2VFZmZlY3QsIHVzZUxheW91dEVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcclxuaW1wb3J0IHsgVG9hc3RDb250YWluZXIgfSBmcm9tICdyZWFjdC10b2FzdGlmeSc7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJztcclxuaW1wb3J0ICdyZWFjdC10b2FzdGlmeS9kaXN0L1JlYWN0VG9hc3RpZnkubWluLmNzcyc7XHJcbmltcG9ydCAncmVhY3QtaW1hZ2UtbGlnaHRib3gvc3R5bGUuY3NzJztcclxuaW1wb3J0ICdyZWFjdC1pbnB1dC1yYW5nZS9saWIvY3NzL2luZGV4LmNzcyc7XHJcblxyXG5pbXBvcnQgQUxpbmsgZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL2N1c3RvbS1saW5rJztcclxuXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnfi9jb21wb25lbnRzL2NvbW1vbi9oZWFkZXInO1xyXG5pbXBvcnQgRm9vdGVyIGZyb20gJ34vY29tcG9uZW50cy9jb21tb24vZm9vdGVyJztcclxuaW1wb3J0IFN0aWNreUZvb3RlciBmcm9tICd+L2NvbXBvbmVudHMvY29tbW9uL3N0aWNreS1mb290ZXInO1xyXG5pbXBvcnQgUXVpY2t2aWV3IGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9wcm9kdWN0L2NvbW1vbi9xdWlja3ZpZXctbW9kYWwnO1xyXG5pbXBvcnQgVmlkZW9Nb2RhbCBmcm9tICd+L2NvbXBvbmVudHMvZmVhdHVyZXMvbW9kYWxzL3ZpZGVvLW1vZGFsJztcclxuaW1wb3J0IE1vYmlsZU1lbnUgZnJvbSAnfi9jb21wb25lbnRzL2NvbW1vbi9wYXJ0aWFscy9tb2JpbGUtbWVudSc7XHJcblxyXG5pbXBvcnQgeyBtb2RhbEFjdGlvbnMgfSBmcm9tICd+L3N0b3JlL21vZGFsJztcclxuXHJcbmltcG9ydCB7IHNob3dTY3JvbGxUb3BIYW5kbGVyLCBzY3JvbGxUb3BIYW5kbGVyLCBzdGlja3lIZWFkZXJIYW5kbGVyLCBzdGlja3lGb290ZXJIYW5kbGVyLCByZXNpemVIYW5kbGVyIH0gZnJvbSAnfi91dGlscyc7XHJcblxyXG5mdW5jdGlvbiBMYXlvdXQoIHsgY2hpbGRyZW4sIGNsb3NlUXVpY2t2aWV3IH0gKSB7XHJcbiAgICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuXHJcbiAgICB1c2VMYXlvdXRFZmZlY3QoICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnYm9keScgKSAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnYm9keScgKS5jbGFzc0xpc3QucmVtb3ZlKCAnbG9hZGVkJyApO1xyXG4gICAgfSwgWyByb3V0ZXIucGF0aG5hbWUgXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdzY3JvbGwnLCBzaG93U2Nyb2xsVG9wSGFuZGxlciwgdHJ1ZSApO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnc2Nyb2xsJywgc3RpY2t5SGVhZGVySGFuZGxlciwgdHJ1ZSApO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnc2Nyb2xsJywgc3RpY2t5Rm9vdGVySGFuZGxlciwgdHJ1ZSApO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAncmVzaXplJywgc3RpY2t5SGVhZGVySGFuZGxlciApO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAncmVzaXplJywgc3RpY2t5Rm9vdGVySGFuZGxlciApO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAncmVzaXplJywgcmVzaXplSGFuZGxlciApO1xyXG5cclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ3Njcm9sbCcsIHNob3dTY3JvbGxUb3BIYW5kbGVyLCB0cnVlICk7XHJcbiAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAnc2Nyb2xsJywgc3RpY2t5SGVhZGVySGFuZGxlciwgdHJ1ZSApO1xyXG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ3Njcm9sbCcsIHN0aWNreUZvb3RlckhhbmRsZXIsIHRydWUgKTtcclxuICAgICAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoICdyZXNpemUnLCBzdGlja3lIZWFkZXJIYW5kbGVyICk7XHJcbiAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCAncmVzaXplJywgc3RpY2t5Rm9vdGVySGFuZGxlciApO1xyXG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lciggJ3Jlc2l6ZScsIHJlc2l6ZUhhbmRsZXIgKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgY2xvc2VRdWlja3ZpZXcoKTtcclxuXHJcbiAgICAgICAgbGV0IGJvZHlDbGFzc2VzID0gWyAuLi5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcImJvZHlcIiApLmNsYXNzTGlzdCBdO1xyXG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGJvZHlDbGFzc2VzLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnYm9keScgKS5jbGFzc0xpc3QucmVtb3ZlKCBib2R5Q2xhc3Nlc1sgaSBdICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdib2R5JyApLmNsYXNzTGlzdC5hZGQoICdsb2FkZWQnICk7XHJcbiAgICAgICAgfSwgNTAgKTtcclxuICAgIH0sIFsgcm91dGVyLnBhdGhuYW1lIF0gKVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPD5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYWdlLXdyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgIDxIZWFkZXIgLz5cclxuXHJcbiAgICAgICAgICAgICAgICB7IGNoaWxkcmVuIH1cclxuXHJcbiAgICAgICAgICAgICAgICA8Rm9vdGVyIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgPFN0aWNreUZvb3RlciAvPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDxBTGluayBpZD1cInNjcm9sbC10b3BcIiBocmVmPVwiI1wiIHRpdGxlPVwiVG9wXCIgcm9sZT1cImJ1dHRvblwiIGNsYXNzTmFtZT1cInNjcm9sbC10b3BcIiBvbkNsaWNrPXsgKCkgPT4gc2Nyb2xsVG9wSGFuZGxlciggZmFsc2UgKSB9PjxpIGNsYXNzTmFtZT1cImQtaWNvbi1hcnJvdy11cFwiPjwvaT48L0FMaW5rPlxyXG5cclxuICAgICAgICAgICAgPE1vYmlsZU1lbnUgLz5cclxuXHJcbiAgICAgICAgICAgIDxUb2FzdENvbnRhaW5lclxyXG4gICAgICAgICAgICAgICAgYXV0b0Nsb3NlPXsgMzAwMCB9XHJcbiAgICAgICAgICAgICAgICBkdXJhdGlvbj17IDMwMCB9XHJcbiAgICAgICAgICAgICAgICBuZXdlc3RPblRvPXsgdHJ1ZSB9XHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ0b2FzdC1jb250YWluZXJcIlxyXG4gICAgICAgICAgICAgICAgcG9zaXRpb249XCJib3R0b20tbGVmdFwiXHJcbiAgICAgICAgICAgICAgICBjbG9zZUJ1dHRvbj17IGZhbHNlIH1cclxuICAgICAgICAgICAgICAgIGhpZGVQcm9ncmVzc0Jhcj17IHRydWUgfVxyXG4gICAgICAgICAgICAgICAgbmV3ZXN0T25Ub3A9eyB0cnVlIH1cclxuICAgICAgICAgICAgLz5cclxuXHJcbiAgICAgICAgICAgIDxRdWlja3ZpZXcgLz5cclxuXHJcbiAgICAgICAgICAgIDxWaWRlb01vZGFsIC8+XHJcbiAgICAgICAgPC8+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QoIG51bGwsIHsgY2xvc2VRdWlja3ZpZXc6IG1vZGFsQWN0aW9ucy5jbG9zZVF1aWNrdmlldyB9ICkoIExheW91dCApOyIsImltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJztcclxuaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBDb2xsYXBzZSBmcm9tICdyZWFjdC1ib290c3RyYXAvY29sbGFwc2UnO1xyXG5cclxuaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcbmltcG9ydCBDb3VudGRvd24gZnJvbSAnfi9jb21wb25lbnRzL2ZlYXR1cmVzL2NvdW50ZG93bic7XHJcbmltcG9ydCBRdWFudGl0eSBmcm9tICd+L2NvbXBvbmVudHMvZmVhdHVyZXMvcXVhbnRpdHknO1xyXG5cclxuaW1wb3J0IFByb2R1Y3ROYXYgZnJvbSAnfi9jb21wb25lbnRzL3BhcnRpYWxzL3Byb2R1Y3QvcHJvZHVjdC1uYXYnO1xyXG5cclxuaW1wb3J0IHsgd2lzaGxpc3RBY3Rpb25zIH0gZnJvbSAnfi9zdG9yZS93aXNobGlzdCc7XHJcbmltcG9ydCB7IGNhcnRBY3Rpb25zIH0gZnJvbSAnfi9zdG9yZS9jYXJ0JztcclxuXHJcbmltcG9ydCB7IHRvRGVjaW1hbCB9IGZyb20gJ34vdXRpbHMnO1xyXG5cclxuZnVuY3Rpb24gRGV0YWlsT25lKCBwcm9wcyApIHtcclxuICAgIGxldCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuICAgIGNvbnN0IHsgZGF0YSwgaXNTdGlja3lDYXJ0ID0gZmFsc2UsIGFkQ2xhc3MgPSAnJywgaXNOYXYgPSB0cnVlIH0gPSBwcm9wcztcclxuICAgIGNvbnN0IHsgdG9nZ2xlV2lzaGxpc3QsIGFkZFRvQ2FydCwgd2lzaGxpc3QgfSA9IHByb3BzO1xyXG4gICAgY29uc3QgWyBjdXJDb2xvciwgc2V0Q3VyQ29sb3IgXSA9IHVzZVN0YXRlKCAnbnVsbCcgKTtcclxuICAgIGNvbnN0IFsgY3VyU2l6ZSwgc2V0Q3VyU2l6ZSBdID0gdXNlU3RhdGUoICdudWxsJyApO1xyXG4gICAgY29uc3QgWyBjdXJJbmRleCwgc2V0Q3VySW5kZXggXSA9IHVzZVN0YXRlKCAtMSApO1xyXG4gICAgY29uc3QgWyBjYXJ0QWN0aXZlLCBzZXRDYXJ0QWN0aXZlIF0gPSB1c2VTdGF0ZSggZmFsc2UgKTtcclxuICAgIGNvbnN0IFsgcXVhbnRpdHksIHNldFFhdW50aXR5IF0gPSB1c2VTdGF0ZSggMSApO1xyXG4gICAgbGV0IHByb2R1Y3QgPSBkYXRhICYmIGRhdGEucHJvZHVjdDtcclxuXHJcbiAgICAvLyBkZWNpZGUgaWYgdGhlIHByb2R1Y3QgaXMgd2lzaGxpc3RlZFxyXG4gICAgbGV0IGlzV2lzaGxpc3RlZCwgY29sb3JzID0gW10sIHNpemVzID0gW107XHJcbiAgICBpc1dpc2hsaXN0ZWQgPSB3aXNobGlzdC5maW5kSW5kZXgoIGl0ZW0gPT4gaXRlbS5zbHVnID09PSBwcm9kdWN0LmRhdGEuc2x1ZyApID4gLTEgPyB0cnVlIDogZmFsc2U7XHJcblxyXG4gICAgaWYgKCBwcm9kdWN0LmRhdGEgJiYgcHJvZHVjdC5kYXRhLnZhcmlhbnRzLmxlbmd0aCA+IDAgKSB7XHJcbiAgICAgICAgaWYgKCBwcm9kdWN0LmRhdGEudmFyaWFudHNbIDAgXS5zaXplIClcclxuICAgICAgICAgICAgcHJvZHVjdC5kYXRhLnZhcmlhbnRzLmZvckVhY2goIGl0ZW0gPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBzaXplcy5maW5kSW5kZXgoIHNpemUgPT4gc2l6ZS5uYW1lID09PSBpdGVtLnNpemUubmFtZSApID09PSAtMSApIHtcclxuICAgICAgICAgICAgICAgICAgICBzaXplcy5wdXNoKCB7IG5hbWU6IGl0ZW0uc2l6ZS5uYW1lLCB2YWx1ZTogaXRlbS5zaXplLnNpemUgfSApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9ICk7XHJcblxyXG4gICAgICAgIGlmICggcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyAwIF0uY29sb3IgKSB7XHJcbiAgICAgICAgICAgIHByb2R1Y3QuZGF0YS52YXJpYW50cy5mb3JFYWNoKCBpdGVtID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICggY29sb3JzLmZpbmRJbmRleCggY29sb3IgPT4gY29sb3IubmFtZSA9PT0gaXRlbS5jb2xvci5uYW1lICkgPT09IC0xIClcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcnMucHVzaCggeyBuYW1lOiBpdGVtLmNvbG9yLm5hbWUsIHZhbHVlOiBpdGVtLmNvbG9yLmNvbG9yIH0gKTtcclxuICAgICAgICAgICAgfSApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1c2VFZmZlY3QoICgpID0+IHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICBzZXRDdXJJbmRleCggLTEgKTtcclxuICAgICAgICAgICAgcmVzZXRWYWx1ZUhhbmRsZXIoKTtcclxuICAgICAgICB9XHJcbiAgICB9LCBbIHByb2R1Y3QgXSApXHJcblxyXG4gICAgdXNlRWZmZWN0KCAoKSA9PiB7XHJcbiAgICAgICAgaWYgKCBwcm9kdWN0LmRhdGEudmFyaWFudHMubGVuZ3RoID4gMCApIHtcclxuICAgICAgICAgICAgaWYgKCAoIGN1clNpemUgIT09ICdudWxsJyAmJiBjdXJDb2xvciAhPT0gJ251bGwnICkgfHwgKCBjdXJTaXplID09PSAnbnVsbCcgJiYgcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyAwIF0uc2l6ZSA9PT0gbnVsbCAmJiBjdXJDb2xvciAhPT0gJ251bGwnICkgfHwgKCBjdXJDb2xvciA9PT0gJ251bGwnICYmIHByb2R1Y3QuZGF0YS52YXJpYW50c1sgMCBdLmNvbG9yID09PSBudWxsICYmIGN1clNpemUgIT09ICdudWxsJyApICkge1xyXG4gICAgICAgICAgICAgICAgc2V0Q2FydEFjdGl2ZSggdHJ1ZSApO1xyXG4gICAgICAgICAgICAgICAgc2V0Q3VySW5kZXgoIHByb2R1Y3QuZGF0YS52YXJpYW50cy5maW5kSW5kZXgoIGl0ZW0gPT4gKCBpdGVtLnNpemUgIT09IG51bGwgJiYgaXRlbS5jb2xvciAhPT0gbnVsbCAmJiBpdGVtLmNvbG9yLm5hbWUgPT09IGN1ckNvbG9yICYmIGl0ZW0uc2l6ZS5uYW1lID09PSBjdXJTaXplICkgfHwgKCBpdGVtLnNpemUgPT09IG51bGwgJiYgaXRlbS5jb2xvci5uYW1lID09PSBjdXJDb2xvciApIHx8ICggaXRlbS5jb2xvciA9PT0gbnVsbCAmJiBpdGVtLnNpemUubmFtZSA9PT0gY3VyU2l6ZSApICkgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHNldENhcnRBY3RpdmUoIGZhbHNlICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZXRDYXJ0QWN0aXZlKCB0cnVlICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIHByb2R1Y3Quc3RvY2sgPT09IDAgKSB7XHJcbiAgICAgICAgICAgIHNldENhcnRBY3RpdmUoIGZhbHNlICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgWyBjdXJDb2xvciwgY3VyU2l6ZSwgcHJvZHVjdCBdIClcclxuXHJcbiAgICBjb25zdCB3aXNobGlzdEhhbmRsZXIgPSAoIGUgKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBpZiAoIHRvZ2dsZVdpc2hsaXN0ICYmICFpc1dpc2hsaXN0ZWQgKSB7XHJcbiAgICAgICAgICAgIGxldCBjdXJyZW50VGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0O1xyXG4gICAgICAgICAgICBjdXJyZW50VGFyZ2V0LmNsYXNzTGlzdC5hZGQoICdsb2FkLW1vcmUtb3ZlcmxheScsICdsb2FkaW5nJyApO1xyXG4gICAgICAgICAgICB0b2dnbGVXaXNobGlzdCggcHJvZHVjdC5kYXRhICk7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50VGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoICdsb2FkLW1vcmUtb3ZlcmxheScsICdsb2FkaW5nJyApO1xyXG4gICAgICAgICAgICB9LCAxMDAwICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcm91dGVyLnB1c2goICcvcGFnZXMvd2lzaGxpc3QnICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHNldENvbG9ySGFuZGxlciA9ICggZSApID0+IHtcclxuICAgICAgICBzZXRDdXJDb2xvciggZS50YXJnZXQudmFsdWUgKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBzZXRTaXplSGFuZGxlciA9ICggZSApID0+IHtcclxuICAgICAgICBzZXRDdXJTaXplKCBlLnRhcmdldC52YWx1ZSApO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGFkZFRvQ2FydEhhbmRsZXIgPSAoKSA9PiB7XHJcbiAgICAgICAgaWYgKCBwcm9kdWN0LmRhdGEuc3RvY2sgPiAwICYmIGNhcnRBY3RpdmUgKSB7XHJcbiAgICAgICAgICAgIGlmICggcHJvZHVjdC5kYXRhLnZhcmlhbnRzLmxlbmd0aCA+IDAgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdG1wTmFtZSA9IHByb2R1Y3QuZGF0YS5uYW1lLCB0bXBQcmljZTtcclxuICAgICAgICAgICAgICAgIHRtcE5hbWUgKz0gY3VyQ29sb3IgIT09ICdudWxsJyA/ICctJyArIGN1ckNvbG9yIDogJyc7XHJcbiAgICAgICAgICAgICAgICB0bXBOYW1lICs9IGN1clNpemUgIT09ICdudWxsJyA/ICctJyArIGN1clNpemUgOiAnJztcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIHByb2R1Y3QuZGF0YS5wcmljZVsgMCBdID09PSBwcm9kdWN0LmRhdGEucHJpY2VbIDEgXSApIHtcclxuICAgICAgICAgICAgICAgICAgICB0bXBQcmljZSA9IHByb2R1Y3QuZGF0YS5wcmljZVsgMCBdO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICggIXByb2R1Y3QuZGF0YS52YXJpYW50c1sgMCBdLnByaWNlICYmIHByb2R1Y3QuZGF0YS5kaXNjb3VudCA+IDAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG1wUHJpY2UgPSBwcm9kdWN0LmRhdGEucHJpY2VbIDAgXTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdG1wUHJpY2UgPSBwcm9kdWN0LmRhdGEudmFyaWFudHNbIGN1ckluZGV4IF0uc2FsZV9wcmljZSA/IHByb2R1Y3QuZGF0YS52YXJpYW50c1sgY3VySW5kZXggXS5zYWxlX3ByaWNlIDogcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyBjdXJJbmRleCBdLnByaWNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGFkZFRvQ2FydCggeyAuLi5wcm9kdWN0LmRhdGEsIG5hbWU6IHRtcE5hbWUsIHF0eTogcXVhbnRpdHksIHByaWNlOiB0bXBQcmljZSB9ICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBhZGRUb0NhcnQoIHsgLi4ucHJvZHVjdC5kYXRhLCBxdHk6IHF1YW50aXR5LCBwcmljZTogcHJvZHVjdC5kYXRhLnByaWNlWyAwIF0gfSApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHJlc2V0VmFsdWVIYW5kbGVyID0gKCBlICkgPT4ge1xyXG4gICAgICAgIHNldEN1ckNvbG9yKCAnbnVsbCcgKTtcclxuICAgICAgICBzZXRDdXJTaXplKCAnbnVsbCcgKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBpc0Rpc2FibGVkKCBjb2xvciwgc2l6ZSApIHtcclxuICAgICAgICBpZiAoIGNvbG9yID09PSAnbnVsbCcgfHwgc2l6ZSA9PT0gJ251bGwnICkgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAoIHNpemVzLmxlbmd0aCA9PT0gMCApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHByb2R1Y3QuZGF0YS52YXJpYW50cy5maW5kSW5kZXgoIGl0ZW0gPT4gaXRlbS5jb2xvci5uYW1lID09PSBjdXJDb2xvciApID09PSAtMTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggY29sb3JzLmxlbmd0aCA9PT0gMCApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHByb2R1Y3QuZGF0YS52YXJpYW50cy5maW5kSW5kZXgoIGl0ZW0gPT4gaXRlbS5zaXplLm5hbWUgPT09IGN1clNpemUgKSA9PT0gLTE7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcHJvZHVjdC5kYXRhLnZhcmlhbnRzLmZpbmRJbmRleCggaXRlbSA9PiBpdGVtLmNvbG9yLm5hbWUgPT09IGNvbG9yICYmIGl0ZW0uc2l6ZS5uYW1lID09PSBzaXplICkgPT09IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIGNoYW5nZVF0eSggcXR5ICkge1xyXG4gICAgICAgIHNldFFhdW50aXR5KCBxdHkgKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXsgXCJwcm9kdWN0LWRldGFpbHMgXCIgKyBhZENsYXNzIH0+XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGlzTmF2ID9cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtbmF2aWdhdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwiYnJlYWRjcnVtYiBicmVhZGNydW1iLWxnXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+PEFMaW5rIGhyZWY9XCIvXCI+PGkgY2xhc3NOYW1lPVwiZC1pY29uLWhvbWVcIj48L2k+PC9BTGluaz48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxBTGluayBocmVmPVwiI1wiIGNsYXNzTmFtZT1cImFjdGl2ZVwiPlByb2R1Y3RzPC9BTGluaz48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPkRldGFpbDwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8UHJvZHVjdE5hdiBwcm9kdWN0PXsgcHJvZHVjdCB9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+IDogJydcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgPGgyIGNsYXNzTmFtZT1cInByb2R1Y3QtbmFtZVwiPnsgcHJvZHVjdC5kYXRhLm5hbWUgfTwvaDI+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0ncHJvZHVjdC1tZXRhJz5cclxuICAgICAgICAgICAgICAgIFNLVTogPHNwYW4gY2xhc3NOYW1lPSdwcm9kdWN0LXNrdSc+eyBwcm9kdWN0LmRhdGEuc2t1IH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICBDQVRFR09SSUVTOiA8c3BhbiBjbGFzc05hbWU9J3Byb2R1Y3QtYnJhbmQnPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC5kYXRhLmNhdGVnb3JpZXMubWFwKCAoIGl0ZW0sIGluZGV4ICkgPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSZWFjdC5GcmFnbWVudCBrZXk9eyBpdGVtLm5hbWUgKyAnLScgKyBpbmRleCB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgeyBwYXRobmFtZTogJy9zaG9wJywgcXVlcnk6IHsgY2F0ZWdvcnk6IGl0ZW0uc2x1ZyB9IH0gfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpdGVtLm5hbWUgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpbmRleCA8IHByb2R1Y3QuZGF0YS5jYXRlZ29yaWVzLmxlbmd0aCAtIDEgPyAnLCAnIDogJycgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKSB9XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LXByaWNlIG1iLTJcIj5cclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LmRhdGEucHJpY2VbIDAgXSAhPT0gcHJvZHVjdC5kYXRhLnByaWNlWyAxIF0gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LmRhdGEudmFyaWFudHMubGVuZ3RoID09PSAwIHx8ICggcHJvZHVjdC5kYXRhLnZhcmlhbnRzLmxlbmd0aCA+IDAgJiYgIXByb2R1Y3QuZGF0YS52YXJpYW50c1sgMCBdLnByaWNlICkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5zIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnByaWNlWyAwIF0gKSB9PC9pbnM+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRlbCBjbGFzc05hbWU9XCJvbGQtcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QuZGF0YS5wcmljZVsgMSBdICkgfTwvZGVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IGRlbCBjbGFzc05hbWU9XCJuZXctcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QuZGF0YS5wcmljZVsgMCBdICkgfSDigJMgJHsgdG9EZWNpbWFsKCBwcm9kdWN0LmRhdGEucHJpY2VbIDEgXSApIH08L2RlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgOiA8aW5zIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnByaWNlWyAwIF0gKSB9PC9pbnM+XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcHJvZHVjdC5kYXRhLnByaWNlWyAwIF0gIT09IHByb2R1Y3QuZGF0YS5wcmljZVsgMSBdICYmIHByb2R1Y3QuZGF0YS52YXJpYW50cy5sZW5ndGggPT09IDAgP1xyXG4gICAgICAgICAgICAgICAgICAgIDxDb3VudGRvd24gdHlwZT17IDIgfSAvPiA6ICcnXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0aW5ncy1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0aW5ncy1mdWxsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicmF0aW5nc1wiIHN0eWxlPXsgeyB3aWR0aDogMjAgKiBwcm9kdWN0LmRhdGEucmF0aW5ncyArICclJyB9IH0+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInRvb2x0aXB0ZXh0IHRvb2x0aXAtdG9wXCI+eyB0b0RlY2ltYWwoIHByb2R1Y3QuZGF0YS5yYXRpbmdzICkgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiIGNsYXNzTmFtZT1cInJhdGluZy1yZXZpZXdzXCI+KCB7IHByb2R1Y3QuZGF0YS5yZXZpZXdzIH0gcmV2aWV3cyApPC9BTGluaz5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJwcm9kdWN0LXNob3J0LWRlc2NcIj57IHByb2R1Y3QuZGF0YS5zaG9ydF9kZXNjcmlwdGlvbiB9PC9wPlxyXG5cclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcHJvZHVjdCAmJiBwcm9kdWN0LmRhdGEudmFyaWFudHMubGVuZ3RoID4gMCA/XHJcbiAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyAwIF0uY29sb3IgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdwcm9kdWN0LWZvcm0gcHJvZHVjdC12YXJpYXRpb25zIHByb2R1Y3QtY29sb3InPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+Q29sb3I6PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J3NlbGVjdC1ib3gnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCBuYW1lPSdjb2xvcicgY2xhc3NOYW1lPSdmb3JtLWNvbnRyb2wgc2VsZWN0LWNvbG9yJyBvbkNoYW5nZT17IHNldENvbG9ySGFuZGxlciB9IHZhbHVlPXsgY3VyQ29sb3IgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwibnVsbFwiPkNob29zZSBhbiBvcHRpb248L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9ycy5tYXAoIGl0ZW0gPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICFpc0Rpc2FibGVkKCBpdGVtLm5hbWUsIGN1clNpemUgKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT17IGl0ZW0ubmFtZSB9IGtleT17IFwiY29sb3ItXCIgKyBpdGVtLm5hbWUgfT57IGl0ZW0ubmFtZSB9PC9vcHRpb24+IDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gOiBcIlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2R1Y3QuZGF0YS52YXJpYW50c1sgMCBdLnNpemUgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdwcm9kdWN0LWZvcm0gcHJvZHVjdC12YXJpYXRpb25zIHByb2R1Y3Qtc2l6ZSBtYi0wIHBiLTInPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWw+U2l6ZTo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0ncHJvZHVjdC1mb3JtLWdyb3VwJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPSdzZWxlY3QtYm94Jz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c2VsZWN0IG5hbWU9J3NpemUnIGNsYXNzTmFtZT0nZm9ybS1jb250cm9sIHNlbGVjdC1zaXplJyBvbkNoYW5nZT17IHNldFNpemVIYW5kbGVyIH0gdmFsdWU9eyBjdXJTaXplIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJudWxsXCI+Q2hvb3NlIGFuIG9wdGlvbjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaXplcy5tYXAoIGl0ZW0gPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAhaXNEaXNhYmxlZCggY3VyQ29sb3IsIGl0ZW0ubmFtZSApID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT17IGl0ZW0ubmFtZSB9IGtleT17IFwic2l6ZS1cIiArIGl0ZW0ubmFtZSB9PnsgaXRlbS5uYW1lIH08L29wdGlvbj4gOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sbGFwc2UgaW49eyAnbnVsbCcgIT09IGN1ckNvbG9yIHx8ICdudWxsJyAhPT0gY3VyU2l6ZSB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC13cmFwcGVyIG92ZXJmbG93LWhpZGRlbiByZXNldC12YWx1ZS1idXR0b24gdy0xMDAgbWItMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj0nIycgY2xhc3NOYW1lPSdwcm9kdWN0LXZhcmlhdGlvbi1jbGVhbicgb25DbGljaz17IHJlc2V0VmFsdWVIYW5kbGVyIH0+Q2xlYW4gQWxsPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQ29sbGFwc2U+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiA6IFwiXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9J3Byb2R1Y3QtdmFyaWF0aW9uLXByaWNlJz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDb2xsYXBzZSBpbj17IGNhcnRBY3RpdmUgJiYgY3VySW5kZXggPiAtMSB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC13cmFwcGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1ckluZGV4ID4gLTEgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2luZ2xlLXByb2R1Y3QtcHJpY2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyBjdXJJbmRleCBdLnByaWNlID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LmRhdGEudmFyaWFudHNbIGN1ckluZGV4IF0uc2FsZV9wcmljZSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1wcmljZSBtYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5zIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyBjdXJJbmRleCBdLnNhbGVfcHJpY2UgKSB9PC9pbnM+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGVsIGNsYXNzTmFtZT1cIm9sZC1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyBjdXJJbmRleCBdLnByaWNlICkgfTwvZGVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtcHJpY2UgbWItMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucyBjbGFzc05hbWU9XCJuZXctcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QuZGF0YS52YXJpYW50c1sgY3VySW5kZXggXS5wcmljZSApIH08L2lucz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBcIlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NvbGxhcHNlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8Lz4gOiAnJ1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICA8aHIgY2xhc3NOYW1lPVwicHJvZHVjdC1kaXZpZGVyXCI+PC9ocj5cclxuXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGlzU3RpY2t5Q2FydCA/XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzdGlja3ktY29udGVudCBmaXgtdG9wIHByb2R1Y3Qtc3RpY2t5LWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3RpY2t5LXByb2R1Y3QtZGV0YWlsc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmaWd1cmUgY2xhc3NOYW1lPVwicHJvZHVjdC1pbWFnZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj17ICcvcHJvZHVjdC9kZWZhdWx0LycgKyBwcm9kdWN0LmRhdGEuc2x1ZyB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9eyBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BU1NFVF9VUkkgKyBwcm9kdWN0LmRhdGEucGljdHVyZXNbIDAgXS51cmwgfSB3aWR0aD1cIjkwXCIgaGVpZ2h0PVwiOTBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlByb2R1Y3RcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZmlndXJlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJwcm9kdWN0LXRpdGxlXCI+PEFMaW5rIGhyZWY9eyAnL3Byb2R1Y3QvZGVmYXVsdC8nICsgcHJvZHVjdC5kYXRhLnNsdWcgfT57IHByb2R1Y3QuZGF0YS5uYW1lIH08L0FMaW5rPjwvaDQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1pbmZvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtcHJpY2UgbWItMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VySW5kZXggPiAtMSAmJiBwcm9kdWN0LmRhdGEudmFyaWFudHNbIDAgXSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0LmRhdGEudmFyaWFudHNbIGN1ckluZGV4IF0ucHJpY2UgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2R1Y3QuZGF0YS52YXJpYW50c1sgY3VySW5kZXggXS5zYWxlX3ByaWNlID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnMgY2xhc3NOYW1lPVwibmV3LXByaWNlXCI+JHsgdG9EZWNpbWFsKCBwcm9kdWN0LmRhdGEudmFyaWFudHNbIGN1ckluZGV4IF0uc2FsZV9wcmljZSApIH08L2lucz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkZWwgY2xhc3NOYW1lPVwib2xkLXByaWNlXCI+JHsgdG9EZWNpbWFsKCBwcm9kdWN0LmRhdGEudmFyaWFudHNbIGN1ckluZGV4IF0ucHJpY2UgKSB9PC9kZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5zIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnZhcmlhbnRzWyBjdXJJbmRleCBdLnByaWNlICkgfTwvaW5zPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFwiXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2R1Y3QuZGF0YS5wcmljZVsgMCBdICE9PSBwcm9kdWN0LmRhdGEucHJpY2VbIDEgXSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdC5kYXRhLnZhcmlhbnRzLmxlbmd0aCA9PT0gMCA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5zIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnByaWNlWyAwIF0gKSB9PC9pbnM+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGVsIGNsYXNzTmFtZT1cIm9sZC1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnByaWNlWyAxIF0gKSB9PC9kZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwgZGVsIGNsYXNzTmFtZT1cIm5ldy1wcmljZVwiPiR7IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnByaWNlWyAwIF0gKSB9IOKAkyAkeyB0b0RlY2ltYWwoIHByb2R1Y3QuZGF0YS5wcmljZVsgMSBdICkgfTwvZGVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogPGlucyBjbGFzc05hbWU9XCJuZXctcHJpY2VcIj4keyB0b0RlY2ltYWwoIHByb2R1Y3QuZGF0YS5wcmljZVsgMCBdICkgfTwvaW5zPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0aW5ncy1jb250YWluZXIgbWItMFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmF0aW5ncy1mdWxsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInJhdGluZ3NcIiBzdHlsZT17IHsgd2lkdGg6IDIwICogcHJvZHVjdC5kYXRhLnJhdGluZ3MgKyAnJScgfSB9Pjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidG9vbHRpcHRleHQgdG9vbHRpcC10b3BcIj57IHRvRGVjaW1hbCggcHJvZHVjdC5kYXRhLnJhdGluZ3MgKSB9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIiBjbGFzc05hbWU9XCJyYXRpbmctcmV2aWV3c1wiPiggeyBwcm9kdWN0LmRhdGEucmV2aWV3cyB9IHJldmlld3MgKTwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1mb3JtIHByb2R1Y3QtcXR5IHBiLTBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwiZC1ub25lXCI+UVRZOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0LWZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFF1YW50aXR5IG1heD17IHByb2R1Y3QuZGF0YS5zdG9jayB9IHByb2R1Y3Q9eyBwcm9kdWN0IH0gb25DaGFuZ2VRdHk9eyBjaGFuZ2VRdHkgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT17IGBidG4tcHJvZHVjdCBidG4tY2FydCB0ZXh0LW5vcm1hbCBscy1ub3JtYWwgZm9udC13ZWlnaHQtc2VtaS1ib2xkICR7IGNhcnRBY3RpdmUgPyAnJyA6ICdkaXNhYmxlZCcgfWAgfSBvbkNsaWNrPXsgYWRkVG9DYXJ0SGFuZGxlciB9PjxpIGNsYXNzTmFtZT0nZC1pY29uLWJhZyc+PC9pPkFkZCB0byBDYXJ0PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgOlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1mb3JtIHByb2R1Y3QtcXR5IHBiLTBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImQtbm9uZVwiPlFUWTo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3QtZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFF1YW50aXR5IG1heD17IHByb2R1Y3QuZGF0YS5zdG9jayB9IHByb2R1Y3Q9eyBwcm9kdWN0IH0gb25DaGFuZ2VRdHk9eyBjaGFuZ2VRdHkgfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9eyBgYnRuLXByb2R1Y3QgYnRuLWNhcnQgdGV4dC1ub3JtYWwgbHMtbm9ybWFsIGZvbnQtd2VpZ2h0LXNlbWktYm9sZCAkeyBjYXJ0QWN0aXZlID8gJycgOiAnZGlzYWJsZWQnIH1gIH0gb25DbGljaz17IGFkZFRvQ2FydEhhbmRsZXIgfT48aSBjbGFzc05hbWU9J2QtaWNvbi1iYWcnPjwvaT5BZGQgdG8gQ2FydDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgPGhyIGNsYXNzTmFtZT1cInByb2R1Y3QtZGl2aWRlciBtYi0zXCI+PC9ocj5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdC1mb290ZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic29jaWFsLWxpbmtzIG1yLTRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIiBjbGFzc05hbWU9XCJzb2NpYWwtbGluayBzb2NpYWwtZmFjZWJvb2sgZmFiIGZhLWZhY2Vib29rLWZcIj48L0FMaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPVwiI1wiIGNsYXNzTmFtZT1cInNvY2lhbC1saW5rIHNvY2lhbC10d2l0dGVyIGZhYiBmYS10d2l0dGVyXCI+PC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj1cIiNcIiBjbGFzc05hbWU9XCJzb2NpYWwtbGluayBzb2NpYWwtcGludGVyZXN0IGZhYiBmYS1waW50ZXJlc3QtcFwiPjwvQUxpbms+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj4gPHNwYW4gY2xhc3NOYW1lPVwiZGl2aWRlciBkLWxnLXNob3dcIj48L3NwYW4+IDxhIGhyZWY9XCIjXCIgY2xhc3NOYW1lPXsgYGJ0bi1wcm9kdWN0IGJ0bi13aXNobGlzdGAgfSB0aXRsZT17IGlzV2lzaGxpc3RlZCA/ICdCcm93c2Ugd2lzaGxpc3QnIDogJ0FkZCB0byB3aXNobGlzdCcgfSBvbkNsaWNrPXsgd2lzaGxpc3RIYW5kbGVyIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPXsgaXNXaXNobGlzdGVkID8gXCJkLWljb24taGVhcnQtZnVsbFwiIDogXCJkLWljb24taGVhcnRcIiB9PjwvaT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc1dpc2hsaXN0ZWQgPyAnQnJvd3NlIHdpc2hsaXN0JyA6ICdBZGQgdG8gV2lzaGxpc3QnXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIClcclxufVxyXG5cclxuZnVuY3Rpb24gbWFwU3RhdGVUb1Byb3BzKCBzdGF0ZSApIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgd2lzaGxpc3Q6IHN0YXRlLndpc2hsaXN0LmRhdGEgPyBzdGF0ZS53aXNobGlzdC5kYXRhIDogW11cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdCggbWFwU3RhdGVUb1Byb3BzLCB7IHRvZ2dsZVdpc2hsaXN0OiB3aXNobGlzdEFjdGlvbnMudG9nZ2xlV2lzaGxpc3QsIGFkZFRvQ2FydDogY2FydEFjdGlvbnMuYWRkVG9DYXJ0IH0gKSggRGV0YWlsT25lICk7IiwiaW1wb3J0IEFMaW5rIGZyb20gJ34vY29tcG9uZW50cy9mZWF0dXJlcy9jdXN0b20tbGluayc7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFByb2R1Y3ROYXYgKCBwcm9wcyApIHtcclxuICAgIGNvbnN0IHsgcHJvZHVjdCB9ID0gcHJvcHM7XHJcblxyXG4gICAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8dWwgY2xhc3NOYW1lPVwicHJvZHVjdC1uYXZcIj5cclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcHJvZHVjdC5wcmV2ID9cclxuICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPXsgYHByb2R1Y3QtbmF2LSR7IHByb2R1Y3QubmV4dCA/ICdwcmV2JyA6ICduZXh0IG5vLW5leHQnIH1gIH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBTGluayBocmVmPXsgeyBwYXRobmFtZTogcm91dGVyLnBhdGhuYW1lLCBxdWVyeTogeyBzbHVnOiBwcm9kdWN0LnByZXYuc2x1ZyB9IH0gfSBzY3JvbGw9eyBmYWxzZSB9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZC1pY29uLWFycm93LWxlZnRcIj48L2k+IFByZXZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJvZHVjdC1uYXYtcG9wdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FTU0VUX1VSSSArIHByb2R1Y3QucHJldi5waWN0dXJlc1sgMCBdLnVybCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cInByb2R1Y3QgdGh1bWJuYWlsXCIgd2lkdGg9XCIxMTBcIiBoZWlnaHQ9XCIxMjNcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInByb2R1Y3QtbmFtZVwiPnsgcHJvZHVjdC5wcmV2Lm5hbWUgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPiA6IFwiXCJcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcHJvZHVjdC5uZXh0ID9cclxuICAgICAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwicHJvZHVjdC1uYXYtbmV4dFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8QUxpbmsgaHJlZj17IHsgcGF0aG5hbWU6IHJvdXRlci5wYXRobmFtZSwgcXVlcnk6IHsgc2x1ZzogcHJvZHVjdC5uZXh0LnNsdWcgfSB9IH0gc2Nyb2xsPXsgZmFsc2UgfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5leHQgPGkgY2xhc3NOYW1lPVwiZC1pY29uLWFycm93LXJpZ2h0XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJvZHVjdC1uYXYtcG9wdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FTU0VUX1VSSSArIHByb2R1Y3QubmV4dC5waWN0dXJlc1sgMCBdLnVybCB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cInByb2R1Y3QgdGh1bWJuYWlsXCIgd2lkdGg9XCIxMTBcIiBoZWlnaHQ9XCIxMjNcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInByb2R1Y3QtbmFtZVwiPnsgcHJvZHVjdC5uZXh0Lm5hbWUgfTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPiA6IFwiXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIDwvdWw+XHJcbiAgICApXHJcbn0iLCJpbXBvcnQgeyB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB3aXRoUmVkdXggZnJvbSBcIm5leHQtcmVkdXgtd3JhcHBlclwiO1xyXG5pbXBvcnQgeyBQcm92aWRlciB9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xyXG5pbXBvcnQgeyBQZXJzaXN0R2F0ZSB9IGZyb20gJ3JlZHV4LXBlcnNpc3QvaW50ZWdyYXRpb24vcmVhY3QnO1xyXG5pbXBvcnQgSGVsbWV0IGZyb20gXCJyZWFjdC1oZWxtZXRcIjtcclxuXHJcbmltcG9ydCBMYXlvdXQgZnJvbSAnfi9jb21wb25lbnRzL2xheW91dCc7XHJcblxyXG5pbXBvcnQgbWFrZVN0b3JlIGZyb20gXCJ+L3N0b3JlXCI7XHJcbmltcG9ydCB7IGRlbW9BY3Rpb25zIH0gZnJvbSAnfi9zdG9yZS9kZW1vJztcclxuXHJcbmltcG9ydCB7IGN1cnJlbnREZW1vIH0gZnJvbSAnfi9zZXJ2ZXIvcXVlcmllcyc7XHJcblxyXG5pbXBvcnQgXCJ+L3B1YmxpYy9zYXNzL3N0eWxlLnNjc3NcIjtcclxuXHJcbmNvbnN0IEFwcCA9ICh7IENvbXBvbmVudCwgcGFnZVByb3BzLCBzdG9yZSB9KSA9PiB7XHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIGlmIChzdG9yZS5nZXRTdGF0ZSgpLmRlbW8uY3VycmVudCAhPT0gY3VycmVudERlbW8pIHtcclxuICAgICAgICAgICAgc3RvcmUuZGlzcGF0Y2goZGVtb0FjdGlvbnMucmVmcmVzaFN0b3JlKGN1cnJlbnREZW1vKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgW10pXHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8UHJvdmlkZXIgc3RvcmU9e3N0b3JlfT5cclxuICAgICAgICAgICAgPFBlcnNpc3RHYXRlXHJcbiAgICAgICAgICAgICAgICBwZXJzaXN0b3I9e3N0b3JlLl9fcGVyc2lzdG9yfVxyXG4gICAgICAgICAgICAgICAgbG9hZGluZz17PGRpdiBjbGFzc05hbWU9XCJsb2FkaW5nLW92ZXJsYXlcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJvdW5jZS1sb2FkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJib3VuY2UxXCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYm91bmNlMlwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJvdW5jZTNcIj48L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJib3VuY2U0XCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj59PlxyXG4gICAgICAgICAgICAgICAgPEhlbG1ldD5cclxuICAgICAgICAgICAgICAgICAgICA8bWV0YSBjaGFyU2V0PVwiVVRGLThcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxtZXRhIGh0dHAtZXF1aXY9XCJYLVVBLUNvbXBhdGlibGVcIiBjb250ZW50PVwiSUU9ZWRnZVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPG1ldGEgbmFtZT1cInZpZXdwb3J0XCIgY29udGVudD1cIndpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xLCBzaHJpbmstdG8tZml0PW5vXCIgLz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPHRpdGxlPlJpb2RlIC0gUmVhY3QgZUNvbW1lcmNlIFRlbXBsYXRlPC90aXRsZT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPG1ldGEgbmFtZT1cImtleXdvcmRzXCIgY29udGVudD1cIlJlYWN0IFRlbXBsYXRlXCIgLz5cclxuICAgICAgICAgICAgICAgICAgICA8bWV0YSBuYW1lPVwiZGVzY3JpcHRpb25cIiBjb250ZW50PVwiUmlvZGUgLSBSZWFjdCBlQ29tbWVyY2UgVGVtcGxhdGVcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxtZXRhIG5hbWU9XCJhdXRob3JcIiBjb250ZW50PVwiRC1USEVNRVNcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9IZWxtZXQ+XHJcblxyXG4gICAgICAgICAgICAgICAgPExheW91dD5cclxuICAgICAgICAgICAgICAgICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XHJcbiAgICAgICAgICAgICAgICA8L0xheW91dD5cclxuICAgICAgICAgICAgPC9QZXJzaXN0R2F0ZT5cclxuICAgICAgICA8L1Byb3ZpZGVyPlxyXG4gICAgKTtcclxufVxyXG5cclxuQXBwLmdldEluaXRpYWxQcm9wcyA9IGFzeW5jICh7IENvbXBvbmVudCwgY3R4IH0pID0+IHtcclxuICAgIGxldCBwYWdlUHJvcHMgPSB7fTtcclxuICAgIGlmIChDb21wb25lbnQuZ2V0SW5pdGlhbFByb3BzKSB7XHJcbiAgICAgICAgcGFnZVByb3BzID0gYXdhaXQgQ29tcG9uZW50LmdldEluaXRpYWxQcm9wcyhjdHgpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHsgcGFnZVByb3BzIH07XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCB3aXRoUmVkdXgobWFrZVN0b3JlKShBcHApOyIsImltcG9ydCB7IHBlcnNpc3RSZWR1Y2VyIH0gZnJvbSBcInJlZHV4LXBlcnNpc3RcIjtcclxuaW1wb3J0IHN0b3JhZ2UgZnJvbSAncmVkdXgtcGVyc2lzdC9saWIvc3RvcmFnZSc7XHJcblxyXG5leHBvcnQgY29uc3QgYWN0aW9uVHlwZXMgPSB7XHJcbiAgICBSZWZyZXNoU3RvcmU6IFwiUkVGUkVTSF9TVE9SRVwiXHJcbn07XHJcblxyXG5sZXQgaW5pdGlhbFN0YXRlID0ge1xyXG4gICAgY3VycmVudDogMVxyXG59O1xyXG5cclxuY29uc3QgZGVtb1JlZHVjZXIgPSAoIHN0YXRlID0gaW5pdGlhbFN0YXRlLCBhY3Rpb24gKSA9PiB7XHJcbiAgICBzd2l0Y2ggKCBhY3Rpb24udHlwZSApIHtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLlJlZnJlc2hTdG9yZTpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgY3VycmVudDogYWN0aW9uLnBheWxvYWQuY3VycmVudFxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGRlbW9BY3Rpb25zID0ge1xyXG4gICAgcmVmcmVzaFN0b3JlOiAoIGN1cnJlbnQgKSA9PiAoIHsgdHlwZTogYWN0aW9uVHlwZXMuUmVmcmVzaFN0b3JlLCBwYXlsb2FkOiB7IGN1cnJlbnQgfSB9IClcclxufTtcclxuXHJcbmNvbnN0IHBlcnNpc3RDb25maWcgPSB7XHJcbiAgICBrZXlQcmVmaXg6IFwicmlvZGUtXCIsXHJcbiAgICBrZXk6IFwiZGVtb1wiLFxyXG4gICAgc3RvcmFnZVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBwZXJzaXN0UmVkdWNlciggcGVyc2lzdENvbmZpZywgZGVtb1JlZHVjZXIgKTsiLCJpbXBvcnQgeyBjb21iaW5lUmVkdWNlcnMsIGNyZWF0ZVN0b3JlLCBhcHBseU1pZGRsZXdhcmUgfSBmcm9tICdyZWR1eCc7XHJcbmltcG9ydCBjcmVhdGVTYWdhTWlkZGxlV2FyZSBmcm9tICdyZWR1eC1zYWdhJztcclxuaW1wb3J0IHsgcGVyc2lzdFN0b3JlIH0gZnJvbSAncmVkdXgtcGVyc2lzdCc7XHJcblxyXG5pbXBvcnQgcm9vdFNhZ2EgZnJvbSAnfi9zdG9yZS9yb290LXNhZ2EnO1xyXG5cclxuaW1wb3J0IGNhcnRSZWR1Y2VyIGZyb20gJ34vc3RvcmUvY2FydCc7XHJcbmltcG9ydCBtb2RhbFJlZHVjZXIgZnJvbSAnfi9zdG9yZS9tb2RhbCc7XHJcbmltcG9ydCB3aXNobGlzdFJlZHVjZXIgZnJvbSAnfi9zdG9yZS93aXNobGlzdCc7XHJcbmltcG9ydCBkZW1vUmVkdWNlciBmcm9tICd+L3N0b3JlL2RlbW8nO1xyXG5cclxuY29uc3Qgc2FnYU1pZGRsZXdhcmUgPSBjcmVhdGVTYWdhTWlkZGxlV2FyZSgpO1xyXG5cclxuY29uc3Qgcm9vdFJlZHVjZXJzID0gY29tYmluZVJlZHVjZXJzKCB7XHJcbiAgICBjYXJ0OiBjYXJ0UmVkdWNlcixcclxuICAgIG1vZGFsOiBtb2RhbFJlZHVjZXIsXHJcbiAgICB3aXNobGlzdDogd2lzaGxpc3RSZWR1Y2VyLFxyXG4gICAgZGVtbzogZGVtb1JlZHVjZXJcclxufSApXHJcblxyXG5jb25zdCBtYWtlU3RvcmUgPSAoIGluaXRpYWxTdGF0ZSwgb3B0aW9ucyApID0+IHtcclxuICAgIGNvbnN0IHN0b3JlID0gY3JlYXRlU3RvcmUoIHJvb3RSZWR1Y2VycywgYXBwbHlNaWRkbGV3YXJlKCBzYWdhTWlkZGxld2FyZSApICk7XHJcblxyXG4gICAgc3RvcmUuc2FnYVRhc2sgPSBzYWdhTWlkZGxld2FyZS5ydW4oIHJvb3RTYWdhICk7XHJcblxyXG4gICAgLy8gc3RvcmUuc2FnYVRhc2sgPSBzYWdhTWlkZGxld2FyZS5ydW4oIHJvb3RTYWdhICk7XHJcbiAgICBzdG9yZS5fX3BlcnNpc3RvciA9IHBlcnNpc3RTdG9yZSggc3RvcmUgKTtcclxuICAgIHJldHVybiBzdG9yZTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgbWFrZVN0b3JlO1xyXG4iLCJpbXBvcnQgeyBhbGwgfSBmcm9tICdyZWR1eC1zYWdhL2VmZmVjdHMnO1xyXG5cclxuaW1wb3J0IHsgY2FydFNhZ2EgfSBmcm9tICd+L3N0b3JlL2NhcnQnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24qIHJvb3RTYWdhKCkge1xyXG4gICAgeWllbGQgYWxsKCBbXHJcbiAgICAgICAgY2FydFNhZ2EoKVxyXG4gICAgXSApXHJcbn0iLCJleHBvcnQgY29uc3QgbWFpbk1lbnUgPSB7XHJcbiAgICBcInNob3BcIjoge1xyXG4gICAgICAgIFwidmFyaWF0aW9uMVwiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwidGl0bGVcIjogXCJJbmZpbml0ZSBBamF4c2Nyb2xsXCIsXHJcbiAgICAgICAgICAgICAgICBcInVybFwiOiBcInNob3AvaW5maW5pdGUtc2Nyb2xsXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH0sXHJcbiAgICBcInByb2R1Y3RcIjoge1xyXG4gICAgICAgIFwicGFnZXNcIjogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInRpdGxlXCI6IFwiU2ltcGxlIFByb2R1Y3RcIixcclxuICAgICAgICAgICAgICAgIFwidXJsXCI6IFwicHJvZHVjdC9kZWZhdWx0L2Zhc2hpb25hYmxlLW92ZXJuaWdodC1iYWdcIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXSxcclxuICAgICAgICBcImxheW91dFwiOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwidGl0bGVcIjogXCJHcmlkIEltYWdlc1wiLFxyXG4gICAgICAgICAgICAgICAgXCJ1cmxcIjogXCJwcm9kdWN0L2dyaWQvZmFzaGlvbmFibGUtbGVhdGhlci1zYXRjaGVsXCIsXHJcbiAgICAgICAgICAgICAgICBcIm5ld1wiOiB0cnVlXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9LFxyXG4gICAgXCJvdGhlclwiOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiQWJvdXRcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJwYWdlcy9hYm91dC11c1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJDb250YWN0IFVzXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwicGFnZXMvY29udGFjdC11c1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJNeSBBY2NvdW50XCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwicGFnZXMvYWNjb3VudFwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJGQVFzXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwicGFnZXMvZmFxc1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJFcnJvciA0MDRcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJwYWdlcy80MDRcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiQ29taW5nIFNvb25cIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJwYWdlcy9jb21pbmctc29vblwiXHJcbiAgICAgICAgfVxyXG4gICAgXSxcclxuICAgIFwiYmxvZ1wiOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiQ2xhc3NpY1wiLFxyXG4gICAgICAgICAgICBcInVybFwiOiBcImJsb2cvY2xhc3NpY1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJMaXN0aW5nXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiYmxvZy9saXN0aW5nXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIkdyaWRcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJibG9nL2dyaWQvMmNvbHNcIixcclxuICAgICAgICAgICAgXCJzdWJQYWdlc1wiOiBbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIkdyaWQgMiBjb2x1bW5zXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ1cmxcIjogXCJibG9nL2dyaWQvMmNvbHNcIlxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcInRpdGxlXCI6IFwiR3JpZCAzIGNvbHVtbnNcIixcclxuICAgICAgICAgICAgICAgICAgICBcInVybFwiOiBcImJsb2cvZ3JpZC8zY29sc1wiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwidGl0bGVcIjogXCJHcmlkIDQgY29sdW1uc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwidXJsXCI6IFwiYmxvZy9ncmlkLzRjb2xzXCJcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIkdyaWQgc2lkZWJhclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwidXJsXCI6IFwiYmxvZy9ncmlkL3NpZGViYXJcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJNYXNvbnJ5XCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiYmxvZy9tYXNvbnJ5LzJjb2xzXCIsXHJcbiAgICAgICAgICAgIFwic3ViUGFnZXNcIjogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwidGl0bGVcIjogXCJNYXNvbnJ5IDIgY29sdW1uc1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwidXJsXCI6IFwiYmxvZy9tYXNvbnJ5LzJjb2xzXCJcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIk1hc29ucnkgMyBjb2x1bW5zXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJ1cmxcIjogXCJibG9nL21hc29ucnkvM2NvbHNcIlxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcInRpdGxlXCI6IFwiTWFzb25yeSA0IGNvbHVtbnNcIixcclxuICAgICAgICAgICAgICAgICAgICBcInVybFwiOiBcImJsb2cvbWFzb25yeS80Y29sc1wiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwidGl0bGVcIjogXCJNYXNvbnJ5IHNpZGViYXJcIixcclxuICAgICAgICAgICAgICAgICAgICBcInVybFwiOiBcImJsb2cvbWFzb25yeS9zaWRlYmFyXCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiTWFza1wiLFxyXG4gICAgICAgICAgICBcInVybFwiOiBcImJsb2cvbWFzay9ncmlkXCIsXHJcbiAgICAgICAgICAgIFwic3ViUGFnZXNcIjogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwidGl0bGVcIjogXCJCbG9nIG1hc2sgZ3JpZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwidXJsXCI6IFwiYmxvZy9tYXNrL2dyaWRcIlxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcInRpdGxlXCI6IFwiQmxvZyBtYXNrIG1hc29ucnlcIixcclxuICAgICAgICAgICAgICAgICAgICBcInVybFwiOiBcImJsb2cvbWFzay9tYXNvbnJ5XCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiU2luZ2xlIFBvc3RcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJibG9nL3NpbmdsZS9wZWxsZW50ZXNxdWUtZnVzY2Utc3VzY2lwaXRcIlxyXG4gICAgICAgIH1cclxuICAgIF0sXHJcbiAgICBcImVsZW1lbnRcIjogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIlByb2R1Y3RzXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiZWxlbWVudHMvcHJvZHVjdHNcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiVHlwb2dyYXBoeVwiLFxyXG4gICAgICAgICAgICBcInVybFwiOiBcImVsZW1lbnRzL3R5cG9ncmFwaHlcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcInRpdGxlXCI6IFwiVGl0bGVzXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiZWxlbWVudHMvdGl0bGVzXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIlByb2R1Y3QgQ2F0ZWdvcnlcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJlbGVtZW50cy9wcm9kdWN0LWNhdGVnb3J5XCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIkJ1dHRvbnNcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJlbGVtZW50cy9idXR0b25zXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIkFjY29yZGlvbnNcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJlbGVtZW50cy9hY2NvcmRpb25zXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIkFsZXJ0ICYgTm90aWZpY2F0aW9uXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiZWxlbWVudHMvYWxlcnRzXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIlRhYnNcIixcclxuICAgICAgICAgICAgXCJ1cmxcIjogXCJlbGVtZW50cy90YWJzXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIlRlc3RpbW9uaWFsc1wiLFxyXG4gICAgICAgICAgICBcInVybFwiOiBcImVsZW1lbnRzL3Rlc3RpbW9uaWFsc1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJCbG9nIFBvc3RzXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiZWxlbWVudHMvYmxvZy1wb3N0c1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJJbnN0YWdyYW1zXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiZWxlbWVudHMvaW5zdGFncmFtc1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJDYWxsIHRvIEFjdGlvblwiLFxyXG4gICAgICAgICAgICBcInVybFwiOiBcImVsZW1lbnRzL2N0YVwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJJY29uIEJveGVzXCIsXHJcbiAgICAgICAgICAgIFwidXJsXCI6IFwiZWxlbWVudHMvaWNvbi1ib3hlc1wiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwidGl0bGVcIjogXCJJY29uc1wiLFxyXG4gICAgICAgICAgICBcInVybFwiOiBcImVsZW1lbnRzL2ljb25zXCJcclxuICAgICAgICB9XHJcbiAgICBdXHJcbn1cclxuXHJcblxyXG5leHBvcnQgY29uc3QgZWxlbWVudHNMaXN0ID0gW1xyXG4gICAge1xyXG4gICAgICAgIFwidXJsXCI6IFwiYWNjb3JkaW9uc1wiLFxyXG4gICAgICAgIFwiY2xhc3NcIjogXCJlbGVtZW50LWFjY29yZGlhblwiLFxyXG4gICAgICAgIFwidGl0bGVcIjogXCJhY2NvcmRpb25zXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJ1cmxcIjogXCJibG9nLXBvc3RzXCIsXHJcbiAgICAgICAgXCJjbGFzc1wiOiBcImVsZW1lbnQtYmxvZ1wiLFxyXG4gICAgICAgIFwidGl0bGVcIjogXCJibG9nIHBvc3RzXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJ1cmxcIjogXCJidXR0b25zXCIsXHJcbiAgICAgICAgXCJjbGFzc1wiOiBcImVsZW1lbnQtYnV0dG9uXCIsXHJcbiAgICAgICAgXCJ0aXRsZVwiOiBcImJ1dHRvbnNcIlxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcInVybFwiOiBcImN0YVwiLFxyXG4gICAgICAgIFwiY2xhc3NcIjogXCJlbGVtZW50LWN0YVwiLFxyXG4gICAgICAgIFwidGl0bGVcIjogXCJjYWxsIHRvIGFjdGlvblwiXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwidXJsXCI6IFwiaWNvbi1ib3hlc1wiLFxyXG4gICAgICAgIFwiY2xhc3NcIjogXCJlbGVtZW50LWljb24tYm94XCIsXHJcbiAgICAgICAgXCJ0aXRsZVwiOiBcImljb24gYm94ZXNcIlxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcInVybFwiOiBcImljb25zXCIsXHJcbiAgICAgICAgXCJjbGFzc1wiOiBcImVsZW1lbnQtaWNvblwiLFxyXG4gICAgICAgIFwidGl0bGVcIjogXCJJY29uc1wiXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwidXJsXCI6IFwiaW5zdGFncmFtc1wiLFxyXG4gICAgICAgIFwiY2xhc3NcIjogXCJlbGVtZW50LXBvcnRmb2xpb1wiLFxyXG4gICAgICAgIFwidGl0bGVcIjogXCJpbnN0YWdyYW1zXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJ1cmxcIjogXCJjYXRlZ29yaWVzXCIsXHJcbiAgICAgICAgXCJjbGFzc1wiOiBcImVsZW1lbnQtY2F0ZWdvcnlcIixcclxuICAgICAgICBcInRpdGxlXCI6IFwicHJvZHVjdCBjYXRlZ29yaWVzXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJ1cmxcIjogXCJwcm9kdWN0c1wiLFxyXG4gICAgICAgIFwiY2xhc3NcIjogXCJlbGVtZW50LXByb2R1Y3RcIixcclxuICAgICAgICBcInRpdGxlXCI6IFwicHJvZHVjdHNcIlxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcInVybFwiOiBcInRhYnNcIixcclxuICAgICAgICBcImNsYXNzXCI6IFwiZWxlbWVudC10YWJcIixcclxuICAgICAgICBcInRpdGxlXCI6IFwidGFic1wiXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIFwidXJsXCI6IFwidGVzdGltb25pYWxzXCIsXHJcbiAgICAgICAgXCJjbGFzc1wiOiBcImVsZW1lbnQtdGVzdGltb25pYWxcIixcclxuICAgICAgICBcInRpdGxlXCI6IFwidGVzdGltb25pYWxzXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgXCJ1cmxcIjogXCJ0aXRsZXNcIixcclxuICAgICAgICBcImNsYXNzXCI6IFwiZWxlbWVudC10aXRsZVwiLFxyXG4gICAgICAgIFwidGl0bGVcIjogXCJ0aXRsZXNcIlxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcInVybFwiOiBcInR5cG9ncmFwaHlcIixcclxuICAgICAgICBcImNsYXNzXCI6IFwiZWxlbWVudC10eXBvZ3JhcGh5XCIsXHJcbiAgICAgICAgXCJ0aXRsZVwiOiBcInR5cG9ncmFwaHlcIlxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBcInVybFwiOiBcImFsZXJ0c1wiLFxyXG4gICAgICAgIFwiY2xhc3NcIjogXCJlbGVtZW50LXZpZGVvXCIsXHJcbiAgICAgICAgXCJ0aXRsZVwiOiBcIk5vdGlmaWNhdGlvblwiXHJcbiAgICB9XHJcbl1cclxuXHJcbmV4cG9ydCBjb25zdCBoZWFkZXJCb3JkZXJSZW1vdmVMaXN0ID0gW1xyXG4gICAgXCIvXCIsXHJcbiAgICBcIi9zaG9wXCIsXHJcbiAgICBcIi9zaG9wL2luZmluaXRlLXNjcm9sbFwiLFxyXG4gICAgXCIvc2hvcC9ob3Jpem9udGFsLWZpbHRlclwiLFxyXG4gICAgXCIvc2hvcC9uYXZpZ2F0aW9uLWZpbHRlclwiLFxyXG4gICAgXCIvc2hvcC9vZmYtY2FudmFzLWZpbHRlclwiLFxyXG4gICAgXCIvc2hvcC9yaWdodC1zaWRlYmFyXCIsXHJcbiAgICBcIi9zaG9wL2dyaWQvW2dyaWRdXCIsXHJcbiAgICBcIi9wYWdlcy80MDRcIixcclxuICAgIFwiL2VsZW1lbnRzXCIsXHJcbiAgICBcIi9lbGVtZW50cy9wcm9kdWN0c1wiLFxyXG4gICAgXCIvZWxlbWVudHMvdHlwb2dyYXBoeVwiLFxyXG4gICAgXCIvZWxlbWVudHMvdGl0bGVzXCIsXHJcbiAgICBcIi9lbGVtZW50cy9wcm9kdWN0LWNhdGVnb3J5XCIsXHJcbiAgICBcIi9lbGVtZW50cy9idXR0b25zXCIsXHJcbiAgICBcIi9lbGVtZW50cy9hY2NvcmRpb25zXCIsXHJcbiAgICBcIi9lbGVtZW50cy9hbGVydHNcIixcclxuICAgIFwiL2VsZW1lbnRzL3RhYnNcIixcclxuICAgIFwiL2VsZW1lbnRzL3Rlc3RpbW9uaWFsc1wiLFxyXG4gICAgXCIvZWxlbWVudHMvYmxvZy1wb3N0c1wiLFxyXG4gICAgXCIvZWxlbWVudHMvaW5zdGFncmFtc1wiLFxyXG4gICAgXCIvZWxlbWVudHMvY3RhXCIsXHJcbiAgICBcIi9lbGVtZW50cy9pY29uLWJveGVzXCIsXHJcbiAgICBcIi9lbGVtZW50cy9pY29uc1wiXHJcbl0iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAYXBvbGxvL3JlYWN0LWhvb2tzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJhcG9sbG8tYm9vc3RcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImF4aW9zXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJncmFwaHFsLXRhZ1wiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiaW1hZ2VzbG9hZGVkXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0LWFwb2xsb1wiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC1yZWR1eC13cmFwcGVyXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L2Rpc3QvbmV4dC1zZXJ2ZXIvbGliL3JvdXRlci1jb250ZXh0LmpzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L2Rpc3QvbmV4dC1zZXJ2ZXIvbGliL3JvdXRlci91dGlscy9nZXQtYXNzZXQtcGF0aC1mcm9tLXJvdXRlLmpzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWJvb3RzdHJhcC9jb2xsYXBzZVwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtY291bnRkb3duXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1oZWxtZXRcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWltYWdlLW1hZ25pZmllcnNcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWlzXCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1sYXp5LWxvYWQtaW1hZ2UtY29tcG9uZW50XCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1tb2RhbFwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3Qtb3dsLWNhcm91c2VsMlwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtcmVkdXhcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LXNsaWRlLXRvZ2dsZVwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtdGFic1wiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtdG9hc3RpZnlcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXhcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXBlcnNpc3RcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXBlcnNpc3QvaW50ZWdyYXRpb24vcmVhY3RcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXBlcnNpc3QvbGliL3N0b3JhZ2VcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXNhZ2FcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXNhZ2EvZWZmZWN0c1wiKTs7Il0sInNvdXJjZVJvb3QiOiIifQ==