exports.id = "components_features_custom-link_jsx";
exports.ids = ["components_features_custom-link_jsx"];
exports.modules = {

/***/ "./components/features/custom-link.jsx":
/*!*********************************************!*\
  !*** ./components/features/custom-link.jsx ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ ALink; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ~/utils */ "./utils/index.js");

var _jsxFileName = "D:\\dsquare\\components\\features\\custom-link.jsx";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function ALink(_ref) {
  let {
    children,
    className,
    content,
    style
  } = _ref,
      props = _objectWithoutProperties(_ref, ["children", "className", "content", "style"]);

  const preventDefault = e => {
    if (props.href === '#') {
      e.preventDefault();
    }

    if (props.onClick) {
      e.preventDefault();
      props.onClick();
    }
  };

  return content ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_1___default()), _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      className: className,
      style: style,
      onClick: preventDefault,
      dangerouslySetInnerHTML: (0,_utils__WEBPACK_IMPORTED_MODULE_2__.parseContent)(content),
      children: children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 17
    }, this)
  }), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 20,
    columnNumber: 13
  }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_1___default()), _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
      className: className,
      style: style,
      onClick: preventDefault,
      children: children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 17
    }, this)
  }), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 25,
    columnNumber: 13
  }, this);
}

/***/ }),

/***/ "./utils/index.js":
/*!************************!*\
  !*** ./utils/index.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "parseOptions": function() { return /* binding */ parseOptions; },
/* harmony export */   "isIEBrowser": function() { return /* binding */ isIEBrowser; },
/* harmony export */   "isSafariBrowser": function() { return /* binding */ isSafariBrowser; },
/* harmony export */   "isEdgeBrowser": function() { return /* binding */ isEdgeBrowser; },
/* harmony export */   "findIndex": function() { return /* binding */ findIndex; },
/* harmony export */   "findArrayIndex": function() { return /* binding */ findArrayIndex; },
/* harmony export */   "parseContent": function() { return /* binding */ parseContent; },
/* harmony export */   "stickyHeaderHandler": function() { return /* binding */ stickyHeaderHandler; },
/* harmony export */   "resizeHandler": function() { return /* binding */ resizeHandler; },
/* harmony export */   "stickyFooterHandler": function() { return /* binding */ stickyFooterHandler; },
/* harmony export */   "parallaxHandler": function() { return /* binding */ parallaxHandler; },
/* harmony export */   "showScrollTopHandler": function() { return /* binding */ showScrollTopHandler; },
/* harmony export */   "scrollTopHandler": function() { return /* binding */ scrollTopHandler; },
/* harmony export */   "videoHandler": function() { return /* binding */ videoHandler; },
/* harmony export */   "getTotalPrice": function() { return /* binding */ getTotalPrice; },
/* harmony export */   "getCartCount": function() { return /* binding */ getCartCount; },
/* harmony export */   "toDecimal": function() { return /* binding */ toDecimal; }
/* harmony export */ });
/**
 * utils to parse options string to object
 * @param {string} options 
 * @return {object}
 */
const parseOptions = function (options) {
  if ("string" === typeof options) {
    return JSON.parse(options.replace(/'/g, '"').replace(';', ''));
  }

  return {};
};
/**
 * utils to dectect IE browser
 * @return {bool}
 */

const isIEBrowser = function () {
  let sUsrAg = navigator.userAgent;

  if (sUsrAg.indexOf("Trident") > -1) {
    return true;
  }

  return false;
};
/**
 * utils to detect safari browser
 * @return {bool}
 */

const isSafariBrowser = function () {
  let sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf('Safari') !== -1 && sUsrAg.indexOf('Chrome') === -1) return true;
  return false;
};
/**
 * utils to detect Edge browser
 * @return {bool}
 */

const isEdgeBrowser = function () {
  let sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf("Edge") > -1) return true;
  return false;
};
/**
 * utils to find index in array
 * @param {array} array
 * @param {callback} cb
 * @returns {number} index
 */

const findIndex = function (array, cb) {
  for (let i = 0; i < array.length; i++) {
    if (cb(array[i])) {
      return i;
    }
  }

  return -1;
};
/**
 * utils to get the position of the first element of search array in array
 * @param {array} array
 * @param {array} searchArray
 * @param {callback} cb
 * @returns {number} index
 */

const findArrayIndex = function (array, searchArray, cb) {
  for (let i = 0; i < searchArray.length; i++) {
    if (cb(searchArray[i])) {
      return i;
    }
  }

  return -1;
};
/**
 * utils to remove all XSS  attacks potential
 * @param {String} html
 * @return {Object}
 */

const parseContent = html => {
  const SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi; //Removing the <script> tags

  while (SCRIPT_REGEX.test(html)) {
    html = html.replace(SCRIPT_REGEX, '');
  } //Removing all events from tags...


  html = html.replace(/ on\w+="[^"]*"/g, '');
  return {
    __html: html
  };
};
/**
 * Apply sticky header
 */

const stickyHeaderHandler = function () {
  let top = document.querySelector('main') ? document.querySelector('main').offsetTop : 300;
  let stickyHeader = document.querySelector('.sticky-header');
  let height = 0;

  if (stickyHeader) {
    height = stickyHeader.offsetHeight;
  }

  if (window.pageYOffset >= top && window.innerWidth >= 992) {
    if (stickyHeader) {
      stickyHeader.classList.add('fixed');

      if (!document.querySelector('.sticky-wrapper')) {
        let newNode = document.createElement("div");
        newNode.className = "sticky-wrapper";
        stickyHeader.parentNode.insertBefore(newNode, stickyHeader);
        document.querySelector('.sticky-wrapper').insertAdjacentElement('beforeend', stickyHeader);
        document.querySelector('.sticky-wrapper').setAttribute("style", "height: " + height + "px");
      }

      if (!document.querySelector('.sticky-wrapper').getAttribute("style")) {
        document.querySelector('.sticky-wrapper').setAttribute("style", "height: " + height + "px");
      }
    }
  } else {
    if (stickyHeader) {
      stickyHeader.classList.remove('fixed');
    }

    if (document.querySelector('.sticky-wrapper')) {
      document.querySelector('.sticky-wrapper').removeAttribute("style");
    }
  }

  if (window.outerWidth >= 992 && document.querySelector('body').classList.contains('right-sidebar-active')) {
    document.querySelector('body').classList.remove('right-sidebar-active');
  }
};
/**
 * Add or remove settings when the window is resized
 */

const resizeHandler = function (width = 992, attri = 'right-sidebar-active') {
  let bodyClasses = document.querySelector("body") && document.querySelector("body").classList;
  bodyClasses = bodyClasses.value.split(' ').filter(item => item !== 'home' && item !== 'loaded');

  for (let i = 0; i < bodyClasses.length; i++) {
    document.querySelector("body") && document.querySelector('body').classList.remove(bodyClasses[i]);
  }
};
/**
 * Apply sticky footer
 */

const stickyFooterHandler = function () {
  let stickyFooter = document.querySelector('.sticky-footer');
  let top = document.querySelector('main') ? document.querySelector('main').offsetTop : 300;
  let height = 0;

  if (stickyFooter) {
    height = stickyFooter.offsetHeight;
  }

  if (window.pageYOffset >= top && window.innerWidth < 768) {
    if (stickyFooter) {
      stickyFooter.classList.add('fixed');

      if (!document.querySelector('.sticky-content-wrapper')) {
        let newNode = document.createElement("div");
        newNode.className = "sticky-content-wrapper";
        stickyFooter.parentNode.insertBefore(newNode, stickyFooter);
        document.querySelector('.sticky-content-wrapper').insertAdjacentElement('beforeend', stickyFooter);
      }

      document.querySelector('.sticky-content-wrapper').setAttribute("style", "height: " + height + "px");
    }
  } else {
    if (stickyFooter) {
      stickyFooter.classList.remove('fixed');
    }

    if (document.querySelector('.sticky-content-wrapper')) {
      document.querySelector('.sticky-content-wrapper').removeAttribute("style");
    }
  }

  if (window.innerWidth > 768 && document.querySelector('.sticky-content-wrapper')) {
    document.querySelector('.sticky-content-wrapper').style.height = 'auto';
  }
};
/**
 * utils to make background parallax
 */

const parallaxHandler = function () {
  let parallaxItems = document.querySelectorAll('.parallax');

  if (parallaxItems) {
    for (let i = 0; i < parallaxItems.length; i++) {
      // calculate background y Position;
      let parallax = parallaxItems[i],
          yPos,
          parallaxSpeed = 1;

      if (parallax.getAttribute('data-option')) {
        parallaxSpeed = parseInt(parseOptions(parallax.getAttribute('data-option')).speed);
      }

      yPos = (parallax.offsetTop - window.pageYOffset) * 50 * parallaxSpeed / parallax.offsetTop + 50;
      parallax.style.backgroundPosition = "50% " + yPos + "%";
    }
  }
};
/**
 * utils to show scrollTop button
 */

const showScrollTopHandler = function () {
  let scrollTop = document.querySelector(".scroll-top");

  if (window.pageYOffset >= 768) {
    scrollTop.classList.add("show");
  } else {
    scrollTop.classList.remove("show");
  }
};
/**
 * utils to scroll to top
 */

function scrollTopHandler(isCustom = true, speed = 15) {
  let offsetTop = 0;

  if (isCustom && !isEdgeBrowser()) {
    if (document.querySelector('.main .container > .row')) {
      offsetTop = document.querySelector('.main .container > .row').getBoundingClientRect().top + window.pageYOffset - document.querySelector('.sticky-header').offsetHeight + 2;
    }
  } else {
    offsetTop = 0;
  }

  if (isSafariBrowser() || isEdgeBrowser()) {
    let pos = window.pageYOffset;
    let timerId = setInterval(() => {
      if (pos <= offsetTop) clearInterval(timerId);
      window.scrollBy(0, -speed);
      pos -= speed;
    }, 1);
  } else {
    window.scrollTo({
      top: offsetTop,
      behavior: 'smooth'
    });
  }
}
/**
 * utils to play and pause video
 */

const videoHandler = e => {
  e.stopPropagation();
  e.preventDefault();

  if (e.currentTarget.closest('.post-video')) {
    let video = e.currentTarget.closest('.post-video');

    if (video.classList.contains('playing')) {
      video.classList.remove('playing');
      video.classList.add('paused');
      video.querySelector('video').pause();
    } else {
      video.classList.add('playing');
      video.querySelector('video').play();
    }

    video.querySelector('video').addEventListener('ended', function () {
      video.classList.remove('playing');
      video.classList.remove('paused');
    });
  }
};
/**
 * utils to get total Price of products in cart.
 */

const getTotalPrice = cartItems => {
  let total = 0;

  if (cartItems) {
    for (let i = 0; i < cartItems.length; i++) {
      total += cartItems[i].price * parseInt(cartItems[i].qty, 10);
    }
  }

  return total;
};
/**
 * utils to get number of products in cart
 */

const getCartCount = cartItems => {
  let total = 0;

  for (let i = 0; i < cartItems.length; i++) {
    total += parseInt(cartItems[i].qty, 10);
  }

  return total;
};
/**
 * utils to show number to n places of decimals
 */

const toDecimal = (price, fixedCount = 2) => {
  return price.toLocaleString(undefined, {
    minimumFractionDigits: fixedCount,
    maximumFractionDigits: fixedCount
  });
};

/***/ }),

/***/ "?ca47":
/*!******************************************!*\
  !*** ./utils/resolve-rewrites (ignored) ***!
  \******************************************/
/***/ (function() {

/* (ignored) */

/***/ })

};
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yaW9kZS1yZWFjdC8uL2NvbXBvbmVudHMvZmVhdHVyZXMvY3VzdG9tLWxpbmsuanN4Iiwid2VicGFjazovL3Jpb2RlLXJlYWN0Ly4vdXRpbHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vcmlvZGUtcmVhY3QvaWdub3JlZHxEOlxcZHNxdWFyZVxcbm9kZV9tb2R1bGVzXFxuZXh0XFxkaXN0XFxuZXh0LXNlcnZlclxcbGliXFxyb3V0ZXJ8Li91dGlscy9yZXNvbHZlLXJld3JpdGVzIl0sIm5hbWVzIjpbIkFMaW5rIiwiY2hpbGRyZW4iLCJjbGFzc05hbWUiLCJjb250ZW50Iiwic3R5bGUiLCJwcm9wcyIsInByZXZlbnREZWZhdWx0IiwiZSIsImhyZWYiLCJvbkNsaWNrIiwicGFyc2VDb250ZW50IiwicGFyc2VPcHRpb25zIiwib3B0aW9ucyIsIkpTT04iLCJwYXJzZSIsInJlcGxhY2UiLCJpc0lFQnJvd3NlciIsInNVc3JBZyIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsImluZGV4T2YiLCJpc1NhZmFyaUJyb3dzZXIiLCJpc0VkZ2VCcm93c2VyIiwiZmluZEluZGV4IiwiYXJyYXkiLCJjYiIsImkiLCJsZW5ndGgiLCJmaW5kQXJyYXlJbmRleCIsInNlYXJjaEFycmF5IiwiaHRtbCIsIlNDUklQVF9SRUdFWCIsInRlc3QiLCJfX2h0bWwiLCJzdGlja3lIZWFkZXJIYW5kbGVyIiwidG9wIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwib2Zmc2V0VG9wIiwic3RpY2t5SGVhZGVyIiwiaGVpZ2h0Iiwib2Zmc2V0SGVpZ2h0Iiwid2luZG93IiwicGFnZVlPZmZzZXQiLCJpbm5lcldpZHRoIiwiY2xhc3NMaXN0IiwiYWRkIiwibmV3Tm9kZSIsImNyZWF0ZUVsZW1lbnQiLCJwYXJlbnROb2RlIiwiaW5zZXJ0QmVmb3JlIiwiaW5zZXJ0QWRqYWNlbnRFbGVtZW50Iiwic2V0QXR0cmlidXRlIiwiZ2V0QXR0cmlidXRlIiwicmVtb3ZlIiwicmVtb3ZlQXR0cmlidXRlIiwib3V0ZXJXaWR0aCIsImNvbnRhaW5zIiwicmVzaXplSGFuZGxlciIsIndpZHRoIiwiYXR0cmkiLCJib2R5Q2xhc3NlcyIsInZhbHVlIiwic3BsaXQiLCJmaWx0ZXIiLCJpdGVtIiwic3RpY2t5Rm9vdGVySGFuZGxlciIsInN0aWNreUZvb3RlciIsInBhcmFsbGF4SGFuZGxlciIsInBhcmFsbGF4SXRlbXMiLCJxdWVyeVNlbGVjdG9yQWxsIiwicGFyYWxsYXgiLCJ5UG9zIiwicGFyYWxsYXhTcGVlZCIsInBhcnNlSW50Iiwic3BlZWQiLCJiYWNrZ3JvdW5kUG9zaXRpb24iLCJzaG93U2Nyb2xsVG9wSGFuZGxlciIsInNjcm9sbFRvcCIsInNjcm9sbFRvcEhhbmRsZXIiLCJpc0N1c3RvbSIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsInBvcyIsInRpbWVySWQiLCJzZXRJbnRlcnZhbCIsImNsZWFySW50ZXJ2YWwiLCJzY3JvbGxCeSIsInNjcm9sbFRvIiwiYmVoYXZpb3IiLCJ2aWRlb0hhbmRsZXIiLCJzdG9wUHJvcGFnYXRpb24iLCJjdXJyZW50VGFyZ2V0IiwiY2xvc2VzdCIsInZpZGVvIiwicGF1c2UiLCJwbGF5IiwiYWRkRXZlbnRMaXN0ZW5lciIsImdldFRvdGFsUHJpY2UiLCJjYXJ0SXRlbXMiLCJ0b3RhbCIsInByaWNlIiwicXR5IiwiZ2V0Q2FydENvdW50IiwidG9EZWNpbWFsIiwiZml4ZWRDb3VudCIsInRvTG9jYWxlU3RyaW5nIiwidW5kZWZpbmVkIiwibWluaW11bUZyYWN0aW9uRGlnaXRzIiwibWF4aW11bUZyYWN0aW9uRGlnaXRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBRWUsU0FBU0EsS0FBVCxPQUFxRTtBQUFBLE1BQXBEO0FBQUVDLFlBQUY7QUFBWUMsYUFBWjtBQUF1QkMsV0FBdkI7QUFBZ0NDO0FBQWhDLEdBQW9EO0FBQUEsTUFBVkMsS0FBVTs7QUFFaEYsUUFBTUMsY0FBYyxHQUFLQyxDQUFGLElBQVM7QUFDNUIsUUFBS0YsS0FBSyxDQUFDRyxJQUFOLEtBQWUsR0FBcEIsRUFBMEI7QUFDdEJELE9BQUMsQ0FBQ0QsY0FBRjtBQUNIOztBQUVELFFBQUtELEtBQUssQ0FBQ0ksT0FBWCxFQUFxQjtBQUNqQkYsT0FBQyxDQUFDRCxjQUFGO0FBQ0FELFdBQUssQ0FBQ0ksT0FBTjtBQUNIO0FBQ0osR0FURDs7QUFXQSxTQUNJTixPQUFPLGdCQUNILDhEQUFDLGtEQUFELGtDQUFXRSxLQUFYO0FBQUEsMkJBQ0k7QUFBRyxlQUFTLEVBQUdILFNBQWY7QUFBMkIsV0FBSyxFQUFHRSxLQUFuQztBQUEyQyxhQUFPLEVBQUdFLGNBQXJEO0FBQXNFLDZCQUF1QixFQUFHSSxvREFBWSxDQUFFUCxPQUFGLENBQTVHO0FBQUEsZ0JBQ01GO0FBRE47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERyxnQkFNSCw4REFBQyxrREFBRCxrQ0FBV0ksS0FBWDtBQUFBLDJCQUNJO0FBQUcsZUFBUyxFQUFHSCxTQUFmO0FBQTJCLFdBQUssRUFBR0UsS0FBbkM7QUFBMkMsYUFBTyxFQUFHRSxjQUFyRDtBQUFBLGdCQUNNTDtBQUROO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBUFI7QUFhSCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUJEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxNQUFNVSxZQUFZLEdBQUcsVUFBV0MsT0FBWCxFQUFxQjtBQUM3QyxNQUFLLGFBQWEsT0FBT0EsT0FBekIsRUFBbUM7QUFDL0IsV0FBT0MsSUFBSSxDQUFDQyxLQUFMLENBQVlGLE9BQU8sQ0FBQ0csT0FBUixDQUFpQixJQUFqQixFQUF1QixHQUF2QixFQUE2QkEsT0FBN0IsQ0FBc0MsR0FBdEMsRUFBMkMsRUFBM0MsQ0FBWixDQUFQO0FBQ0g7O0FBQ0QsU0FBTyxFQUFQO0FBQ0gsQ0FMTTtBQU9QO0FBQ0E7QUFDQTtBQUNBOztBQUNPLE1BQU1DLFdBQVcsR0FBRyxZQUFZO0FBQ25DLE1BQUlDLE1BQU0sR0FBR0MsU0FBUyxDQUFDQyxTQUF2Qjs7QUFDQSxNQUFLRixNQUFNLENBQUNHLE9BQVAsQ0FBZ0IsU0FBaEIsSUFBOEIsQ0FBQyxDQUFwQyxFQUF3QztBQUNwQyxXQUFPLElBQVA7QUFDSDs7QUFFRCxTQUFPLEtBQVA7QUFDSCxDQVBNO0FBU1A7QUFDQTtBQUNBO0FBQ0E7O0FBQ08sTUFBTUMsZUFBZSxHQUFHLFlBQVk7QUFDdkMsTUFBSUosTUFBTSxHQUFHQyxTQUFTLENBQUNDLFNBQXZCO0FBQ0EsTUFBS0YsTUFBTSxDQUFDRyxPQUFQLENBQWdCLFFBQWhCLE1BQStCLENBQUMsQ0FBaEMsSUFBcUNILE1BQU0sQ0FBQ0csT0FBUCxDQUFnQixRQUFoQixNQUErQixDQUFDLENBQTFFLEVBQ0ksT0FBTyxJQUFQO0FBQ0osU0FBTyxLQUFQO0FBQ0gsQ0FMTTtBQU9QO0FBQ0E7QUFDQTtBQUNBOztBQUNPLE1BQU1FLGFBQWEsR0FBRyxZQUFZO0FBQ3JDLE1BQUlMLE1BQU0sR0FBR0MsU0FBUyxDQUFDQyxTQUF2QjtBQUNBLE1BQUtGLE1BQU0sQ0FBQ0csT0FBUCxDQUFnQixNQUFoQixJQUEyQixDQUFDLENBQWpDLEVBQ0ksT0FBTyxJQUFQO0FBQ0osU0FBTyxLQUFQO0FBQ0gsQ0FMTTtBQU9QO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDTyxNQUFNRyxTQUFTLEdBQUcsVUFBV0MsS0FBWCxFQUFrQkMsRUFBbEIsRUFBdUI7QUFDNUMsT0FBTSxJQUFJQyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHRixLQUFLLENBQUNHLE1BQTNCLEVBQW1DRCxDQUFDLEVBQXBDLEVBQXlDO0FBQ3JDLFFBQUtELEVBQUUsQ0FBRUQsS0FBSyxDQUFFRSxDQUFGLENBQVAsQ0FBUCxFQUF3QjtBQUNwQixhQUFPQSxDQUFQO0FBQ0g7QUFDSjs7QUFDRCxTQUFPLENBQUMsQ0FBUjtBQUNILENBUE07QUFTUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDTyxNQUFNRSxjQUFjLEdBQUcsVUFBV0osS0FBWCxFQUFrQkssV0FBbEIsRUFBK0JKLEVBQS9CLEVBQW9DO0FBQzlELE9BQU0sSUFBSUMsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBR0csV0FBVyxDQUFDRixNQUFqQyxFQUF5Q0QsQ0FBQyxFQUExQyxFQUErQztBQUMzQyxRQUFLRCxFQUFFLENBQUVJLFdBQVcsQ0FBRUgsQ0FBRixDQUFiLENBQVAsRUFBOEI7QUFDMUIsYUFBT0EsQ0FBUDtBQUNIO0FBQ0o7O0FBQ0QsU0FBTyxDQUFDLENBQVI7QUFDSCxDQVBNO0FBVVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDTyxNQUFNaEIsWUFBWSxHQUFLb0IsSUFBRixJQUFZO0FBQ3BDLFFBQU1DLFlBQVksR0FBRyxxREFBckIsQ0FEb0MsQ0FHcEM7O0FBQ0EsU0FBUUEsWUFBWSxDQUFDQyxJQUFiLENBQW1CRixJQUFuQixDQUFSLEVBQW9DO0FBQ2hDQSxRQUFJLEdBQUdBLElBQUksQ0FBQ2YsT0FBTCxDQUFjZ0IsWUFBZCxFQUE0QixFQUE1QixDQUFQO0FBQ0gsR0FObUMsQ0FRcEM7OztBQUNBRCxNQUFJLEdBQUdBLElBQUksQ0FBQ2YsT0FBTCxDQUFjLGlCQUFkLEVBQWlDLEVBQWpDLENBQVA7QUFFQSxTQUFPO0FBQ0hrQixVQUFNLEVBQUVIO0FBREwsR0FBUDtBQUdILENBZE07QUFnQlA7QUFDQTtBQUNBOztBQUNPLE1BQU1JLG1CQUFtQixHQUFHLFlBQVk7QUFDM0MsTUFBSUMsR0FBRyxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsSUFBbUNELFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ0MsU0FBcEUsR0FBZ0YsR0FBMUY7QUFFQSxNQUFJQyxZQUFZLEdBQUdILFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixnQkFBeEIsQ0FBbkI7QUFDQSxNQUFJRyxNQUFNLEdBQUcsQ0FBYjs7QUFFQSxNQUFLRCxZQUFMLEVBQW9CO0FBQ2hCQyxVQUFNLEdBQUdELFlBQVksQ0FBQ0UsWUFBdEI7QUFDSDs7QUFFRCxNQUFLQyxNQUFNLENBQUNDLFdBQVAsSUFBc0JSLEdBQXRCLElBQTZCTyxNQUFNLENBQUNFLFVBQVAsSUFBcUIsR0FBdkQsRUFBNkQ7QUFDekQsUUFBS0wsWUFBTCxFQUFvQjtBQUNoQkEsa0JBQVksQ0FBQ00sU0FBYixDQUF1QkMsR0FBdkIsQ0FBNEIsT0FBNUI7O0FBQ0EsVUFBSyxDQUFDVixRQUFRLENBQUNDLGFBQVQsQ0FBd0IsaUJBQXhCLENBQU4sRUFBb0Q7QUFDaEQsWUFBSVUsT0FBTyxHQUFHWCxRQUFRLENBQUNZLGFBQVQsQ0FBd0IsS0FBeEIsQ0FBZDtBQUNBRCxlQUFPLENBQUM3QyxTQUFSLEdBQW9CLGdCQUFwQjtBQUNBcUMsb0JBQVksQ0FBQ1UsVUFBYixDQUF3QkMsWUFBeEIsQ0FBc0NILE9BQXRDLEVBQStDUixZQUEvQztBQUNBSCxnQkFBUSxDQUFDQyxhQUFULENBQXdCLGlCQUF4QixFQUE0Q2MscUJBQTVDLENBQW1FLFdBQW5FLEVBQWdGWixZQUFoRjtBQUNBSCxnQkFBUSxDQUFDQyxhQUFULENBQXdCLGlCQUF4QixFQUE0Q2UsWUFBNUMsQ0FBMEQsT0FBMUQsRUFBbUUsYUFBYVosTUFBYixHQUFzQixJQUF6RjtBQUNIOztBQUVELFVBQUssQ0FBQ0osUUFBUSxDQUFDQyxhQUFULENBQXdCLGlCQUF4QixFQUE0Q2dCLFlBQTVDLENBQTBELE9BQTFELENBQU4sRUFBNEU7QUFDeEVqQixnQkFBUSxDQUFDQyxhQUFULENBQXdCLGlCQUF4QixFQUE0Q2UsWUFBNUMsQ0FBMEQsT0FBMUQsRUFBbUUsYUFBYVosTUFBYixHQUFzQixJQUF6RjtBQUNIO0FBQ0o7QUFDSixHQWZELE1BZU87QUFDSCxRQUFLRCxZQUFMLEVBQW9CO0FBQ2hCQSxrQkFBWSxDQUFDTSxTQUFiLENBQXVCUyxNQUF2QixDQUErQixPQUEvQjtBQUNIOztBQUVELFFBQUtsQixRQUFRLENBQUNDLGFBQVQsQ0FBd0IsaUJBQXhCLENBQUwsRUFBbUQ7QUFDL0NELGNBQVEsQ0FBQ0MsYUFBVCxDQUF3QixpQkFBeEIsRUFBNENrQixlQUE1QyxDQUE2RCxPQUE3RDtBQUNIO0FBQ0o7O0FBRUQsTUFBS2IsTUFBTSxDQUFDYyxVQUFQLElBQXFCLEdBQXJCLElBQTRCcEIsUUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDUSxTQUFqQyxDQUEyQ1ksUUFBM0MsQ0FBcUQsc0JBQXJELENBQWpDLEVBQWlIO0FBQzdHckIsWUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEVBQWlDUSxTQUFqQyxDQUEyQ1MsTUFBM0MsQ0FBbUQsc0JBQW5EO0FBQ0g7QUFDSixDQXRDTTtBQXdDUDtBQUNBO0FBQ0E7O0FBQ08sTUFBTUksYUFBYSxHQUFHLFVBQVdDLEtBQUssR0FBRyxHQUFuQixFQUF3QkMsS0FBSyxHQUFHLHNCQUFoQyxFQUF5RDtBQUNsRixNQUFJQyxXQUFXLEdBQUd6QixRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsS0FBb0NELFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ1EsU0FBdkY7QUFDQWdCLGFBQVcsR0FBR0EsV0FBVyxDQUFDQyxLQUFaLENBQWtCQyxLQUFsQixDQUF5QixHQUF6QixFQUErQkMsTUFBL0IsQ0FBdUNDLElBQUksSUFBSUEsSUFBSSxLQUFLLE1BQVQsSUFBbUJBLElBQUksS0FBSyxRQUEzRSxDQUFkOztBQUNBLE9BQU0sSUFBSXZDLENBQUMsR0FBRyxDQUFkLEVBQWlCQSxDQUFDLEdBQUdtQyxXQUFXLENBQUNsQyxNQUFqQyxFQUF5Q0QsQ0FBQyxFQUExQyxFQUErQztBQUMzQ1UsWUFBUSxDQUFDQyxhQUFULENBQXdCLE1BQXhCLEtBQW9DRCxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsRUFBaUNRLFNBQWpDLENBQTJDUyxNQUEzQyxDQUFtRE8sV0FBVyxDQUFFbkMsQ0FBRixDQUE5RCxDQUFwQztBQUNIO0FBQ0osQ0FOTTtBQVFQO0FBQ0E7QUFDQTs7QUFDTyxNQUFNd0MsbUJBQW1CLEdBQUcsWUFBWTtBQUMzQyxNQUFJQyxZQUFZLEdBQUcvQixRQUFRLENBQUNDLGFBQVQsQ0FBd0IsZ0JBQXhCLENBQW5CO0FBQ0EsTUFBSUYsR0FBRyxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsTUFBeEIsSUFBbUNELFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixNQUF4QixFQUFpQ0MsU0FBcEUsR0FBZ0YsR0FBMUY7QUFFQSxNQUFJRSxNQUFNLEdBQUcsQ0FBYjs7QUFFQSxNQUFLMkIsWUFBTCxFQUFvQjtBQUNoQjNCLFVBQU0sR0FBRzJCLFlBQVksQ0FBQzFCLFlBQXRCO0FBQ0g7O0FBRUQsTUFBS0MsTUFBTSxDQUFDQyxXQUFQLElBQXNCUixHQUF0QixJQUE2Qk8sTUFBTSxDQUFDRSxVQUFQLEdBQW9CLEdBQXRELEVBQTREO0FBQ3hELFFBQUt1QixZQUFMLEVBQW9CO0FBQ2hCQSxrQkFBWSxDQUFDdEIsU0FBYixDQUF1QkMsR0FBdkIsQ0FBNEIsT0FBNUI7O0FBQ0EsVUFBSyxDQUFDVixRQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLENBQU4sRUFBNEQ7QUFDeEQsWUFBSVUsT0FBTyxHQUFHWCxRQUFRLENBQUNZLGFBQVQsQ0FBd0IsS0FBeEIsQ0FBZDtBQUNBRCxlQUFPLENBQUM3QyxTQUFSLEdBQW9CLHdCQUFwQjtBQUNBaUUsb0JBQVksQ0FBQ2xCLFVBQWIsQ0FBd0JDLFlBQXhCLENBQXNDSCxPQUF0QyxFQUErQ29CLFlBQS9DO0FBQ0EvQixnQkFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixFQUFvRGMscUJBQXBELENBQTJFLFdBQTNFLEVBQXdGZ0IsWUFBeEY7QUFDSDs7QUFFRC9CLGNBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsRUFBb0RlLFlBQXBELENBQWtFLE9BQWxFLEVBQTJFLGFBQWFaLE1BQWIsR0FBc0IsSUFBakc7QUFDSDtBQUNKLEdBWkQsTUFZTztBQUNILFFBQUsyQixZQUFMLEVBQW9CO0FBQ2hCQSxrQkFBWSxDQUFDdEIsU0FBYixDQUF1QlMsTUFBdkIsQ0FBK0IsT0FBL0I7QUFDSDs7QUFFRCxRQUFLbEIsUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFMLEVBQTJEO0FBQ3ZERCxjQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLEVBQW9Ea0IsZUFBcEQsQ0FBcUUsT0FBckU7QUFDSDtBQUNKOztBQUVELE1BQUtiLE1BQU0sQ0FBQ0UsVUFBUCxHQUFvQixHQUFwQixJQUEyQlIsUUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixDQUFoQyxFQUFzRjtBQUNsRkQsWUFBUSxDQUFDQyxhQUFULENBQXdCLHlCQUF4QixFQUFvRGpDLEtBQXBELENBQTBEb0MsTUFBMUQsR0FBbUUsTUFBbkU7QUFDSDtBQUNKLENBbkNNO0FBcUNQO0FBQ0E7QUFDQTs7QUFDTyxNQUFNNEIsZUFBZSxHQUFHLFlBQVk7QUFDdkMsTUFBSUMsYUFBYSxHQUFHakMsUUFBUSxDQUFDa0MsZ0JBQVQsQ0FBMkIsV0FBM0IsQ0FBcEI7O0FBRUEsTUFBS0QsYUFBTCxFQUFxQjtBQUNqQixTQUFNLElBQUkzQyxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHMkMsYUFBYSxDQUFDMUMsTUFBbkMsRUFBMkNELENBQUMsRUFBNUMsRUFBaUQ7QUFDN0M7QUFDQSxVQUFJNkMsUUFBUSxHQUFHRixhQUFhLENBQUUzQyxDQUFGLENBQTVCO0FBQUEsVUFBbUM4QyxJQUFuQztBQUFBLFVBQXlDQyxhQUFhLEdBQUcsQ0FBekQ7O0FBRUEsVUFBS0YsUUFBUSxDQUFDbEIsWUFBVCxDQUF1QixhQUF2QixDQUFMLEVBQThDO0FBQzFDb0IscUJBQWEsR0FBR0MsUUFBUSxDQUFFL0QsWUFBWSxDQUFFNEQsUUFBUSxDQUFDbEIsWUFBVCxDQUF1QixhQUF2QixDQUFGLENBQVosQ0FBdURzQixLQUF6RCxDQUF4QjtBQUNIOztBQUVESCxVQUFJLEdBQUcsQ0FBRUQsUUFBUSxDQUFDakMsU0FBVCxHQUFxQkksTUFBTSxDQUFDQyxXQUE5QixJQUE4QyxFQUE5QyxHQUFtRDhCLGFBQW5ELEdBQW1FRixRQUFRLENBQUNqQyxTQUE1RSxHQUF3RixFQUEvRjtBQUVBaUMsY0FBUSxDQUFDbkUsS0FBVCxDQUFld0Usa0JBQWYsR0FBb0MsU0FBU0osSUFBVCxHQUFnQixHQUFwRDtBQUNIO0FBQ0o7QUFDSixDQWpCTTtBQW1CUDtBQUNBO0FBQ0E7O0FBQ08sTUFBTUssb0JBQW9CLEdBQUcsWUFBWTtBQUM1QyxNQUFJQyxTQUFTLEdBQUcxQyxRQUFRLENBQUNDLGFBQVQsQ0FBd0IsYUFBeEIsQ0FBaEI7O0FBRUEsTUFBS0ssTUFBTSxDQUFDQyxXQUFQLElBQXNCLEdBQTNCLEVBQWlDO0FBQzdCbUMsYUFBUyxDQUFDakMsU0FBVixDQUFvQkMsR0FBcEIsQ0FBeUIsTUFBekI7QUFDSCxHQUZELE1BRU87QUFDSGdDLGFBQVMsQ0FBQ2pDLFNBQVYsQ0FBb0JTLE1BQXBCLENBQTRCLE1BQTVCO0FBQ0g7QUFDSixDQVJNO0FBVVA7QUFDQTtBQUNBOztBQUNPLFNBQVN5QixnQkFBVCxDQUEyQkMsUUFBUSxHQUFHLElBQXRDLEVBQTRDTCxLQUFLLEdBQUcsRUFBcEQsRUFBeUQ7QUFDNUQsTUFBSXJDLFNBQVMsR0FBRyxDQUFoQjs7QUFFQSxNQUFLMEMsUUFBUSxJQUFJLENBQUMxRCxhQUFhLEVBQS9CLEVBQW9DO0FBQ2hDLFFBQUtjLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3Qix5QkFBeEIsQ0FBTCxFQUEyRDtBQUN2REMsZUFBUyxHQUFHRixRQUFRLENBQUNDLGFBQVQsQ0FBd0IseUJBQXhCLEVBQW9ENEMscUJBQXBELEdBQTRFOUMsR0FBNUUsR0FBa0ZPLE1BQU0sQ0FBQ0MsV0FBekYsR0FBdUdQLFFBQVEsQ0FBQ0MsYUFBVCxDQUF3QixnQkFBeEIsRUFBMkNJLFlBQWxKLEdBQWlLLENBQTdLO0FBQ0g7QUFDSixHQUpELE1BSU87QUFDSEgsYUFBUyxHQUFHLENBQVo7QUFDSDs7QUFFRCxNQUFLakIsZUFBZSxNQUFNQyxhQUFhLEVBQXZDLEVBQTRDO0FBQ3hDLFFBQUk0RCxHQUFHLEdBQUd4QyxNQUFNLENBQUNDLFdBQWpCO0FBQ0EsUUFBSXdDLE9BQU8sR0FBR0MsV0FBVyxDQUFFLE1BQU07QUFDN0IsVUFBS0YsR0FBRyxJQUFJNUMsU0FBWixFQUF3QitDLGFBQWEsQ0FBRUYsT0FBRixDQUFiO0FBQ3hCekMsWUFBTSxDQUFDNEMsUUFBUCxDQUFpQixDQUFqQixFQUFvQixDQUFDWCxLQUFyQjtBQUNBTyxTQUFHLElBQUlQLEtBQVA7QUFDSCxLQUp3QixFQUl0QixDQUpzQixDQUF6QjtBQUtILEdBUEQsTUFPTztBQUNIakMsVUFBTSxDQUFDNkMsUUFBUCxDQUFpQjtBQUNicEQsU0FBRyxFQUFFRyxTQURRO0FBRWJrRCxjQUFRLEVBQUU7QUFGRyxLQUFqQjtBQUlIO0FBQ0o7QUFFRDtBQUNBO0FBQ0E7O0FBQ08sTUFBTUMsWUFBWSxHQUFLbEYsQ0FBRixJQUFTO0FBQ2pDQSxHQUFDLENBQUNtRixlQUFGO0FBQ0FuRixHQUFDLENBQUNELGNBQUY7O0FBRUEsTUFBS0MsQ0FBQyxDQUFDb0YsYUFBRixDQUFnQkMsT0FBaEIsQ0FBeUIsYUFBekIsQ0FBTCxFQUFnRDtBQUM1QyxRQUFJQyxLQUFLLEdBQUd0RixDQUFDLENBQUNvRixhQUFGLENBQWdCQyxPQUFoQixDQUF5QixhQUF6QixDQUFaOztBQUNBLFFBQUtDLEtBQUssQ0FBQ2hELFNBQU4sQ0FBZ0JZLFFBQWhCLENBQTBCLFNBQTFCLENBQUwsRUFBNkM7QUFDekNvQyxXQUFLLENBQUNoRCxTQUFOLENBQWdCUyxNQUFoQixDQUF3QixTQUF4QjtBQUNBdUMsV0FBSyxDQUFDaEQsU0FBTixDQUFnQkMsR0FBaEIsQ0FBcUIsUUFBckI7QUFDQStDLFdBQUssQ0FBQ3hELGFBQU4sQ0FBcUIsT0FBckIsRUFBK0J5RCxLQUEvQjtBQUNILEtBSkQsTUFJTztBQUNIRCxXQUFLLENBQUNoRCxTQUFOLENBQWdCQyxHQUFoQixDQUFxQixTQUFyQjtBQUNBK0MsV0FBSyxDQUFDeEQsYUFBTixDQUFxQixPQUFyQixFQUErQjBELElBQS9CO0FBQ0g7O0FBRURGLFNBQUssQ0FBQ3hELGFBQU4sQ0FBcUIsT0FBckIsRUFBK0IyRCxnQkFBL0IsQ0FBaUQsT0FBakQsRUFBMEQsWUFBWTtBQUNsRUgsV0FBSyxDQUFDaEQsU0FBTixDQUFnQlMsTUFBaEIsQ0FBd0IsU0FBeEI7QUFDQXVDLFdBQUssQ0FBQ2hELFNBQU4sQ0FBZ0JTLE1BQWhCLENBQXdCLFFBQXhCO0FBQ0gsS0FIRDtBQUlIO0FBQ0osQ0FwQk07QUFzQlA7QUFDQTtBQUNBOztBQUNPLE1BQU0yQyxhQUFhLEdBQUdDLFNBQVMsSUFBSTtBQUN0QyxNQUFJQyxLQUFLLEdBQUcsQ0FBWjs7QUFDQSxNQUFLRCxTQUFMLEVBQWlCO0FBQ2IsU0FBTSxJQUFJeEUsQ0FBQyxHQUFHLENBQWQsRUFBaUJBLENBQUMsR0FBR3dFLFNBQVMsQ0FBQ3ZFLE1BQS9CLEVBQXVDRCxDQUFDLEVBQXhDLEVBQTZDO0FBQ3pDeUUsV0FBSyxJQUFJRCxTQUFTLENBQUV4RSxDQUFGLENBQVQsQ0FBZTBFLEtBQWYsR0FBdUIxQixRQUFRLENBQUV3QixTQUFTLENBQUV4RSxDQUFGLENBQVQsQ0FBZTJFLEdBQWpCLEVBQXNCLEVBQXRCLENBQXhDO0FBQ0g7QUFDSjs7QUFDRCxTQUFPRixLQUFQO0FBQ0gsQ0FSTTtBQVVQO0FBQ0E7QUFDQTs7QUFDTyxNQUFNRyxZQUFZLEdBQUdKLFNBQVMsSUFBSTtBQUNyQyxNQUFJQyxLQUFLLEdBQUcsQ0FBWjs7QUFFQSxPQUFNLElBQUl6RSxDQUFDLEdBQUcsQ0FBZCxFQUFpQkEsQ0FBQyxHQUFHd0UsU0FBUyxDQUFDdkUsTUFBL0IsRUFBdUNELENBQUMsRUFBeEMsRUFBNkM7QUFDekN5RSxTQUFLLElBQUl6QixRQUFRLENBQUV3QixTQUFTLENBQUV4RSxDQUFGLENBQVQsQ0FBZTJFLEdBQWpCLEVBQXNCLEVBQXRCLENBQWpCO0FBQ0g7O0FBRUQsU0FBT0YsS0FBUDtBQUNILENBUk07QUFVUDtBQUNBO0FBQ0E7O0FBQ08sTUFBTUksU0FBUyxHQUFHLENBQUVILEtBQUYsRUFBU0ksVUFBVSxHQUFHLENBQXRCLEtBQTZCO0FBQ2xELFNBQU9KLEtBQUssQ0FBQ0ssY0FBTixDQUFzQkMsU0FBdEIsRUFBaUM7QUFBRUMseUJBQXFCLEVBQUVILFVBQXpCO0FBQXFDSSx5QkFBcUIsRUFBRUo7QUFBNUQsR0FBakMsQ0FBUDtBQUNILENBRk0sQzs7Ozs7Ozs7OztBQ3hUUCxlIiwiZmlsZSI6ImNvbXBvbmVudHNfZmVhdHVyZXNfY3VzdG9tLWxpbmtfanN4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IExpbmsgZnJvbSBcIm5leHQvbGlua1wiO1xyXG5cclxuaW1wb3J0IHsgcGFyc2VDb250ZW50IH0gZnJvbSAnfi91dGlscyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBBTGluayAoIHsgY2hpbGRyZW4sIGNsYXNzTmFtZSwgY29udGVudCwgc3R5bGUsIC4uLnByb3BzIH0gKSB7XHJcblxyXG4gICAgY29uc3QgcHJldmVudERlZmF1bHQgPSAoIGUgKSA9PiB7XHJcbiAgICAgICAgaWYgKCBwcm9wcy5ocmVmID09PSAnIycgKSB7XHJcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggcHJvcHMub25DbGljayApIHtcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBwcm9wcy5vbkNsaWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgY29udGVudCA/XHJcbiAgICAgICAgICAgIDxMaW5rIHsgLi4ucHJvcHMgfSA+XHJcbiAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9eyBjbGFzc05hbWUgfSBzdHlsZT17IHN0eWxlIH0gb25DbGljaz17IHByZXZlbnREZWZhdWx0IH0gZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9eyBwYXJzZUNvbnRlbnQoIGNvbnRlbnQgKSB9PlxyXG4gICAgICAgICAgICAgICAgICAgIHsgY2hpbGRyZW4gfVxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8L0xpbms+IDpcclxuICAgICAgICAgICAgPExpbmsgeyAuLi5wcm9wcyB9ID5cclxuICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT17IGNsYXNzTmFtZSB9IHN0eWxlPXsgc3R5bGUgfSBvbkNsaWNrPXsgcHJldmVudERlZmF1bHQgfT5cclxuICAgICAgICAgICAgICAgICAgICB7IGNoaWxkcmVuIH1cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9MaW5rPlxyXG4gICAgKVxyXG59IiwiLyoqXHJcbiAqIHV0aWxzIHRvIHBhcnNlIG9wdGlvbnMgc3RyaW5nIHRvIG9iamVjdFxyXG4gKiBAcGFyYW0ge3N0cmluZ30gb3B0aW9ucyBcclxuICogQHJldHVybiB7b2JqZWN0fVxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHBhcnNlT3B0aW9ucyA9IGZ1bmN0aW9uICggb3B0aW9ucyApIHtcclxuICAgIGlmICggXCJzdHJpbmdcIiA9PT0gdHlwZW9mIG9wdGlvbnMgKSB7XHJcbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UoIG9wdGlvbnMucmVwbGFjZSggLycvZywgJ1wiJyApLnJlcGxhY2UoICc7JywgJycgKSApO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHt9O1xyXG59XHJcblxyXG4vKipcclxuICogdXRpbHMgdG8gZGVjdGVjdCBJRSBicm93c2VyXHJcbiAqIEByZXR1cm4ge2Jvb2x9XHJcbiAqL1xyXG5leHBvcnQgY29uc3QgaXNJRUJyb3dzZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgc1VzckFnID0gbmF2aWdhdG9yLnVzZXJBZ2VudDtcclxuICAgIGlmICggc1VzckFnLmluZGV4T2YoIFwiVHJpZGVudFwiICkgPiAtMSApIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBkZXRlY3Qgc2FmYXJpIGJyb3dzZXJcclxuICogQHJldHVybiB7Ym9vbH1cclxuICovXHJcbmV4cG9ydCBjb25zdCBpc1NhZmFyaUJyb3dzZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgc1VzckFnID0gbmF2aWdhdG9yLnVzZXJBZ2VudDtcclxuICAgIGlmICggc1VzckFnLmluZGV4T2YoICdTYWZhcmknICkgIT09IC0xICYmIHNVc3JBZy5pbmRleE9mKCAnQ2hyb21lJyApID09PSAtMSApXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBkZXRlY3QgRWRnZSBicm93c2VyXHJcbiAqIEByZXR1cm4ge2Jvb2x9XHJcbiAqL1xyXG5leHBvcnQgY29uc3QgaXNFZGdlQnJvd3NlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBzVXNyQWcgPSBuYXZpZ2F0b3IudXNlckFnZW50O1xyXG4gICAgaWYgKCBzVXNyQWcuaW5kZXhPZiggXCJFZGdlXCIgKSA+IC0xIClcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIHJldHVybiBmYWxzZTtcclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIGZpbmQgaW5kZXggaW4gYXJyYXlcclxuICogQHBhcmFtIHthcnJheX0gYXJyYXlcclxuICogQHBhcmFtIHtjYWxsYmFja30gY2JcclxuICogQHJldHVybnMge251bWJlcn0gaW5kZXhcclxuICovXHJcbmV4cG9ydCBjb25zdCBmaW5kSW5kZXggPSBmdW5jdGlvbiAoIGFycmF5LCBjYiApIHtcclxuICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGFycmF5Lmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgIGlmICggY2IoIGFycmF5WyBpIF0gKSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIC0xO1xyXG59XHJcblxyXG4vKipcclxuICogdXRpbHMgdG8gZ2V0IHRoZSBwb3NpdGlvbiBvZiB0aGUgZmlyc3QgZWxlbWVudCBvZiBzZWFyY2ggYXJyYXkgaW4gYXJyYXlcclxuICogQHBhcmFtIHthcnJheX0gYXJyYXlcclxuICogQHBhcmFtIHthcnJheX0gc2VhcmNoQXJyYXlcclxuICogQHBhcmFtIHtjYWxsYmFja30gY2JcclxuICogQHJldHVybnMge251bWJlcn0gaW5kZXhcclxuICovXHJcbmV4cG9ydCBjb25zdCBmaW5kQXJyYXlJbmRleCA9IGZ1bmN0aW9uICggYXJyYXksIHNlYXJjaEFycmF5LCBjYiApIHtcclxuICAgIGZvciAoIGxldCBpID0gMDsgaSA8IHNlYXJjaEFycmF5Lmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgIGlmICggY2IoIHNlYXJjaEFycmF5WyBpIF0gKSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIC0xO1xyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIHJlbW92ZSBhbGwgWFNTICBhdHRhY2tzIHBvdGVudGlhbFxyXG4gKiBAcGFyYW0ge1N0cmluZ30gaHRtbFxyXG4gKiBAcmV0dXJuIHtPYmplY3R9XHJcbiAqL1xyXG5leHBvcnQgY29uc3QgcGFyc2VDb250ZW50ID0gKCBodG1sICkgPT4ge1xyXG4gICAgY29uc3QgU0NSSVBUX1JFR0VYID0gLzxzY3JpcHRcXGJbXjxdKig/Oig/ITxcXC9zY3JpcHQ+KTxbXjxdKikqPFxcL3NjcmlwdD4vZ2k7XHJcblxyXG4gICAgLy9SZW1vdmluZyB0aGUgPHNjcmlwdD4gdGFnc1xyXG4gICAgd2hpbGUgKCBTQ1JJUFRfUkVHRVgudGVzdCggaHRtbCApICkge1xyXG4gICAgICAgIGh0bWwgPSBodG1sLnJlcGxhY2UoIFNDUklQVF9SRUdFWCwgJycgKTtcclxuICAgIH1cclxuXHJcbiAgICAvL1JlbW92aW5nIGFsbCBldmVudHMgZnJvbSB0YWdzLi4uXHJcbiAgICBodG1sID0gaHRtbC5yZXBsYWNlKCAvIG9uXFx3Kz1cIlteXCJdKlwiL2csICcnICk7XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBfX2h0bWw6IGh0bWxcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEFwcGx5IHN0aWNreSBoZWFkZXJcclxuICovXHJcbmV4cG9ydCBjb25zdCBzdGlja3lIZWFkZXJIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IHRvcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdtYWluJyApID8gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ21haW4nICkub2Zmc2V0VG9wIDogMzAwO1xyXG5cclxuICAgIGxldCBzdGlja3lIZWFkZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1oZWFkZXInICk7XHJcbiAgICBsZXQgaGVpZ2h0ID0gMDtcclxuXHJcbiAgICBpZiAoIHN0aWNreUhlYWRlciApIHtcclxuICAgICAgICBoZWlnaHQgPSBzdGlja3lIZWFkZXIub2Zmc2V0SGVpZ2h0O1xyXG4gICAgfVxyXG5cclxuICAgIGlmICggd2luZG93LnBhZ2VZT2Zmc2V0ID49IHRvcCAmJiB3aW5kb3cuaW5uZXJXaWR0aCA+PSA5OTIgKSB7XHJcbiAgICAgICAgaWYgKCBzdGlja3lIZWFkZXIgKSB7XHJcbiAgICAgICAgICAgIHN0aWNreUhlYWRlci5jbGFzc0xpc3QuYWRkKCAnZml4ZWQnICk7XHJcbiAgICAgICAgICAgIGlmICggIWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LXdyYXBwZXInICkgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbmV3Tm9kZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoIFwiZGl2XCIgKTtcclxuICAgICAgICAgICAgICAgIG5ld05vZGUuY2xhc3NOYW1lID0gXCJzdGlja3ktd3JhcHBlclwiO1xyXG4gICAgICAgICAgICAgICAgc3RpY2t5SGVhZGVyLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKCBuZXdOb2RlLCBzdGlja3lIZWFkZXIgKTtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LXdyYXBwZXInICkuaW5zZXJ0QWRqYWNlbnRFbGVtZW50KCAnYmVmb3JlZW5kJywgc3RpY2t5SGVhZGVyICk7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS13cmFwcGVyJyApLnNldEF0dHJpYnV0ZSggXCJzdHlsZVwiLCBcImhlaWdodDogXCIgKyBoZWlnaHQgKyBcInB4XCIgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCAhZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktd3JhcHBlcicgKS5nZXRBdHRyaWJ1dGUoIFwic3R5bGVcIiApICkge1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktd3JhcHBlcicgKS5zZXRBdHRyaWJ1dGUoIFwic3R5bGVcIiwgXCJoZWlnaHQ6IFwiICsgaGVpZ2h0ICsgXCJweFwiICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmICggc3RpY2t5SGVhZGVyICkge1xyXG4gICAgICAgICAgICBzdGlja3lIZWFkZXIuY2xhc3NMaXN0LnJlbW92ZSggJ2ZpeGVkJyApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS13cmFwcGVyJyApICkge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS13cmFwcGVyJyApLnJlbW92ZUF0dHJpYnV0ZSggXCJzdHlsZVwiICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICggd2luZG93Lm91dGVyV2lkdGggPj0gOTkyICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdib2R5JyApLmNsYXNzTGlzdC5jb250YWlucyggJ3JpZ2h0LXNpZGViYXItYWN0aXZlJyApICkge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdib2R5JyApLmNsYXNzTGlzdC5yZW1vdmUoICdyaWdodC1zaWRlYmFyLWFjdGl2ZScgKTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEFkZCBvciByZW1vdmUgc2V0dGluZ3Mgd2hlbiB0aGUgd2luZG93IGlzIHJlc2l6ZWRcclxuICovXHJcbmV4cG9ydCBjb25zdCByZXNpemVIYW5kbGVyID0gZnVuY3Rpb24gKCB3aWR0aCA9IDk5MiwgYXR0cmkgPSAncmlnaHQtc2lkZWJhci1hY3RpdmUnICkge1xyXG4gICAgbGV0IGJvZHlDbGFzc2VzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCJib2R5XCIgKSAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcImJvZHlcIiApLmNsYXNzTGlzdDtcclxuICAgIGJvZHlDbGFzc2VzID0gYm9keUNsYXNzZXMudmFsdWUuc3BsaXQoICcgJyApLmZpbHRlciggaXRlbSA9PiBpdGVtICE9PSAnaG9tZScgJiYgaXRlbSAhPT0gJ2xvYWRlZCcgKTtcclxuICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGJvZHlDbGFzc2VzLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiYm9keVwiICkgJiYgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ2JvZHknICkuY2xhc3NMaXN0LnJlbW92ZSggYm9keUNsYXNzZXNbIGkgXSApO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogQXBwbHkgc3RpY2t5IGZvb3RlclxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHN0aWNreUZvb3RlckhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgc3RpY2t5Rm9vdGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktZm9vdGVyJyApO1xyXG4gICAgbGV0IHRvcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICdtYWluJyApID8gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJ21haW4nICkub2Zmc2V0VG9wIDogMzAwO1xyXG5cclxuICAgIGxldCBoZWlnaHQgPSAwO1xyXG5cclxuICAgIGlmICggc3RpY2t5Rm9vdGVyICkge1xyXG4gICAgICAgIGhlaWdodCA9IHN0aWNreUZvb3Rlci5vZmZzZXRIZWlnaHQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCB3aW5kb3cucGFnZVlPZmZzZXQgPj0gdG9wICYmIHdpbmRvdy5pbm5lcldpZHRoIDwgNzY4ICkge1xyXG4gICAgICAgIGlmICggc3RpY2t5Rm9vdGVyICkge1xyXG4gICAgICAgICAgICBzdGlja3lGb290ZXIuY2xhc3NMaXN0LmFkZCggJ2ZpeGVkJyApO1xyXG4gICAgICAgICAgICBpZiAoICFkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1jb250ZW50LXdyYXBwZXInICkgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbmV3Tm9kZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoIFwiZGl2XCIgKTtcclxuICAgICAgICAgICAgICAgIG5ld05vZGUuY2xhc3NOYW1lID0gXCJzdGlja3ktY29udGVudC13cmFwcGVyXCI7XHJcbiAgICAgICAgICAgICAgICBzdGlja3lGb290ZXIucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoIG5ld05vZGUsIHN0aWNreUZvb3RlciApO1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLmluc2VydEFkamFjZW50RWxlbWVudCggJ2JlZm9yZWVuZCcsIHN0aWNreUZvb3RlciApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLnN0aWNreS1jb250ZW50LXdyYXBwZXInICkuc2V0QXR0cmlidXRlKCBcInN0eWxlXCIsIFwiaGVpZ2h0OiBcIiArIGhlaWdodCArIFwicHhcIiApO1xyXG4gICAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKCBzdGlja3lGb290ZXIgKSB7XHJcbiAgICAgICAgICAgIHN0aWNreUZvb3Rlci5jbGFzc0xpc3QucmVtb3ZlKCAnZml4ZWQnICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKSApIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApLnJlbW92ZUF0dHJpYnV0ZSggXCJzdHlsZVwiICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICggd2luZG93LmlubmVyV2lkdGggPiA3NjggJiYgZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktY29udGVudC13cmFwcGVyJyApICkge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoICcuc3RpY2t5LWNvbnRlbnQtd3JhcHBlcicgKS5zdHlsZS5oZWlnaHQgPSAnYXV0byc7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBtYWtlIGJhY2tncm91bmQgcGFyYWxsYXhcclxuICovXHJcbmV4cG9ydCBjb25zdCBwYXJhbGxheEhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgcGFyYWxsYXhJdGVtcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoICcucGFyYWxsYXgnICk7XHJcblxyXG4gICAgaWYgKCBwYXJhbGxheEl0ZW1zICkge1xyXG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IHBhcmFsbGF4SXRlbXMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIC8vIGNhbGN1bGF0ZSBiYWNrZ3JvdW5kIHkgUG9zaXRpb247XHJcbiAgICAgICAgICAgIGxldCBwYXJhbGxheCA9IHBhcmFsbGF4SXRlbXNbIGkgXSwgeVBvcywgcGFyYWxsYXhTcGVlZCA9IDE7XHJcblxyXG4gICAgICAgICAgICBpZiAoIHBhcmFsbGF4LmdldEF0dHJpYnV0ZSggJ2RhdGEtb3B0aW9uJyApICkge1xyXG4gICAgICAgICAgICAgICAgcGFyYWxsYXhTcGVlZCA9IHBhcnNlSW50KCBwYXJzZU9wdGlvbnMoIHBhcmFsbGF4LmdldEF0dHJpYnV0ZSggJ2RhdGEtb3B0aW9uJyApICkuc3BlZWQgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgeVBvcyA9ICggcGFyYWxsYXgub2Zmc2V0VG9wIC0gd2luZG93LnBhZ2VZT2Zmc2V0ICkgKiA1MCAqIHBhcmFsbGF4U3BlZWQgLyBwYXJhbGxheC5vZmZzZXRUb3AgKyA1MDtcclxuXHJcbiAgICAgICAgICAgIHBhcmFsbGF4LnN0eWxlLmJhY2tncm91bmRQb3NpdGlvbiA9IFwiNTAlIFwiICsgeVBvcyArIFwiJVwiO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIHV0aWxzIHRvIHNob3cgc2Nyb2xsVG9wIGJ1dHRvblxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHNob3dTY3JvbGxUb3BIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IHNjcm9sbFRvcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiLnNjcm9sbC10b3BcIiApO1xyXG5cclxuICAgIGlmICggd2luZG93LnBhZ2VZT2Zmc2V0ID49IDc2OCApIHtcclxuICAgICAgICBzY3JvbGxUb3AuY2xhc3NMaXN0LmFkZCggXCJzaG93XCIgKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2Nyb2xsVG9wLmNsYXNzTGlzdC5yZW1vdmUoIFwic2hvd1wiICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBzY3JvbGwgdG8gdG9wXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gc2Nyb2xsVG9wSGFuZGxlciggaXNDdXN0b20gPSB0cnVlLCBzcGVlZCA9IDE1ICkge1xyXG4gICAgbGV0IG9mZnNldFRvcCA9IDA7XHJcblxyXG4gICAgaWYgKCBpc0N1c3RvbSAmJiAhaXNFZGdlQnJvd3NlcigpICkge1xyXG4gICAgICAgIGlmICggZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5tYWluIC5jb250YWluZXIgPiAucm93JyApICkge1xyXG4gICAgICAgICAgICBvZmZzZXRUb3AgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCAnLm1haW4gLmNvbnRhaW5lciA+IC5yb3cnICkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wICsgd2luZG93LnBhZ2VZT2Zmc2V0IC0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggJy5zdGlja3ktaGVhZGVyJyApLm9mZnNldEhlaWdodCArIDI7XHJcbiAgICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBvZmZzZXRUb3AgPSAwO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICggaXNTYWZhcmlCcm93c2VyKCkgfHwgaXNFZGdlQnJvd3NlcigpICkge1xyXG4gICAgICAgIGxldCBwb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcbiAgICAgICAgbGV0IHRpbWVySWQgPSBzZXRJbnRlcnZhbCggKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIHBvcyA8PSBvZmZzZXRUb3AgKSBjbGVhckludGVydmFsKCB0aW1lcklkICk7XHJcbiAgICAgICAgICAgIHdpbmRvdy5zY3JvbGxCeSggMCwgLXNwZWVkICk7XHJcbiAgICAgICAgICAgIHBvcyAtPSBzcGVlZDtcclxuICAgICAgICB9LCAxICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbygge1xyXG4gICAgICAgICAgICB0b3A6IG9mZnNldFRvcCxcclxuICAgICAgICAgICAgYmVoYXZpb3I6ICdzbW9vdGgnXHJcbiAgICAgICAgfSApO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogdXRpbHMgdG8gcGxheSBhbmQgcGF1c2UgdmlkZW9cclxuICovXHJcbmV4cG9ydCBjb25zdCB2aWRlb0hhbmRsZXIgPSAoIGUgKSA9PiB7XHJcbiAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgIGlmICggZS5jdXJyZW50VGFyZ2V0LmNsb3Nlc3QoICcucG9zdC12aWRlbycgKSApIHtcclxuICAgICAgICBsZXQgdmlkZW8gPSBlLmN1cnJlbnRUYXJnZXQuY2xvc2VzdCggJy5wb3N0LXZpZGVvJyApO1xyXG4gICAgICAgIGlmICggdmlkZW8uY2xhc3NMaXN0LmNvbnRhaW5zKCAncGxheWluZycgKSApIHtcclxuICAgICAgICAgICAgdmlkZW8uY2xhc3NMaXN0LnJlbW92ZSggJ3BsYXlpbmcnICk7XHJcbiAgICAgICAgICAgIHZpZGVvLmNsYXNzTGlzdC5hZGQoICdwYXVzZWQnICk7XHJcbiAgICAgICAgICAgIHZpZGVvLnF1ZXJ5U2VsZWN0b3IoICd2aWRlbycgKS5wYXVzZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZpZGVvLmNsYXNzTGlzdC5hZGQoICdwbGF5aW5nJyApO1xyXG4gICAgICAgICAgICB2aWRlby5xdWVyeVNlbGVjdG9yKCAndmlkZW8nICkucGxheSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmlkZW8ucXVlcnlTZWxlY3RvciggJ3ZpZGVvJyApLmFkZEV2ZW50TGlzdGVuZXIoICdlbmRlZCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmlkZW8uY2xhc3NMaXN0LnJlbW92ZSggJ3BsYXlpbmcnICk7XHJcbiAgICAgICAgICAgIHZpZGVvLmNsYXNzTGlzdC5yZW1vdmUoICdwYXVzZWQnICk7XHJcbiAgICAgICAgfSApXHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBnZXQgdG90YWwgUHJpY2Ugb2YgcHJvZHVjdHMgaW4gY2FydC5cclxuICovXHJcbmV4cG9ydCBjb25zdCBnZXRUb3RhbFByaWNlID0gY2FydEl0ZW1zID0+IHtcclxuICAgIGxldCB0b3RhbCA9IDA7XHJcbiAgICBpZiAoIGNhcnRJdGVtcyApIHtcclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBjYXJ0SXRlbXMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIHRvdGFsICs9IGNhcnRJdGVtc1sgaSBdLnByaWNlICogcGFyc2VJbnQoIGNhcnRJdGVtc1sgaSBdLnF0eSwgMTAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdG90YWw7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBnZXQgbnVtYmVyIG9mIHByb2R1Y3RzIGluIGNhcnRcclxuICovXHJcbmV4cG9ydCBjb25zdCBnZXRDYXJ0Q291bnQgPSBjYXJ0SXRlbXMgPT4ge1xyXG4gICAgbGV0IHRvdGFsID0gMDtcclxuXHJcbiAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBjYXJ0SXRlbXMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgdG90YWwgKz0gcGFyc2VJbnQoIGNhcnRJdGVtc1sgaSBdLnF0eSwgMTAgKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdG90YWw7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB1dGlscyB0byBzaG93IG51bWJlciB0byBuIHBsYWNlcyBvZiBkZWNpbWFsc1xyXG4gKi9cclxuZXhwb3J0IGNvbnN0IHRvRGVjaW1hbCA9ICggcHJpY2UsIGZpeGVkQ291bnQgPSAyICkgPT4ge1xyXG4gICAgcmV0dXJuIHByaWNlLnRvTG9jYWxlU3RyaW5nKCB1bmRlZmluZWQsIHsgbWluaW11bUZyYWN0aW9uRGlnaXRzOiBmaXhlZENvdW50LCBtYXhpbXVtRnJhY3Rpb25EaWdpdHM6IGZpeGVkQ291bnQgfSApO1xyXG59IiwiLyogKGlnbm9yZWQpICovIl0sInNvdXJjZVJvb3QiOiIifQ==