import React, { useEffect, useState } from 'react';
import axios from "axios";
import Helmet from 'react-helmet';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import { ADD_ADDRESS_API, SENDOTP_API, SIGNUP_API, VERIFY_API } from '~/components/common/Api';
import ALink from '~/components/features/custom-link';

function Login() {

    const [phoneNumber, setPhone] = useState('');
    const [step, setStep] = useState(1);
    const [OTP, setOtp] = useState('');
    const [vertifyOtp , setvertifyData] = useState('');
    
    // const history = useHistory();
    // Getting form value here
  const [form, setForm] = useState({

    userName: "",
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    age: ""
  });

  const [formAddress, setAddressForm] = useState({

    addressId: "",
    name: "",
    locality: "",
    area: "",
    street: "",
    pincodeId: "",
    cityId: "",
    stateId: "",
    addressType: ""
  });

 
    const config = {  
        headers: { accessToken: 'a' }
      };
  
   
  
  
    // Only numbers allowed
    const handlePhoneChange = (e) => {
      const value = e.target.value.replace(/\D/g, "");
      setPhone(value);
    };
  
   // Send otp 
    const sendOtp = (e) =>{
  
      e.preventDefault();
      const bodyParameters = {
          phoneNumber: phoneNumber,
          deviceId: "314145165185"
        };
  
        axios.post(SENDOTP_API, bodyParameters,config)
          .then((response) => {
            setStep(step + 1)
  
          }, (error) => {
  
          });
    }
  
   // Verify OTp
   const verifyOtp = (e) =>{
  
      e.preventDefault();
      const bodyParameters = {
          phoneNumber: phoneNumber,
          otp: parseInt(OTP),
          deviceId: "314145165185"
        };
  
  
        axios.post(VERIFY_API, bodyParameters,config)
          .then((response) => {
             setvertifyData(response.data);
             localStorage.setItem('session_id', response.data.token);
             setStep(step + 1)
  
          }, (error) => {
  
          
          });
    }
   
   
  
  //   Get all form fields at once
  
  const handleChange = e => {
      setForm({
        ...form,
        [e.target.name]: e.target.value,
      })
    }
  
  
   const handleAddress = e =>{
       setAddressForm({
           ...formAddress,
           [e.target.name]: e.target.value,
       })
   }
   // Create or register account 
   const createAccount = (e) =>{
      e.preventDefault();
  
      const config = {  
          headers: { Authorization: `${vertifyOtp.token}` }
        };
    
      const bodyParameters = {
          userName: form.userName,
          firstName: form.firstName,
          lastName: form.lastName,
          email: form.email,
          password: form.password,
          age: parseInt(form.age) 
        };
  
        axios.post(SIGNUP_API,bodyParameters,config)
        .then((response) =>{
          setStep(step + 1)
        }, (error) =>{
  
        });
   }
  
  //  Address Field enter 
  const addAddress = (e) =>{
      e.preventDefault();
  
      const config = {  
          headers: { Authorization: `${vertifyOtp.token}` }
        };
  
       
      const bodyParameters = {
          addressId: formAddress.addressId,
          name: formAddress.name,
          locality: formAddress.locality,
          area: formAddress.area,
          street: formAddress.street,
          pincodeId: parseInt(formAddress.pincodeId),
          cityId: parseInt(formAddress.cityId),
          stateId: parseInt(formAddress.stateId),
          addressType: formAddress.addressType
  
        };
  
        axios.post(ADD_ADDRESS_API,bodyParameters,config)
        .then((response) =>{
  
          console.log(response,"aaaaaaaaaaa");
          setStep(step + 1)
        }, (error) =>{
  
        });
   }
  
    const tabScreen = () => {
  
      switch (step) {
        case 1:
          return (
              <>
              <TabList className="nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5">
              <Tab className="nav-item">
                  <span className="nav-link border-no lh-1 ls-normal">Dsquare</span>
              </Tab>
             
              </TabList>
              <form action="#">
              <div className="form-group mb-3">
                      <input className="form-control" name="phone_number" id="phone_number" maxlength="19" type="text" placeholder="Enter Phone Number *" value={phoneNumber} onChange={handlePhoneChange} required />
              </div>
                 
                  <button className="btn btn-dark btn-block btn-rounded" type="submit" onClick={sendOtp}>Send OTP</button>
              </form>
              </>
          );
        case 2:
          return (
              <>
              <TabList className="nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5">
              <Tab className="nav-item">
                  <span className="nav-link border-no lh-1 ls-normal">Enter Code</span>
              </Tab>
             
              </TabList>
              
              <form action="#">
              <div className="form-group mb-3">
                      <input className="form-control" name="otp_number" id="otp_number" maxlength="6" type="text" placeholder="Enter OTP Number *" value={OTP} onChange={e => setOtp(e.target.value)} required />
              </div>
                 
                  <button className="btn btn-dark btn-block btn-rounded" type="submit" onClick={verifyOtp}>Verify OTP</button>
              </form>
              </>
          );
        case 3:
          return (
              <>
              <TabList className="nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5">
              <Tab className="nav-item">
                  <span className="nav-link border-no lh-1 ls-normal">Create your profile</span>
              </Tab>
             
              </TabList>
              
              <form action="#">
              <div className="form-group">
                  <label htmlFor="singin-email">Username:</label>
                  <input type="text" className="form-control" id="userName" name="userName" placeholder="Enter username *"  value={form.userName} onChange={handleChange} required  />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-email">First Name:</label>
                  <input type="text" className="form-control" id="firstName" name="firstName" placeholder="Enter first name" value={form.firstName} onChange={handleChange} />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-email">Last Name:</label>
                  <input type="text" className="form-control" id="lastName" name="lastName" placeholder="Enter last name" value={form.lastName} onChange={handleChange} />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-email">Your email address:</label>
                  <input type="email" className="form-control" id="email" name="email" placeholder="Your Email address *" value={form.email} onChange={handleChange} required />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-password">Password:</label>
                  <input type="password" className="form-control" id="password" name="password" placeholder="Password *" value={form.password} onChange={handleChange} required />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-password">Password:</label>
                  <input type="number" className="form-control" id="age" name="age" placeholder="age" value={form.age} onChange={handleChange} />
              </div>
  
              <div className="form-footer">
                  <div className="form-checkbox">
                      <input type="checkbox" className="custom-checkbox" id="register-agree" name="register-agree"
                          required />
                      <label className="form-control-label" htmlFor="register-agree">I agree to the privacy policy</label>
                  </div>
              </div>
              <button className="btn btn-dark btn-block btn-rounded" type="submit" onClick={ createAccount }>Create account</button>
          </form>
          </>
          );
        case 4:
          return (
              <>
              <TabList className="nav nav-tabs nav-fill align-items-center border-no justify-content-center mb-5">
              <Tab className="nav-item">
                  <span className="nav-link border-no lh-1 ls-normal">Add your address</span>
              </Tab>
             
              </TabList>
  
              <form action="#">
              <div className="form-group">
                  <label htmlFor="singin-email">Address ID:</label>
                  <input type="text" className="form-control" id="addressId" name="addressId" placeholder="Enter address id *"  value={formAddress.addressId} onChange={handleAddress} required  />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-email">Name:</label>
                  <input type="text" className="form-control" id="name" name="name" placeholder="Enter name" value={formAddress.name} onChange={handleAddress} />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-email">Locality:</label>
                  <input type="text" className="form-control" id="locality" name="locality" placeholder="Enter your locality" value={formAddress.locality} onChange={handleAddress} />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-email">Area:</label>
                  <input type="text" className="form-control" id="area" name="area" placeholder="Enter your area *" value={formAddress.area} onChange={handleAddress} required />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-password">street:</label>
                  <input type="text" className="form-control" id="street" name="street" placeholder="street " value={formAddress.street} onChange={handleAddress}  />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-password">Pincode ID:</label>
                  <input type="number" className="form-control" id="pincodeId" name="pincodeId" placeholder="Pincode Id *" value={formAddress.pincodeId} onChange={handleAddress} required />
              </div>
       
              <div className="form-group">
                  <label htmlFor="singin-password">City ID:</label>
                  <input type="number" className="form-control" id="cityId" name="cityId" placeholder="City Id" value={formAddress.cityId} onChange={handleAddress} />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-password">State ID:</label>
                  <input type="number" className="form-control" id="stateId" name="stateId" placeholder="State Id" value={formAddress.stateId} onChange={handleAddress} />
              </div>
              <div className="form-group">
                  <label htmlFor="singin-password">Address Type:</label>
                  <input type="text" className="form-control" id="addressType" name="addressType" placeholder="Address Type" value={formAddress.addressType} onChange={handleAddress} />
              </div>
  
              <div className="form-footer">
                  <div className="form-checkbox">
                      <input type="checkbox" className="custom-checkbox" id="register-agree" name="register-agree"
                          required />
                      <label className="form-control-label" htmlFor="register-agree">I agree to the privacy policy</label>
                  </div>
              </div>
              <button className="btn btn-dark btn-block btn-rounded" type="submit" onClick={ addAddress }>Add Address</button>
          </form>
          </>
          );
        
        default:
          return 'foo';
      }
  
    }
  
  

    return (
        <main className="main">
            <Helmet>
                <title>Dsqaure | Login</title>
            </Helmet>

            <h1 className="d-none">Dsqaure - Login</h1>
            <nav className="breadcrumb-nav">
                <div className="container">
                    <ul className="breadcrumb">
                        <li><ALink href="/"><i className="d-icon-home"></i></ALink></li>
                        <li><ALink href="/shop">Dsqaure Shop</ALink></li>
                        <li>My Account</li>
                    </ul>
                </div>
            </nav>
            <div className="page-content mt-6 pb-2 mb-10">
                <div className="container">
                    <div className="login-popup">
                        <div className="form-box">
                            <div className="tab tab-nav-simple tab-nav-boxed form-tab">
                                <Tabs selectedTabClassName="active" selectedTabPanelClassName="active">
                                    
                                    <div className="tab-content">
                                    {tabScreen()}
                                       
                                    </div>
                                </Tabs>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main >
    )
}

export default React.memo(Login);