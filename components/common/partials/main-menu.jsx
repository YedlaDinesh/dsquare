import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import ALink from '~/components/features/custom-link';
import { mainMenu } from '~/utils/data/menu';
import { PRODUCT_CATEGORY_API } from '../Api';

function MainMenu() {
    const pathname = useRouter().pathname;
    const [allCategory, setCategory] = useState([])
    

     const GetAllCategory = async () => {
    
        const config = {  
            headers: { accessToken: 'a' }
          };

        try {
          const { data: { body, error } } = await axios.get(PRODUCT_CATEGORY_API,config)
            setCategory(body);
        }
        catch (err) {
        
        }
      }
    
    //   console.log(allCategory,"rrrrrrrrrrrr");
     useEffect(() =>{
        GetAllCategory();
     },[])
    
    return (
        <nav className="main-nav">
            <ul className="menu">
                <li id="menu-home" className={ pathname === '/' ? 'active' : '' }>
                    <ALink href='/'>Home</ALink>
                </li>

                <li className={ `submenu  ${ pathname.includes( '/shop' ) ? 'active' : '' }` }>
                    <ALink href="#">Categories</ALink>

                   
                         <ul>
                                    {
                                        allCategory.map( ( item, index ) => (
                                            <li key={ `shop-${ item.id }` }>
                                                <ALink href={ '/' + item.categoryName }>
                                                    { item.categoryName }
                                                    { item.hot ? <span className="tip tip-hot">Hot</span> : "" }
                                                </ALink>
                                            </li>
                                        ) )
                                    }
                        </ul>
                   
                </li>

{/* 
                <li className={ `submenu  ${ pathname.includes( '/pages' ) ? 'active' : '' }` }>
                    <ALink href="#">Pages</ALink>

                    <ul>
                        {
                            mainMenu.other.map( ( item, index ) => (
                                <li key={ `other-${ item.title }` }>
                                    <ALink href={ '/' + item.url }>
                                        { item.title }
                                        { item.new ? <span className="tip tip-new">New</span> : "" }
                                    </ALink>
                                </li>
                            ) )
                        }
                    </ul>
                </li> */}

                <li>
                <ALink href="#"><i className="d-icon-card"></i>Special Offers</ALink>  
                </li>    
                <li>
                    <ALink href="/pages/contact-us">Contact Us</ALink>
                </li>
                <li>
                    <ALink href="/pages/about-us">About Us</ALink>
                </li>
            </ul>
        </nav>
    )
}

export default MainMenu;