import React, { Component } from 'react';

export const API_BASE_URL = "http://3.16.7.21:3000/dsquare/api/";
export const getApiUrl = (endpoint) => API_BASE_URL + endpoint;
export const SIGNUP_API = getApiUrl("create-account");
export const SENDOTP_API = getApiUrl("generate-otp");
export const VERIFY_API = getApiUrl("verify-otp");
export const ADD_ADDRESS_API = getApiUrl("add-address");
export const GET_ADDRESS_API = getApiUrl("get-my-adress");
export const DELETE_ADDRESS_API = getApiUrl("delete-my-adress");
export const PRODUCT_CATEGORY_API = getApiUrl("product-categories");


