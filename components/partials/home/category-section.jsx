import React, { useEffect, useState } from 'react';
import axios from "axios";
import Reveal from "react-awesome-reveal";
import { LazyLoadImage } from 'react-lazy-load-image-component';

import ALink from '~/components/features/custom-link';

import { fadeIn } from '~/utils/data/keyframes';
import { PRODUCT_CATEGORY_API } from '~/components/common/Api';

function CategorySection () {

    const token = localStorage.getItem('session_id');
    const [allCategory, setCategory] = useState([]);
    const config = {  
        headers: { Authorization: `${token}` }
      };
   
  
  
   //Get Category
    const getCategory = async() =>{
  
        const { data: { body } } = await axios.get(PRODUCT_CATEGORY_API, config)
        setCategory(body);
       
    }

    useEffect(() =>{
        getCategory();
    },[token])
  
    console.log(allCategory);

    

    return (
        <Reveal keyframes={ fadeIn } delay={ 300 } duration={ 1200 } triggerOnce>
            <section className="pt-10 mt-7">
                <div className="container">
                    <h2 className="title title-center mb-5">Browse Our Categories</h2>

                    <div className="row">
                    
              {!!allCategory && allCategory.slice(0, 4).map((items,i) =>{
                  {var categoryImage = "./images/categories/category"+i+".jpg"}

                  return (
                        <div className="col-xs-6 col-lg-3 mb-4">
                            <div className="category category-default1 category-absolute banner-radius overlay-zoom" key={i} id={items.id}>
                                <ALink href={ { pathname: '/shop', query: { category: `${items.categoryName}` } } }>
                                    <figure className="category-media">

                                  
                                        <LazyLoadImage
                                            src={categoryImage}
                                            alt="Intro Slider"
                                            effect="opacity; transform"
                                            width={ 280 }
                                            height={ 280 }
                                        />
                                    </figure>

                                    <div className="category-content">
                                        <h4 className="category-name font-weight-bold ls-l">{items.categoryName}</h4>
                                    </div>
                                </ALink>
                            </div>
                        </div>
                  )
              })}
                        

                        
                    </div>
                </div>
            </section>
        </Reveal>
    )
}

export default React.memo( CategorySection );